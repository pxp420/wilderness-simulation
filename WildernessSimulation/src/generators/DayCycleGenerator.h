#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "../entities/Light.h"

class DayCycleGenerator {
private:
	float time;
	Light* light;

	static DayCycleGenerator* instance;
public:
	enum Time {
		DAY, EVENING, NIGHT
	};

	static Time TIME_OF_DAY;
	static float HOURS_SURVIVED;

	DayCycleGenerator(Light* light);

	void Cycle(float deltaTime);

	static void CreateInstance(Light* light);

	static void DestroyInstance();

	static DayCycleGenerator* GetInstance();
};