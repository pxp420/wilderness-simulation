#pragma once

#include "../toolbox/RandomGenerator.h"
#include "../objConverter/OBJFileLoader.h"
#include "../renderEngine/EntityRenderer.h"
#include "../toolbox/StringManipulation.h"
#include "../entities/LowPolyTree.h"
#include "../entities/Tree.h"
#include "../entities/Fern.h"
#include "../entities/TreeLog.h"
#include "../entities/MossyRock.h"
#include "../fontMeshCreator/FontType.h"

#include <map>
#include <tuple>

class VegetationGenerator {
private:
	static VegetationGenerator* instance;
public:
	static unsigned int HUMAN_COUNT;
	static unsigned int LOW_POLY_TREE_COUNT;
	static unsigned int TREE_COUNT;
	static unsigned int FERN_COUNT;
	static unsigned int MOSSY_ROCK_COUNT;

	/**
	 * Class for generation of different models in the world.
	*/
	VegetationGenerator();

	/**
	 * Generates a number of models and prepares them to be spawned.
	*/
	void SpawnModels();

	static void CreateInstance();

	static void DeleteInstance();

	static VegetationGenerator* GetInstance();
};
