#include "VegetationGenerator.h"
#include "../entities/Human.h"
#include "../fontMeshCreator/FontType.h"

VegetationGenerator* VegetationGenerator::instance = nullptr;

unsigned int VegetationGenerator::HUMAN_COUNT = 2;
unsigned int VegetationGenerator::FERN_COUNT = 4500;
unsigned int VegetationGenerator::LOW_POLY_TREE_COUNT = 2000;
unsigned int VegetationGenerator::TREE_COUNT = 1000;
unsigned int VegetationGenerator::MOSSY_ROCK_COUNT = 500;

void VegetationGenerator::CreateInstance() {
	instance = new VegetationGenerator();
}

void VegetationGenerator::DeleteInstance() {
	delete instance;
}

VegetationGenerator* VegetationGenerator::GetInstance() {
	return instance;
}

VegetationGenerator::VegetationGenerator() { }

void VegetationGenerator::SpawnModels() {
	for (unsigned int i = 0; i < LOW_POLY_TREE_COUNT; i++) {
		float x = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float z = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float y = Terrain::GetInstance()->GetHeightOfTerrain(x, z);
		glm::vec3 position(x, y, z);

		if (y > -40.0f) {
			float rotY = RandomGenerator::GetNextFloat(0.0f, 360.0f);
			LowPolyTree* lowPolyTree = new LowPolyTree(position, rotY);
		}
		else {
			i--;
		}
	}

	for (unsigned int i = 0; i < TREE_COUNT; i++) {
		float x = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float z = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float y = Terrain::GetInstance()->GetHeightOfTerrain(x, z);
		glm::vec3 position(x, y, z);

		if (y > -40.0f) {
			Tree* tree = new Tree(position);
		}
		else {
			i--;
		}
	}

	for (unsigned int i = 0; i < FERN_COUNT; i++) {
		float x = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float z = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float y = Terrain::GetInstance()->GetHeightOfTerrain(x, z);
		glm::vec3 position(x, y, z);

		if (y > -40.0f) {
			Fern* fern = new Fern(position);
		}
		else {
			i--;
		}
	}

	for (unsigned int i = 0; i < MOSSY_ROCK_COUNT; i++) {
		float x = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float z = Terrain::GetInstance()->TransformToWorldSpace(RandomGenerator::GetNextFloat(0.0f, 1.0f));
		float y = Terrain::GetInstance()->GetHeightOfTerrain(x, z);
		glm::vec3 position(x, y, z);

		if (y > -40.0f) {
			MossyRock* mossyRock = new MossyRock(position);
		}
		else {
			i--;
		}
	}

	TreeLog* log = new TreeLog(glm::vec3(9.0f, 9.0f, 9.0f));
}
