#include "DayCycleGenerator.h"
#include "../reinforcedLearning/Simulation.h"

DayCycleGenerator::Time DayCycleGenerator::TIME_OF_DAY = DayCycleGenerator::Time::DAY;
float DayCycleGenerator::HOURS_SURVIVED = 0.0f;

DayCycleGenerator* DayCycleGenerator::instance = nullptr;

DayCycleGenerator::DayCycleGenerator(Light* light) : time(0.0f), light(light) { }

void DayCycleGenerator::CreateInstance(Light* light) {
	instance = new DayCycleGenerator(light);
	TIME_OF_DAY = DayCycleGenerator::Time::DAY;
	instance->time = 0.0f;
	light->SetColour(glm::vec3(1.0f, 1.0f, 1.0f));
}

void DayCycleGenerator::DestroyInstance() {
	delete instance;
}

DayCycleGenerator* DayCycleGenerator::GetInstance() {
	return instance;
}

void DayCycleGenerator::Cycle(float deltaTime) {
	HOURS_SURVIVED = HOURS_SURVIVED + deltaTime * 400.0f;
	time = time + deltaTime * 400.0f;
	time = std::fmod(time, 24000.0f);
	if (time >= 0.0f && time <= 10.0f) {
		//Simulation::SIMULATION_COUNTER++;
	}

	if (time >= 0.0f && time <= 8000.0f) {
		float rColour = ((8000.0f - 0.0f) / (8000.0f - 0.0f)) / 2.0f + 1.0f;
		float gColour = light->GetColour().g;
		float bColour = light->GetColour().b;

		light->SetColour(glm::vec3(rColour, gColour, bColour));
		TIME_OF_DAY = Time::DAY;
	}
	else if (time > 8000.0f && time <= 12000.0f) {  
		float rColour = ((((time - 8000.0f) / (12000.0f - 8000.0f)) - 1.0f ) / -2.0f ) + 1.0f;
		float gColour = light->GetColour().g;
		float bColour = (((time - 8000.0f) / (12000.0f - 8000.0f)) * 1.5f) + 1.0f;

		light->SetColour(glm::vec3(rColour, gColour, bColour));
		TIME_OF_DAY = Time::EVENING;

	}
	else if (time > 12000.0f && time <= 24000.0f) {
		float rColour = light->GetColour().r;
		float gColour = light->GetColour().g;
		float bColour = ((((time - 12000.0f) / (24000.0f - 12000.0f)) - 1.0f) * -1.5f) + 1.0f;

		light->SetColour(glm::vec3(rColour, gColour, bColour));
		TIME_OF_DAY = Time::NIGHT;
	}
}

