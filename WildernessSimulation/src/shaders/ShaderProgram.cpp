#include "ShaderProgram.h"

ShaderProgram::ShaderProgram(const std::string& vertexFile, const std::string& fragmentFile,
	std::map<unsigned int, std::string> attributes) {
	vertexShaderID = LoadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentShaderID = LoadShader(fragmentFile, GL_FRAGMENT_SHADER);
	programID = glCreateProgram();
	
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);

	std::map<unsigned int, std::string>::iterator it = attributes.begin();
	while (it != attributes.end()) {
		BindAttribute(it->first, it->second);
		it++;
	}

	glLinkProgram(programID);
	glValidateProgram(programID);
}

void ShaderProgram::Start() {
	glUseProgram(programID);
}

void ShaderProgram::Stop() {
	glUseProgram(0);
}

void ShaderProgram::CleanUp() {
	Stop();
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
	glDeleteProgram(programID);
}

void ShaderProgram::BindAttribute(int attribute, const std::string& variableName) {
	glBindAttribLocation(programID, attribute, variableName.c_str());
}

unsigned int ShaderProgram::GetUniformLocation(const std::string& uniformName) {
	return glGetUniformLocation(programID, uniformName.c_str());
}

void ShaderProgram::LoadFloat(unsigned int location, float value) {
	glUniform1f(location, value);
}

void ShaderProgram::LoadInt(unsigned int location, int value) {
	glUniform1i(location, value);
}

void ShaderProgram::LoadVector(unsigned int location, glm::vec2 vector) {
	glUniform2f(location, vector.x, vector.y);
}

void ShaderProgram::LoadVector(unsigned int location, glm::vec3 vector) {
	glUniform3f(location, vector.x, vector.y, vector.z);
}

void ShaderProgram::LoadVector(unsigned int location, glm::vec4 vector) {
	glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}

void ShaderProgram::LoadBool(unsigned int location, bool value) {
	float toLoad = 0;
	if (value) {
		toLoad = 1;
	}
	glUniform1f(location, toLoad);
}

void ShaderProgram::LoadMatrix(unsigned int location, glm::mat4 matrix) {
	glUniformMatrix4fv(location, 1, false, &matrix[0][0]);
}

int ShaderProgram::LoadShader(const std::string& file, int type) {
	std::string shaderCode;
	std::ifstream shaderFile;

	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try {
		shaderFile.open(file);

		std::stringstream shaderStream;
		shaderStream << shaderFile.rdbuf();
		shaderFile.close();

		shaderCode = shaderStream.str();
	}
	catch (std::ifstream::failure exception) {
		std::cout << "File was not successfully read!" << std::endl;
	}

	const char* cShaderCode = shaderCode.c_str();
	unsigned int shaderID = glCreateShader(type);
	glShaderSource(shaderID, 1, &cShaderCode, NULL);
	glCompileShader(shaderID);

	int success;
	char infoLog[512];
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shaderID, 1024, NULL, infoLog);
		std::cout << "Shader compilation error!\n" << infoLog << std::endl;
	}

	return shaderID;
}
