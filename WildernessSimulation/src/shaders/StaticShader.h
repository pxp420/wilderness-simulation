#pragma once
#include "ShaderProgram.h"
#include "../entities/Camera.h"
#include "../toolbox/Maths.h"
#include "../entities/Light.h"
#include <array>

class StaticShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_transformationMatrix;
	unsigned int location_viewMatrix;
	unsigned int location_staticViewMatrix;
	unsigned int location_projectionMatrix;
	unsigned int location_lightPosition[MAX_LIGHTS];
	unsigned int location_lightColour[MAX_LIGHTS];
	unsigned int location_attenuation[MAX_LIGHTS];
	unsigned int location_shineDamper;
	unsigned int location_reflectivity;
	unsigned int location_useFakeLighting;
	unsigned int location_skyColour;
	unsigned int location_numberOfRows;
	unsigned int location_offset;
	unsigned int location_plane;
	unsigned int location_terrainSize;
public:
	/**
	 * Calls the parent constructor to create the program
	*/
	StaticShader();

	/**
	 * Loads the transformation matrix into a uniform.
	 *
	 * @param matrix
	 *		- The matrix passed out to the shaders.
	*/
	void LoadTransformationMatrix(glm::mat4 matrix);

	/**
	 * Loads the view matrix into a uniform.
	 *
	 * @param camera
	 *		- The camera needed to create the view matrix.
	*/
	void LoadViewMatrix(Camera& camera);

	/**
	* Loads the static view matrix into a uniform.
	*
	* @param staticCamera
	*		- The camera needed to create the view matrix.
	*/
	void LoadStaticViewMatrix(Camera& staticCamera);

	/**
	 * Loads the projection matrix into a uniform.
	 *
	 * @param matrix
	 *		- The matrix passed out to the shaders.
	*/
	void LoadProjectionMatrix(glm::mat4 matrix);

	/**
	 * Loads the light colour and position into a uniform.
	 *
	 * @param lights
	 *		- The list of light objects containing the position and colour.
	*/
	void LoadLights(std::vector<Light*>& lights);

	/**
	 * Loads the damper and reflectivity values into their uniforms.
	 *
	 * @param damper 
	 *		- The damper value of the object.
	 *
	 * @param reflectivity
	 *		- The reflectivity value of the object.
	*/
	void LoadShineVariables(float damper, float reflectivity);

	/**
	 * Loads the fake lighting variable into a uniform.
	 *
	 * @param useFake
	 *		- The fake lighting value.
	*/
	void LoadFakeLightingVariable(bool useFake);

	/**
	 * Loads a colour into a uniform for the sky.
	 *
	 * @param r
	 *		- The red channel of the colour.
	 *
	 * @param g
	 *		- The green channel of the colour.
	 *
	 * @param b
	 *		- The blue channel of the colour.
	*/
	void LoadSkyColour(float r, float g, float b);

	/**
	 * Loads the number of rows into a uniform.
	 *
	 * @param numberOfRows
	 *		- The number of rows of the texture.
	*/
	void LoadNumberOfRows(unsigned int numberOfRows);

	/**
	* Loads the offsets of the texture into a uniform.
	*
	* @param x
	*		- The x offset of the texture.
	*
	* @param y
	*		- The y offset of the texture.
	*/
	void LoadOffset(float x, float y);

	/**
	 * Loads the clip plane vector into a uniform.
	 *
	 * @param plane
	 *		- The plane vector.
	*/
	void LoadClipPlane(glm::vec4 plane);

	/**
	* Load the terrain size into a uniform.
	*
	* @param terrainSize
	*		- The size of the terrain.
	*/
	void LoadTerrainSize(float terrainSize);
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};