#version 330

in vec3 position;
in vec2 textureCoords;
in vec3 normal;

out vec2 pass_textureCoords;
out vec3 surfaceNormal;
out vec3 toLightVector[4];
out vec3 toCameraVector;
out float visibility;
out vec4 shadowCoords;
out vec3 pass_normal;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 staticViewMatrix;

uniform vec3 lightPosition[4];

uniform mat4 toShadowMapSpace;
uniform float shadowDistance;
uniform float terrainSize;

const float gradient = 5.0f;
const float transitionDistance = 10.0f;

uniform vec4 plane;

void main(void) {
	vec4 worldPosition = transformationMatrix * vec4(position, 1.0f);
	shadowCoords = toShadowMapSpace * worldPosition;

	gl_ClipDistance[0] = dot(worldPosition, plane);

	vec4 positionRelativeToCam = viewMatrix * worldPosition;
	gl_Position = projectionMatrix * positionRelativeToCam;
	pass_textureCoords = textureCoords;
	pass_normal = normal;

	surfaceNormal = (transformationMatrix * vec4(normal, 0.0f)).xyz;
	for (int i = 0; i < 4; i++) {
		toLightVector[i] = lightPosition[i] - worldPosition.xyz;
	}
	toCameraVector = (inverse(viewMatrix) * vec4(0.0f, 0.0f, 0.0f, 1.0f)).xyz - worldPosition.xyz;

	float maxTerrainOffset = terrainSize - 300.0f;
	vec4 posToStaticCam = staticViewMatrix * worldPosition;
	if ((posToStaticCam.x >= maxTerrainOffset || posToStaticCam.x <= -maxTerrainOffset) &&
		(posToStaticCam.z >= maxTerrainOffset || posToStaticCam.z <= -maxTerrainOffset)) {
		visibility = max(((abs(posToStaticCam.x) - maxTerrainOffset) / (terrainSize - maxTerrainOffset)),
			((abs(posToStaticCam.z) - maxTerrainOffset) / (terrainSize - maxTerrainOffset)));
		visibility = exp(-visibility * gradient);
	}
	else if (posToStaticCam.x >= maxTerrainOffset || posToStaticCam.x <= -maxTerrainOffset) {
		visibility = (abs(posToStaticCam.x) - maxTerrainOffset) / (terrainSize - maxTerrainOffset);
		visibility = exp(-visibility * gradient);
	}
	else if (posToStaticCam.z >= maxTerrainOffset || posToStaticCam.z <= -maxTerrainOffset) {
		visibility = (abs(posToStaticCam.z) - maxTerrainOffset) / (terrainSize - maxTerrainOffset);
		visibility = exp(-visibility * gradient);
	}
	else {
		visibility = 1.0f;
	}
	visibility = clamp(visibility, 0.0f, 1.0f);

	float distance = length(positionRelativeToCam.xyz);
	distance = distance - (shadowDistance - transitionDistance);
	distance = distance / transitionDistance;
	shadowCoords.w = clamp(1.0f - distance, 0.0f, 1.0f);
}
