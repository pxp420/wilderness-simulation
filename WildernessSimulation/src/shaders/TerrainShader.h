#pragma once

#include "ShaderProgram.h"
#include "../shadows/ShadowMapMasterRenderer.h"
#include "../entities/Camera.h"
#include "../entities/Light.h"
#include "../toolbox/Maths.h"

class TerrainShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_transformationMatrix;
	unsigned int location_viewMatrix;
	unsigned int location_staticViewMatrix;
	unsigned int location_projectionMatrix;
	unsigned int location_lightPosition[MAX_LIGHTS];
	unsigned int location_lightColour[MAX_LIGHTS];
	unsigned int location_attenuation[MAX_LIGHTS];
	unsigned int location_shineDamper;
	unsigned int location_reflectivity;
	unsigned int location_skyColour;
	unsigned int location_mudTexture;
	unsigned int location_grassTexture;
	unsigned int location_flowerGrassTexture;
	unsigned int location_dirtTexture;
	unsigned int location_rockTexture;
	unsigned int location_blendMap;
	unsigned int location_plane;
	unsigned int location_toShadowMapSpace;
	unsigned int location_shadowMap;
	unsigned int location_shadowDistance;
	unsigned int location_shadowMapSize;
	unsigned int location_terrainSize;
public:
	/**
	* Calls the parent constructor to create the program
	*
	* @param vertexFile
	*		- The filepath of the vertex shader.
	*
	* @param fragmentFile
	*		- The filepath of the fragment shader.
	*/
	TerrainShader();

	/**
	* Loads the transformation matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadTransformationMatrix(glm::mat4 matrix);

	/**
	* Loads the view matrix into a uniform.
	*
	* @param camera
	*		- The camera needed to create the view matrix.
	*/
	void LoadViewMatrix(Camera& camera);

	/**
	* Loads the static view matrix into a uniform.
	*
	* @param staticCamera
	*		- The camera needed to create the view matrix.
	*/
	void LoadStaticViewMatrix(Camera& staticCamera);

	/**
	* Loads the projection matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadProjectionMatrix(glm::mat4 matrix);

	/**
	* Loads the light colour and position into a uniform.
	*
	* @param light
	*		- The light object containing the position and colour.
	*/
	void LoadLights(std::vector<Light*>& lights);

	/**
	* Loads the damper and reflectivity values into their uniforms.
	*
	* @param damper
	*		- The damper value of the object.
	*
	* @param reflectivity
	*		- The reflectivity value of the object.
	*/
	void LoadShineVariables(float damper, float reflectivity);

	/**
	* Loads a acolour into a uniform for the sky.
	*
	* @param r
	*		- The red channel of the colour.
	*
	* @param g
	*		- The green channel of the colour.
	*
	* @param b
	*		- The blue channel of the colour.
	*/
	void LoadSkyColour(float r, float g, float b);

	/**
	* Loads the clip plane vector into a uniform.
	*
	* @param plane
	*		- The plane vector.
	*/
	void LoadClipPlane(glm::vec4 plane);

	/**
	 * Loads the shadow space matrix into a uniform.
	 *
	 * @param matrix
	 *		- The matrix to be loaded.
	*/
	void LoadToShadowSpaceMatrix(glm::mat4 matrix);

	/**
	 * Load the shadow distance into a uniform.
	 *
	 * @param shadowDistance
	 *		- The distance of the shadow.
	*/
	void LoadShadowDistance(float shadowDistance);

	/**
	 * Load the shadow map size into a uniform.
	 *
	 * @param shadowMapSize
	 *		- The shadow map size.
	*/
	void LoadShadowMapSize(unsigned int shadowMapSize);

	/**
	 * Load the terrain size into a uniform.
	 *
	 * @param terrainSize
	 *		- The size of the terrain.
	*/
	void LoadTerrainSize(float terrainSize);

	/**
	 * Loads all the textures for the terrain in their corresponding
	 * units.
	*/
	void ConnectTextureUnits();
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
