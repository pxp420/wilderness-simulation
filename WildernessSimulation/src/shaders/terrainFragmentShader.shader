#version 330

in vec2 pass_textureCoords;
in vec3 pass_normal;
in vec3 surfaceNormal;
in vec3 toLightVector[4];
in vec3 toCameraVector;

in float visibility;
in vec4 shadowCoords;

out vec4 out_Color;

uniform sampler2D mudTexture;
uniform sampler2D grassTexture;
uniform sampler2D flowerGrassTexture;
uniform sampler2D dirtTexture;
uniform sampler2D rockTexture;
uniform sampler2D blendMap;

uniform vec3 lightColour[4];
uniform vec3 attenuation[4];

uniform float shineDamper;
uniform float reflectivity;

uniform vec3 skyColour;

uniform sampler2D shadowMap;
uniform int mapSize;

const int pcfCount = 2;
const float totalTexels = (pcfCount * 2.0f + 1.0f) * (pcfCount * 2.0f + 1.0f);

void main(void) {
	float texelSize = 1.0f / mapSize;
	float total = 0.0f;

	for (int x = -pcfCount; x <= pcfCount; x++) {
		for (int y = -pcfCount; y <= pcfCount; y++) {
			float objectNearestLight = texture(shadowMap, shadowCoords.xy + vec2(x, y) * texelSize).r;
			if (shadowCoords.z > objectNearestLight) {
				total += 1.0f;
			}
		}
	}

	total /= totalTexels;
	float lightFactor = 1.0f - (total * shadowCoords.w);

	vec4 blendMapColour = texture(blendMap, pass_textureCoords);
	vec2 tiledCoords = pass_textureCoords * 100.0f;

	vec4 mudTextureColour = texture(mudTexture, tiledCoords);
	vec4 grassTextureColour = texture(grassTexture, tiledCoords);
	vec4 flowerGrassTextureColour = texture(flowerGrassTexture, tiledCoords);
	vec4 dirtTextureColour = texture(dirtTexture, tiledCoords);
	vec4 rockTextureColour = texture(rockTexture, tiledCoords);

	vec4 totalColour = blendMapColour;

	if (blendMapColour.x < 0.3f) {
		totalColour = mix(mudTextureColour, grassTextureColour, (blendMapColour.x - 0 ) / (0.3f - 0));
	}
	else if (blendMapColour.x > 0.3f && blendMapColour.x < 0.7f) {
		totalColour = mix(grassTextureColour, flowerGrassTextureColour, (blendMapColour.x - 0.3f) / (0.7f - 0.3f));
	}
	else if (blendMapColour.x > 0.7f && blendMapColour.x < 0.8f) {
		totalColour = mix(flowerGrassTextureColour, dirtTextureColour, (blendMapColour.x - 0.7f) / (0.8f - 0.7f));
	}
	else {
		totalColour = mix(dirtTextureColour, rockTextureColour, (blendMapColour.x - 0.8f) / (1.0f - 0.8f));
	}

	float rockAngle = 0.65f;
	if (abs(pass_normal.x) > rockAngle) {
		totalColour = mix(totalColour, rockTextureColour, 1.0f - abs(pass_normal.x));
	}
	else if (abs(pass_normal.z) > rockAngle) {
		totalColour = mix(totalColour, rockTextureColour, 1.0f - abs(pass_normal.z));
	}

	vec3 unitNormal = normalize(surfaceNormal);

	vec3 totalDiffuse = vec3(0.0f);
	vec3 totalSpecular = vec3(0.0f);

	for (int i = 0; i < 4; i++) {
		float distance = length(toLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);

		vec3 unitLightVector = normalize(toLightVector[i]);
		float nDotl = dot(unitNormal, unitLightVector);
		float brightness = max(nDotl, 0.0f);
		vec3 unitVectorToCamera = normalize(toCameraVector);
		vec3 lightDirection = -unitLightVector;
		vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
		float specularFactor = dot(reflectedLightDirection, unitVectorToCamera);
		specularFactor = max(specularFactor, 0.0f);
		float dampedFactor = pow(specularFactor, shineDamper);
		totalDiffuse = totalDiffuse + (brightness * lightColour[i]) / attFactor;
		totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColour[i]) / attFactor;
	}

	totalDiffuse = max(totalDiffuse * lightFactor, 0.6f);

	out_Color = vec4(totalDiffuse, 1.0f) * totalColour + vec4(totalSpecular, 1.0f);
	out_Color = mix(vec4(skyColour, 1.0f), out_Color, visibility);
}
