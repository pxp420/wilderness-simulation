#include "StaticShader.h"

const std::string StaticShader::VERTEX_FILE = "src/shaders/vertexShader.shader";
const std::string StaticShader::FRAGMENT_FILE = "src/shaders/fragmentShader.shader";

const std::map<unsigned int, std::string> StaticShader::ATTRIBUTES = {
	{0, "position"},
	{1, "textureCoords"},
	{2, "normals"},
	{3, "transformMatrix"},
    {7, "textureOffsets"}
};

StaticShader::StaticShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void StaticShader::LoadTransformationMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_transformationMatrix, matrix);
}

void StaticShader::LoadViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix = Maths::CreateViewMatrix(camera);
	ShaderProgram::LoadMatrix(location_viewMatrix, viewMatrix);
}

void StaticShader::LoadStaticViewMatrix(Camera& staticCamera) {
	glm::mat4 viewMatrix = Maths::CreateStaticViewMatrix(staticCamera);
	ShaderProgram::LoadMatrix(location_staticViewMatrix, viewMatrix);
}

void StaticShader::LoadProjectionMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_projectionMatrix, matrix);
}

void StaticShader::LoadLights(std::vector<Light*>& lights) {
	for (unsigned int i = 0; i < MAX_LIGHTS; i++) {
		if (i < lights.size()) {
			ShaderProgram::LoadVector(location_lightPosition[i], lights.at(i)->GetPosition());
			ShaderProgram::LoadVector(location_lightColour[i], lights.at(i)->GetColour());
			ShaderProgram::LoadVector(location_attenuation[i], lights.at(i)->GetAttenuation());
		}
		else {
			ShaderProgram::LoadVector(location_lightPosition[i], glm::vec3(0.0f, 0.0f, 0.0f));
			ShaderProgram::LoadVector(location_lightColour[i], glm::vec3(0.0f, 0.0f, 0.0f));
			ShaderProgram::LoadVector(location_attenuation[i], glm::vec3(1.0f, 0.0f, 0.0f));
		}
	}
}

void StaticShader::LoadShineVariables(float damper, float reflectivity) {
	ShaderProgram::LoadFloat(location_shineDamper, damper);
	ShaderProgram::LoadFloat(location_reflectivity, reflectivity);
}

void StaticShader::LoadFakeLightingVariable(bool useFake) {
	ShaderProgram::LoadBool(location_useFakeLighting, useFake);
}

void StaticShader::LoadSkyColour(float r, float g, float b) {
	ShaderProgram::LoadVector(location_skyColour, glm::vec3(r, g, b));
}

void StaticShader::LoadNumberOfRows(unsigned int numberOfRows) {
	ShaderProgram::LoadFloat(location_numberOfRows, (float)numberOfRows);
}

void StaticShader::LoadOffset(float x, float y) {
	ShaderProgram::LoadVector(location_offset, glm::vec2(x, y));
}

void StaticShader::LoadClipPlane(glm::vec4 plane) {
	ShaderProgram::LoadVector(location_plane, plane);
}

void StaticShader::LoadTerrainSize(float terrainSize) {
	ShaderProgram::LoadFloat(location_terrainSize, terrainSize);
}

void StaticShader::GetAllUniformLocations() {
	location_transformationMatrix = ShaderProgram::GetUniformLocation("transformationMatrix");
	location_projectionMatrix = ShaderProgram::GetUniformLocation("projectionMatrix");
	location_viewMatrix = ShaderProgram::GetUniformLocation("viewMatrix");
	location_staticViewMatrix = ShaderProgram::GetUniformLocation("staticViewMatrix");
	location_shineDamper = ShaderProgram::GetUniformLocation("shineDamper");
	location_reflectivity = ShaderProgram::GetUniformLocation("reflectivity");
	location_useFakeLighting = ShaderProgram::GetUniformLocation("useFakeLighting");
	location_skyColour = ShaderProgram::GetUniformLocation("skyColour");
	location_numberOfRows = ShaderProgram::GetUniformLocation("numberOfRows");
	location_offset = ShaderProgram::GetUniformLocation("offset");
	location_plane = ShaderProgram::GetUniformLocation("plane");
	location_terrainSize = ShaderProgram::GetUniformLocation("terrainSize");

	for (int i = 0; i < MAX_LIGHTS; i++) {
		location_lightPosition[i] = ShaderProgram::GetUniformLocation("lightPosition[" + std::to_string(i) + "]");
		location_lightColour[i] = ShaderProgram::GetUniformLocation("lightColour[" + std::to_string(i) + "]");
		location_attenuation[i] = ShaderProgram::GetUniformLocation("attenuation[" + std::to_string(i) + "]");
	}
}
