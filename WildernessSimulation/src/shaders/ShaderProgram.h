#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <iterator>

#include <GLAD/glad.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

class ShaderProgram {
private:
	#define MAX_LIGHTS 4

	int programID;
	int vertexShaderID;
	int fragmentShaderID;

public:
	/**
	 * The constructor for the shaders. This creates a program and attaches the shaders
	 * passed in by the filename of the shaders. When the shaders are attached the
	 * program gets linked and validated.
	 *
	 * @param vertexFile
	 *		- The filepath of the vertex shader.
	 *
	 * @param fragmentFile
	 *		- The filepath of the fragment shader.
	 *
	 * @param attributes
	 *		- The attributes to be bound for the shaders.
	*/
	ShaderProgram(const std::string& vertexFile, const std::string& fragmentFile,
		std::map<unsigned int, std::string> attributes);
	
	/**
	 * When this function is called, it tells OpenGL that we want to use this program for
	 * rendering.
	*/
	void Start();

	/**
	 * When this function is called, it telss OpenGL that we don't want to use this program
	 * anymore for rendering.
	*/
	void Stop();

	/**
	 * Detaches the shaders from the program and deletes them. After that, deletes the program
	 * altogether so that it doesn't stay in memory.
	*/
	void CleanUp();

protected:
	/**
	 * Binds an attribute with a value so that it can be used in the GPU.
	 *
	 * @param attribute
	 *		- The value of the attribute.
	 *
	 * @param variableName
	 *		- The variable name of the attribute.
	*/
	void BindAttribute(int attribute, const std::string& variableName);

	/**
	 * Gets the location of the uniform passed in by it's name
	 *
	 * @return Id of the location of the uniform
	*/
	unsigned int GetUniformLocation(const std::string& uniformName);

	/**
	 * Loads a float variable into the memory to be passed to the GPU.
	 *
	 * @param location
	 *		- Location of the uniform.
	 *
	 * @param value
	 *		- Value of the uniform.
	*/
	void LoadFloat(unsigned int location, float value);

	/**
	* Loads an int variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param value
	*		- Value of the uniform.
	*/
	void LoadInt(unsigned int location, int value);

	/**
	* Loads a 2D vector variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param vector
	*		- Vector values of the uniform.
	*/
	void LoadVector(unsigned int location, glm::vec2 vector);

	/**
	* Loads a 3D vector variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param vector
	*		- Vector values of the uniform.
	*/
	void LoadVector(unsigned int location, glm::vec3 vector);

	/**
	* Loads a 4D vector variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param vector
	*		- Vector values of the uniform.
	*/
	void LoadVector(unsigned int location, glm::vec4 vector);

	/**
	* Loads a bool variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param vector
	*		- Bool value of the uniform.
	*/
	void LoadBool(unsigned int location, bool value);

	/**
	* Loads a matrix variable into the memory to be passed to the GPU.
	*
	* @param location
	*		- Location of the uniform.
	*
	* @param vector
	*		- Matrix values of the uniform.
	*/
	void LoadMatrix(unsigned int location, glm::mat4 matrix);

private:
	/**
	 * Loads the shader from the file and compiles it. 
	 *
	 * @param file
	 *		- The filepath of the shader code.
	 *
	 * @param type
	 *		- The type of the shader (example Vertex or Fragment).
	 *
	 * @return The shader ID.
	*/
	static int LoadShader(const std::string& file, int type);
};
