#include "TerrainShader.h"

const std::string TerrainShader::VERTEX_FILE = "src/shaders/terrainVertexShader.shader";
const std::string TerrainShader::FRAGMENT_FILE = "src/shaders/terrainFragmentShader.shader";

const std::map<unsigned int, std::string> TerrainShader::ATTRIBUTES = {
	{ 0, "position" },
    { 1, "textureCoords" },
    { 2, "normals" }
};

TerrainShader::TerrainShader() :
	ShaderProgram::ShaderProgram(TerrainShader::VERTEX_FILE, TerrainShader::FRAGMENT_FILE, 
		TerrainShader::ATTRIBUTES) {
	GetAllUniformLocations();
}

void TerrainShader::LoadTransformationMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_transformationMatrix, matrix);
}

void TerrainShader::LoadViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix = Maths::CreateViewMatrix(camera);
	ShaderProgram::LoadMatrix(location_viewMatrix, viewMatrix);
}

void TerrainShader::LoadStaticViewMatrix(Camera& staticCamera) {
	glm::mat4 viewMatrix = Maths::CreateStaticViewMatrix(staticCamera);
	ShaderProgram::LoadMatrix(location_staticViewMatrix, viewMatrix);
}

void TerrainShader::LoadProjectionMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_projectionMatrix, matrix);
}

void TerrainShader::LoadLights(std::vector<Light*>& lights) {
	for (unsigned int i = 0; i < MAX_LIGHTS; i++) {
		if (i < lights.size()) {
			ShaderProgram::LoadVector(location_lightPosition[i], lights.at(i)->GetPosition());
			ShaderProgram::LoadVector(location_lightColour[i], lights.at(i)->GetColour());
			ShaderProgram::LoadVector(location_attenuation[i], lights.at(i)->GetAttenuation());
		}
		else {
			ShaderProgram::LoadVector(location_lightPosition[i], glm::vec3(0.0f, 0.0f, 0.0f));
			ShaderProgram::LoadVector(location_lightColour[i], glm::vec3(0.0f, 0.0f, 0.0f));
			ShaderProgram::LoadVector(location_attenuation[i], glm::vec3(1.0f, 0.0f, 0.0f));
		}
	}
}

void TerrainShader::LoadShineVariables(float damper, float reflectivity) {
	ShaderProgram::LoadFloat(location_shineDamper, damper);
	ShaderProgram::LoadFloat(location_reflectivity, reflectivity);
}

void TerrainShader::LoadSkyColour(float r, float g, float b) {
	ShaderProgram::LoadVector(location_skyColour, glm::vec3(r, g, b));
}

void TerrainShader::LoadClipPlane(glm::vec4 plane) {
	ShaderProgram::LoadVector(location_plane, plane);
}

void TerrainShader::LoadToShadowSpaceMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_toShadowMapSpace, matrix);
}

void TerrainShader::LoadShadowDistance(float shadowDistance) {
	ShaderProgram::LoadFloat(location_shadowDistance, shadowDistance);
}

void TerrainShader::LoadShadowMapSize(unsigned int shadowMapSize) {
	ShaderProgram::LoadInt(location_shadowMapSize, shadowMapSize);
}

void TerrainShader::LoadTerrainSize(float terrainSize) {
	ShaderProgram::LoadFloat(location_terrainSize, terrainSize);
}

void TerrainShader::ConnectTextureUnits() {
	ShaderProgram::LoadInt(location_mudTexture, 0);
	ShaderProgram::LoadInt(location_grassTexture, 1);
	ShaderProgram::LoadInt(location_flowerGrassTexture, 2);
	ShaderProgram::LoadInt(location_dirtTexture, 3);
	ShaderProgram::LoadInt(location_rockTexture, 4);
	ShaderProgram::LoadInt(location_blendMap, 5);
	ShaderProgram::LoadInt(location_shadowMap, 6);
}

void TerrainShader::GetAllUniformLocations() {
	location_transformationMatrix = ShaderProgram::GetUniformLocation("transformationMatrix");
	location_projectionMatrix = ShaderProgram::GetUniformLocation("projectionMatrix");
	location_viewMatrix = ShaderProgram::GetUniformLocation("viewMatrix");
	location_staticViewMatrix = ShaderProgram::GetUniformLocation("staticViewMatrix");
	location_shineDamper = ShaderProgram::GetUniformLocation("shineDamper");
	location_reflectivity = ShaderProgram::GetUniformLocation("reflectivity");
	location_skyColour = ShaderProgram::GetUniformLocation("skyColour");
	location_mudTexture = ShaderProgram::GetUniformLocation("mudTexture");
	location_grassTexture = ShaderProgram::GetUniformLocation("grassTexture");
	location_flowerGrassTexture = ShaderProgram::GetUniformLocation("flowerGrassTexture");
	location_dirtTexture = ShaderProgram::GetUniformLocation("dirtTexture");
	location_rockTexture = ShaderProgram::GetUniformLocation("rockTexture");
	location_blendMap = ShaderProgram::GetUniformLocation("blendMap");
	location_plane = ShaderProgram::GetUniformLocation("plane");
	location_toShadowMapSpace = ShaderProgram::GetUniformLocation("toShadowMapSpace");
	location_shadowMap = ShaderProgram::GetUniformLocation("shadowMap");
	location_shadowDistance = ShaderProgram::GetUniformLocation("shadowDistance");
	location_shadowMapSize = ShaderProgram::GetUniformLocation("mapSize");
	location_terrainSize = ShaderProgram::GetUniformLocation("terrainSize");

	for (int i = 0; i < MAX_LIGHTS; i++) {
		location_lightPosition[i] = ShaderProgram::GetUniformLocation("lightPosition[" + std::to_string(i) + "]");
		location_lightColour[i] = ShaderProgram::GetUniformLocation("lightColour[" + std::to_string(i) + "]");
		location_attenuation[i] = ShaderProgram::GetUniformLocation("attenuation[" + std::to_string(i) + "]");
	}
}
