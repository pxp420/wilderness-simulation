#pragma once

#include <vector>

#include "../terrains/Terrain.h"
#include "../entities/Entity.h"

struct MapBlock {
	enum Type{EMPTY, GRASS, PLANT, WOOD, HUMAN, ROCK, LOG} type;
	bool canPassThrough;
	Entity* entity;

	MapBlock(Type type, bool canPassThrough, Entity* entity);
};

class Map {
private:
	static std::vector<std::vector<MapBlock>>* MAP;
	Map() {}
public:
	void AddBlockGivenPosition(const glm::vec3& position, MapBlock& block);

	void RemoveBlockGivenPosition(const glm::vec3& position);

	static Map& GetInstance() {
		static Map instance;
		return instance;
	}

	Map(Map const&) = delete;
	void operator=(Map const&) = delete;

	void InitialiseMap();

	std::vector<std::vector<MapBlock>>* GetMap();
};