#include "Map.h"
#include "../pathFinding/AStarGenerator.h"

std::vector<std::vector<MapBlock>>* Map::MAP = nullptr;

MapBlock::MapBlock(Type type, bool canPassThrough, Entity* entity) : 
	type(type), canPassThrough(canPassThrough), entity(entity) { }

void Map::AddBlockGivenPosition(const glm::vec3& position, MapBlock& block) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();

	int i = std::round((position.x - gridSquareSize / 2) / gridSquareSize);
	int j = std::round((position.z - gridSquareSize / 2) / gridSquareSize);

	MAP->at(i).at(j) = block;

	if (!block.canPassThrough) {
		AStarGenerator::GetGenerator()->addCollision({ i, j });
	}
}

void Map::RemoveBlockGivenPosition(const glm::vec3& position) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();

	int i = std::round((position.x - gridSquareSize / 2) / gridSquareSize);
	int j = std::round((position.z - gridSquareSize / 2) / gridSquareSize);

	MAP->at(i).at(j) = MapBlock(MapBlock::EMPTY, true, nullptr);
	AStarGenerator::GetGenerator()->removeCollision({ i, j });
}

void Map::InitialiseMap() {
	MAP = new std::vector<std::vector<MapBlock>>(Terrain::GetInstance()->GetGridSize(),
		std::vector<MapBlock>(Terrain::GetInstance()->GetGridSize(), MapBlock(MapBlock::EMPTY, true, nullptr)));
}

std::vector<std::vector<MapBlock>>* Map::GetMap() {
	return MAP;
}
