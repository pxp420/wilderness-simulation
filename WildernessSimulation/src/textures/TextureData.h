#pragma once

#include <vector>

class TextureData {
private:
	int width;
	int height;
	unsigned char* buffer;
public:
	/**
	 * Stores the data of an image into a structure with this constructor.
	 *
	 * @param buffer
	 *		- The data of the image.
	 *
	 * @param width
	 *		- The width of the image.
	 *
	 * @param height
	 *		- The height of the image.
	*/
	TextureData(unsigned char* buffer, int width, int height);

	/**
	 * Gets the width of the image.
	 *
	 * @return The width of the image.
	*/
	int GetWidth();

	/**
	 * Gets the height of the image.
	 *
	 * @return The height of the image.
	*/
	int GetHeight();

	/**
	 * Gets the data of the image.
	 *
	 * @return The data of the image.
	*/
	unsigned char* GetBuffer();
};
