#pragma once

#include "TerrainTexture.h"

class TerrainTexturePack {
private: 
	TerrainTexture mudTexture;
	TerrainTexture grassTexture;
	TerrainTexture flowerGrassTexture;
	TerrainTexture dirtTexture;
	TerrainTexture rockTexture;

public:
	/**
	 * Constructor that holds all the terrain textures for the r, g, b channel and
	 * the background as well.
	 *
	 * @param backgroundTexture
	 *		- The background texture.
	 *
	 * @param rTexture
	 *		- The red channel texture.
	 *
	 * @param gTexture
	 *		- The green channel texture.
	 *
	 * @param bTexture
	 *		- The blue channel texture.
	*/
	TerrainTexturePack(TerrainTexture mudTexture, TerrainTexture grassTexture,
		TerrainTexture flowerGrassTexture, TerrainTexture dirtTexture, TerrainTexture rockTexture);

	/**
	 * Gets the background texture.
	 *
	 * @return The background texture.
	*/
	TerrainTexture GetMudTexture();

	/**
	* Gets the red channel terrain texture.
	*
	* @return The red channel terrain texture.
	*/
	TerrainTexture GetGrassTexture();

	/**
	* Gets the green channel terrain texture.
	*
	* @return The green channel terrain texture.
	*/
	TerrainTexture GetFlowerGrassTexture();

	/**
	* Gets the blue channel terrain texture.
	*
	* @return The blue channel terrain texture.
	*/
	TerrainTexture GetDirtTexture();

	/**
	* Gets the blue channel terrain texture.
	*
	* @return The blue channel terrain texture.
	*/
	TerrainTexture GetRockTexture();
};
