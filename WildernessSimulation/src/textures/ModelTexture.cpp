#pragma once

#include "ModelTexture.h"

ModelTexture::ModelTexture() {}

ModelTexture::ModelTexture(unsigned int id) :
	textureID(id), shineDamper(1), reflectivity(0), hasTransparency(false),
	useFakeLighting(false), numberOfRows(1) {}

unsigned int ModelTexture::GetID() {
	return this->textureID;
}

bool ModelTexture::GetTransparency() {
	return this->hasTransparency;
}

void ModelTexture::SetTransparency(bool transparency) {
	this->hasTransparency = transparency;
}

bool ModelTexture::GetFakeLighting() {
	return this->useFakeLighting;
}

void ModelTexture::SetFakeLighting(bool fakeLighting) {
	this->useFakeLighting = fakeLighting;
}

float ModelTexture::GetShineDamper() {
	return shineDamper;
}

void ModelTexture::SetShineDamper(float shineDamper) {
	this->shineDamper = shineDamper;
}

float ModelTexture::GetReflectivity() {
	return reflectivity;
}

void ModelTexture::SetReflectivity(float reflectivity) {
	this->reflectivity = reflectivity;
}

unsigned int ModelTexture::GetNumberOfRows() {
	return numberOfRows;
}

void ModelTexture::SetNumberOfRows(unsigned int numberOfRows) {
	this->numberOfRows = numberOfRows;
}
