#pragma once

class TerrainTexture {
private:
	unsigned int textureID;
public:
	/**
	 * Constructor for a terrain texture.
	 *
	 * @param textureID
	 *		- The texture id of the terrain.
	*/
	TerrainTexture(unsigned int textureID);

	/**
	 * Gets the texture id to know in what unit it will be stored.
	 *
	 * @return The texture id.
	*/
	unsigned int GetTextureID();
};
