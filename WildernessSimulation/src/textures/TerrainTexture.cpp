#include "TerrainTexture.h"

TerrainTexture::TerrainTexture(unsigned int textureID) :
	textureID(textureID) {}

unsigned int TerrainTexture::GetTextureID() {
	return textureID;
}
