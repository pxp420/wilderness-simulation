#include "TerrainTexturePack.h"

TerrainTexturePack::TerrainTexturePack(TerrainTexture mudTexture, TerrainTexture grassTexture,
	TerrainTexture flowerGrassTexture, TerrainTexture dirtTexture, TerrainTexture rockTexture) :
	mudTexture(mudTexture), grassTexture(grassTexture), flowerGrassTexture(flowerGrassTexture),
	dirtTexture(dirtTexture), rockTexture(rockTexture) {}

TerrainTexture TerrainTexturePack::GetMudTexture() {
	return mudTexture;
}

TerrainTexture TerrainTexturePack::GetGrassTexture() {
	return grassTexture;
}

TerrainTexture TerrainTexturePack::GetFlowerGrassTexture() {
	return flowerGrassTexture;
}

TerrainTexture TerrainTexturePack::GetDirtTexture() {
	return dirtTexture;
}

TerrainTexture TerrainTexturePack::GetRockTexture() {
	return rockTexture;
}
