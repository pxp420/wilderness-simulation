#include "TextureData.h"

TextureData::TextureData(unsigned char* buffer, int width, int height) :
	buffer(buffer), width(width), height(height) {}

int TextureData::GetWidth() {
	return width;
}

int TextureData::GetHeight() {
	return height;
}

unsigned char* TextureData::GetBuffer() {
	return buffer;
}
