#pragma once

class ModelTexture {
private:
	unsigned int textureID;
	unsigned int numberOfRows;

	float shineDamper;
	float reflectivity;

	bool hasTransparency;
	bool useFakeLighting;
public:
	ModelTexture();
	/**
	 * Model texture constructor for storing the texture ID
	 *
	 * @param id
	 *		- The ID of the texture.
	*/
	ModelTexture(unsigned int id);

	/**
	 * @return The ID of the texture
	*/
	unsigned int GetID();

	/**
	 * Check if the texture has transparancy (alpha value from rgba).
	 *
	 * @return The transparency of the model.
	*/
	bool GetTransparency();

	/**
	 * Sets the transparency of a model.
	 *
	 * @param transparency
	 *		- The value of the transparency (true/false);
	*/
	void SetTransparency(bool transparency);

	/**
	 * Gets the fake lighting value that represents if the object has fake
	 * lighting or not. Normals come from the y axis downwards.
	 *
	 * @return The fake lighting boolean.
	*/
	bool GetFakeLighting();

	/**
	 * Sets the fake lighting value to true or false.
	 *
	 * @param fakeLighting
	 *		- The value of the fake lighting.
	*/
	void SetFakeLighting(bool fakeLighting);

	/**
	 * Gets the shine damper value. This is used to determine how close should
	 * the camera be to the direction of a reflected light on an object's point
	 * to see shininess.
	 *
	 * @return The shine damper value.
	*/
	float GetShineDamper();

	/**
	 * Sets the shine damper to a new value.
	 *
	 * @param shineDamper
	 *		- The value of the shine damper.
	*/
	void SetShineDamper(float shineDamper);

	/**
	 * Gets the reflectivity of a vertex. The reflectivity is used to determine
	 * if the material can reflect light or not. If the material has 0 reflectivity
	 * then there would be no shininess involved, only diffuse lighting.
	 *
	 * @return The reflectivity value of a vertex.
	*/
	float GetReflectivity();

	/**
	 * Sets the reflectivity to a new value.
	 *
	 * @param reflectivity
	 *		- The new reflectivity value.
	*/
	void SetReflectivity(float reflectivity);

	/**
	 * Get the number of rows of the texture image.
	 *
	 * @return The number of rows.
	*/
	unsigned int GetNumberOfRows();

	/**
	 * Sets the number of rows of the texture image.
	 *
	 * @param numberOfRows
	 *		- The number of rows.
	*/
	void SetNumberOfRows(unsigned int numberOfRows);
};
