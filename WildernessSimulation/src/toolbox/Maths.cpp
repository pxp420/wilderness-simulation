#include "Maths.h"

glm::mat4 Maths::CreateTransformationMatrix(glm::vec3 translation,
	float rx, float ry, float rz, float scale) {
	glm::mat4 matrix(1.0f);
	matrix = glm::translate(matrix, translation);
	matrix = glm::rotate(matrix, glm::radians(rx), glm::vec3(1.0f, 0.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(ry), glm::vec3(0.0f, 1.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(rz), glm::vec3(0.0f, 0.0f, 1.0f));
	matrix = glm::scale(matrix, glm::vec3(scale, scale, scale));

	return matrix;
}

glm::mat4 Maths::CreateTransformationMatrix(glm::vec3 translation,
	float rx, float ry, float rz, glm::vec2 scale) {
	glm::mat4 matrix(1.0f);
	matrix = glm::translate(matrix, translation);
	matrix = glm::rotate(matrix, glm::radians(rx), glm::vec3(1.0f, 0.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(ry), glm::vec3(0.0f, 1.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(rz), glm::vec3(0.0f, 0.0f, 1.0f));
	matrix = glm::scale(matrix, glm::vec3(scale.x, scale.y, 1.0f));

	return matrix;
}

glm::mat4 Maths::CreateTransformationMatrix(glm::vec2 translation, glm::vec2 scale) {
	glm::mat4 matrix(1.0f);
	matrix = glm::translate(matrix, glm::vec3(translation.x, translation.y, 0.0f));
	matrix = glm::scale(matrix, glm::vec3(scale.x, scale.y, 1.0f));

	return matrix;
}

glm::mat4 Maths::CreateTransformationMatrix(glm::vec3 translation, glm::vec2 scale) {
	glm::mat4 matrix(1.0f);
	matrix = glm::translate(matrix, translation);
	matrix = glm::scale(matrix, glm::vec3(scale.x, scale.y, 1.0f));

	return matrix;
}

glm::mat4 Maths::CreateViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix(1.0f);
	viewMatrix = glm::rotate(viewMatrix, glm::radians(camera.GetPitch()), glm::vec3(1.0f, 0.0f, 0.0f));
	viewMatrix = glm::rotate(viewMatrix, glm::radians(camera.GetYaw()), glm::vec3(0.0f, 1.0f, 0.0f));
	
	glm::vec3 cameraPos = camera.GetPosition();
	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	viewMatrix = glm::translate(viewMatrix, negativeCameraPos);

	return viewMatrix;
}

glm::mat4 Maths::CreateStaticViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix(1.0f);
	viewMatrix = glm::rotate(viewMatrix, glm::radians(0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	viewMatrix = glm::rotate(viewMatrix, glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	glm::vec3 cameraPos(Terrain::SIZE / 2, 0.0f, Terrain::SIZE / 2);
	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	viewMatrix = glm::translate(viewMatrix, negativeCameraPos);

	return viewMatrix;
}

float Maths::BarryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;

	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

glm::vec4 Maths::TransformToScreenSpace(glm::vec3 positionAndScale) {
	glm::mat4 projectionMatrix = glm::ortho(0.0f, (float)DisplayManager::WIDTH, 0.0f, (float)DisplayManager::HEIGHT, -1.0f, 1.0f);
	glm::vec4 vertexPosition(positionAndScale.x, positionAndScale.y, 0.0f, 1.0f);

	float scaleY = positionAndScale.z;
	float scaleX = scaleY / DisplayManager::WIDTH * DisplayManager::HEIGHT;

	glm::vec4 transformedPosition = projectionMatrix * vertexPosition;
	return glm::vec4(transformedPosition.x, transformedPosition.y, scaleX, scaleY);
}
