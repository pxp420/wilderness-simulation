#pragma once

#include "../libraries/stb_image.h"
#include "../terrains/Terrain.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <string>
#include <vector>
#include <fstream>

/**
 * Simple structure for a pixel.
*/
struct Pixel {
	char r, g, b;
};

class ColourInterpolation {
private:
	static glm::vec3 BLUE;
	static glm::vec3 BROWN;
	static glm::vec3 LIGHT_BLUE;
	static glm::vec3 GREEN;
	static glm::vec3 DARK_GREEN;
	static glm::vec3 DARK_YELLOW;

	enum {
		WATER = 0,
		GRASS = 1,
		MUD = 2,
		ROCK = 3
	};

	/**
	 * Get the minimum height in the 2D array of heights.
	 *
	 * @param heights
	 *		- The 2D array of heights.
	 *
	 * @return The minimum height.
	*/
	static float GetMinHeight(const std::vector<std::vector<float>>& heights);

	/**
	* Get the maximum height in the 2D array of heights.
	*
	* @param heights
	*		- The 2D array of heights.
	*
	* @return The maximum height.
	*/
	static float GetMaxHeight(const std::vector<std::vector<float>>& heights);

	/**
	 * Transform the normalized value to the original 0 - 255 rgb range.
	 *
	 * @param height
	 *		- The normalized height.
	 *
	 * @return The pixel value of the height.
	*/
	static float TransformToPixelValue(float height);

	/**
	 * Save the interpolated map into a PPM image.
	 *
	 * @param heights
	 *		- The interpolated colours based on the heights.
	*/
	static void SaveImage(const std::vector<std::vector<glm::vec3>>& heights);

	/**
	 * Initialise the intervals based on the heights. The intervals are used to find out what
	 * colours should be interpolated taking into consideration the height.
	 *
	 * @param heightsIntervals
	 *		- The vector with all the intervals.
	 *
	 * @param min
	 *		- The minmum value of the interval.
	 *
	 * @param max
	 *		- The maximum value of the interval.
	*/
	static void InitialiseIntervals(std::vector<glm::vec2>& heightIntervals, float min, float max);

	/**
	 * Checks wether a height is in a interval or not.
	 *
	 * @param height
	 *		- The height being tested.
	 *
	 * @param interval
	 *		- The interval the height is being tested against.
	 *
	 * @return True if the height is in the interval.
	*/
	static bool IsHeightInInterval(float height, glm::vec2 interval);

public:
	/**
	 * Go through all the heights in the 2D array and interpolate the colours
	 * based on the values retrieved.
	 *
	 * @param heights
	 *		- The 2D array of heights.
	*/
	static void InterpolateMap(const std::vector<std::vector<float>>& heights);

	/**
	* Interpolate between two colours to make a different one.
	*
	* @param colour1
	*		- The first colour.
	*
	* @param colour2
	*		- The second colour.
	*
	* @param factor
	*		- The amount of interpolation.
	*
	* @return The new interpolated colour.
	*/
	static glm::vec3 Interpolate(glm::vec3 colour1, glm::vec3 colour2, float factor);
};
