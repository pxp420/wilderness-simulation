#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

class StringManipulation {
public:
	/**
	* Splits a string by a delimiter
	*
	* @param string
	*		- The string to be split.
	*
	* @param delimiter
	*		- The delimiter used for splitting.
	*
	* @return A vector with the splitted words.
	*/
	static std::vector<std::string> Split(const std::string& string, char delimeter);
};
