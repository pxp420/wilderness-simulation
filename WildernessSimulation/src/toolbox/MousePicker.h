#pragma once

#include "Maths.h"
#include "../terrains/Terrain.h"

class MousePicker {
private:
	const static unsigned int RECURSION_COUNT;
	const static float RAY_RANGE;

	glm::vec3 currentRay;

	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;
	
	Camera* camera;

	glm::vec3 currentTerrainPoint;
public:
	/**
	 * The mouse picker constructor used for moving objects with the cursor
	 * on the main window.
	 *
	 * @param projection
	 *		- The projection matrix.
	 *
	 * @param terrain
	 *		- The terrain the object is placed on.
	*/
	MousePicker(Camera* camera, glm::mat4 projection);

	/**
	 * Gets the current ray formed by the camera.
	 *
	 * @return The current ray.
	*/
	glm::vec3 GetCurrentRay();

	/**
	 * Gets the current terrain point where the ray points to.
	 *
	 * @return The terrain point.
	*/
	glm::vec3 GetCurrentTerrainPoint();

	/**
	 * Gets the corner coordinates in world space of the screen.
	 *
	 * @param xScreenPos 
	 *		- The x screen coordinate.
	 *
	 * @param yScreenPos
	 *		- The y screen coordinate.
	 *
	 * @return The vector with the coordinates.
	*/
	glm::vec2 GetCornerCoordinates(float xScreenPos, float yScreenPos);

	/**
	 * Calculates the terrain point each frame until the recurssion count
	 * has been achieved.
	*/
	void Update();
private:
	/**
	 * Calculates the mouse ray formed by the camera.
	 *
	 * @return The mouse ray formed by the camera.
	*/
	glm::vec3 CalculateMouseRay();

	/**
	 * Tranform the screen x and y coordinates formed by the mouse in
	 * values between (-1 and 1).
	 *
	 * @return The normalized device coordinates.
	*/
	glm::vec2 GetNormalizedDeviceCoords(float mouseX, float mouseY);

	/**
	 * Transform the 2D screen position into 3D screen position.
	 *
	 * @param clipCoords
	 *		- The clip coordinates transformed in 3D.
	 *
	 * @return The 3D screen position of the mouse.
	*/
	glm::vec4 ToEyeCoords(glm::vec4 clipCoords);

	/**
	 * Transform the eye coordinates into world coordinates so that
	 * the vector result is in global space.
	 *
	 * @param eyeCoords
	 *		- The eye coordinates passed in.
	 *
	 * @return The world coordinates of the mouse.
	*/
	glm::vec3 ToWorldCoords(glm::vec4 eyeCoords);

	/**
	 * Gets the coordinates of where the mouse is pointing to.
	 *
	 * @param ray
	 *		- The ray projected by the camera.
	 *
	 * @param distance
	 *		- How long the ray should be.
	 *
	 * @return The coordinates of where the mouse is pointing to.
	*/
	glm::vec3 GetPointOnRay(glm::vec3 ray, float distance);

	/**
	 * Binary search algorithm for finding the intersection of the ray 
	 * and terrain.
	 *
	 * @param count
	 *		- The maximum iterations of the algorithm.
	 *
	 * @param start
	 *		- The minimum for the search.
	 *
	 * @param finish
	 *		- The maximum for the search.
	 *
	 * @param ray
	 *		- The ray projected by the camera.
	 *
	 * @return The position of where the object should be placed on.
	*/
	glm::vec3 BinarySearch(int count, float start, float finish, glm::vec3 ray);

	/**
	 * Checks to see if there is any intersection between the ray
	 * and the terrain.
	 *
	 * @param start
	 *		- The minimum value of the ray.
	 *
	 * @param finish
	 *		- The maximum value of the ray.
	 *
	 * @param ray
	 *		- The projected ray by the camera.
	 *
	 * @return True if the intersection is in range.
	*/
	bool IntersectionInRange(float start, float finish, glm::vec3 ray);

	/**
	 * Checks to see if the given point is below or above the terrain.
	 *
	 * @param testPoint
	 *		- The coordinate passed in for checking.
	 *
	 * @return True if the test point is below the terrain.
	*/
	bool IsUnderGround(glm::vec3 testPoint);
};
