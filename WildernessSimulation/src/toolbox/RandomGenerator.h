#pragma once

#include <random>
#include <chrono>

class RandomGenerator {
public:
	static std::mt19937_64 range;
	static uint64_t timeSeed;
	static std::seed_seq ss;

	static float GetNextFloat(float min, float max);
	static int GetNextInt(int min, int max);
};