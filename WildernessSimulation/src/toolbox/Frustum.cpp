#include "Frustum.h"

const float Frustum::OFFSET = 15.0f;
const glm::vec4 Frustum::UP = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
const glm::vec4 Frustum::FORWARD = glm::vec4(0.0f, 0.0f, -1.0f, 0.0f);

Frustum::Frustum(glm::mat4* viewMatrix, Camera* camera, float nearPlane, float farPlane) :
	viewMatrix(viewMatrix), cam(camera), farHeight(0.0f), farWidth(0.0f),
	nearHeight(0.0f), nearWidth(0.0f), nearPlane(nearPlane), farPlane(farPlane) {
	CalculateWidthsAndHeights();
}

void Frustum::Update() {
	glm::mat4 rotation = CalculateCameraRotationMatrix();
	glm::vec3 forwardVector = rotation * FORWARD;

	glm::vec3 toFar = forwardVector;
	toFar = toFar * farPlane;

	glm::vec3 toNear = forwardVector;
	toNear = toNear * nearPlane;

	glm::vec3 centerNear = cam->GetPosition() + toNear;
	glm::vec3 centerFar = cam->GetPosition() + toFar;

	points = CalculateFrustumVertices(rotation, forwardVector, centerNear, centerFar);

	bool first = true;
	for (glm::vec3 point : points) {
		if (first) {
			minX = point.x;
			maxX = point.x;
			minY = point.y;
			maxY = point.y;
			minZ = point.z;
			maxZ = point.z;
			first = false;
			continue;
		}
		if (point.x > maxX) {
			maxX = point.x;
		}
		else if (point.x < minX) {
			minX = point.x;
		}
		if (point.y > maxY) {
			maxY = point.y;
		}
		else if (point.y < minY) {
			minY = point.y;
		}
		if (point.z > maxZ) {
			maxZ = point.z;
		}
		else if (point.z < minZ) {
			minZ = point.z;
		}
	}
	
	maxZ += OFFSET;
}

glm::vec3 Frustum::GetCenter() {
	float x = (minX + maxX) / 2.0f;
	float y = (minY + maxY) / 2.0f;
	float z = (minZ + maxZ) / 2.0f;

	glm::vec4 center(x, y, z, 1.0f);
	glm::mat4 invertedLight = glm::inverse(*viewMatrix);

	return invertedLight * center;
}

float Frustum::GetWidth() {
	return maxX - minX;
}

float Frustum::GetHeight() {
	return maxY - minY;
}

float Frustum::GetLength() {
	return maxZ - minZ;
}

std::vector<glm::vec3> Frustum::GetPoints() {
	return points;
}

std::vector<glm::vec3> Frustum::CalculateFrustumVertices(glm::mat4 rotation, glm::vec3 forwardVector,
	glm::vec3 centerNear, glm::vec3 centerFar) {
	glm::vec3 upVector = rotation * UP;
	glm::vec3 rightVector = glm::cross(forwardVector, upVector);
	glm::vec3 downVector(-upVector.x, -upVector.y, -upVector.z);
	glm::vec3 leftVector(-rightVector.x, -rightVector.y, -rightVector.z);

	glm::vec3 farTop = centerFar + glm::vec3(upVector.x * farHeight, upVector.y * farHeight, upVector.z * farHeight);
	glm::vec3 farBottom = centerFar + glm::vec3(downVector.x * farHeight, downVector.y * farHeight, downVector.z * farHeight);
	glm::vec3 nearTop = centerNear + glm::vec3(upVector.x * nearHeight, upVector.y * nearHeight, upVector.z * nearHeight);
	glm::vec3 nearBottom = centerNear + glm::vec3(downVector.x * nearHeight, downVector.y * nearHeight, downVector.z * nearHeight);

	points.clear();
	points = std::vector<glm::vec3>(8);

	points[0] = CalculateLightSpaceFrustumCorner(farTop, rightVector, farWidth); // left top far corner
	points[1] = CalculateLightSpaceFrustumCorner(farTop, leftVector, farWidth); // right top far corner
	points[2] = CalculateLightSpaceFrustumCorner(farBottom, rightVector, farWidth); // left bottom far corner
	points[3] = CalculateLightSpaceFrustumCorner(farBottom, leftVector, farWidth); // right bottom far corner
	points[4] = CalculateLightSpaceFrustumCorner(nearTop, rightVector, nearWidth);// left top near corner
	points[5] = CalculateLightSpaceFrustumCorner(nearTop, leftVector, nearWidth); // right top near corner
	points[6] = CalculateLightSpaceFrustumCorner(nearBottom, rightVector, nearWidth); // left bottom near corner
	points[7] = CalculateLightSpaceFrustumCorner(nearBottom, leftVector, nearWidth); // right bottom near corner

	return points;
}

glm::vec4 Frustum::CalculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width) {
	glm::vec3 point = startPoint + glm::vec3(direction.x * width, direction.y * width, direction.z * width);
	glm::vec4 point4f(point.x, point.y, point.z, 1.0f);
	point4f = *viewMatrix * point4f;

	return point4f;
}

glm::mat4 Frustum::CalculateCameraRotationMatrix() {
	glm::mat4 rotation(1.0f);
	rotation = glm::rotate(rotation, (float)glm::radians(-cam->GetYaw()), glm::vec3(0.0f, 1.0f, 0.0f));
	rotation = glm::rotate(rotation, (float)glm::radians(-cam->GetPitch()), glm::vec3(1.0f, 0.0f, 0.0f));

	return rotation;
}

void Frustum::CalculateWidthsAndHeights() {
	nearHeight = (float)(2 * nearPlane * glm::tan(glm::radians(45.0f) / 2.0f));
	farHeight = (float)(2 * farPlane * glm::tan(glm::radians(45.0f) / 2.0f));

	nearWidth = nearHeight * GetAspectRatio();
	farWidth = farHeight * GetAspectRatio();
}

float Frustum::GetAspectRatio() {
	return ((float)DisplayManager::WIDTH / (float)DisplayManager::HEIGHT) * 2.0f;
}

bool Frustum::PointInFrustum(glm::vec3& point) {
	glm::mat4 rotation = CalculateCameraRotationMatrix();

	glm::vec3 upVector = rotation * UP;
	glm::vec3 forwardVector = rotation * FORWARD;
	glm::vec3 rightVector = glm::cross(upVector, forwardVector);

	glm::vec3 toFar = forwardVector * farPlane;
	glm::vec3 toNear = forwardVector * farPlane;

	glm::vec3 centerNear = cam->GetPosition() + toNear;
	glm::vec3 centerFar = cam->GetPosition() + toFar;

	float pcz, pcx, pcy, aux;
	float offset = 40.0f;

	glm::vec3 camToPoint = point - cam->GetPosition();

	if (camToPoint.z > toFar.z) {
		camToPoint.z -= offset;
	}
	else if (camToPoint.z < toNear.z) {
		camToPoint.z += offset;
	}

	pcz = glm::dot(camToPoint, forwardVector);
	if (pcz > farPlane || pcz < nearPlane) {
		return false;
	}

	float tang = (float)glm::tan(glm::radians(45.0f) * 0.5f);
	pcy = glm::dot(camToPoint, upVector);
	aux = pcz * tang;
	if (pcy > aux + 2.0f * offset || pcy < -aux - 2.0f * offset) {
		return false;
	}

	if (camToPoint.x < centerFar.x) {
		camToPoint.x += offset;
	}
	else {
		camToPoint.x -= offset;
	}

	pcx = glm::dot(camToPoint, rightVector);
	aux = aux * GetAspectRatio();
	if (pcx > aux || pcx < -aux) {
		return false;
	}

	return true;
}