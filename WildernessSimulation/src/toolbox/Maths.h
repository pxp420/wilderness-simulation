#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "../entities/Camera.h"

class Camera;
class Maths {
public:
	/**
	 * Creates a transformation matrix that can be applied to each model
	 * loaded into the shaders. This will be passed out to the shaders by
	 * a uniform.
	 *
	 * @param translation
	 *		- The translation vector representing where the object should 
	 *		move.
	 *
	 * @param rx
	 *		- Rotation of the model on the x axis.
	 *
	 * @param ry
	 *		- Rotation of the model on the y axis.
	 *
	 * @param rz
	 *		- Rotation of the model on the z axis.
	 *
	 * @param scale
	 *		- The scale factor of the object. How big/small the object 
	 *		will become.
	 *
	 * @return The resulting matrix after transformations.
	*/
	static glm::mat4 CreateTransformationMatrix(glm::vec3 translation,
		float rx, float ry, float rz, float scale);

	/**
	* Creates a transformation matrix that can be applied to each model
	* loaded into the shaders. This will be passed out to the shaders by
	* a uniform.
	*
	* @param translation
	*		- The translation vector representing where the object should
	*		move.
	*
	* @param rx
	*		- Rotation of the model on the x axis.
	*
	* @param ry
	*		- Rotation of the model on the y axis.
	*
	* @param rz
	*		- Rotation of the model on the z axis.
	*
	* @param scale
	*		- The scale factor of the object. How big/small the object
	*		will become.
	*
	* @return The resulting matrix after transformations.
	*/
	static glm::mat4 CreateTransformationMatrix(glm::vec3 translation,
		float rx, float ry, float rz, glm::vec2 scale);

	/** 
	 * Creates a transformation matrix that can be applied to each model
	 * loaded into the shaders. This will be passed out to the shaders by
	 * a uniform.
	 *
	 * @param translation
	 *		- The translation vector representing where the object should
	 *		move
	 *
	 * @param scale
	 *		- The scale factor of the object. How big/small the object
	 *		will become.
	 *
	 * @return The resulting matrix after transformations.
	 */
	static glm::mat4 CreateTransformationMatrix(glm::vec2 translation,
		glm::vec2 scale);

	/**
	* Creates a transformation matrix that can be applied to each model
	* loaded into the shaders. This will be passed out to the shaders by
	* a uniform.
	*
	* @param translation
	*		- The translation vector representing where the object should
	*		move
	*
	* @param scale
	*		- The scale factor of the object. How big/small the object
	*		will become.
	*
	* @return The resulting matrix after transformations.
	*/
	static glm::mat4 CreateTransformationMatrix(glm::vec3 translation,
		glm::vec2 scale);

	/**
	 * Creates a view matrix used for simulating a camera on the screen.
	 * This gets multiplied by each position of an object and therefore 
	 * making it look like we move to the left although the object moves
	 * to the right.
	 *
	 * @param camera
	 *		- The camera entity used for creating the matrix.
	*/
	static glm::mat4 CreateViewMatrix(Camera& camera);

	/**
	* Creates a static view matrix used for simulating a camera on the screen.
	* This is used for fading the shadows at the edges of the map.
	*
	* @param camera
	*		- The camera entity used for creating the matrix.
	*/
	static glm::mat4 CreateStaticViewMatrix(Camera& camera);

	/**
	 * Returns the variable of a point inside a triangle based on all the
	 * other three values of the variables for the triangle corners.
	*/
	static float BarryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);

	static glm::vec4 TransformToScreenSpace(glm::vec3 positionAndScale);

	template<typename Iter, typename RandomGenerator>
	static Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
		std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
		std::advance(start, dis(g));
		return start;
	}

	template<typename Iter>
	static Iter SelectRandomly(Iter start, Iter end) {
		static std::random_device rd;
		static std::mt19937 gen(rd());
		return select_randomly(start, end, gen);
	}

	template<class BidiIter >
	static BidiIter RandomUnique(BidiIter begin, BidiIter end, size_t num_random) {
		size_t left = std::distance(begin, end);
		while (num_random--) {
			BidiIter r = begin;
			std::advance(r, rand() % left);
			std::swap(*begin, *r);
			++begin;
			--left;
		}
		return begin;
	}
};
