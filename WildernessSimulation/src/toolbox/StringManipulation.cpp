#include "StringManipulation.h"

std::vector<std::string> StringManipulation::Split(const std::string& string, char delimeter) {
	std::stringstream ss(string);
	std::string item;
	std::vector<std::string> splittedStrings;
	while (std::getline(ss, item, delimeter)) {
		splittedStrings.emplace_back(item);
	}
	return splittedStrings;
}
