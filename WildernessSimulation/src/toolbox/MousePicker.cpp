#include "MousePicker.h"

const unsigned int MousePicker::RECURSION_COUNT = 10;
const float MousePicker::RAY_RANGE = 1200.0f;

MousePicker::MousePicker(Camera* camera, glm::mat4 projection) :
	camera(camera), projectionMatrix(projection), viewMatrix(Maths::CreateViewMatrix(*camera)),
	currentRay(glm::vec3(0.0f, 0.0f, 0.0f)) {}

glm::vec3 MousePicker::GetCurrentRay() {
	return currentRay;
}

glm::vec3 MousePicker::GetCurrentTerrainPoint() {
	return currentTerrainPoint;
}

glm::vec2 MousePicker::GetCornerCoordinates(float xScreenPos, float yScreenPos) {
	glm::vec2 normalizedCoords = GetNormalizedDeviceCoords(xScreenPos, yScreenPos);
	glm::vec4 clipCoords(normalizedCoords.x, normalizedCoords.y, -1.0f, 1.0f);
	glm::vec4 eyeCoords = ToEyeCoords(clipCoords);
	glm::vec3 worldRay = ToWorldCoords(eyeCoords);

	glm::vec3 terrainPoint(0.0f);
	if (IntersectionInRange(0.0f, MousePicker::RAY_RANGE * 5, worldRay)) {
		terrainPoint = BinarySearch(0, 0.0f, MousePicker::RAY_RANGE * 5, worldRay);
	}
	return glm::vec2(terrainPoint.x, terrainPoint.z);

}

void MousePicker::Update() {
	double xpos, ypos;
	glfwGetCursorPos(DisplayManager::window, &xpos, &ypos);
	glm::vec2 x = GetCornerCoordinates(xpos, ypos);


	viewMatrix = Maths::CreateViewMatrix(*camera);
	currentRay = CalculateMouseRay();
	if (IntersectionInRange(0.0f, MousePicker::RAY_RANGE, currentRay)) {
		currentTerrainPoint = BinarySearch(0, 0.0f, MousePicker::RAY_RANGE, currentRay);
	}
}

glm::vec3 MousePicker::CalculateMouseRay() {
	double mouseX, mouseY;
	glfwGetCursorPos(DisplayManager::window, &mouseX, &mouseY);

	glm::vec2 normalizedCoords = GetNormalizedDeviceCoords((float)mouseX, (float)mouseY);
	glm::vec4 clipCoords(normalizedCoords.x, normalizedCoords.y, -1.0f, 1.0f);
	glm::vec4 eyeCoords = ToEyeCoords(clipCoords);
	glm::vec3 worldRay = ToWorldCoords(eyeCoords);

	return worldRay;
}

glm::vec2 MousePicker::GetNormalizedDeviceCoords(float mouseX, float mouseY) {
	float x = ((2.0f * mouseX) / (float)DisplayManager::WIDTH) - 1.0f;
	float y = 1.0f - ((2.0f * mouseY) / (float)DisplayManager::HEIGHT);

	return glm::vec2(x, y);
}

glm::vec4 MousePicker::ToEyeCoords(glm::vec4 clipCoords) {
	glm::mat4 invertedProjection = glm::inverse(projectionMatrix);
	glm::vec4 eyeCoords = invertedProjection * clipCoords;

	return glm::vec4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);
}

glm::vec3 MousePicker::ToWorldCoords(glm::vec4 eyeCoords) {
	glm::mat4 invertedView = glm::inverse(viewMatrix);
	glm::vec4 rayWorld = invertedView * eyeCoords;
	glm::vec3 mouseRay(rayWorld.x, rayWorld.y, rayWorld.z);

	mouseRay = glm::normalize(mouseRay);
	return mouseRay;
}

glm::vec3 MousePicker::GetPointOnRay(glm::vec3 ray, float distance) {
	glm::vec3 camPos = camera->GetPosition();
	glm::vec3 start(camPos.x, camPos.y, camPos.z);
	glm::vec3 scaledRay(ray.x * distance, ray.y * distance, ray.z * distance);

	return start + scaledRay;
}

glm::vec3 MousePicker::BinarySearch(int count, float start, float finish, glm::vec3 ray) {
	float half = start + ((finish - start) / 2.0f);
	if (count >= MousePicker::RECURSION_COUNT) {
		glm::vec3 endPoint = GetPointOnRay(ray, half);

		return endPoint;
	}
	if (IntersectionInRange(start, half, ray)) {
		return BinarySearch(count + 1, start, half, ray);
	}
	else {
		return BinarySearch(count + 1, half, finish, ray);
	}
}

bool MousePicker::IntersectionInRange(float start, float finish, glm::vec3 ray) {
	glm::vec3 startPoint = GetPointOnRay(ray, start);
	glm::vec3 endPoint = GetPointOnRay(ray, finish);

	if (!IsUnderGround(startPoint) && IsUnderGround(endPoint)) {
		return true;
	}
	else {
		return false;
	}
}

bool MousePicker::IsUnderGround(glm::vec3 testPoint) {
	float height = Terrain::GetInstance()->GetHeightOfTerrain(testPoint.x, testPoint.z);

	if (testPoint.y < height) {
		return true;
	}
	else {
		return false;
	}
}
