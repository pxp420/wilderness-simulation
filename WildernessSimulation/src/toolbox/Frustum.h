#pragma once

#include "../toolbox/Maths.h"
#include "../renderEngine/DisplayManager.h"

class Frustum {
private:
	const static float OFFSET;
	const static glm::vec4 UP;
	const static glm::vec4 FORWARD;

	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;

	glm::mat4* viewMatrix;
	Camera* cam;
	std::vector<glm::vec3> points;

	float farHeight, farWidth, nearHeight, nearWidth;
	float nearPlane, farPlane;
public:
	/**
	 * Creates a new shadow box and calculates some initial values relating
	 * to the camera's view frustum, namely the width and height of the near 
	 * plane and (possibly adjusted) far plane.
	 *
	 * @param viewMatrix
	 *		- The view matrix of the light. Can be used to transform a point
	 *		from world space into "Light" space (i.e. changes a point's
	 *      coordinates from being in relation to the world's axis to being
	 *		in terms of the light's local axis).
	 *
	 * @param camera
	 *		- The in simulation camera.
	*/
	Frustum(glm::mat4* viewMatrix, Camera* camera, float nearPlane, float farPlane);

	/**
	 * Updates the bounds of the shadow box based on the light direction and the
	 * camera's view frustum, to make sure that the box covers the smallest area
	 * possible while still ensuring that everything inside the camera's view 
	 * (within a certain range) will cast shadows.
	*/
	void Update();

	/**
	 * Calculates the center of the "view cuboid" in light space first, and then
	 * converts this to world space using the inverse light's view matrix.
	 *
	 * @return The center of the "view cuboid" in world space.
	*/
	glm::vec3 GetCenter();

	/**
	 * Gets the width of the "view cuboid" (ortographic projection area).
	 *
	 * @return The width of the "view cuboid".
	*/
	float GetWidth();

	/**
	* Gets the height of the "view cuboid" (ortographic projection area).
	*
	* @return The height of the "view cuboid".
	*/
	float GetHeight();

	/**
	* Gets the length of the "view cuboid" (ortographic projection area).
	*
	* @return The length of the "view cuboid".
	*/
	float GetLength();

	/**
	 * Get the points that create the frustum.
	 *
	 * @return The frustum points.
	*/
	std::vector<glm::vec3> GetPoints();

	void GetFrustumCorners(std::vector<glm::vec3>& corners, glm::mat4 projection);

	/**
	 * Calculates the position of the vertex at each corner of the view frustum
	 * in light space (8 vertices in total, so this returns 8 positions).
	 *
	 * @param rotation
	 *		- Camera's rotation.

	 * @param forwardVector
	 *		- The direction that the camera is aiming, and thus the direction
	 *		of the frustum.
	 *
	 * @param centerNear
	 *		- The center point of the frustum's near plane.
	 *
	 * @param centerFar
	 *		- The center point of the frustum's (possibly adjusted) far plane.
	 *
	 * @return The position of the vertices of the frustum in light space.
	*/
	std::vector<glm::vec3> CalculateFrustumVertices(glm::mat4 rotation, glm::vec3 forwardVector,
		glm::vec3 centerNear, glm::vec3 centerFar);

	/**
	 * Calculates one of the corner vertices of the view frustum in world space
	 * and converts it to light space.
	 *
	 * @param startPoint
	 *		- The starting center point on the view frustum.
	 *
	 * @param direction
	 *		- The direction of the corner from the start point.
	 *
	 * @param width
	 *		- The distance of the corner from the start point.
	 *
	 * @return The relevant corner vertex of the view frustum in light space.
	*/
	glm::vec4 CalculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction,
		float width);

	/**
	 * The rotation of the camera represented as a matrix.
	 *
	 * @return The rotation of the camera.
	*/
	glm::mat4 CalculateCameraRotationMatrix();

	/**
	 * Calculates the width and height of the near and far planes of the camera's
	 * view frustum. However, this doesn't have to use the "actual" far plane of
	 * the view frustum.
	 *
	 * It can use a shortened view frustum if desired by bringing the far-plane
	 * closer, which would increase shadow resolution but means that distant objects 
	 * wouldn't cast shadows.
	*/
	void CalculateWidthsAndHeights();
	
	/**
	 * Gets the aspect ratio of the display.
	 *
	 * @return The aspect ratio of the display (width:height ratio)
	*/
	float GetAspectRatio();

	/**
	 * Check to see if the point is inside the frustum or not.
	 *
	 * @param point
	 *		- The point to be checked.
	*/
	bool PointInFrustum(glm::vec3& point);
};
