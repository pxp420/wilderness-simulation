#include "RandomGenerator.h"

uint64_t RandomGenerator::timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
std::seed_seq RandomGenerator::ss{ uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32) };
std::mt19937_64 RandomGenerator::range(ss);

float RandomGenerator::GetNextFloat(float min, float max) {
	std::uniform_real_distribution<float> uniform(min, max);
	return uniform(RandomGenerator::range);
}

int RandomGenerator::GetNextInt(int min, int max) {
	std::uniform_int_distribution<int> uniform(min, max);
	return uniform(RandomGenerator::range);
}
