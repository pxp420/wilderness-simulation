#include "ColourInterpolation.h"

glm::vec3 ColourInterpolation::BLUE = glm::vec3(51.0f, 51.0f, 255.0f);
glm::vec3 ColourInterpolation::LIGHT_BLUE = glm::vec3(51.0f, 153.0f, 255.0f);
glm::vec3 ColourInterpolation::GREEN = glm::vec3(178.0f, 255.0f, 102.0f);
glm::vec3 ColourInterpolation::DARK_GREEN = glm::vec3(0.0f, 102.0f, 0.0f);
glm::vec3 ColourInterpolation::DARK_YELLOW = glm::vec3(204.0f, 204.0f, 0.0f);
glm::vec3 ColourInterpolation::BROWN = glm::vec3(153.0f, 76.0f, 0.0f);

glm::vec3 ColourInterpolation::Interpolate(glm::vec3 colour1, glm::vec3 colour2, float factor) {
	glm::vec3 interpolatedColour(0.0f);

	interpolatedColour.r = glm::round(colour1.r + factor * (colour2.r - colour1.r));
	interpolatedColour.g = glm::round(colour1.g + factor * (colour2.g - colour1.g));
	interpolatedColour.b = glm::round(colour1.b + factor * (colour2.b - colour1.b));

	return interpolatedColour;
}

float ColourInterpolation::TransformToPixelValue(float height) {
	height = height / (Terrain::MAX_HEIGHT / Terrain::MAX_PIXEL_COLOUR) / 2.0f;
	height = height + Terrain::MAX_PIXEL_COLOUR / 2.0f;

	return height;
}

void ColourInterpolation::InitialiseIntervals(std::vector<glm::vec2>& heightIntervals, float min, float max) {
	float diff = max - min;

	glm::vec2 waterInterval(min + 0.0f, min + diff * 0.2f);
	glm::vec2 grassInterval(min + diff * 0.2f, min + diff * 0.7f);
	glm::vec2 mudInterval(min + diff * 0.7f, min + diff * 0.8f);
	glm::vec2 rockInterval(min + diff * 0.8f, min + diff);

	heightIntervals.emplace_back(waterInterval);
	heightIntervals.emplace_back(grassInterval);
	heightIntervals.emplace_back(mudInterval);
	heightIntervals.emplace_back(rockInterval);
}

bool ColourInterpolation::IsHeightInInterval(float height, glm::vec2 interval) {
	return height <= interval.y && height >= interval.x;
}

void ColourInterpolation::InterpolateMap(const std::vector<std::vector<float>>& heights) {
	float minHeight = TransformToPixelValue(GetMinHeight(heights));
	float maxHeight = TransformToPixelValue(GetMaxHeight(heights));

	std::vector<glm::vec2> heightIntervals;
	InitialiseIntervals(heightIntervals, minHeight, maxHeight);

	std::vector<std::vector<float>> pixelHeights(heights.size(), std::vector<float>(heights.size()));
	for (unsigned int i = 0; i < heights.size(); i++) {
		for (unsigned int j = 0; j < heights[i].size(); j++) {
			pixelHeights[i][j] = TransformToPixelValue(heights[i][j]);
		}
	}

	std::vector<std::vector<glm::vec3>> interpolatedHeights(pixelHeights.size(), std::vector<glm::vec3>(pixelHeights.size()));
	for (unsigned int i = 0; i < pixelHeights.size(); i++) {
		for (unsigned int j = 0; j < pixelHeights[i].size(); j++) {
			if (IsHeightInInterval(pixelHeights[i][j], heightIntervals[WATER])) {
				glm::vec2 interval = heightIntervals[WATER];
				float factor = (pixelHeights[i][j] - interval.x) / (interval.y - interval.x);
				interpolatedHeights[i][j] = Interpolate(LIGHT_BLUE, DARK_GREEN, factor);
			}
			else if (IsHeightInInterval(pixelHeights[i][j], heightIntervals[GRASS])) {
				glm::vec2 interval = heightIntervals[GRASS];
				float factor = (pixelHeights[i][j] - interval.x) / (interval.y - interval.x);
				interpolatedHeights[i][j] = Interpolate(DARK_GREEN, GREEN, factor);
			}
			else if (IsHeightInInterval(pixelHeights[i][j], heightIntervals[MUD])) {
				glm::vec2 interval = heightIntervals[MUD];
				float factor = (pixelHeights[i][j] - interval.x) / (interval.y - interval.x);
				interpolatedHeights[i][j] = Interpolate(GREEN, DARK_YELLOW, factor);
			}
			else if (IsHeightInInterval(pixelHeights[i][j], heightIntervals[ROCK])) {
				glm::vec2 interval = heightIntervals[ROCK];
				float factor = (pixelHeights[i][j] - interval.x) / (interval.y - interval.x);
				interpolatedHeights[i][j] = Interpolate(DARK_YELLOW, BROWN, factor);
			}
		}
	}

	SaveImage(interpolatedHeights);
}

void ColourInterpolation::SaveImage(const std::vector<std::vector<glm::vec3>>& heights) {
	std::ofstream file("res/textures/blendHeightMap.ppm", std::ios::binary);
	std::string width = std::to_string(heights.size()) + " ";
	std::string height = std::to_string(heights[0].size()) + " ";

	file.write("P6 ", 3);
	file.write(width.c_str(), width.size());
	file.write(height.c_str(), height.size());
	file.write("255 ", 4);

	for (unsigned int i = 0; i < heights.size(); i++) {
		for (unsigned int j = 0; j < heights[0].size(); j++) {
			glm::vec3 pixel = heights[j][i];

			Pixel s;
			s.r = (char)pixel.r;
			s.g = (char)pixel.g;
			s.b = (char)pixel.b;

			file.write(reinterpret_cast<char*>(&s), sizeof(Pixel));
		}
	}

	file.close();
}

float ColourInterpolation::GetMinHeight(const std::vector<std::vector<float>>& heights) {
	float minHeight = heights[0][0];
	for (unsigned int i = 0; i < heights.size(); i++) {
		for (unsigned int j = 0; j < heights[i].size(); j++) {
			if (heights[i][j] < minHeight) {
				minHeight = heights[i][j];
			}
		}
	}

	return minHeight;
}

float ColourInterpolation::GetMaxHeight(const std::vector<std::vector<float>>& heights) {
	float maxHeight = heights[0][0];
	for (unsigned int i = 0; i < heights.size(); i++) {
		for (unsigned int j = 0; j < heights[i].size(); j++) {
			if (heights[i][j] > maxHeight) {
				maxHeight = heights[i][j];
			}
		}
	}

	return maxHeight;
}
