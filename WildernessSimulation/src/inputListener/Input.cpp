#include "Input.h"

bool Input::FULL_SCREEN = false;

Input::Input(VegetationGenerator generator, Camera* camera) : generator(generator), camera(camera) { }

void Input::Listen() {
	int state = glfwGetKey(DisplayManager::window, GLFW_KEY_ESCAPE);
	if (glfwGetKey(DisplayManager::window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		exit(0);
	}
	else if (glfwGetKey(DisplayManager::window, GLFW_KEY_F) == GLFW_PRESS) {
		if (!FULL_SCREEN) {
			glfwSetWindowMonitor(DisplayManager::window, glfwGetPrimaryMonitor(), 0, 0, DisplayManager::WIDTH, DisplayManager::HEIGHT, 60);
			FULL_SCREEN = true;
		}
		else {
			glfwSetWindowMonitor(DisplayManager::window, NULL, 200, 200, DisplayManager::WIDTH, DisplayManager::HEIGHT, 60);
			FULL_SCREEN = false;
		}
	}
}
