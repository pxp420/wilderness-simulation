#pragma once

#include "../toolbox/MousePicker.h"
#include "../pathFinding/AStarGenerator.h"
#include "../entities/Light.h"
#include "../generators/VegetationGenerator.h"
#include "../renderEngine/MasterRenderer.h"
#include "../entities/Human.h"

class Input {
private:
	VegetationGenerator generator;
	Camera* camera;

	static bool FULL_SCREEN;
public:
	Input(VegetationGenerator generator, Camera* camera);

	void Listen();
};
