#pragma once

#include "HorizontalBlurShader.h"
#include "../postProcessing/ImageRenderer.h"

class HorizontalBlur {
private:
	ImageRenderer renderer;
	HorizontalBlurShader shader;

public:
	HorizontalBlur(unsigned int targetFboWidth, unsigned int targetFboHeight);

	void Render(unsigned int texture);

	unsigned int GetOutputTexture();

	void CleanUp();
};
