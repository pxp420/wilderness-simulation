#include "HorizontalBlurShader.h"

const std::string HorizontalBlurShader::VERTEX_FILE = "src/gaussianBlur/horizontalBlurVertexShader.shader";
const std::string HorizontalBlurShader::FRAGMENT_FILE = "src/gaussianBlur/blurFragmentShader.shader";

const std::map<unsigned int, std::string> HorizontalBlurShader::ATTRIBUTES = {
	{ 0, "position" }
};

HorizontalBlurShader::HorizontalBlurShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void HorizontalBlurShader::LoadTargetWidth(float width) {
	ShaderProgram::LoadFloat(location_targetWidth, width);
}

void HorizontalBlurShader::GetAllUniformLocations() {
	location_targetWidth = ShaderProgram::GetUniformLocation("targetWidth");
}
