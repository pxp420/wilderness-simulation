#version 330

in vec2 position;

out vec2 blurTextureCoords[11];

uniform float targetWidth;

void main(void) {
	gl_Position = vec4(position, 0.0f, 1.0f);
	vec2 centerTexCoords = position * 0.5f + 0.5f;

	float pixelSize = 1.0f / targetWidth;
	for (int i = -5; i <= 5; i++) {
		blurTextureCoords[i + 5] = centerTexCoords + vec2(pixelSize * i, 0.0f);
	}
}
