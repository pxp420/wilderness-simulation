#pragma once

#include "VerticalBlurShader.h"
#include "../postProcessing/ImageRenderer.h"

class VerticalBlur {
private:
	ImageRenderer renderer;
	VerticalBlurShader shader;

public:
	VerticalBlur(unsigned int targetFboWidth, unsigned int targetFboHeight);

	void Render(unsigned int texture);

	unsigned int GetOutputTexture();

	void CleanUp();
};
