#include "VerticalBlur.h"

VerticalBlur::VerticalBlur(unsigned int targetFboWidth, unsigned int targetFboHeight) :
	renderer(ImageRenderer(targetFboWidth, targetFboHeight)) {
	shader.Start();
	shader.LoadTargetHeight((float)targetFboHeight);
	shader.Stop();
}

void VerticalBlur::Render(unsigned int texture) {
	shader.Start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	renderer.RenderQuad(false);
	shader.Stop();
}

unsigned int VerticalBlur::GetOutputTexture() {
	return renderer.GetOutputTexture();
}

void VerticalBlur::CleanUp() {
	renderer.CleanUp();
	shader.CleanUp();
}
