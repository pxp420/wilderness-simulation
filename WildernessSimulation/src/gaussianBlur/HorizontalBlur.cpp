#include "HorizontalBlur.h"

HorizontalBlur::HorizontalBlur(unsigned int targetFboWidth, unsigned int targetFboHeight) :
	renderer(ImageRenderer(targetFboWidth, targetFboHeight)) {
	shader.Start();
	shader.LoadTargetWidth((float)targetFboWidth);
	shader.Stop();
}

void HorizontalBlur::Render(unsigned int texture) {
	shader.Start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	renderer.RenderQuad(false);
	shader.Stop();
}

unsigned int HorizontalBlur::GetOutputTexture() {
	return renderer.GetOutputTexture();
}

void HorizontalBlur::CleanUp() {
	renderer.CleanUp();
	shader.CleanUp();
}
