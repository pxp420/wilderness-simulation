#include "VerticalBlurShader.h"

const std::string VerticalBlurShader::VERTEX_FILE = "src/gaussianBlur/verticalBlurVertexShader.shader";
const std::string VerticalBlurShader::FRAGMENT_FILE = "src/gaussianBlur/blurFragmentShader.shader";

const std::map<unsigned int, std::string> VerticalBlurShader::ATTRIBUTES = {
	{ 0, "position" }
};

VerticalBlurShader::VerticalBlurShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void VerticalBlurShader::LoadTargetHeight(float height) {
	ShaderProgram::LoadFloat(location_targetHeight, height);
}

void VerticalBlurShader::GetAllUniformLocations() {
	location_targetHeight = ShaderProgram::GetUniformLocation("targetHeight");
}
