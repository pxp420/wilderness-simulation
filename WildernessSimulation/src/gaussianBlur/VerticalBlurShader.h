#pragma once

#include "../shaders/ShaderProgram.h"

class VerticalBlurShader : public ShaderProgram {
private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_targetHeight;

public:
	VerticalBlurShader();

	/**
	 * Loads the height of the gaussian blur into a uniform.
	 *
	 * @param height
	 *		- The height of the gaussian blur.
	*/
	void LoadTargetHeight(float height);

protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
