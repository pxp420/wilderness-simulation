#pragma once

#include "../shaders/ShaderProgram.h"

class HorizontalBlurShader : public ShaderProgram {
private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_targetWidth;

public:
	HorizontalBlurShader();

	/**
	* Loads the width of the gaussian blur into a uniform.
	*
	* @param width
	*		- The width of the gaussian blur.
	*/
	void LoadTargetWidth(float width);

protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
