#pragma once

#include <string>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

class FontType;
class GUIText {
private:
	std::string textString;
	float fontSize;
	float offset;

	unsigned int textMeshVao;
	unsigned int vertexCount;
	glm::vec3 colour;

	glm::vec3 position;
	glm::vec2 translation;
	glm::vec2 scale;

	float lineMaxSize;
	unsigned int numberOfLines;

	FontType* font;
	bool centerText;

	static glm::vec3 WHITE;
	static glm::vec2 NO_TRANSLATION;

public:
	enum Dimensions { TWO_D, THREE_D } dimensions;

	/**
	 * Creates a new text, loads the text's quads into a VAO and adds the text
	 * to the screen.
	 *
	 * @param text
	 *		- the text to be loaded in.
	 *
	 * @param fontSize
	 *		- the font size of the text, where a font size of 1 is the default size.
	 *
	 * @param font
	 *		- the font that this text should use.
	 *
	 * @param position
	 *		- the position of the text rendered on the world space on the screen.
	 *
	 * @param translation
	 *		- the offset where the text will be placed onto.
	 *
	 * @param scale
	 *		- the scale of the quads.
	 * 
	 * @param maxLineLength
	 *		- the maximum line length where 1 represents the whole width of the screen.
	 *
	 * @param centered
	 *		- wheter the text should be centered or not.
	*/
	GUIText(std::string text, float fontSize, FontType* font, glm::vec3 position, 
		glm::vec2 translation, glm::vec2 scale, float maxLineLength, bool centered, Dimensions d);

	/**
	 * Creates a new text, loads the text's quads into a VAO and adds the text
	 * to the screen.
	 *
	 * @param text
	 *		- the text to be loaded in.
	 *
	 * @param font
	 *		- the font that this text should use.
	 *
	 * @param position
	 *		- the position of the text rendered on the world space on the screen.
	 *
	 * @param scale
	 *		- the scale of the quads.
	*/
	GUIText(std::string text, FontType* font, glm::vec3 position, glm::vec2 scale, float offset, Dimensions d);

	/**
	* Creates a new text, loads the text's quads into a VAO and adds the text
	* to the screen.
	*
	* @param text
	*		- the text to be loaded in.
	*
	* @param font
	*		- the font that this text should use.
	*
	* @param position
	*		- the position of the text rendered on the world space on the screen.
	*/
	GUIText(std::string text, FontType* font, glm::vec3 position, float offset, Dimensions d);

	/**
	* Creates a new text, loads the text's quads into a VAO and adds the text
	* to the screen.
	*
	* @param text
	*		- the text to be loaded in.
	*
	* @param font
	*		- the font that this text should use.
	*
	* @param position
	*		- the position of the text rendered on the world space on the screen.
	*/
	GUIText(std::string text, FontType* font, glm::vec2 translation, Dimensions d);

	/**
	 * Remove the text from the screen.
	*/
	void Remove();

	/**
	 * Get the font used by this text.
	 *
	 * @return The font used by this text.
	*/
	FontType* GetFont();

	Dimensions GetDimensions();

	/**
	 * Set the colour of the text.
	 *
	 * @param r
	 *		- The red channel value.
	 *
	 * @param g
	 *		- The green channel value.
	 *
	 * @param b
	 *		- The blue channel value.
	*/
	void SetColour(float r, float g, float b);

	/**
	 * Get the colour of the text.
	*/
	glm::vec3 GetColour();

	/**
	 * The number of lines of text.
	*/
	unsigned int GetNumberOfLines();

	/**
	 * Get the position of the text in world space.
	*/
	glm::vec3 GetPosition();

	/**
	 * Sets the position of the text in world space.
	 *
	 * @param position
	 *		- The new position of this text.
	*/
	void SetPosition(glm::vec3 position);

	/**
	 * Get the offset of the text.
	 *
	 * @return The offset of this text.
	*/
	glm::vec2 GetTranslation();

	/**
	 * Get the scale of the text.
	 *
	 * @return The scale of the text.
	*/
	glm::vec2 GetScale();

	/**
	 * Get the ID of the text's VAO, which contains all the vertex data for
	 * the quads on which the text will be rendered.
	 *
	 * @return The ID of the VAO.
	*/
	unsigned int GetMesh();

	/**
	 * Set up the VAO and vertex count for this text.
	 *
	 * @param vao
	 *		- the VAO containing all the vertex data for the quads on which the
	 *		text will be rendered.
	 *
	 * @param vertices
	 *		- the total number of vertices in all of the quads.
	*/
	void SetMeshInfo(unsigned int vao, unsigned int verticesCount);

	/**
	 * Get the total number of vertices of all the text's quads.
	 *
	 * @return the number of vertices.
	*/
	unsigned int GetVertexCount();

	/**
	 * Get the font size of the text.
	 *
	 * @return The font size of the text.
	*/
	float GetFontSize();

	/**
	 * Get the offset where the GUI will be positioned.
	 *
	 * @return The offset for the GUI.
	*/
	float GetOffset();

	/**
	 * Set the font size of the text.
	 *
	 * @param fontSize
	 *		- The font size of the text.
	*/
	void SetFontSize(float fontSize);

	/**
	 * Set the number of lines that this texts covers. Used only in
	 * loading.
	 *
	 * @param number
	 *		- The number of lines to be set.
	*/
	void SetNumberOfLines(unsigned int number);

	/**
	 * Checks if the text is centered or not.
	 *
	 * @return Wheter the text is centered or not.
	*/
	bool IsCentered();

	/**
	 * Get the maximum length of a line of this text.
	 *
	 * @return The maximum length of a line.
	*/
	float GetMaxLineSize();

	/**
	 * The text represented as a string.
	 *
	 * @return The text represented by a string.
	*/
	std::string GetTextString() const;

	/**
	 * Set the text represented by a string.
	 *
	 * @param textString
	 *		- The new text.
	*/
	void SetTextString(const std::string& textString);
};
