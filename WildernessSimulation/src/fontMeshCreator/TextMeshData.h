#pragma once

#include <vector>

class TextMeshData {
private:
	std::vector<float> vertexPositions;
	std::vector<float> textureCoords;
public:
	/**
	 * Data structure that contains the vertex positions and the texture coordinates of
	 * a given text to be rendered on the screen.
	 *
	 * @param vertexPositions
	 *		- The vertex positions of the text.
	 *
	 * @param textureCoords
	 *		- The texture coordinates for the text.
	*/
	TextMeshData(std::vector<float>& vertexPositions, std::vector<float>& textureCoords);

	/**
	 * Get the vertex positions of the text.
	 *
	 * @return The vertex positions of the text.
	*/
	std::vector<float> GetVertexPositions();

	/**
	 * Get the texture coordinates of the text.
	 *
	 * @return The texture coordinates of the text.
	*/
	std::vector<float> GetTextureCoords();

	/**
	 * Get the number of vertices that form the text.
	 *
	 * @return The number of vertices.
	*/
	unsigned int GetVertexCount();
};
