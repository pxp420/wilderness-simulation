#include "GUIText.h"
#include "../fontRenderer/TextMaster.h"

glm::vec3 GUIText::WHITE = glm::vec3(1.0f, 1.0f, 1.0f);
glm::vec2 GUIText::NO_TRANSLATION = glm::vec2(0.0f, 0.0f);

GUIText::GUIText(std::string text, float fontSize, FontType* font, glm::vec3 position, 
	glm::vec2 translation, glm::vec2 scale, float maxLineLength, bool centered, Dimensions d) : colour(WHITE), 
	textString(text), fontSize(fontSize), font(font), scale(scale), dimensions(d),
	position(position), translation(translation), lineMaxSize(maxLineLength), centerText(centered) {
	TextMaster::LoadText(this);
}

GUIText::GUIText(std::string text, FontType* font, glm::vec3 position, glm::vec2 scale, float offset, Dimensions d) :
	colour(WHITE), textString(text), fontSize(8.0f), font(font), position(position), dimensions(d),
	translation(NO_TRANSLATION), scale(scale), lineMaxSize(1.0f), centerText(true), offset(offset) {
	TextMaster::LoadText(this);
}

GUIText::GUIText(std::string text, FontType* font, glm::vec3 position, float offset, Dimensions d) :
	colour(WHITE), textString(text), fontSize(8.0f), font(font), position(position), dimensions(d),
	translation(NO_TRANSLATION), scale(glm::vec2(25.0f, 25.0f)), lineMaxSize(1.0f), centerText(true), offset(offset) {
	TextMaster::LoadText(this);
}

GUIText::GUIText(std::string text, FontType* font, glm::vec2 translation, Dimensions d) :
	colour(WHITE), textString(text), fontSize(0.6f), font(font), position(glm::vec3(0.0f)), dimensions(d),
	translation(translation), scale(glm::vec2(0.0f)), lineMaxSize(1.0f), centerText(true), offset(0.0f) {
	TextMaster::LoadText(this);
}

void GUIText::Remove() {
	TextMaster::RemoveText(this);
}

FontType* GUIText::GetFont() {
	return font;
}

GUIText::Dimensions GUIText::GetDimensions() {
	return dimensions;
}

void GUIText::SetColour(float r, float g, float b) {
	this->colour = glm::vec3(r, g, b);
}

glm::vec3 GUIText::GetColour() {
	return colour;
}

unsigned int GUIText::GetNumberOfLines() {
	return numberOfLines;
}

glm::vec3 GUIText::GetPosition() {
	return position;
}

void GUIText::SetPosition(glm::vec3 position) {
	this->position = position;
}

glm::vec2 GUIText::GetTranslation() {
	return translation;
}

glm::vec2 GUIText::GetScale() {
	return scale;
}

unsigned int GUIText::GetMesh() {
	return textMeshVao;
}

void GUIText::SetMeshInfo(unsigned int vao, unsigned int verticesCount) {
	this->textMeshVao = vao;
	this->vertexCount = verticesCount;
}

unsigned int GUIText::GetVertexCount() {
	return vertexCount;
}

float GUIText::GetFontSize() {
	return fontSize;
}

float GUIText::GetOffset() {
	return offset;
}

void GUIText::SetFontSize(float fontSize) {
	this->fontSize = fontSize;
}

void GUIText::SetNumberOfLines(unsigned int number) {
	this->numberOfLines = number;
}

bool GUIText::IsCentered() {
	return centerText;
}

float GUIText::GetMaxLineSize() {
	return lineMaxSize;
}

std::string GUIText::GetTextString() const {
	return textString;
}

void GUIText::SetTextString(const std::string& textString) {
	this->textString = textString;
}
