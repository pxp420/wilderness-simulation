#include "TextMeshCreator.h"

const float TextMeshCreator::LINE_HEIGHT = 0.03f;
const unsigned int TextMeshCreator::SPACE_ASCII = 32;

TextMeshCreator::TextMeshCreator(const std::string& metaFileName) :
	metaData(MetaFile(metaFileName)) { }

TextMeshData TextMeshCreator::CreateTextMesh(GUIText& text) {
	std::vector<Line> lines = CreateStructure(text);
	TextMeshData data = CreateQuadVertices(text, lines);
	return data;
}

std::vector<Line> TextMeshCreator::CreateStructure(GUIText& text) {
	std::string textString = text.GetTextString();
	std::vector<char> chars(textString.begin(), textString.end());
	std::vector<Line> lines;
	
	Line currentLine(metaData.GetSpaceWidth(), text.GetFontSize(), text.GetMaxLineSize());
	Word currentWord(text.GetFontSize());

	for (char c : chars) {
		unsigned int ascii = (unsigned int)c;
		if (ascii == SPACE_ASCII) {
			bool added = currentLine.AttemptToAddWord(currentWord);
			if (!added) {
				lines.emplace_back(currentLine);
				currentLine = Line(metaData.GetSpaceWidth(), text.GetFontSize(), text.GetMaxLineSize());
				currentLine.AttemptToAddWord(currentWord);
			}
			currentWord = Word(text.GetFontSize());
			continue;
		}
		Character* character = metaData.GetCharacter(ascii);
		currentWord.AddCharacter(*character);
	}
	CompleteStructure(lines, currentLine, currentWord, text);
	
	return lines;
}

void TextMeshCreator::CompleteStructure(std::vector<Line>& lines, Line& currentLine, Word& currentWord,
	GUIText& text) {
	bool added = currentLine.AttemptToAddWord(currentWord);
	if (!added) {
		lines.emplace_back(currentLine);
		currentLine = Line(metaData.GetSpaceWidth(), text.GetFontSize(), text.GetMaxLineSize());
		currentLine.AttemptToAddWord(currentWord);
	}
	lines.emplace_back(currentLine);
}

TextMeshData TextMeshCreator::CreateQuadVertices(GUIText& text, std::vector<Line>& lines) {
	text.SetNumberOfLines(lines.size());
	float curserX = 0.0f;
	float curserY = 0.0f;

	std::vector<float> vertices;
	std::vector<float> textureCoords;
	for (Line& line : lines) {
		if (text.IsCentered()) {
			curserX = (line.GetMaxLength() - line.GetLineLength()) / 2;
		}
		for (Word& word : line.GetWords()) {
			for (Character& letter : word.GetCharacters()) {
				AddVerticesForCharacter(curserX, curserY, letter, text.GetFontSize(), vertices);
				AddTexCoords(textureCoords, letter.GetXTextureCoord(), letter.GetYTextureCoord(),
					letter.GetXMaxTextureCoord(), letter.GetYMaxTextureCoord());
				curserX += letter.GetXAdvance() * text.GetFontSize();
			}
			curserX += metaData.GetSpaceWidth() * text.GetFontSize();
		}
		curserX = 0.0f;
		curserY += LINE_HEIGHT * text.GetFontSize();
	}

	return TextMeshData(vertices, textureCoords);
}

void TextMeshCreator::AddVerticesForCharacter(float curserX, float curserY, Character& character, float fontSize,
	std::vector<float>& vertices) {
	float x = curserX + (character.GetXOffset() * fontSize);
	float y = curserY + (character.GetYOffset() * fontSize);
	float maxX = x + (character.GetSizeX() * fontSize);
	float maxY = y + (character.GetSizeY() * fontSize);
	float properX = (2 * x) - 1;
	float properY = (-2 * y) + 1;
	float properMaxX = (2 * maxX) - 1;
	float properMaxY = (-2 * maxY) + 1;
	AddVertices(vertices, properX, properY, properMaxX, properMaxY);
}

void TextMeshCreator::AddVertices(std::vector<float>& vertices, float x, float y, float maxX,
	float maxY) {
	vertices.emplace_back((float)x);
	vertices.emplace_back((float)y);
	vertices.emplace_back((float)x);
	vertices.emplace_back((float)maxY);
	vertices.emplace_back((float)maxX);
	vertices.emplace_back((float)maxY);
	vertices.emplace_back((float)maxX);
	vertices.emplace_back((float)maxY);
	vertices.emplace_back((float)maxX);
	vertices.emplace_back((float)y);
	vertices.emplace_back((float)x);
	vertices.emplace_back((float)y);
}

void TextMeshCreator::AddTexCoords(std::vector<float>& texCoords, float x, float y, float maxX,
	float maxY) {
	texCoords.emplace_back((float)x);
	texCoords.emplace_back((float)y);
	texCoords.emplace_back((float)x);
	texCoords.emplace_back((float)maxY);
	texCoords.emplace_back((float)maxX);
	texCoords.emplace_back((float)maxY);
	texCoords.emplace_back((float)maxX);
	texCoords.emplace_back((float)maxY);
	texCoords.emplace_back((float)maxX);
	texCoords.emplace_back((float)y);
	texCoords.emplace_back((float)x);
	texCoords.emplace_back((float)y);
}
