#include "Word.h"

Word::Word(float fontSize) : characters(std::vector<Character>()), width(0.0f), fontSize(fontSize) {}

void Word::AddCharacter(Character& character) {
	characters.emplace_back(character);
	width += character.GetXAdvance() * fontSize;
}

std::vector<Character> Word::GetCharacters() {
	return characters;
}

float Word::GetWordWidth() {
	return width;
}
