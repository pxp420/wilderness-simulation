#pragma once

#include "Word.h"

class Line {
private:
	float maxLength;
	float spaceSize;

	std::vector<Word> words;
	float currentLineLength;
public:
	/**
	 * Creates an empty line.
	 *
	 * @param spaceWidth
	 *		- the screen-space width of a space character.
	 *
	 * @param fontSize
	 *		- the size of font being used.
	 *
	 * @param maxLength
	 *		- the screen-space maximum length of a line.
	*/
	Line(float spaceWidth, float fontSize, float maxLength);

	/**
	 * Attempt to add a word to the line. If the line can fit the word in
	 * without reaching the maximum line length then the word is added and the
	 * line length is increased.
	 *
	 * @param word
	 *		- the word to try to add.
	 *
	 * @return If the word has successfully been added to the line or not.
	*/
	bool AttemptToAddWord(Word& word);

	/**
	 * Get the max length of the line.
	 *
	 * @return The max length of the line.
	*/
	float GetMaxLength();

	/**
	 * Get the current screen-space length of the line.
	 *
	 * @return The length of the line.
	*/
	float GetLineLength();

	/**
	 * Get the list of words in the line.
	 *
	 * @return The list of words in the line.
	*/
	std::vector<Word> GetWords();
};
