#pragma once

#include "TextMeshCreator.h"

class FontType {
private:
	unsigned int textureAtlas;
	TextMeshCreator loader;

public:
	/**
	 * Creates a new font and loads up the data about each character from the font file.
	 *
	 * @param textureAtlas
	 *		- the ID of the font atlas texture.
	 *
	 * @param fontFileName
	 *		- the name of the font file name containing information about each character in
	 *		the texture atlas.
	*/
	FontType(unsigned int textureAtlas, const std::string& fontFileName);

	/**
	 * Get the texture atlas ID.
	 *
	 * @return The ID of the texture atlas.
	*/
	unsigned int GetTextureAtlas();

	/**
	 * Takes in an unloaded text and calculates all of the vertices for the quads
	 * on which this text will be rendered. The vertexx positions and texture coords
	 * are calculated based on the information from the font file.
	 *
	 * @param text
	 *		- The unloaded text.
	 *
	 * @return The information about the vertices of all the quads.
	*/
	TextMeshData LoadText(GUIText& text);
};
