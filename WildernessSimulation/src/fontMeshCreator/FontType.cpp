#include "FontType.h"

FontType::FontType(unsigned int textureAtlas, const std::string& fontFileName) :
	textureAtlas(textureAtlas), loader(TextMeshCreator(fontFileName)) { }

unsigned int FontType::GetTextureAtlas() {
	return textureAtlas;
}

TextMeshData FontType::LoadText(GUIText& text) {
	return loader.CreateTextMesh(text);
}