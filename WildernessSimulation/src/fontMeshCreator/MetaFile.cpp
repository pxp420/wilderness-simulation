#include "MetaFile.h"

const unsigned int MetaFile::PAD_TOP = 0;
const unsigned int MetaFile::PAD_LEFT = 1;
const unsigned int MetaFile::PAD_BOTTOM = 2;
const unsigned int MetaFile::PAD_RIGHT = 3;

const int MetaFile::DESIRED_PADDING = 3;

const char MetaFile::SPLITTER = ' ';
const char MetaFile::NUMBER_SEPARATOR = ',';

const float MetaFile::LINE_HEIGHT = 0.03f;
const unsigned int MetaFile::SPACE_ASCII = 32;

MetaFile::MetaFile(const std::string& fileName) : padding(std::vector<int>()),
	metaData(std::map<unsigned int, Character*>()), values(std::map<std::string, std::string>()),
	aspectRatio((float)(DisplayManager::WIDTH / DisplayManager::HEIGHT)), reader(std::ifstream(fileName)) { 
	LoadPaddingData();
	LoadLineSizes();

	unsigned int imageWidth = GetValueOfVariable("scaleW");
	LoadCharacterData(imageWidth);
	Close();
}

float MetaFile::GetSpaceWidth() {
	return spaceWidth;
}

Character* MetaFile::GetCharacter(unsigned int ascii) {
	auto iterator = metaData.find(ascii);
	if (iterator != metaData.end()) {
		return iterator->second;
	}

	return nullptr;
}

bool MetaFile::ProcessNextLine() {
	values.clear();
	std::string line;

	getline(reader, line);
	if (line.empty()) {
		return false;
	}

	for (std::string& part : StringManipulation::Split(line, SPLITTER)) {
		std::vector<std::string> valuePairs = StringManipulation::Split(part, '=');
		if (valuePairs.size() == 2) {
			values[valuePairs[0]] = valuePairs[1];
		}
	}

	return true;
}

int MetaFile::GetValueOfVariable(const std::string& variable) {
	auto iterator = values.find(variable);
	if (iterator != values.end()) {
		return stoi(iterator->second);
	}

	return 0;
}

std::vector<int> MetaFile::GetValuesOfVariable(const std::string& variable) {
	std::vector<std::string> numbers;
	auto iterator = values.find(variable);
	if (iterator != values.end()) {
		numbers = StringManipulation::Split(iterator->second, NUMBER_SEPARATOR);
	}

	std::vector<int> actualValues(numbers.size());
	for (unsigned int i = 0; i < actualValues.size(); i++) {
		actualValues[i] = stoi(numbers[i]);
	}

	return actualValues;
}

void MetaFile::Close() {
	reader.close();
}

void MetaFile::LoadPaddingData() {
	ProcessNextLine();
	this->padding = GetValuesOfVariable("padding");
	this->paddingWidth = padding[PAD_LEFT] + padding[PAD_RIGHT];
	this->paddingHeight = padding[PAD_TOP] + padding[PAD_BOTTOM];
}

void MetaFile::LoadLineSizes() {
	ProcessNextLine();
	unsigned int lineHeightPixels = GetValueOfVariable("lineHeight") - paddingHeight;
	verticalPerPixelSize = LINE_HEIGHT / (float)lineHeightPixels;
	horizontalPerPixelSize = verticalPerPixelSize / aspectRatio;
}

void MetaFile::LoadCharacterData(unsigned int imageWidth) {
	ProcessNextLine();
	ProcessNextLine();
	while (ProcessNextLine()) {
		Character* character = LoadCharacter(imageWidth);
		if (character != nullptr) {
			metaData[character->GetId()] = character;
		}
	}
}

Character* MetaFile::LoadCharacter(unsigned int imageSize) {
	unsigned int id = GetValueOfVariable("id");
	if (id == SPACE_ASCII) {
		this->spaceWidth = (GetValueOfVariable("xadvance") - paddingWidth) * horizontalPerPixelSize;
		return nullptr;
	}
	float xTex = ((float)GetValueOfVariable("x") + (padding[PAD_LEFT] - DESIRED_PADDING)) / imageSize;
	float yTex = ((float)GetValueOfVariable("y") + (padding[PAD_TOP] - DESIRED_PADDING)) / imageSize;

	unsigned int width = GetValueOfVariable("width") - (paddingWidth - (2 * DESIRED_PADDING));
	unsigned int height = GetValueOfVariable("height") - ((paddingHeight)-(2 * DESIRED_PADDING));

	float quadWidth = width * horizontalPerPixelSize;
	float quadHeight = height * verticalPerPixelSize;

	float xTexSize = (float)width / imageSize;
	float yTexSize = (float)height / imageSize;

	float xOff = ((int)GetValueOfVariable("xoffset") + padding[PAD_LEFT] - DESIRED_PADDING) * horizontalPerPixelSize;
	float yOff = (GetValueOfVariable("yoffset") + (padding[PAD_TOP] - DESIRED_PADDING)) * verticalPerPixelSize;
	float xAdvance = (GetValueOfVariable("xadvance") - paddingWidth) * horizontalPerPixelSize;

	return new Character(id, xTex, yTex, xTexSize, yTexSize, xOff, yOff, quadWidth, quadHeight, xAdvance);
}
