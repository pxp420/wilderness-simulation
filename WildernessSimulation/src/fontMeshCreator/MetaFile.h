#pragma once

#include "Character.h"
#include "../renderEngine/DisplayManager.h"
#include "../toolbox/StringManipulation.h"

#include <vector>
#include <string>
#include <map>

class MetaFile {
private:
	const static unsigned int PAD_TOP;
	const static unsigned int PAD_LEFT;
	const static unsigned int PAD_BOTTOM;
	const static unsigned int PAD_RIGHT;

	const static int DESIRED_PADDING;

	const static char SPLITTER;
	const static char NUMBER_SEPARATOR;

	float aspectRatio;

	float verticalPerPixelSize;
	float horizontalPerPixelSize;
	float spaceWidth;

	std::vector<int> padding;
	unsigned int paddingWidth;
	unsigned int paddingHeight;

	std::map<unsigned int, Character*> metaData;
	std::ifstream reader;
	std::map<std::string, std::string> values;

	const static float LINE_HEIGHT;
	const static unsigned int SPACE_ASCII;
public:
	/**
	 * Opens a font file in preparation for reading.
	 *
	 * @param fileName
	 *		- The name of the file to be read.
	*/
	MetaFile(const std::string& fileName);

	/**
	 * Get the space width between characters.
	 *
	 * @return The space width between characters.
	*/
	float GetSpaceWidth();

	/**
	 * Get the character represented by an ASCII value.
	 *
	 * @param ascii
	 *		- The ASCII value of the character.
	 *
	 * @return The character fro the ASCII value.
	*/
	Character* GetCharacter(unsigned int ascii);

	/**
	 * Read in the next line and store the variable values.
	 *
	 * @return If the end of the file has reached the end or not.
	*/
	bool ProcessNextLine();

	/**
	 * Gets the value of the variable with a certain name on the current 
	 * line.
	 *
	 * @param variable
	 *		- The name of the variable.
	 *
	 * @return The value of the variable.
	*/
	int GetValueOfVariable(const std::string& variable);

	/**
	 * Gets the vector of ints associated with a variable on the current line.
	 *
	 * @param variable
	 *		- The name of the variable.
	 *
	 * @return The vector of ints with values associated to the variable.
	*/
	std::vector<int> GetValuesOfVariable(const std::string& variable);

	/**
	 * Closes the font file after finishing reading.
	*/
	void Close();

	/**
	 * Loads the data about how much padding is used around each character in
	 * the texture atlas.
	*/
	void LoadPaddingData();

	/**
	 * Loads information about the line height for this font in pixels, and uses
	 * this as a way to find the conversion rate between pixels in the texture
	 * atlas and screen-space.
	*/
	void LoadLineSizes();

	/**
	 * Loads in data about each character and stores the data in a Character.
	 *
	 * @param imageWidth
	 *		- The width of the texture atlas in pixels.
	*/
	void LoadCharacterData(unsigned int imageWidth);

	/**
	 * Loads all the data about one character in the texture atlas and converts
	 * it all from pixels to screen-space before storing. The effects of 
	 * padding are also removed from the data.
	 *
	 * @param imageSize
	 *		- The size of the texture atlas in pixesl.
	 *
	 * @return The data about the character.
	*/
	Character* LoadCharacter(unsigned int imageSize);
};
