#include "TextMeshData.h"

TextMeshData::TextMeshData(std::vector<float>& vertexPositions, std::vector<float>& textureCoords) :
	vertexPositions(vertexPositions), textureCoords(textureCoords) {}

std::vector<float> TextMeshData::GetVertexPositions() {
	return vertexPositions;
}

std::vector<float> TextMeshData::GetTextureCoords() {
	return textureCoords;
}

unsigned int TextMeshData::GetVertexCount() {
	return vertexPositions.size() / 2;
}
