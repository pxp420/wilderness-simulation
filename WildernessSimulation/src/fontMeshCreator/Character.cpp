#include "Character.h"

Character::Character(unsigned int id, float xTextureCoord, float yTextureCoord, float xTexSize, float yTexSize,
	float xOffset, float yOffset, float sizeX, float sizeY, float xAdvance) : id(id), xTextureCoord(xTextureCoord),
	yTextureCoord(yTextureCoord), xOffset(xOffset), yOffset(yOffset), sizeX(sizeX), sizeY(sizeY),
	xMaxTextureCoord(xTexSize + xTextureCoord), yMaxTextureCoord(yTexSize + yTextureCoord), xAdvance(xAdvance) {}

unsigned int Character::GetId() {
	return id;
}

float Character::GetXTextureCoord() {
	return xTextureCoord;
}

float Character::GetYTextureCoord() {
	return yTextureCoord;
}

float Character::GetXMaxTextureCoord() {
	return xMaxTextureCoord;
}

float Character::GetYMaxTextureCoord() {
	return yMaxTextureCoord;
}

float Character::GetXOffset() {
	return xOffset;
}

float Character::GetYOffset() {
	return yOffset;
}

float Character::GetSizeX() {
	return sizeX;
}

float Character::GetSizeY() {
	return sizeY;
}

float Character::GetXAdvance() {
	return xAdvance;
}
