#pragma once

#include "MetaFile.h"
#include "Line.h"
#include "GUIText.h"
#include "TextMeshData.h"

class TextMeshCreator {
private:
	MetaFile metaData;

	/**
	 * Creates the internal structure for the text to be able to be rendered
	 * on the screen.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	 *
	 * @return The vector of lines containing the sentence to be rendered.
	*/
	std::vector<Line> CreateStructure(GUIText& text);

	/**
	 * Completes the structure and finalises the line adding.
	 *
	 * @param lines
	 *		- The vector of lines representing the sentence.
	 *
	 * @param currentLine
	 *		- The current line for the sentence.
	 *
	 * @param currentWord
	 *		- The current word of the sentence.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	*/
	void CompleteStructure(std::vector<Line>& lines, Line& currentLine,
		Word& currentWord, GUIText& text);

	/**
	 * Create the vertices and texture coordinates for each character individually.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	 *
	 * @param lines
	 *		- The vector of lines containing the text.
	 *
	 * @return The vertices and textures contained in a new data structure.
	*/
	TextMeshData CreateQuadVertices(GUIText& text, std::vector<Line>& lines);

	/**
	 * Adds the vertices for the current character.
	 *
	 * @param curserX
	 *		- The x position where the character starts (top-left).
	 *
	 * @param curserY
	 *		- The y position where the character starts (top-left).
	 *
	 * @param character
	 *		- The character for whose the vertices are created.
	 *
	 * @param fontSize
	 *		- The font size of the character,
	 *
	 * @param vertices
	 *		- The vertices vector where the newly created ones are added to.
	*/
	void AddVerticesForCharacter(float curserX, float curserY, Character& character,
		float fontSize, std::vector<float>& vertices);

	/**
	 * Add the vertices into the vector of vertices.
	 *
	 * @param vertices
	 *		- The vector of vertices.
	 *
	 * @param x
	 *		- The top left of the quad.
	 *
	 * @param y
	 *		- The top left of the quad.
	 *
	 * @param maxX
	 *		- The bottom right of the quad.
	 *
	 * @param maxY
	 *		- The bottom right of the quad.
	*/
	static void AddVertices(std::vector<float>& vertices, float x, float y, float maxX,
		float maxY);

	/**
	* Add the texture coordinates into the vector of texture coordinates.
	*
	* @param texCoords
	*		- The vector of texture coordinates.
	*
	* @param x
	*		- The top left of the quad.
	*
	* @param y
	*		- The top left of the quad.
	*
	* @param maxX
	*		- The bottom right of the quad.
	*
	* @param maxY
	*		- The bottom right of the quad.
	*/
	static void AddTexCoords(std::vector<float>& texCoords, float x, float y, float maxX,
		float maxY);
public:
	const static float LINE_HEIGHT;
	const static unsigned int SPACE_ASCII;

	/**
	 * Opens up the font file meta data and stores it's information into a variable.
	 *
	 * @param metaFileName
	 *		- The file name of the font file.
	*/
	TextMeshCreator(const std::string& metaFileName);

	/**
	 * Creates the vertices and texture coordinates from a given text.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	 *
	 * @return The vertices and texture coordinates that form the text.
	*/
	TextMeshData CreateTextMesh(GUIText& text);
};
