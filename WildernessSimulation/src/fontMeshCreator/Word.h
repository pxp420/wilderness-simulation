#pragma once
#include "Character.h"

#include <vector>

class Word {
private:
	std::vector<Character> characters;
	float width;
	float fontSize;
public:
	/**
	 * Creates a new empty word.
	 *
	 * @param fontSize
	 *		- The font size of the text which this word is in.
	*/
	Word(float fontSize);

	/**
	 * Adds a character to the end of the current word and increases the
	 * screen-space width of the word.
	 *
	 * @param character
	 *		- The character to be added.
	*/
	void AddCharacter(Character& character);

	/**
	 * Get all the characters that form the word.
	 *
	 * @return The list of charaters in the word.
	*/
	std::vector<Character> GetCharacters();

	/**
	 * Get the width of the word in terms of screen size.
	 *
	 * @return The width of the word.
	*/
	float GetWordWidth();
};
