#include "Line.h"

Line::Line(float spaceWidth, float fontSize, float maxLength) :
	spaceSize(spaceWidth * fontSize), maxLength(maxLength), currentLineLength(0) {}

bool Line::AttemptToAddWord(Word& word) {
	float additionalLength = word.GetWordWidth();
	additionalLength += !words.empty() ? spaceSize : 0;
	if (currentLineLength + additionalLength <= maxLength) {
		words.emplace_back(word);
		currentLineLength += additionalLength;
		return true;
	}
	else {
		return false;
	}
}

float Line::GetMaxLength() {
	return maxLength;
}

float Line::GetLineLength() {
	return currentLineLength;
}

std::vector<Word> Line::GetWords() {
	return words;
}
