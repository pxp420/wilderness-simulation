#include "BrightFilter.h"

BrightFilter::BrightFilter(unsigned int width, unsigned int height) :
	renderer(ImageRenderer(width, height)) {}

void BrightFilter::Render(unsigned int texture) {
	shader.Start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	renderer.RenderQuad(false);
	shader.Stop();
}

unsigned int BrightFilter::GetOutputTexture() {
	return renderer.GetOutputTexture();
}

void BrightFilter::CleanUp() {
	renderer.CleanUp();
	shader.CleanUp();
}
