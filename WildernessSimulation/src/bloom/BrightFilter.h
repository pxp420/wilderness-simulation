#pragma once

#include "BrightFilterShader.h"
#include "../postProcessing/ImageRenderer.h"

class BrightFilter {
private:
	ImageRenderer renderer;
	BrightFilterShader shader;

public:
	BrightFilter(unsigned int width, unsigned int height);

	void Render(unsigned int texture);

	unsigned int GetOutputTexture();

	void CleanUp();
};
