#pragma once

#include "../shaders/ShaderProgram.h"

class CombineShader : public ShaderProgram {
private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_colourTexture;
	unsigned int location_highlightTexture;

public:
	CombineShader();

	/**
	* Loads all the textures for the terrain in their corresponding
	* units.
	*/
	void ConnectTextureUnits();

protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
