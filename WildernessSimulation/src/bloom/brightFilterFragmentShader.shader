#version 330

in vec2 textureCoords;

out vec4 out_Colour;

uniform sampler2D colourTexture;

void main(void) {
	vec4 colour = texture(colourTexture, textureCoords);

	float brightness = (colour.r * 0.2126f) + (colour.g * 0.7152f) + (colour.b * 0.0722f);
	out_Colour = colour * brightness * 1.25f;
}
