#include "CombineShader.h"

const std::string CombineShader::VERTEX_FILE = "src/bloom/simpleVertexShader.shader";
const std::string CombineShader::FRAGMENT_FILE = "src/bloom/combineFragmentShader.shader";

const std::map<unsigned int, std::string> CombineShader::ATTRIBUTES = {
	{ 0, "position" }
};

CombineShader::CombineShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void CombineShader::ConnectTextureUnits() {
	ShaderProgram::LoadInt(location_colourTexture, 0);
	ShaderProgram::LoadInt(location_highlightTexture, 1);
}

void CombineShader::GetAllUniformLocations() {
	location_colourTexture = ShaderProgram::GetUniformLocation("colourTexture");
	location_highlightTexture = ShaderProgram::GetUniformLocation("highlightTexture");
}
