#pragma once

#include "CombineShader.h"
#include "../postProcessing/ImageRenderer.h"

class CombineFilter {
private:
	ImageRenderer renderer;
	CombineShader shader;

public:
	CombineFilter();

	void Render(unsigned int colourTexture, unsigned int highlightTexture);

	unsigned int GetOutputTexture();

	void CleanUp();
};
