#include "BrightFilterShader.h"

const std::string BrightFilterShader::VERTEX_FILE = "src/bloom/simpleVertexShader.shader";
const std::string BrightFilterShader::FRAGMENT_FILE = "src/bloom/brightFilterFragmentShader.shader";

const std::map<unsigned int, std::string> BrightFilterShader::ATTRIBUTES = {
	{ 0, "position" }
};

BrightFilterShader::BrightFilterShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void BrightFilterShader::GetAllUniformLocations() {
}
