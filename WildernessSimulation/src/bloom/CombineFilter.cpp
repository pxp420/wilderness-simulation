#include "CombineFilter.h"

CombineFilter::CombineFilter() :
	renderer(ImageRenderer()) {
	shader.Start();
	shader.ConnectTextureUnits();
	shader.Stop();
}

void CombineFilter::Render(unsigned int colourTexture, unsigned int highlightTexture) {
	shader.Start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, colourTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, highlightTexture);
	renderer.RenderQuad(true);
	shader.Stop();
}

unsigned int CombineFilter::GetOutputTexture() {
	return renderer.GetOutputTexture();
}

void CombineFilter::CleanUp() {
	renderer.CleanUp();
	shader.CleanUp();
}
