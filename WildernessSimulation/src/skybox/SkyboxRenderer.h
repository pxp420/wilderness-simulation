#pragma once

#include "SkyboxShader.h"
#include "../models/RawModel.h"

#include <vector>
#include <memory>

class SkyboxRenderer {
private:
	const static float SIZE;
	const static std::vector<float> VERTICES;

	static std::vector<std::string> TEXTURE_FILES;
	static std::vector<std::string> NIGHT_TEXTURE_FILES;

	std::unique_ptr<RawModel> cube;
	unsigned int texture;
	unsigned int nightTexture;
	SkyboxShader shader;

	float time;

	/**
	 * Bind the textures in their coresponding units.
	*/
	void BindTextures();
public:
	/**
	* The constructor for the renderer takes in the loader as the parameter
	* and loads the projection matrix to it.
	*
	* @param loader
	*		- The loader used for loading the matrix.
	*
	* @param projectionMatrix
	*		- The matrix that is used for perspective view.
	*/
	SkyboxRenderer(Loader& loader, glm::mat4 projectionMatrix);

	/**
	 * Renders the skybox on the screen.
	 *
	 * @param camera
	 *		- The camera needed for rotating the skybox.
	*/
	void Render(Camera& camera, float r, float g, float b);
};