#include "SkyboxShader.h"

const std::string SkyboxShader::VERTEX_FILE = "src/skybox/skyboxVertexShader.shader";
const std::string SkyboxShader::FRAGMENT_FILE = "src/skybox/skyboxFragmentShader.shader";

const std::map<unsigned int, std::string> SkyboxShader::ATTRIBUTES = {
	{ 0, "position" }
};

const float SkyboxShader::ROTATE_SPEED = 0.1f;

SkyboxShader::SkyboxShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES), rotation(0) {
	GetAllUniformLocations();
}

void SkyboxShader::LoadProjectionMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_projectionMatrix, matrix);
}

void SkyboxShader::LoadViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix = Maths::CreateViewMatrix(camera);
	viewMatrix[3][0] = 0.0f;
	viewMatrix[3][1] = 0.0f;
	viewMatrix[3][2] = 0.0f;
	rotation += SkyboxShader::ROTATE_SPEED * DisplayManager::GetFrameTimeSeconds();
	viewMatrix = glm::rotate(viewMatrix, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
	ShaderProgram::LoadMatrix(location_viewMatrix, viewMatrix);
}

void SkyboxShader::LoadFogColour(float r, float g, float b) {
	ShaderProgram::LoadVector(location_fogColour, glm::vec3(r, g, b));
}

void SkyboxShader::LoadBlendFactor(float blend) {
	ShaderProgram::LoadFloat(location_blendFactor, blend);
}

void SkyboxShader::ConnectTextureUnits() {
	ShaderProgram::LoadInt(location_cubeMap, 0);
	ShaderProgram::LoadInt(location_cubeMap2, 1);
}

void SkyboxShader::GetAllUniformLocations() {
	location_projectionMatrix = ShaderProgram::GetUniformLocation("projectionMatrix");
	location_viewMatrix = ShaderProgram::GetUniformLocation("viewMatrix");
	location_fogColour = ShaderProgram::GetUniformLocation("fogColour");
	location_cubeMap = ShaderProgram::GetUniformLocation("cubeMap");
	location_cubeMap2 = ShaderProgram::GetUniformLocation("cubeMap2");
	location_blendFactor = ShaderProgram::GetUniformLocation("blendFactor");
}
