#include "SkyboxRenderer.h"

const float SkyboxRenderer::SIZE = 4500.0f;
const std::vector<float> SkyboxRenderer::VERTICES = {
	-SIZE,  SIZE, -SIZE,
	-SIZE, -SIZE, -SIZE,
	SIZE, -SIZE, -SIZE,
	SIZE, -SIZE, -SIZE,
	SIZE,  SIZE, -SIZE,
	-SIZE,  SIZE, -SIZE,

	-SIZE, -SIZE,  SIZE,
	-SIZE, -SIZE, -SIZE,
	-SIZE,  SIZE, -SIZE,
	-SIZE,  SIZE, -SIZE,
	-SIZE,  SIZE,  SIZE,
	-SIZE, -SIZE,  SIZE,

	SIZE, -SIZE, -SIZE,
	SIZE, -SIZE,  SIZE,
	SIZE,  SIZE,  SIZE,
	SIZE,  SIZE,  SIZE,
	SIZE,  SIZE, -SIZE,
	SIZE, -SIZE, -SIZE,

	-SIZE, -SIZE,  SIZE,
	-SIZE,  SIZE,  SIZE,
	SIZE,  SIZE,  SIZE,
	SIZE,  SIZE,  SIZE,
	SIZE, -SIZE,  SIZE,
	-SIZE, -SIZE,  SIZE,

	-SIZE,  SIZE, -SIZE,
	SIZE,  SIZE, -SIZE,
	SIZE,  SIZE,  SIZE,
	SIZE,  SIZE,  SIZE,
	-SIZE,  SIZE,  SIZE,
	-SIZE,  SIZE, -SIZE,

	-SIZE, -SIZE, -SIZE,
	-SIZE, -SIZE,  SIZE,
	SIZE, -SIZE, -SIZE,
	SIZE, -SIZE, -SIZE,
	-SIZE, -SIZE,  SIZE,
	SIZE, -SIZE,  SIZE
};

std::vector<std::string> SkyboxRenderer::TEXTURE_FILES = { "dayRight", "dayLeft", "dayUp", "dayDown", "dayBack", "dayFront" };
std::vector<std::string> SkyboxRenderer::NIGHT_TEXTURE_FILES = { "nightRight", "nightLeft", "nightTop",
	"nightBottom", "nightBack", "nightFront" };

SkyboxRenderer::SkyboxRenderer(Loader& loader, glm::mat4 projectionMatrix) :
	cube(loader.LoadToVAO(SkyboxRenderer::VERTICES, 3)), texture(loader.LoadCubeMap(TEXTURE_FILES)),
	nightTexture(loader.LoadCubeMap(NIGHT_TEXTURE_FILES)), time(0.0f) {
	shader.Start();
	shader.LoadProjectionMatrix(projectionMatrix);
	shader.ConnectTextureUnits();
	shader.Stop();
}

void SkyboxRenderer::Render(Camera& camera, float r, float g, float b) {
	shader.Start();
	shader.LoadViewMatrix(camera);
	shader.LoadFogColour(r, g, b);

	glBindVertexArray(cube->GetVaoID());
	glEnableVertexAttribArray(0);

	BindTextures();
	glDrawArrays(GL_TRIANGLES, 0, cube->GetVertexCount());

	glDisableVertexAttribArray(0);
	glBindVertexArray(0);

	shader.Stop();
}

void SkyboxRenderer::BindTextures() {
	time += DisplayManager::GetFrameTimeSeconds() * 1000.0f;
	time = std::fmod(time, 24000.0f);

	unsigned int texture1;
	unsigned int texture2;

	float blendFactor = 0.0f;

	if (time >= 0.0f && time < 5000.0f) {
		texture1 = nightTexture;
		texture2 = nightTexture;
	}
	else if (time >= 5000.0f && time < 8000.0f) {
		texture1 = nightTexture;
		texture2 = texture;
		blendFactor = (time - 5000.0f) / (8000.0f - 5000.0f);
	}
	else if (time >= 8000.0f && time < 21000.0f) {
		texture1 = texture;
		texture2 = texture;
		blendFactor = (time - 8000.0f) / (21000.0f - 8000.0f);
	}
	else {
		texture1 = texture;
		texture2 = nightTexture;
		blendFactor = (time - 21000.0f) / (24000.0f - 21000.0f);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, nightTexture);

	shader.LoadBlendFactor(0.5f);
}
