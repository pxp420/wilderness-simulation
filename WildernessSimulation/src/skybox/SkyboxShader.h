#pragma once

#include "../shaders/ShaderProgram.h"
#include "../entities/Camera.h"

class SkyboxShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	const static float ROTATE_SPEED;

	unsigned int location_projectionMatrix;
	unsigned int location_viewMatrix;
	unsigned int location_fogColour;
	unsigned int location_cubeMap;
	unsigned int location_cubeMap2;
	unsigned int location_blendFactor;


	float rotation;
public:
	/**
	* Calls the parent constructor to create the program
	*/
	SkyboxShader();

	/**
	* Loads the transformation matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadProjectionMatrix(glm::mat4 matrix);

	/**
	* Loads the view matrix into a uniform.
	*
	* @param camera
	*		- The camera needed to create the view matrix.
	*/
	void LoadViewMatrix(Camera& camera);

	/**
	 * Loads the fog colour into a uniform.
	 *
	 * @param r
	 *		- The red channel of the fog colour.
	 *
	 * @param g
	 *		- The green channel of the fog colour.
	 *
	 * @param b
	 *		- The blue channel of the fog colour.
	*/
	void LoadFogColour(float r, float g, float b);

	/**
	 * Loads the blend factor between two textures to a uniform.
	 *
	 * @param blend
	 *		- The blend factor value.
	*/
	void LoadBlendFactor(float blend);

	/**
	* Loads all the textures for the skybox in their corresponding
	* units.
	*/
	void ConnectTextureUnits();
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};