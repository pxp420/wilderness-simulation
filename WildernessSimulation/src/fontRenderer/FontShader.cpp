#include "FontShader.h"

const std::string FontShader::VERTEX_FILE = "src/fontRenderer/fontVertex.shader";
const std::string FontShader::FRAGMENT_FILE = "src/fontRenderer/fontFragment.shader";

const std::map<unsigned int, std::string> FontShader::ATTRIBUTES = {
	{ 0, "position" },
    { 1, "textureCoords" }
};

FontShader::FontShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void FontShader::LoadTransformationMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_transformationMatrix, matrix);
}

void FontShader::LoadViewMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_viewMatrix, matrix);
}

void FontShader::LoadProjectionMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_projectionMatrix, matrix);
}

void FontShader::LoadColour(glm::vec3 colour) {
	ShaderProgram::LoadVector(location_colour, colour);
}

void FontShader::LoadTranslation(glm::vec2 translation) {
	ShaderProgram::LoadVector(location_translation, translation);
}

void FontShader::GetAllUniformLocations() {
	location_transformationMatrix = ShaderProgram::GetUniformLocation("transformationMatrix");
	location_viewMatrix = ShaderProgram::GetUniformLocation("viewMatrix");
	location_projectionMatrix = ShaderProgram::GetUniformLocation("projectionMatrix");
	location_colour = ShaderProgram::GetUniformLocation("colour");
	location_translation = ShaderProgram::GetUniformLocation("translation");
}
