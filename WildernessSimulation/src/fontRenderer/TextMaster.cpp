#include "TextMaster.h"

bool operator==(GUIText* lhs, const GUIText& rhs) {
	return lhs->GetTextString() == rhs.GetTextString();
}

Loader* TextMaster::loader = nullptr;
std::map<FontType*, std::vector<GUIText*>> TextMaster::texts = std::map<FontType*, std::vector<GUIText*>>();
FontRenderer* TextMaster::renderer = nullptr;

void TextMaster::Init(Loader& loader, Camera* camera) {
	TextMaster::loader = &loader;
	TextMaster::renderer = new FontRenderer(camera);
}

void TextMaster::Render() {
	renderer->Render(texts);
}

void TextMaster::LoadText(GUIText* text) {
	FontType* font = text->GetFont();
	TextMeshData data = font->LoadText(*text);

	unsigned int vao = loader->LoadToVAO(data.GetVertexPositions(), data.GetTextureCoords());
	text->SetMeshInfo(vao, data.GetVertexCount());
	std::vector<GUIText*>* textBatch = new std::vector<GUIText*>();

	if (!texts.empty()) {
		textBatch = &texts.at(font);
	}
	if (textBatch->empty()) {
		textBatch = new std::vector<GUIText*>();
		textBatch->emplace_back(text);
		texts[font] = *textBatch;
	}
	textBatch->emplace_back(text);
}

void TextMaster::RemoveText(GUIText* text) {
	std::vector<GUIText*>* textBatch = &texts.at(text->GetFont());

	auto it = std::find(textBatch->begin(), textBatch->end(), text);
	if (it != textBatch->end()) { 
		textBatch->erase(it); 
	}
}

void TextMaster::CleanUp() {
	renderer->CleanUp();
}
