#pragma once

#include "../renderEngine/Loader.h"
#include "../fontRenderer/FontRenderer.h"

class TextMaster {
private:
	static Loader* loader;
	static std::map<FontType*, std::vector<GUIText*>> texts;
	static FontRenderer* renderer;
public:
	/**
	 * Initializes the font renderer so that the upcomming texts can be
	 * rendered on the screen.
	 *
	 * @param loader
	 *		- The loader needed to load the vertices and texture coordinates
	 *		to the GPU.
	*/
	static void Init(Loader& loader, Camera* camera);

	/**
	 * Render the texts each frame.
	*/
	static void Render();

	/**
	 * Loads the text into a batch that gets processed every frame.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	*/
	static void LoadText(GUIText* text);

	/**
	 * Remove the text from the batch.
	 *
	 * @param text
	 *		- The text to be removed.
	*/
	static void RemoveText(GUIText* text);

	/**
	 * Clean up before exiting the application.
	*/
	static void CleanUp();
};
