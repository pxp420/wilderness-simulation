#include "FontRenderer.h"

FontRenderer::FontRenderer(Camera* camera) : camera(camera) {}

void FontRenderer::Render(std::map<FontType*, std::vector<GUIText*>>& texts) {
	Prepare();

	std::map<FontType*, std::vector<GUIText*>>::iterator it;
	for (it = texts.begin(); it != texts.end(); it++) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, it->first->GetTextureAtlas());

		for (GUIText* gText : it->second) {
			if (gText->GetDimensions() == GUIText::Dimensions::TWO_D) {
				glm::mat4 eye = glm::mat4(1.0f);
				shader.LoadProjectionMatrix(eye);
				shader.LoadViewMatrix(eye);
			}
			else {
				glm::mat4 projectionMatrix = MasterRenderer::projectionMatrix;
				shader.LoadProjectionMatrix(projectionMatrix);
				glm::mat4 viewMatrix = Maths::CreateViewMatrix(*camera);
				shader.LoadViewMatrix(viewMatrix);
			}

			RenderText(*gText);
		}
	}
	EndRendering();
}

void FontRenderer::CleanUp() {
	shader.CleanUp();
}

void FontRenderer::Prepare() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	shader.Start();
}

void FontRenderer::RenderText(GUIText& text) {
	glBindVertexArray(text.GetMesh());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	if (text.GetDimensions() == GUIText::Dimensions::THREE_D) {
		glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(text.GetPosition(),
			-camera->GetPitch(), 0.0f, 0.0f, text.GetScale());
		shader.LoadTransformationMatrix(transformationMatrix);
	}
	else {
		glm::mat4 transformationMatrix(1.0f);
		shader.LoadTransformationMatrix(transformationMatrix);
	}

	shader.LoadColour(text.GetColour());
	shader.LoadTranslation(text.GetTranslation());
	glDrawArrays(GL_TRIANGLES, 0, text.GetVertexCount());
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
}

void FontRenderer::EndRendering() {
	shader.Stop();
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}
