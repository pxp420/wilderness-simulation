#pragma once

#include "FontShader.h"
#include "../fontMeshCreator/FontType.h"
#include "../toolbox/Maths.h"
#include "../renderEngine/MasterRenderer.h"

#include <GLAD/glad.h>
#include <GLFW/glfw3.h>

class FontRenderer {
private:
	FontShader shader;
	Camera* camera;
public:
	FontRenderer(Camera* camera);

	/**
	 * Render a list of texts with their own font.
	 *
	 * @param texts
	 *		- A list of texts with their own font.
	*/
	void Render(std::map<FontType*, std::vector<GUIText*>>& texts);

	/**
	 * Cleans up the shaders before exiting the application.
	*/
	void CleanUp();

	/**
	 * Enable blending and make all the transparent bits to not appear on the screen
	 * when rendering.
	*/
	void Prepare();

	/**
	 * Enables the locations of the shader, loads the transformation matrix uniform
	 * and draw the quads on the screen.
	 *
	 * @param text
	 *		- The text to be rendered on the screen.
	*/
	void RenderText(GUIText& text);

	/**
	 * Stop the shader when finished rendering.
	*/
	void EndRendering();
};
