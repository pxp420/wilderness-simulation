#pragma once

#pragma once

#include "../shaders/ShaderProgram.h"

class FontShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_transformationMatrix;
	unsigned int location_viewMatrix;
	unsigned int location_projectionMatrix;

	unsigned int location_colour;
	unsigned int location_translation;
public:
	/**
	* Calls the parent constructor to create the program
	*/
	FontShader();

	/**
	* Loads the transformation matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadTransformationMatrix(glm::mat4 matrix);

	/**
	* Loads the view matrix into a uniform.
	*
	* @param camera
	*		- The camera needed to create the view matrix.
	*/
	void LoadViewMatrix(glm::mat4 matrix);

	/**
	* Loads the projection matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadProjectionMatrix(glm::mat4 matrix);

	/**
	 * Loads the colour of the text into a uniform.
	 *
	 * @param colour
	 *		- The colour of the text.
	*/
	void LoadColour(glm::vec3 colour);

	/**
	 * Loads the translation offset of the text into a uniform.
	 *
	 * @param translation
	 *		- The translation offset.
	*/
	void LoadTranslation(glm::vec2 translation);

protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
