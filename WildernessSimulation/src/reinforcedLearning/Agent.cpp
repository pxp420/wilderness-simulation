#include "Agent.h"
#include "Simulation.h"

float Agent::INITIAL_EPSILON = 50.0f;
float Agent::LEARNING_RATE = 0.01f;
float Agent::GAMMA = 0.9f;

unsigned int Agent::NUMBER_OF_EPOCHS = 1;
unsigned int Agent::BATCH_SIZE = 8;

Agent::HumanData Agent::HUMAN_DATA = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, DayCycleGenerator::TIME_OF_DAY };

Agent::Agent() : learningRate(LEARNING_RATE),
	gamma(GAMMA),
	epsilon(INITIAL_EPSILON), 
	longTermMemory(std::vector<Agent::TrainingData>()),
	human(Human::GetInstance()),
	model(CreateModel()) { }

std::vector<double> Agent::GetState() {
	auto human = Human::GetInstance();

	std::vector<double> state;
	
	float hunger = human->GetHunger();
	if (hunger < 50.0f) {
		state.emplace_back(0.0);
	}
	else {
		state.emplace_back(1.0);
	}

	float thirst = human->GetThirst();
	if (thirst < 50.0f) {
		state.emplace_back(0.0);
	}
	else {
		state.emplace_back(1.0);
	}

	float bodyTemp = human->GetBodyTemperature();
	if (bodyTemp > 0.0f) {
		state.emplace_back(0.0);
	}
	else {
		state.emplace_back(1.0);
	}

	state.emplace_back(human->GetNumberOfBerries());
	state.emplace_back(human->GetNumberOfWater());
	state.emplace_back(human->GetNumberOfLogs());
	state.emplace_back(DayCycleGenerator::TIME_OF_DAY);
	state.emplace_back(human->CampfireInRange());

	return state;
}

Agent::Action Agent::RetrieveAction(std::vector<double>& state, unsigned int simulationCounter) {
	this->epsilon = INITIAL_EPSILON - simulationCounter;

	auto human = Human::GetInstance();
	std::vector<unsigned int> possibleActions;

	bool hasBerries = human->HasBerries();
	bool hasWater = human->HasWater();
	bool hasCampfireLogs = human->HasCampfireLogs();
	unsigned int currentItemsNumber = human->GetCurrentItemsNumber();

	//possibleActions.emplace_back(Action::PLACE_CAMPFIRE);
	if (Human::MAX_INVENTORY_SPACE > currentItemsNumber) {
		possibleActions.emplace_back(Action::FIND_BERRIES);
		possibleActions.emplace_back(Action::FIND_WOOD);
		possibleActions.emplace_back(Action::FIND_WATER);
	}
	if (hasBerries) {
		//possibleActions.emplace_back(Action::EAT_BERRY);
	}
	if (hasCampfireLogs) {
		//possibleActions.emplace_back(Action::PLACE_CAMPFIRE);
	}
	if (hasWater) {
		//possibleActions.emplace_back(Action::DRINK_WATER);
	}

	Agent::Action action;
	std::string from;
	if (RandomGenerator::GetNextInt(0, 40) < this->epsilon) {
		action = (Agent::Action)*Maths::SelectRandomly(possibleActions.begin(), possibleActions.end());
		from += "RANDOM: ";
	}
	else {
		mat convertedState(state);
		mat predictions;
		model.Predict(convertedState, predictions);

		std::cout << std::endl;
		double minP = predictions.min();
		auto pred = predictions;
		for (int i = 0; i < pred.n_rows; i++) {
			pred.at(i) = pred.at(i) - minP;
		}
		double maxP = pred.max();
		for (int i = 0; i < pred.n_rows; i++) {
			pred.at(i) = pred.at(i) / maxP;
		}

		double sum = 0.0;
		for (int i = 0; i < pred.n_rows; i++) {
			sum = sum + pred.at(i);
		}
		for (int i = 0; i < pred.n_rows; i++) {
			pred.at(i) = pred.at(i) / sum;
		}
		std::cout << "EAT BERRY, DRINK WATER, PLACE CAMPFIRE, FIND BERRY, FIND WATER, FIND WOOD, STAY" << std::endl;
		std::cout.precision(4);
		std::cout << pred.at(0) << ", " << pred.at(1) << ", " << pred.at(2) << ", " << pred.at(3) << ", " << pred.at(4) << ", " << pred.at(5) << std::endl;
		std::cout << std::endl;

		int index = possibleActions.at(0);
		double max = predictions.at(possibleActions.at(0));
		for (unsigned int counter : possibleActions) {
			if (predictions.at(counter) > max) {
				max = predictions.at(counter);
				index = counter;
			}
		}

		action = (Agent::Action)index;
		from += "MODEL: ";
	}
	if (action == 0) {
		from += "EAT_BERRY";
	}
	else if (action == 1) {
		from += "DRINK WATER";
	}
	else if (action == 2) {
		from += "PLACE CAMPFIRE";
	}
	else if (action == 3) {
		from += "FIND BERRIES";
	}
	else if (action == 4) {
		from += "FIND_WATER";
	}
	else if (action == 5) {
		from += "FIND WOOD";
	}
	else {
		from += "STAY";
	}
	std::cout << from << std::endl;
	return action;
}

float Agent::SetReward(Agent::HumanData& humanData, Action action) {
	/*auto human = Human::GetInstance();
	float reward = 0.0f;
	float thirst = human->GetThirst();
	float hunger = human->GetHunger();
	if (action == Action::PLACE_CAMPFIRE) {
		if (DayCycleGenerator::TIME_OF_DAY == DayCycleGenerator::Time::NIGHT) {
			reward = 2.0f;
		}
		else {
			reward = -2.0f;
		}
	}
	else if (action == Action::STAY) {
		if (DayCycleGenerator::TIME_OF_DAY == DayCycleGenerator::Time::NIGHT &&
			Human::CAMPFIRES.size() == 1) {
			reward = 4.0f;
		}
		else {
			reward = -4.0f;
		}
	}
	else if (action == DRINK_WATER) {
		if (thirst > 50.0f) {
			reward = 2.0f;
		}
		else {
			reward = -2.0f;
		}
	}
	else if (action == EAT_BERRY) {
		if (hunger > 50.0f) {
			reward = 2.0f;
		}
		else {
			reward = -2.0f;
		}
	}
	else {
		reward = 0.0f;
	}

	return reward / 1000.0f;*/


	/*auto human = Human::GetInstance();
	float reward = 0.0f;

	if (human->GetHealth() <= 0.0f) {
		reward = -1000.0f;
		return reward / 1000.0f;
	}

	if (action == Action::FIND_BERRIES) {
		unsigned int berryCount = humanData.berryCount;
		if (humanData.timeOfDay == DayCycleGenerator::Time::NIGHT) {
			reward = -2.0f * (berryCount + 1);
		}
		else {
			float berryCountFactor = 2.0f;
			if (humanData.timeOfDay == DayCycleGenerator::Time::DAY) {
				reward = 8.0f - berryCount * berryCountFactor;
			}
			else if (humanData.timeOfDay == DayCycleGenerator::Time::EVENING) {
				reward = 6.0f - berryCount * berryCountFactor;
			}
		}
	}
	else if (action == Action::FIND_WATER) {
		unsigned int waterCount = humanData.waterCount;
		if (humanData.timeOfDay == DayCycleGenerator::Time::NIGHT) {
			reward = -2.0f * (waterCount + 1);
		}
		else {
			float waterCFactor = 2.0f;
			if (humanData.timeOfDay == DayCycleGenerator::Time::DAY) {
				reward = 8.0f - waterCount * waterCFactor;
			}
			else if (humanData.timeOfDay == DayCycleGenerator::Time::EVENING) {
				reward = 6.0f - waterCount * waterCFactor;
			}
		}
	}

	else if (action == Action::FIND_WOOD) {
		float logCountFactor = 2.0f;
		unsigned int logCount = humanData.logsCount;

		if (humanData.timeOfDay == DayCycleGenerator::Time::DAY) {
			reward = 6.0f - logCount * logCountFactor;
		}
		else if (humanData.timeOfDay == DayCycleGenerator::Time::EVENING) {
			reward = 8.0f - logCount * logCountFactor;
		}
		else {
			reward = -2.0f * (logCount + 1);
		}
	}
	else if (action == Action::PLACE_CAMPFIRE) {
		if (Human::CAMPFIRES.size() == 1) {
			if (humanData.timeOfDay == DayCycleGenerator::Time::DAY) {
				reward = -4.0f;
			}
			else if (humanData.timeOfDay == DayCycleGenerator::Time::EVENING) {
				reward = 5.0f;
			}
			else {
				reward = 8.0f;
			}
		}
		else {
			reward = -6.0f;
		}
	}
	else if (action == Action::EAT_BERRY) {
		if (humanData.hunger < 50) {
			reward = 4.5f;
		}
		else {
			reward = -4.5f;
		}
	} 
	else if (action == Action::DRINK_WATER) {
		if (humanData.thirst < 50) {
			reward = 4.5f;
		}
		else {
			reward = -4.5f;
		}
	}
	else if (action == Action::STAY) {
		if (humanData.timeOfDay == DayCycleGenerator::Time::NIGHT && 
			Human::CAMPFIRES.size() == 1) {
			reward = 10.0f;
		}
		else {
			reward = -9.0f;
		}
	}
	else {
		reward = -5.0f;
	}

	std::cout << reward << std::endl;
	return reward / 1000.0f;*/


	float reward = 0.0f;
	auto human = Human::GetInstance();
	float hunger = human->GetHunger();
	float thirst = human->GetThirst();
	float health = human->GetHealth();

	if (health <= 0.0f) {
		reward = -10.0f;
		return reward / 1000.0f;
	}

	if (humanData.timeOfDay == DayCycleGenerator::Time::NIGHT &&
		action != Action::STAY) {
		reward = -2.0f;
		return reward / 1000.0f;
	}

	if (action == Action::FIND_BERRIES) {
		float baseReward = 0.6f;
		float hungerDecrease = 0.0f;
		float thirstDecrease = 0.0f;
		if (hunger < 75.0f) {
			hungerDecrease = 0.2f;
		}
		if (thirst < 75.0f) {
			thirstDecrease = 0.2f;
		}
		reward = baseReward - humanData.berryCount * 0.2f - hungerDecrease - thirstDecrease;
	}
	else if (action == Action::EAT_BERRY) {
		if (hunger > 75.0f) {
			reward = -1.0f;
		}
		else {
			reward = 1.0f;
		}
	}
	else if (action == Action::FIND_WOOD) {
		float baseReward = 0.6f;
		float hungerDecrease = 0.0f;
		float thirstDecrease = 0.0f;
		if (hunger < 75.0f) {
			hungerDecrease = 0.2f;
		}
		if (thirst < 75.0f) {
			thirstDecrease = 0.2f;
		}
		reward = baseReward - humanData.logsCount * 0.2f - hungerDecrease - thirstDecrease;
	}
	else if (action == Action::PLACE_CAMPFIRE) {
		float hungerDecrease = 0.0f;
		float thirstDecrease = 0.0f;
		float baseReward = -0.5f;
		float campfireDecrease = -0.4f;
		if (hunger < 75.0f) {
			hungerDecrease = 0.2f;
		}
		if (thirst < 75.0f) {
			thirstDecrease = 0.2f;
		}
		reward = (Human::CAMPFIRES.size() - 1) * campfireDecrease + baseReward - hungerDecrease - thirstDecrease;
	}
	else if (action == Action::STAY) {
		if (humanData.timeOfDay == DayCycleGenerator::Time::NIGHT && Human::CAMPFIRES.size() == 1) {
			reward = 2.0f;
		}
		else {
			float hungerDecrease = 0.0f;
			float thirstDecrease = 0.0f;
			if (hunger < 75.0f) {
				hungerDecrease = 0.2f;
			}
			if (thirst < 75.0f) {
				thirstDecrease = 0.2f;
			}
			reward = -0.1f - hungerDecrease - thirstDecrease;
		}
	} 
	else if (action == Action::FIND_WATER) {
		float baseReward = 0.6f;
		float hungerDecrease = 0.0f;
		float thirstDecrease = 0.0f;
		if (hunger < 75.0f) {
			hungerDecrease = 0.2f;
		}
		if (thirst < 75.0f) {
			thirstDecrease = 0.2f;
		}
		reward = baseReward - humanData.waterCount * 0.2f - hungerDecrease - thirstDecrease;
	}
	else if (action == Action::DRINK_WATER) {
		if (thirst > 75.0f) {
			reward = -1.0f;
		}
		else {
			reward = 1.0f;
		}
	}

	std::cout << "Reward: " << reward << std::endl;
	return reward / 1000.0f;
}

FFN<MeanSquaredError<>> Agent::CreateModel() {
	FFN<MeanSquaredError<>> model;
	model.Add<Linear<>>(8, 7);
	model.Add<ReLULayer<>>();
	model.Add<Dropout<>>(0.15);
	model.Add<Linear<>>(7, 7);
	
	this->model = model;
	return model;
}

void Agent::ReplayNew() {
	auto minibatch = longTermMemory;
	if (longTermMemory.size() > 1000) {
		Maths::RandomUnique(minibatch.begin(), minibatch.end(), 1000);
		minibatch.resize(1000);
	}

	for (auto& q : minibatch) {
		float target = q.reward;
		if (!q.simulationFinished) {
			mat next_state(q.nextState);
			mat predictions;
			model.Predict(next_state, predictions);

			double min = predictions.min();
			for (int i = 0; i < predictions.n_rows; i++) {
				predictions.at(i) = predictions.at(i) - min;
			}

			target = q.reward + gamma * predictions.max();
		}

		mat state(q.state);
		mat target_f;
		model.Predict(state, target_f);

		double min = target_f.min();
		for (int i = 0; i < target_f.n_rows; i++) {
			target_f.at(i) = target_f.at(i) - min;
		}
		target_f.at(q.action) = target;
		for (int i = 0; i < target_f.n_rows; i++) {
			target_f.at(i) = target_f.at(i) + min;
		}

		for (unsigned int i = 0; i < NUMBER_OF_EPOCHS; i++) {
			StandardSGD opt(LEARNING_RATE, BATCH_SIZE, 1);
			model.Train(state, target_f);
		}
	}

 	data::Save("res/weights/weight" + std::to_string(Simulation::SIMULATION_COUNTER) + "-survived" + 
		std::to_string((int)(DayCycleGenerator::HOURS_SURVIVED / 24000.0f)) + "d" + std::to_string((int)(std::fmod(DayCycleGenerator::HOURS_SURVIVED, 24000.0f) / 1000.0f)) + "h" +
		".xml", "model", model, true);
	//SaveLongTermMemory("res/weights/memory" + std::to_string(Simulation::SIMULATION_COUNTER) + ".txt");
}

void Agent::SaveLongTermMemory(const std::string& fileName) {
	std::ofstream file;
	file.open(fileName);
	for (auto data : longTermMemory) {
		for (int i = 0; i < data.state.size(); i++) {
			file << std::to_string(data.state.at(i));
			if (i != data.state.size() - 1) {
				file << " ";
			}
		}

		file << ",";
		file << std::to_string(data.action);
		file << ",";
		file << std::to_string(data.reward);
		file << ",";

		for (int i = 0; i < data.nextState.size(); i++) {
			file << std::to_string(data.nextState.at(i));
			if (i != data.nextState.size() - 1) {
				file << " ";
			}
		}

		file << ",";
		file << std::to_string(data.simulationFinished);
		file << std::endl;
	}
	file.close();
}

void Agent::Remember(TrainingData& data) {
	longTermMemory.emplace_back(data);
}

void Agent::LoadLongTermMemory(const std::string& fileName) {
	std::ifstream stream(fileName);
	std::string line;

	while (getline(stream, line)) {
		TrainingData data;
		auto splittedLine = StringManipulation::Split(line, ',');

		std::vector<double> state;
		for (auto value : StringManipulation::Split(splittedLine.at(0), ' ')) {
			state.emplace_back(std::stod(value));
		}
		data.state = state;
		data.action = (Agent::Action)(std::stoi(splittedLine.at(1)));
		data.reward = std::stof(splittedLine.at(2));

		std::vector<double> nextState;
		for (auto value : StringManipulation::Split(splittedLine.at(3), ' ')) {
			nextState.emplace_back(std::stod(value));
		}
		data.nextState = nextState;
		data.simulationFinished = (bool)std::stoi(splittedLine.at(4));

		this->longTermMemory.emplace_back(data);
	}

	stream.close();
}

void Agent::Train() {
	auto batch = longTermMemory;
	batch.resize(1000);
	for (auto& q : batch) {
		float target = q.reward;
		if (!q.simulationFinished) {
			mat next_state(q.nextState);
			mat predictions;
			model.Predict(next_state, predictions);

			double min = predictions.min();
			for (int i = 0; i < predictions.n_rows; i++) {
				predictions.at(i) = predictions.at(i) - min;
			}

			target = q.reward + gamma * predictions.max();
		}

		mat state(q.state);
		mat target_f;
		model.Predict(state, target_f);

		double min = target_f.min();
		for (int i = 0; i < target_f.n_rows; i++) {
			target_f.at(i) = target_f.at(i) - min;
		}
		target_f.at(q.action) = target;
		for (int i = 0; i < target_f.n_rows; i++) {
			target_f.at(i) = target_f.at(i) + min;
		}

		for (unsigned int i = 0; i < NUMBER_OF_EPOCHS; i++) {
			StandardSGD opt(LEARNING_RATE, BATCH_SIZE, 1);
			model.Train(state, target_f);
		}
	}
}

void Agent::TrainShortMemory(TrainingData& data) {
	float target = data.reward;
	if (!data.simulationFinished) {
		mat next_state(data.nextState);
		mat predictions;
		model.Predict(next_state, predictions);

		double min = predictions.min();
		for (int i = 0; i < predictions.n_rows; i++) {
			predictions.at(i) = predictions.at(i) - min;
		}

		target = data.reward + gamma * predictions.max();
	}

	mat state(data.state);
	mat target_f;
	model.Predict(state, target_f);

	double min = target_f.min();
	for (int i = 0; i < target_f.n_rows; i++) {
		target_f.at(i) = target_f.at(i) - min;
	}
	target_f.at(data.action) = target;
	for (int i = 0; i < target_f.n_rows; i++) {
		target_f.at(i) = target_f.at(i) + min;
	}

	for (unsigned int i = 0; i < NUMBER_OF_EPOCHS; i++) {
		StandardSGD opt(LEARNING_RATE, BATCH_SIZE, 1);
		model.Train(state, target_f);
	}
}

FFN<MeanSquaredError<>> Agent::GetModel() {
	return model;
}

void Agent::SetModel(FFN<MeanSquaredError<>>& model) {
	this->model = model;
}
