#include "Simulation.h"

#include "../entities/Human.h"
#include "../inputListener/Input.h"

#include <stdlib.h>

Simulation* Simulation::instance = nullptr;

bool Simulation::SAVED = false;
bool Simulation::MODEL_SET = false;

std::vector<double> Simulation::CURRENT_STATE = std::vector<double>(0);
Agent::Action Simulation::NEXT_ACTION = Agent::Action::FIND_BERRIES;

unsigned int Simulation::SIMULATION_COUNTER = 0;

Simulation::Simulation(Agent* agent) : agent(agent), simulationScore(0.0f), simulationFinished(false) { }

void Simulation::CreateInstance(Agent* agent) {
	instance = new Simulation(agent);
}

void Simulation::DestroyInstance() {
	delete instance;
}

Simulation* Simulation::GetInstance() {
	return instance;
}

void Simulation::Restart() {
	Map::GetInstance().InitialiseMap();
	AStarGenerator::GetGenerator()->clearCollisions();

	for (Campfire* campfire : Human::CAMPFIRES) {
		campfire->Remove();
	}

	Human::DestroyInstance();
	for (Entity* entity : Entity::ENTITIES) {
		delete entity;
	}
	Entity::ENTITIES.clear();
	Tree::TREES.clear();
	Fern::FERNS.clear();
	LowPolyTree::LOW_POLY_TREES.clear();
	TreeLog::LOGS.clear();

	Entity::COUNT = 0;
	Tree::COUNT = 0;
	Fern::COUNT = 0;
	MossyRock::COUNT = 0;
	LowPolyTree::COUNT = 0;
	Human::COUNT = 0;
	TreeLog::COUNT = 0;

	VegetationGenerator::GetInstance()->SpawnModels();
	MasterRenderer::GetInstance()->DestroyInstance();

	Human::CreateInstance(glm::vec3(1800.0f, 20.0f, 1800.0f));
	Human::GetInstance()->SetOnMap(180, 180);

	MasterRenderer::CreateInstance(Loader::GetInstance(), Camera::GetInstance(), Entity::ENTITIES);

	DayCycleGenerator::DestroyInstance();
	DayCycleGenerator::CreateInstance(Light::LIGHTS[0]);
}

void Simulation::Simulate(const std::string& modelName, const std::string& memoryName) {
	if (!MODEL_SET) {
		auto model = agent->CreateModel();
		data::Load(modelName, "model", model);
		//agent->LoadLongTermMemory(memoryName);
		agent->SetModel(model);
		//agent->Train();
		MODEL_SET = true;
		SIMULATION_COUNTER = 500;
	}

	this->Simulate();
	//auto human = Human::GetInstance();
	//auto actions = human->GetQueueOfActions();
	//if (!actions->empty()) {
	//	Action* action = actions->front();

	//	if (action->move(DisplayManager::GetFrameTimeSeconds())) {
	//		actions->pop_front();
	//		if (action->type == Action::Type::MOVE) {
	//			human->SetPreviousDirection(action->direction);
	//		}

	//		delete action;
	//	}
	//}
	//else {
	//	CURRENT_STATE = agent->GetState();
	//	NEXT_ACTION = agent->RetrieveAction(CURRENT_STATE, SIMULATION_COUNTER);
	//	if (NEXT_ACTION == Agent::Action::FIND_BERRIES) {
	//		human->FindPlant();
	//	}
	//	else if (NEXT_ACTION == Agent::Action::FIND_WATER) {
	//		human->FindWater();
	//	}
	//	else if (NEXT_ACTION == Agent::Action::FIND_WOOD) {
	//		human->FindWood();
	//	}
	//	else if (NEXT_ACTION == Agent::Action::EAT_BERRY) {
	//		Action* action = new Action();
	//		action->name = "Eating...";
	//		action->type = Action::Type::CONSUME;
	//		action->move = std::bind(&Human::EatBerry, human, std::placeholders::_1, 2.0f);

	//		auto actions = human->GetQueueOfActions();
	//		actions->emplace_back(action);
	//	}
	//	else if (NEXT_ACTION == Agent::Action::DRINK_WATER) {
	//		Action* action = new Action();
	//		action->name = "Drinking...";
	//		action->type = Action::Type::CONSUME;
	//		action->move = std::bind(&Human::DrinkWater, human, std::placeholders::_1, 2.0f);

	//		auto actions = human->GetQueueOfActions();
	//		actions->emplace_back(action);
	//	}
	//	else if (NEXT_ACTION == Agent::Action::PLACE_CAMPFIRE) {
	//		Action* action = new Action();
	//		action->name = "Placing Campfire...";
	//		action->type = Action::Type::CONSUME;
	//		action->move = std::bind(&Human::PlaceCampFire, human, std::placeholders::_1, 2.0f);

	//		auto actions = human->GetQueueOfActions();
	//		actions->emplace_back(action);
	//	}
	//	else if (NEXT_ACTION == Agent::Action::STAY) {
	//		Action* action = new Action();
	//		action->name = "Idle...";
	//		action->type = Action::Type::CONSUME;
	//		action->move = std::bind(&Human::Stay, human, std::placeholders::_1, 2.0f);

	//		auto actions = human->GetQueueOfActions();
	//		actions->emplace_back(action);
	//	}
	//}
}

void Simulation::Simulate() {
	auto human = Human::GetInstance();
	auto actions = human->GetQueueOfActions();
	if (!actions->empty()) {
		Action* action = actions->front();

		if (action->move(DisplayManager::GetFrameTimeSeconds())) {
			glm::vec3 humanPos = human->GetPosition();
			//Camera::GetInstance()->SetPosition(glm::vec3(humanPos.x, humanPos.y + 90.0f, humanPos.z + 300.0f));
			actions->pop_front();
			if (action->type == Action::Type::MOVE) {
				human->SetPreviousDirection(action->direction);
			}

			if (actions->empty()) {
				auto newState = agent->GetState();
				float reward = agent->SetReward(Agent::HUMAN_DATA, NEXT_ACTION);

				Agent::TrainingData data;
				data.state = CURRENT_STATE;
				data.action = NEXT_ACTION;
				data.reward = reward;
				data.nextState = newState;
				data.simulationFinished = human->GetHealth() <= 0.0f;

				agent->TrainShortMemory(data);
				agent->Remember(data);

				if (human->GetHealth() <= 0.0f) {
					agent->ReplayNew();
					SIMULATION_COUNTER++;
					DayCycleGenerator::HOURS_SURVIVED = 0.0f;
					SAVED = false;
					std::cout << "SIMULATION COUNTER: " << SIMULATION_COUNTER << std::endl;
					Restart();
					auto camera = Camera::GetInstance();

					auto pos = Human::GetInstance()->GetPosition();
					camera->SetPosition(glm::vec3(pos.x, camera->GetPosition().y, pos.z + 350.0f));
				}
			}

			delete(action);
		}
	}
	else {
		CURRENT_STATE = agent->GetState();
		NEXT_ACTION = agent->RetrieveAction(CURRENT_STATE, SIMULATION_COUNTER);
		Agent::HUMAN_DATA.health = human->GetHealth();
		Agent::HUMAN_DATA.hunger = human->GetHunger();
		Agent::HUMAN_DATA.thirst = human->GetThirst();
		Agent::HUMAN_DATA.berryCount = human->GetNumberOfBerries();
		Agent::HUMAN_DATA.waterCount = human->GetNumberOfWater();
		Agent::HUMAN_DATA.logsCount = human->GetNumberOfLogs();
		Agent::HUMAN_DATA.timeOfDay = DayCycleGenerator::TIME_OF_DAY;
		if (NEXT_ACTION == Agent::Action::FIND_BERRIES) {
			human->FindPlant();
		}
		else if (NEXT_ACTION == Agent::Action::FIND_WATER) {
			human->FindWater();
		}
		else if (NEXT_ACTION == Agent::Action::FIND_WOOD) {
			human->FindWood();
		}
		else if (NEXT_ACTION == Agent::Action::EAT_BERRY) {
			Action* action = new Action();
			action->name = "Eating...";
			action->type = Action::Type::CONSUME;
			action->move = std::bind(&Human::EatBerry, human, std::placeholders::_1, 2.0f);

			auto actions = human->GetQueueOfActions();
			actions->emplace_back(action);
		}
		else if (NEXT_ACTION == Agent::Action::DRINK_WATER) {
			Action* action = new Action();
			action->name = "Drinking...";
			action->type = Action::Type::CONSUME;
			action->move = std::bind(&Human::DrinkWater, human, std::placeholders::_1, 2.0f);

			auto actions = human->GetQueueOfActions();
			actions->emplace_back(action);
		}
		else if (NEXT_ACTION == Agent::Action::PLACE_CAMPFIRE) {
			Action* action = new Action();
			action->name = "Campfire...";
			action->type = Action::Type::CONSUME;
			action->move = std::bind(&Human::PlaceCampFire, human, std::placeholders::_1, 2.0f);

			auto actions = human->GetQueueOfActions();
			actions->emplace_back(action);
		}
		else if (NEXT_ACTION == Agent::Action::STAY) {
			Action* action = new Action();
			action->name = "Idle...";
			action->type = Action::Type::CONSUME;
			action->move = std::bind(&Human::Stay, human, std::placeholders::_1, 2.0f);

			auto actions = human->GetQueueOfActions();
			actions->emplace_back(action);
		}
	}
}