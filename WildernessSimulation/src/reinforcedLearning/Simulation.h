#pragma once

#include "../entities/Human.h"
#include "Agent.h"

class Simulation {
private:
	float simulationScore;
	bool  simulationFinished;

	static Simulation* instance;

	static std::vector<double> CURRENT_STATE;

	static Agent::Action NEXT_ACTION;

	Agent* agent;

	static bool SAVED;
	static bool MODEL_SET;
public:
	static unsigned int SIMULATION_COUNTER;

	static void CreateInstance(Agent* agent);

	static void DestroyInstance();

	static Simulation* GetInstance();

	Simulation(Agent* agent);

	void Simulate();

	void Simulate(const std::string& modelName, const std::string& memoryName);

	void Restart();
};
