#pragma once

#include "../entities/Human.h"

#include <fstream>

#include <mlpack/methods/ann/layer/layer.hpp>
#include <mlpack/methods/ann/ffn.hpp>
#include <mlpack/methods/ann/loss_functions/mean_squared_error.hpp>

using namespace mlpack;
using namespace mlpack::ann;
using namespace mlpack::optimization;

using namespace arma;

class Agent {
private:
	Human* human;
	FFN<MeanSquaredError<>> model;

	float epsilon;
	float learningRate;
	float gamma;

	static float INITIAL_EPSILON;
	static float LEARNING_RATE;
	static float GAMMA;

	static unsigned int BATCH_SIZE;
	static unsigned int NUMBER_OF_EPOCHS;

public:
	enum Action { EAT_BERRY, DRINK_WATER, PLACE_CAMPFIRE, FIND_BERRIES, FIND_WATER, FIND_WOOD, STAY } action;

	struct TrainingData {
		std::vector<double> state;
		Agent::Action action;
		float reward;
		std::vector<double> nextState;
		bool simulationFinished;
	};

	struct HumanData {
		float hunger;
		float thirst;
		float health;
		float berryCount;
		float waterCount;
		float logsCount;
		DayCycleGenerator::Time timeOfDay;
	};

	static HumanData HUMAN_DATA;

	std::vector<Agent::TrainingData> longTermMemory;

	Agent();

	std::vector<double> GetState();

	Action RetrieveAction(std::vector<double>& state, unsigned int simulationCounter);

	float SetReward(Agent::HumanData& humanData, Action action);

	FFN<MeanSquaredError<>> CreateModel();

	void SaveLongTermMemory(const std::string& fileName);

	void Remember(TrainingData& data);

	void LoadLongTermMemory(const std::string& fileName);

	void ReplayNew();

	void Train();

	void TrainShortMemory(TrainingData& data);

	FFN<MeanSquaredError<>> GetModel();

	void SetModel(FFN<MeanSquaredError<>>& model);
};