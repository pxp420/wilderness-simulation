#include "DisplayManager.h"

const unsigned int DisplayManager::WIDTH = 1280;
const unsigned int DisplayManager::HEIGHT = 720;
const unsigned int DisplayManager::FPS_CAP = 120;

float DisplayManager::delta = 0.0f;
float DisplayManager::lastFrameTime = 0.0f;

const char* DisplayManager::TITLE = "Wilderness Simulation";
GLFWwindow* DisplayManager::window = nullptr;

void DisplayManager::CreateDisplay() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	window = glfwCreateWindow(WIDTH, HEIGHT, TITLE, NULL, NULL);
	//window = glfwCreateWindow(WIDTH, HEIGHT, TITLE, glfwGetPrimaryMonitor(), NULL);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	glfwSwapInterval(0);
	glEnable(GL_MULTISAMPLE);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	DisplayManager::lastFrameTime = GetCurrTime();
}

void DisplayManager::UpdateDisplay() {
	glfwSwapBuffers(window);
	glfwPollEvents();
	float currentFrameTime = GetCurrTime();

	DisplayManager::delta = currentFrameTime - DisplayManager::lastFrameTime;
	DisplayManager::lastFrameTime = currentFrameTime;
}

bool DisplayManager::IsCloseRequested() {
	return !glfwWindowShouldClose(window);
}

void DisplayManager::CloseDisplay() {
	glfwTerminate();
}

float DisplayManager::GetFrameTimeSeconds() {
	return 0.05;//delta * 10;
}

float DisplayManager::GetCurrTime() {
	return (float)glfwGetTime();
}
