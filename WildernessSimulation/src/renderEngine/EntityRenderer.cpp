#include "EntityRenderer.h"

EntityRenderer::EntityRenderer(StaticShader& shader, const glm::mat4& projectionMatrix) :
	shader(shader) {
	shader.Start();
	shader.LoadProjectionMatrix(projectionMatrix);
	shader.Stop();
}

void EntityRenderer::Render(std::map<TexturedModel, std::vector<Entity*>>& entities) {
	for (std::map<TexturedModel, std::vector<Entity*>>::iterator it = entities.begin(); it != entities.end(); ++it) {
		TexturedModel model = it->first;
		PrepareTexturedModel(model);
		std::vector<Entity*> batch;
		
		auto iterator = entities.find(model);
		if (iterator != entities.end()) {
			batch = iterator->second;
		}
		
		glDrawElementsInstanced(GL_TRIANGLES, model.GetRawModel().GetVertexCount(), GL_UNSIGNED_INT, 0, batch.size());
		UnbindTexturedModel();
	}
}

void EntityRenderer::PrepareTexturedModel(TexturedModel model) {
	RawModel rawModel = model.GetRawModel();
	glBindVertexArray(rawModel.GetVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);

	ModelTexture texture = model.GetTexture();
	shader.LoadNumberOfRows(texture.GetNumberOfRows());

	if (texture.GetTransparency()) {
		DisableCulling();
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.GetID());
	shader.LoadFakeLightingVariable(texture.GetFakeLighting());
	shader.LoadShineVariables(texture.GetShineDamper(), texture.GetReflectivity());
}

void EntityRenderer::PrepareInstance(Entity& entity) {
	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(entity.GetPosition(),
		entity.GetRotX(), entity.GetRotY(), entity.GetRotZ(), entity.GetScale());
	shader.LoadTransformationMatrix(transformationMatrix);
	shader.LoadOffset(entity.GetTextureXOffset(), entity.GetTextureYOffset());
}

void EntityRenderer::UnbindTexturedModel() {
	EnableCulling();
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glDisableVertexAttribArray(5);
	glDisableVertexAttribArray(6);
	glDisableVertexAttribArray(7);
	glBindVertexArray(0);
}

void EntityRenderer::EnableCulling() {
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void EntityRenderer::DisableCulling() {
	glDisable(GL_CULL_FACE);
}
