#include "MasterRenderer.h"

const float MasterRenderer::FOV = 45.0f;
const float MasterRenderer::NEAR_PLANE = 0.1f;
const float MasterRenderer::FAR_PLANE = 10000.0f;

const float MasterRenderer::RED = 0.2f;
const float MasterRenderer::GREEN = 0.2f;
const float MasterRenderer::BLUE = 0.2f;

const glm::mat4 MasterRenderer::projectionMatrix = glm::perspective(glm::radians(MasterRenderer::FOV),
(float)(DisplayManager::WIDTH / DisplayManager::HEIGHT) * 2.0f, MasterRenderer::NEAR_PLANE, MasterRenderer::FAR_PLANE);

MasterRenderer* MasterRenderer::instance = nullptr;

void MasterRenderer::CreateInstance(Loader& loader, Camera* camera, std::vector<Entity*>& entities) {
	instance = new MasterRenderer(loader, camera, entities);
}

void MasterRenderer::DestroyInstance() {
	MasterRenderer::GetInstance()->entities.clear();
	MasterRenderer::GetInstance()->CleanUp();
	delete instance;
}

MasterRenderer* MasterRenderer::GetInstance() {
	return instance;
}

MasterRenderer::MasterRenderer(Loader& loader, Camera* camera, std::vector<Entity*>& entities) :
	skyboxRenderer(SkyboxRenderer(loader, projectionMatrix)),
	shadowMapRenderer(ShadowMapMasterRenderer(camera)),
	renderer(EntityRenderer(shader, projectionMatrix)),
	terrainRenderer(TerrainRenderer(terrainShader, projectionMatrix)),
	viewMatrix(Maths::CreateViewMatrix(*camera)),
	frustum(Frustum(&viewMatrix, camera, NEAR_PLANE, FAR_PLANE)),
	camera(camera) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	for (Entity* entity : entities) {
		ProcessEntity(*entity);
	}
}

void MasterRenderer::Render(std::vector<Light*>& lights, Camera& camera, glm::vec4 clipPlane) {
	Prepare();
	shader.Start();
	shader.LoadTerrainSize(Terrain::SIZE / 2.0f);
	shader.LoadClipPlane(clipPlane);
	shader.LoadSkyColour(MasterRenderer::RED, MasterRenderer::GREEN, MasterRenderer::BLUE);
	shader.LoadLights(lights);
	shader.LoadViewMatrix(camera);
	shader.LoadStaticViewMatrix(camera);
	renderer.Render(entities);
	shader.Stop();

	terrainShader.Start();
	terrainShader.LoadShadowDistance(ShadowMapMasterRenderer::SHADOW_DISTANCE);
	terrainShader.LoadShadowMapSize(ShadowMapMasterRenderer::SHADOW_MAP_SIZE);
	terrainShader.LoadTerrainSize(Terrain::SIZE / 2.0f);
	terrainShader.LoadClipPlane(clipPlane);
	terrainShader.LoadSkyColour(MasterRenderer::RED, MasterRenderer::GREEN, MasterRenderer::BLUE);
	terrainShader.LoadLights(lights);
	terrainShader.LoadViewMatrix(camera);
	terrainShader.LoadStaticViewMatrix(camera);
	terrainRenderer.Render(shadowMapRenderer.GetToShadowMapSpaceMatrix());
	terrainShader.Stop();

	//skyboxRenderer.Render(camera, MasterRenderer::RED, MasterRenderer::GREEN, MasterRenderer::BLUE);
}

void MasterRenderer::ProcessEntity(Entity& entity) {
	glm::vec3 position = entity.GetPosition();
	TexturedModel entityModel = entity.GetModel();
	std::vector<Entity*> batch;

	auto iterator = entities.find(entityModel);
	if (iterator != entities.end()) {
		batch = iterator->second;
	}

	if (!batch.empty()) {
		batch.push_back(&entity);
		entities.at(entityModel) = batch;
	}
	else {
		std::vector<Entity*> newBatch;
		newBatch.push_back(&entity);
		entities.insert(std::pair<TexturedModel, std::vector<Entity*>>(entityModel, newBatch));
	}
}

void MasterRenderer::RemoveEntity(Entity* entity) {
	for (auto& ents : entities) {
		auto entis = ents.second;
		auto position = std::find(entis.begin(), entis.end(), entity);
		if (position != entis.end()) {
			entis.erase(position);
			ents.second = entis;
		}
	}
}

void MasterRenderer::RenderScene(std::vector<Entity*>& entities,
	std::vector<Light*>& lights, Camera& camera, glm::vec4 clipPlane) {
	Render(lights, camera, clipPlane);
}

void MasterRenderer::Prepare() {
	glClearColor(MasterRenderer::RED, MasterRenderer::GREEN, MasterRenderer::BLUE, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, GetShadowMapTexture());
}

void MasterRenderer::CleanUp() {
	shader.CleanUp();
	terrainShader.CleanUp();
	shadowMapRenderer.CleanUp();
}

glm::mat4 MasterRenderer::GetProjectionMatrix() {
	return projectionMatrix;
}

unsigned int MasterRenderer::GetShadowMapTexture() {
	return shadowMapRenderer.GetShadowMap();
}

void MasterRenderer::RenderShadowMap(Light* sun) {
	shadowMapRenderer.Render(entities, *sun);
}
