#pragma once

#include <iostream>
#include <vector>
#include <array>
#include <memory>

#include "../models/RawModel.h"
#include "../libraries/stb_image.h"
#include "../textures/TextureData.h"

#include <GLAD/glad.h>
#include "GLFW/glfw3.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

class Loader {
#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF
private:
	std::vector<unsigned int> vaos;
	std::vector<unsigned int> vbos;
	std::vector<unsigned int> textures;

	Loader();
public:
	enum ATTRIBUTE {
		POSITIONS = 0,
		TEXTURES = 1,
		NORMALS = 2,
		TRANSFORMATIONS = 3,
		TEXTURE_OFFSETS = 4
	};

	static Loader& GetInstance() {
		static Loader instance; 
		return instance;
	}

	Loader(Loader const&) = delete;
	void operator=(Loader const&) = delete;

	unsigned int LoadToVAO(const std::vector<float>& positions, 
		const std::vector<float>& textureCoords);

	/**
	 * Creates a VAO and stores the position data of the vertices
	 * into attribute 0 of the VAO.
	 *
	 * @param positions 
	 *		- The 3D positions of each vertex in the geometry.
	 *
	 * @param textureCoords
	 *		- The 2D texture coordinates of an image.
	 *					  
	 * @param indices
	 *		- The indices of the model that we want to store in the VAO. The
	 *		indices indicate how the vertices should be connected together to
	 *		form triangles.
     *
	 * @return The loaded model.
	*/
	std::unique_ptr<RawModel> LoadToVAO(const std::vector<float>& positions, 
		const std::vector<float>& textureCoords,
		const std::vector<float>& normals,
		const std::vector<unsigned int>& indices);


	/**
	* Creates a VAO and stores the position data of the vertices
	* into attribute 0 of the VAO.
	*
	* @param positions
	*		- The 3D positions of each vertex in the geometry.
	*
	* @param textureCoords
	*		- The 2D texture coordinates of an image.
	*
	* @param indices
	*		- The indices of the model that we want to store in the VAO. The
	*		indices indicate how the vertices should be connected together to
	*		form triangles.
	*
	*
	* @param transformationMatrices
	*		- The transformation matrix of each of the entity needed for
	*		multi instaced rendering.
	*
	* @return The loaded model.
	*/
	std::unique_ptr<RawModel> LoadToVAO(const std::vector<float>& positions,
		const std::vector<float>& textureCoords,
		const std::vector<float>& normals,
		const std::vector<unsigned int>& indices,
		const std::vector<glm::mat4>& transformationMatrices,
		const std::vector<float>& textureOffsets);

	std::unique_ptr<RawModel> LoadToVAO(const std::vector<float>& positions,
		const std::vector<float>& textureCoords,
		const std::vector<float>& normals,
		const std::vector<unsigned int>& indices,
		const unsigned int numberOfModels,
		const glm::mat4& transformationMatrix,
		const glm::vec2& textureOffsets);

	/**
	 * Loads cubes to the VAO.
	 *
	 * @param positions
	 *		- The 3D positions of each vertex in the geometry.	
	 *
	 * @param dimensions
	 *		- The value specifying if it loads cubes or quads.
	 *
	 * @return The loaded model.
	*/
	std::unique_ptr<RawModel> LoadToVAO(const std::vector<float>& positions,
		unsigned int dimensions);

	/**
	 * Loads a texture from the file system and returns the texture's ID.
	 *
	 * @param fileName
	 *		- The file path of the image.
	 *
	 * @return The texture ID of the image.
	*/
	unsigned int LoadTexture(const std::string& fileName);

	/**
	 * Deletes all the VAOs and VBOs when the game is closed. VAOs
	 * and VBOs are located in video memory.
	*/
	void CleanUp();

	/**
	 * Loads the textures for the cube map.
	 *
	 * @param textureFiles
	 *		- The texture files for creating the textured cube.
	 *
	 * @return The texture ID of the cube.
	*/
	unsigned int LoadCubeMap(const std::vector<std::string>& textureFile);

private:
	/**
	 * Creates a new VAO and returns its ID. A VAO holds geometry data
	 * that we can render and is physically stored in memory on the GPU,
	 * so that it can be accessed very quickly during rendering.
	 *
	 * Like most objects in OpenGL, the new VAO is created using a "gen"
	 * method which returns the ID of the new VAO. In order to use the VAO
	 * it needs to be made the active VAO. Only one VAO can be active at a
	 * time. To make this VAO the active VAO (so that we can store stuff in
	 * it) we have to bind it
	 *
	 * @return The ID of the newly created VAO
	*/
	unsigned int CreateVAO();

	/**
	 * Stores the position data of the vertices into attribute 0 of the VAO.
	 * To do this the positions must first be stored in a VBO. You can simply
	 * think of a VBO as an array of data that is stored in memory on the GPU
	 * for easy access during rendering.
	 *
	 * Just like with the VAO, we create a new VBO using a "gen" method, and
	 * make it the active VBO (so that we do stuff to it) by binding it.
	 *
	 * We then store the positions data in the active VBO by using the
	 * glBufferData method. We also indicate using GL_STATIC_DRAW that this data
	 * won't need to be changed. If we wanted to edit the positions every frame
	 * (perhaps to animate the quad) then we would use GL_DYNAMIC_DRAW instead.
	 *
	 * We then connect the VBO to the VAO using the glVertexAttribPointer() method.
	 * This needs to know the attribute number of the VAO where we want to put the
	 * data, the number of floats used for each vertex (3 floats in this case, 
	 * because each vertexx has a 3D position, an x, y, and z value), the type of
	 * data (in this case we used floats), the stride and the offset of where the
	 * data starts from.
	 *
	 * Now that we've finished using the VBO we can unbind it. This isn't totally
	 * necessary, but it is good practice to unbind the VBO when we're done using it.
	 *
	 * @param attributeNumber 
	 *		- The number of the attribute of the VAO where the the data is to 
	 *      be stored.
	 *
	 * @param coordinateSize
	 *		- The coordinate size of the data passed in the VBO
	 *
	 * @param data 
	 *		- The geometry data to be stored in the VAO, in this case the positions
	 *		of the vertices.
	*/
	unsigned int StoreDataInAttributeList(unsigned int attributeNumber, 
		unsigned int coordinateSize, const std::vector<float>& data, unsigned int attribDivisor);

	unsigned int StoreMatrixInAttributeList(unsigned int attributeNumber,
		unsigned int coordinateSize, std::vector<float>& data);

	/**
	 * Unbinds the VAO after we're finished using it. If we want to edit or use the
	 * VAO we would have to bind it again first.
	*/
	void UnbindVAO();

	/**
	 * Creates an index buffer, binds the index buffer to the currently active VAO, and then
	 * fills it with our indices.
	 *
	 * The index buffer is different from other data that we might store in the attributes of
	 * the VAO. When we stored the positions we were storing data about each vertex. The
	 * poisitions were "attributes" of each vertex. Data like that is stored in an attribute
	 * list of the VAO.
	 *
	 * The index buffer however does not contain data about each vertex. Instead it tells 
	 * OpenGL how the vertices should be connected. Each VAO can only have one index buffer
	 * associated with it. This is why we don't store the index buffer in a certain attribute
	 * of the VAO; each VAO has one special "slot" for index buffer and simply binding the 
	 * index buffer binds it to the currently active VAO. When the VAO is rendered it will use 
	 * the index buffer that is bound to it.
	 *
	 * This is also why we don't unbind the index buffer, as that would unbind it from the VAO.
	 *
	 * Note that we tell OpenGL that this is an index buffer by using "GL_ELEMENT_ARRAY_BUFFER"
	 * insted of "GL_ARRAY_BUFFER". This is how OpenGL knows to bind it as the index buffer for
	 * the current VAO.
	 *
	 * @param indices
	 *		- The indices stored as integers in a vector
	*/
	void BindIndicesBuffer(const std::vector<unsigned int>& indices);

	/**
	* Stores the data of a texture image into the texture data structure.
	*
	* @param fileName
	*		- The file name of the image.
	*
	* @return The data of the image.
	*/
	TextureData DecodeTextureFile(const std::string& fileName);
};
