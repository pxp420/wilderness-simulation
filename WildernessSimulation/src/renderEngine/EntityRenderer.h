#pragma once
#include "../models/TexturedModel.h"
#include "../entities/Entity.h"
#include "../shaders/StaticShader.h"
#include "../toolbox/Maths.h"
#include "DisplayManager.h"

#include <memory>
#include <vector>
#include <map>

#include <GLAD/glad.h>
#include "GLFW/glfw3.h"

namespace std {
	template<> struct less<TexturedModel> {
		bool operator() (const TexturedModel& lhs, const TexturedModel& rhs) const {
			return lhs.GetTexture().GetID() < rhs.GetTexture().GetID();
		}
	};
}

class EntityRenderer {
private:
	StaticShader shader;
	
	/**
	 * Prepares the model to be rendered.
	 *
	 * @param model
	 *		- The model that will pe rendered.
	*/
	void PrepareTexturedModel(TexturedModel model);

	/**
	 * Unbinds the textured model.
	*/
	void UnbindTexturedModel();

	/**
	 * Prepare all the entities and render all the entities that use that
	 * textured model.
	 *
	 * @param entity
	 *		- The entity that has the model to be rendered.
	*/
	void PrepareInstance(Entity& entity);
public:
	/**
	 * The constructor for the renderer takes in the shader as the parameter
	 * and loads the projection matrix to it.
	 *
	 * @param shader
	 *		- The shader where we want our matrix to be transported to.
	 *
	 * @param projectionMatrix
	 *		- The matrix that is used for perspective view.
	*/
	EntityRenderer(StaticShader& shader, const glm::mat4& projectionMatrix);

	/**
	 * Renders a model to the screen.
	 *
	 * Before we can render a VAO it needs to be made active, and we can do this 
	 * by binding it. We also need to enable the relevant attributes of the VAO, 
	 * which in this case is just attribute 0 where we stored the position data.
	 *
	 * The VAO can then be rendered to the screen using glDrawArrays(). We tell
	 * it what type of shapes to render and the number of vertices that it needs
	 * to render.
	 *
	 * We tell it what type of shapes to render and the number of vertices that it
	 * needs to render. We also tell it what format the index data is in (we used 
	 * ints) and finally we indicate where in the index buffer it should start 
	 * rendering. We want it to start right at the beginning and render everything,
	 * so we put 0.
	 *
	 * After rendering, we unbind the VAO and disable the attribute.
	 *
	 * @param entities
	 *		- The models to be rendered.
	 *
	*/
	void Render(std::map<TexturedModel, std::vector<Entity*>>& entities);

	/**
	* Enables culling so that the textures that cannot be seen from the camera
	* are not rendered.
	*/
	static void EnableCulling();

	/**
	* Disables culling.
	*/
	static void DisableCulling();
};
