#pragma once

#include <vector>

#include "../terrains/Terrain.h"
#include "../shaders/TerrainShader.h"
#include "../models/TexturedModel.h"
#include "../entities/Entity.h"
#include "../toolbox/Frustum.h"

class TerrainRenderer {
private:
	TerrainShader shader;

	/**
	* Prepares the model to be rendered.
	*/
	void PrepareTerrain();

	/**
	* Unbinds the textured model.
	*/
	void UnbindTexturedModel();

	/**
	* Prepare all the entities and render all the entities that use that
	* textured model.
	*
	* @param entity
	*		- The entity that has the model to be rendered.
	*/
	void LoadModelMatrix();

	/**
	 * Binds all the textures for a terrain.
	*/
	void BindTextures();
public:
	/**
	* The constructor for the renderer takes in the shader as the parameter
	* and loads the projection matrix to it.
	*
	* @param shader
	*		- The shader where we want our matrix to be transported to.
	*
	* @param projectionMatrix
	*		- The matrix that is used for perspective view.
	*/
	TerrainRenderer(TerrainShader& shader, const glm::mat4& projectionMatrix);

	/**
	 * Renders all the terrains stored in a vector.
	 *
	 * @param terrains
	 *		- The terrains parsed for rendering.
	 *
	 * @param toShadowSpace
	 *		- The matrix for the shadows.
	*/
	void Render(const glm::mat4& toShadowSpace);
};