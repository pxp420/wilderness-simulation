#include "Loader.h"

Loader::Loader() { }

unsigned int Loader::LoadToVAO(const std::vector<float>& positions, 
	const std::vector<float>& textureCoords) {
	unsigned int vaoID = CreateVAO();
	StoreDataInAttributeList(0, 2, positions, 0);
	StoreDataInAttributeList(1, 2, textureCoords, 0);
	UnbindVAO();
	return vaoID;
}

std::unique_ptr<RawModel> Loader::LoadToVAO(const std::vector<float>& positions, 
	const std::vector<float>& textureCoords,
	const std::vector<float>& normals,
	const std::vector<unsigned int>& indices) {
	int vaoID = CreateVAO();
	BindIndicesBuffer(indices);
	unsigned int* vboIDs = new unsigned int[3];
	vboIDs[ATTRIBUTE::POSITIONS] = StoreDataInAttributeList(0, 3, positions, 0);
	vboIDs[ATTRIBUTE::TEXTURES] = StoreDataInAttributeList(1, 2, textureCoords, 0);
	vboIDs[ATTRIBUTE::NORMALS] = StoreDataInAttributeList(2, 3, normals, 0);
	UnbindVAO();
	return std::unique_ptr<RawModel>(new RawModel(vaoID, vboIDs, indices.size()));
}

std::unique_ptr<RawModel> Loader::LoadToVAO(const std::vector<float>& positions,
	const std::vector<float>& textureCoords,
	const std::vector<float>& normals,
	const std::vector<unsigned int>& indices,
	const std::vector<glm::mat4>& transformationMatrices,
	const std::vector<float>& textureOffsets) {
	int vaoID = CreateVAO();
	BindIndicesBuffer(indices);
	unsigned int* vboIDs = new unsigned int[5];
	vboIDs[ATTRIBUTE::POSITIONS] = StoreDataInAttributeList(0, 3, positions, 0);
	vboIDs[ATTRIBUTE::TEXTURES] = StoreDataInAttributeList(1, 2, textureCoords, 0);
	vboIDs[ATTRIBUTE::NORMALS] = StoreDataInAttributeList(2, 3, normals, 0);

	std::vector<float> transformationMatrixData;
	for (glm::mat4 transformationMatrix : transformationMatrices) {
		for (unsigned int i = 0; i < 4; i++) {
			for (unsigned int j = 0; j < 4; j++) {
				transformationMatrixData.emplace_back(transformationMatrix[i][j]);
			}
		}
	}
	vboIDs[ATTRIBUTE::TRANSFORMATIONS] = StoreMatrixInAttributeList(3, 4, transformationMatrixData);
	vboIDs[ATTRIBUTE::TEXTURE_OFFSETS] = StoreDataInAttributeList(7, 2, textureOffsets, 1);

	UnbindVAO();
	return std::unique_ptr<RawModel>(new RawModel(vaoID, vboIDs, indices.size()));
}

std::unique_ptr<RawModel> Loader::LoadToVAO(const std::vector<float>& positions,
	const std::vector<float>& textureCoords,
	const std::vector<float>& normals,
	const std::vector<unsigned int>& indices,
	const unsigned int numberOfModels,
	const glm::mat4& transformationMatrix,
	const glm::vec2& textureOffsets) {
	int vaoID = CreateVAO();
	BindIndicesBuffer(indices);
	unsigned int* vboIDs = new unsigned int[5];
	vboIDs[ATTRIBUTE::POSITIONS] = StoreDataInAttributeList(0, 3, positions, 0);
	vboIDs[ATTRIBUTE::TEXTURES] = StoreDataInAttributeList(1, 2, textureCoords, 0);
	vboIDs[ATTRIBUTE::NORMALS] = StoreDataInAttributeList(2, 3, normals, 0);

	std::vector<float> transformationMatrixData;
	for (int i = 0; i < numberOfModels; i++) {
		for (unsigned int i = 0; i < 4; i++) {
			for (unsigned int j = 0; j < 4; j++) {
				transformationMatrixData.emplace_back(transformationMatrix[i][j]);
			}
		}
	}

	std::vector<float> textureOffsetsData;
	for (int i = 0; i < numberOfModels; i++) {
		textureOffsetsData.emplace_back(textureOffsets.x);
		textureOffsetsData.emplace_back(textureOffsets.y);
	}

	vboIDs[ATTRIBUTE::TRANSFORMATIONS] = StoreMatrixInAttributeList(3, 4, transformationMatrixData);
	vboIDs[ATTRIBUTE::TEXTURE_OFFSETS] = StoreDataInAttributeList(7, 2, textureOffsetsData, 1);

	UnbindVAO();
	return std::unique_ptr<RawModel>(new RawModel(vaoID, vboIDs, indices.size()));
}

std::unique_ptr<RawModel> Loader::LoadToVAO(const std::vector<float>& positions,
	unsigned int dimensions) {
	int vaoID = CreateVAO();

	unsigned int* vboIDs = new unsigned int[1];
	vboIDs[ATTRIBUTE::POSITIONS] = StoreDataInAttributeList(0, dimensions, positions, 0);
	UnbindVAO();
	return std::unique_ptr<RawModel>(new RawModel(vaoID, vboIDs, positions.size() / dimensions));
}

unsigned int Loader::LoadTexture(const std::string& fileName) {
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(("res/textures/" + fileName).c_str(), &width, &height, &nrComponents, 0);

	if (data) {
		unsigned int format;
		if (nrComponents == 1) {
			format = GL_RED;
		}
		else if (nrComponents == 3) {
			format = GL_RGB;
		}
		else if (nrComponents == 4) {
			format = GL_RGBA;
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);

		stbi_image_free(data);
	}
	else {
		std::cout << "Texture failed to load at path: " << fileName.c_str() << std::endl;
		stbi_image_free(data);
	}

	textures.emplace_back(textureID);
	return textureID;
}

void Loader::CleanUp() {
	for (const unsigned int& vao : vaos) {
		glDeleteVertexArrays(1, &vao);
	}
	for (const unsigned int& vbo : vbos) {
		glDeleteBuffers(1, &vbo);
	}
	for (const unsigned int& texture : textures) {
		glDeleteTextures(1, &texture);
	}
}

unsigned int Loader::LoadCubeMap(const std::vector<std::string>& textureFiles) {
	unsigned int texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

	for (unsigned int i = 0; i < textureFiles.size(); i++) {
		TextureData data = DecodeTextureFile(textureFiles[i]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, data.GetWidth(), data.GetHeight(),
			0, GL_RGB, GL_UNSIGNED_BYTE, data.GetBuffer());
		stbi_image_free(data.GetBuffer());
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	textures.emplace_back(texID);
	return texID;
}

unsigned int Loader::CreateVAO() {
	unsigned int vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	return vaoID;
}

unsigned int Loader::StoreDataInAttributeList(unsigned int attributeNumber, 
	unsigned int coordinateSize, const std::vector<float>& data, unsigned int attribDivisor) {
	unsigned int vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data.at(0), GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, false, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribDivisor(attributeNumber, attribDivisor);

	return vboID;
}

unsigned int Loader::StoreMatrixInAttributeList(unsigned int attributeNumber,
	unsigned int coordinateSize, std::vector<float>& data) {
	unsigned int vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data.at(0), GL_STATIC_DRAW);

	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, false,
		sizeof(GLfloat) * coordinateSize * coordinateSize, (void*)(sizeof(float) * coordinateSize * 0));
	glVertexAttribPointer(attributeNumber + 1, coordinateSize, GL_FLOAT, false,
		sizeof(GLfloat) * coordinateSize * coordinateSize, (void*)(sizeof(float) * coordinateSize * 1));
	glVertexAttribPointer(attributeNumber + 2, coordinateSize, GL_FLOAT, false,
		sizeof(GLfloat) * coordinateSize * coordinateSize, (void*)(sizeof(float) * coordinateSize * 2));
	glVertexAttribPointer(attributeNumber + 3, coordinateSize, GL_FLOAT, false,
		sizeof(GLfloat) * coordinateSize * coordinateSize, (void*)(sizeof(float) * coordinateSize * 3));

	glVertexAttribDivisor(attributeNumber, 1);
	glVertexAttribDivisor(attributeNumber + 1, 1);
	glVertexAttribDivisor(attributeNumber + 2, 1);
	glVertexAttribDivisor(attributeNumber + 3, 1);

	return vboID;
}

void Loader::UnbindVAO() {
	glBindVertexArray(0);
}

void Loader::BindIndicesBuffer(const std::vector<unsigned int>& indices) {
	unsigned int vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices.at(0), GL_STATIC_DRAW);
}

TextureData Loader::DecodeTextureFile(const std::string& fileName) {
	int width, height, nrComponents;
	unsigned char* data = stbi_load(("res/textures/" + fileName + ".png").c_str(), &width, &height, &nrComponents, 0);

	if (data == nullptr) {
		std::cout << "Texture failed to load at path: " << fileName.c_str() << std::endl;
		stbi_image_free(data);
	}

	return TextureData(data, width, height);
}
