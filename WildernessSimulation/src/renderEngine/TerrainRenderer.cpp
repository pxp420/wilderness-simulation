#include "TerrainRenderer.h"

TerrainRenderer::TerrainRenderer(TerrainShader& shader, const glm::mat4& projectionMatrix) :
	shader(shader) {
	shader.Start();
	shader.LoadShadowDistance(ShadowMapMasterRenderer::SHADOW_DISTANCE);
	shader.LoadProjectionMatrix(projectionMatrix);
	shader.ConnectTextureUnits();
	shader.Stop();
}

void TerrainRenderer::Render(const glm::mat4& toShadowSpace) {
	shader.LoadToShadowSpaceMatrix(toShadowSpace);
	PrepareTerrain();
	LoadModelMatrix();
	glDrawElements(GL_TRIANGLES, Terrain::GetInstance()->GetModel().GetVertexCount(), GL_UNSIGNED_INT, 0);
	UnbindTexturedModel();
}

void TerrainRenderer::PrepareTerrain() {
	RawModel rawModel = Terrain::GetInstance()->GetModel();
	glBindVertexArray(rawModel.GetVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	BindTextures();

	shader.LoadShineVariables(1, 0);
}

void TerrainRenderer::LoadModelMatrix() {
	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		glm::vec3(Terrain::GetInstance()->GetX(), 0.0f, Terrain::GetInstance()->GetZ()), 0.0f, 0.0f, 0.0f, 1.0f);
	shader.LoadTransformationMatrix(transformationMatrix);
}

void TerrainRenderer::BindTextures() {
	TerrainTexturePack texturePack = Terrain::GetInstance()->GetTexturePack();

	glActiveTexture(GL_TEXTURE0);
	TerrainTexture mudTexture = texturePack.GetMudTexture();
	glBindTexture(GL_TEXTURE_2D, mudTexture.GetTextureID());

	glActiveTexture(GL_TEXTURE1);
	TerrainTexture grassTexture = texturePack.GetGrassTexture();
	glBindTexture(GL_TEXTURE_2D, grassTexture.GetTextureID());

	glActiveTexture(GL_TEXTURE2);
	TerrainTexture flowerGrassTexture = texturePack.GetFlowerGrassTexture();
	glBindTexture(GL_TEXTURE_2D, flowerGrassTexture.GetTextureID());

	glActiveTexture(GL_TEXTURE3);
	TerrainTexture dirtTexture = texturePack.GetDirtTexture();
	glBindTexture(GL_TEXTURE_2D, dirtTexture.GetTextureID());

	glActiveTexture(GL_TEXTURE4);
	TerrainTexture rockTexture = texturePack.GetRockTexture();
	glBindTexture(GL_TEXTURE_2D, rockTexture.GetTextureID());

	glActiveTexture(GL_TEXTURE5);
	TerrainTexture blendMapTexture = Terrain::GetInstance()->GetBlendMap();
	glBindTexture(GL_TEXTURE_2D, blendMapTexture.GetTextureID());

}

void TerrainRenderer::UnbindTexturedModel() {
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}
