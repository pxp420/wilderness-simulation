#pragma once

#include <GLAD/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <chrono>

class DisplayManager {
public:
	static const unsigned int WIDTH;
	static const unsigned int HEIGHT;
	static const unsigned int FPS_CAP;

	static float lastFrameTime;
	static float delta;

	static const char* TITLE;
	static GLFWwindow* window;

	/**
	 * Creates a display window on which we can render our simulation. The dimensions
	 * of the window are determined by setting the display mode. By using "glViewport"
	 * we tell OpenGL which part of the window we want to render our simulation onto. 
	 * We indicated that we want to use the entire window.
	*/
	static void CreateDisplay();

	/**
	 * This method is used to update the display at the end of every frame. When we
	 * have set up a rendering process this method will display whatever we've been
	 * rendering onto the screen. 
	*/
	static void UpdateDisplay();

	/**
	 * This method is used to check if there are any requests to close the displayed
	 * window
	*/
	static bool IsCloseRequested();

	/**
	 * This closes the window when the simulation is closed.
	*/
	static void CloseDisplay();

	/**
	 * Gets the delta value.
	 *
	 * @return The delta value.
	*/
	static float GetFrameTimeSeconds();

	/**
	 * Gets the current time at the exact moment.
	 *
	 * @return The current time.
	*/
	static float GetCurrTime();
};
