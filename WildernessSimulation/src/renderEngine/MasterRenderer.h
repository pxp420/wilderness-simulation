#pragma once

#include <map>
#include <iterator>
#include <vector>
#include <algorithm>

#include "../shaders/StaticShader.h"
#include "../shaders/TerrainShader.h"
#include "EntityRenderer.h"
#include "TerrainRenderer.h"
#include "../skybox/SkyboxRenderer.h"
#include "../shadows/ShadowMapMasterRenderer.h"
#include "../toolbox/MousePicker.h"

class MasterRenderer {
private:
	glm::mat4 viewMatrix;

	Frustum frustum;
	Camera* camera;

	StaticShader shader;
	TerrainShader terrainShader;
	EntityRenderer renderer;
	TerrainRenderer terrainRenderer;

	std::map<TexturedModel, std::vector<Entity*>> entities;

	SkyboxRenderer skyboxRenderer;
	ShadowMapMasterRenderer shadowMapRenderer;

	static MasterRenderer* instance;
public:
	const static float RED;
	const static float GREEN;
	const static float BLUE;

	const static float FOV;
	const static float NEAR_PLANE;
	const static float FAR_PLANE;

	const static glm::mat4 projectionMatrix;

	static void CreateInstance(Loader& loader, Camera* camera, std::vector<Entity*>& entities);

	static void DestroyInstance();

	static MasterRenderer* GetInstance();

	MasterRenderer(MasterRenderer const&) = delete;
	void operator=(MasterRenderer const&) = delete;

	MasterRenderer(Loader& loader, Camera* camera, std::vector<Entity*>& entities);

	/**
	 * Renders the models with the camera and the light.
	 *
	 * @param sun
	 *		- The light source in the world space.
	 *
	 * @param camera
	 *		- The camera object.
	*/
	void Render(std::vector<Light*>& lights, Camera& camera, glm::vec4 clipPlane);

	/**
	 * Renders all the entities, terrain and lights to the screen.
	 *
	 * @param terrains
	 *		- The terrains to be rendered.
	 *
	 * @param lights
	 *		- The lights to be rendered.
	 *
	 * @param camera
	 *		- The in-simulation camera.
	*/
	void RenderScene(std::vector<Entity*>& entities,
		std::vector<Light*>& lights, Camera& camera, glm::vec4 clipPlane);


	/**
	 * Gets the entity texture to see if it has already been loaded.
	 *
	 * @param entity
	 *		- The entity to be processed.
	*/
	void ProcessEntity(Entity& entity);

	/**
	 * Removes the entity that doesn't need to be rendered anymore.
	 *
	 * @param entity
	 *		- The entity to be removed.
	*/
	void RemoveEntity(Entity* entity);

	/**
	* This method must be called each frame, before any rendering is carried
	* out. It basically clears the screen of everything that was rendered
	* last frame (using the glClear() method). The glClearColor() method
	* determines the colour that it uses to clear the screen. In this example,
	* it makes the entire screen red at the start of each frame.
	*/
	void Prepare();

	/**
	* Detaches the shaders from the program and deletes them. After that, deletes the program
	* altogether so that it doesn't stay in memory.
	*/
	void CleanUp();

	/**
	 * Retrieves the projection matrix.
	 *
	 * @return The projectino matrix.
	*/
	glm::mat4 GetProjectionMatrix();

	/**
	 * Get the shadow map texture seen from the sun.
	 *
	 * @return The texture ID of the shadow map.
	*/
	unsigned int GetShadowMapTexture();

	/**
	 * Renders all the shadows for the entities.
	 *
	 * @param sun
	 *		- The light in the world.
	*/
	void RenderShadowMap(Light* sun);
};
