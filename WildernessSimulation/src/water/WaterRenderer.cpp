#include "WaterRenderer.h"

const std::string WaterRenderer::DUDV_MAP = "waterDUDV.png";
const std::string WaterRenderer::NORMAL_MAP = "normalMap.png";

const float WaterRenderer::WAVE_SPEED = 0.03f;

WaterRenderer::WaterRenderer(Loader& loader, WaterShader& shader, glm::mat4 projectionMatrix,
	WaterFrameBuffers& fbos) :
	shader(shader), fbos(fbos), moveFactor(0.0f) {
	dudvTexture = loader.LoadTexture(DUDV_MAP);
	normalMapTexture = loader.LoadTexture(NORMAL_MAP);
	shader.Start();
	shader.LoadPlaneFrustum(MasterRenderer::NEAR_PLANE, MasterRenderer::FAR_PLANE);
	shader.ConnectTextureUnits();
	shader.LoadProjectionMatrix(projectionMatrix);
	shader.Stop();
	SetUpVAO(loader);
}

void WaterRenderer::Render(std::vector<WaterTile>& water, Camera* camera, Light* sun) {
	PrepareRender(camera, sun);
	for (WaterTile tile : water) {
		glm::mat4 modelMatrix = Maths::CreateTransformationMatrix(
			glm::vec3(tile.GetX(), tile.GetHeight(), tile.GetZ()), tile.GetRotX(), tile.GetRotY(), tile.GetRotZ(),
			WaterTile::TILE_SIZE);
		shader.LoadModelMatrix(modelMatrix);
		glDrawArrays(GL_TRIANGLES, 0, quad->GetVertexCount());
	}
	Unbind();
}

void WaterRenderer::PrepareRender(Camera* camera, Light* sun) {
	shader.Start();
	shader.LoadViewMatrix(*camera);
	moveFactor += WAVE_SPEED * DisplayManager::GetFrameTimeSeconds();
	moveFactor = std::fmod(moveFactor, 1.0f);
	shader.LoadMoveFactor(moveFactor);
	shader.LoadLight(sun);
	glBindVertexArray(quad->GetVaoID());
	glEnableVertexAttribArray(0);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fbos.GetReflectionTexture());

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, fbos.GetRefractionTexture());

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, dudvTexture);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, normalMapTexture);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, fbos.GetRefractionDepthTexture());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void WaterRenderer::Unbind() {
	glDisable(GL_BLEND);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
	shader.Stop();
}

void WaterRenderer::SetUpVAO(Loader& loader) {
	std::vector<float> vertices = {
		-1.0f, -1.0f,
		-1.0f,  1.0f,
		 1.0f, -1.0f,
		 1.0f, -1.0f,
		-1.0f,  1.0f,
		 1.0f,  1.0f
	};

	quad = loader.LoadToVAO(vertices, 2);
}
