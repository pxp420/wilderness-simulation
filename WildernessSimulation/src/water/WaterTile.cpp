#include "WaterTile.h"

const float WaterTile::TILE_SIZE = 1800.0f;

WaterTile::WaterTile(float centerX, float centerZ, float height) :
	x(centerX), z(centerZ), height(height), rotX(0.0f), rotY(0.0f), rotZ(0.0f) {}

WaterTile::WaterTile(float centerX, float centerZ, float height, float rotX, float rotY, float rotZ) :
	x(centerX), z(centerZ), height(height), rotX(rotX), rotY(rotY), rotZ(rotZ) {}

float WaterTile::GetHeight() {
	return height;
}

float WaterTile::GetX() {
	return x;
}

float WaterTile::GetZ() {
	return z;
}

float WaterTile::GetRotX() {
	return rotX;
}

float WaterTile::GetRotY() {
	return rotY;
}

float WaterTile::GetRotZ() {
	return rotZ;
}