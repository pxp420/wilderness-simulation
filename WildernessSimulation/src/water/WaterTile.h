#pragma once

class WaterTile {
private:
	float height;
	float x, z;
	float rotX, rotY, rotZ;
public:
	const static float TILE_SIZE;

	/**
	 * Constructor for a water tile.
	 *
	 * @param centerX
	 *		- The center of the water tile in the x coordinate.
	 *
	 * @param centerZ
	 *		- The center of the water tile in the z coordinate.
	 *
	 * @param height
	 *		- The height where the water tile will be placed on.
	*/
	WaterTile(float centerX, float centerZ, float height);

	/**
	* Constructor for a water tile.
	*
	* @param centerX
	*		- The center of the water tile in the x coordinate.
	*
	* @param centerZ
	*		- The center of the water tile in the z coordinate.
	*
	* @param height
	*		- The height where the water tile will be placed on.
	*
	* @param rotX
	*		- The rotation on the x axis.
	*
	* @param rotY
	*		- The rotation on the y axis.
	*
	* @param rotZ
	*		- The rotation on the z axis.
	*/
	WaterTile(float centerX, float centerZ, float height, float rotX, float rotY, float rotZ);

	/**
	 * Gets the height the tile is positioned at.
	 *
	 * @return The height of tile's position.
	*/
	float GetHeight();

	/**
	 * Gets the center of the water tile in the x coordinate.
	 *
	 * @return The center of the water tile in the x coordinate.
	*/
	float GetX();

	/**
	* Gets the center of the water tile in the z coordinate.
	*
	* @return The center of the water tile in the z coordinate.
	*/
	float GetZ();

	/**
	 * Gets the rotation on the x axis.
	 *
	 * @return Rotation on the x axis.
	*/
	float GetRotX();

	/**
	* Gets the rotation on the x axis.
	*
	* @return Rotation on the x axis.
	*/
	float GetRotY();

	/**
	* Gets the rotation on the x axis.
	*
	* @return Rotation on the x axis.
	*/
	float GetRotZ();
};
