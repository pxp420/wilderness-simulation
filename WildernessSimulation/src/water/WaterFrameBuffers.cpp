#include "WaterFrameBuffers.h"
#include "../renderEngine/DisplayManager.h"

const unsigned int WaterFrameBuffers::REFLECTION_WIDTH = 320;
const unsigned int WaterFrameBuffers::REFLECTION_HEIGHT = 180;

const unsigned int WaterFrameBuffers::REFRACTION_WIDTH = 1280;
const unsigned int WaterFrameBuffers::REFRACTION_HEIGHT = 720;

WaterFrameBuffers::WaterFrameBuffers() {
	InitialiseReflectionFrameBuffer();
	InitialiseRefractionFrameBuffer();
}

void WaterFrameBuffers::CleanUp() {
	glDeleteFramebuffers(1, &reflectionFrameBuffer);
	glDeleteTextures(1, &reflectionTexture);
	glDeleteRenderbuffers(1, &reflectionDepthBuffer);
	glDeleteFramebuffers(1, &refractionFrameBuffer);
	glDeleteTextures(1, &refractionTexture);
	glDeleteTextures(1, &refractionDepthTexture);
}

void WaterFrameBuffers::BindReflectionFrameBuffer() {
	BindFrameBuffer(reflectionFrameBuffer, REFLECTION_WIDTH, REFLECTION_HEIGHT);
}

void WaterFrameBuffers::BindRefractionFrameBuffer() {
	BindFrameBuffer(refractionFrameBuffer, REFRACTION_WIDTH, REFRACTION_HEIGHT);
}

void WaterFrameBuffers::UnbindCurrentFrameBuffer() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, DisplayManager::WIDTH, DisplayManager::HEIGHT);
}

unsigned int WaterFrameBuffers::GetReflectionTexture() {
	return reflectionTexture;
}

unsigned int WaterFrameBuffers::GetRefractionTexture() {
	return refractionTexture;
}

unsigned int WaterFrameBuffers::GetRefractionDepthTexture() {
	return refractionDepthTexture;
}

void WaterFrameBuffers::InitialiseReflectionFrameBuffer() {
	reflectionFrameBuffer = CreateFrameBuffer();
	reflectionTexture = CreateTextureAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT);
	reflectionDepthBuffer = CreateDepthBufferAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT);
	UnbindCurrentFrameBuffer();
}

void WaterFrameBuffers::InitialiseRefractionFrameBuffer() {
	refractionFrameBuffer = CreateFrameBuffer();
	refractionTexture = CreateTextureAttachment(REFRACTION_WIDTH, REFRACTION_HEIGHT);
	refractionDepthTexture = CreateDepthTextureAttachment(REFRACTION_WIDTH, REFRACTION_HEIGHT);
	UnbindCurrentFrameBuffer();
}

void WaterFrameBuffers::BindFrameBuffer(unsigned int frameBuffer, unsigned int width, unsigned int height) {
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glViewport(0, 0, width, height);
}

unsigned int WaterFrameBuffers::CreateFrameBuffer() {
	unsigned int frameBuffer;
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	return frameBuffer;
}

unsigned int WaterFrameBuffers::CreateTextureAttachment(unsigned int width, unsigned int height) {
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

	return texture;
}

unsigned int WaterFrameBuffers::CreateDepthTextureAttachment(unsigned int width, unsigned int height) {
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, 
		GL_DEPTH_COMPONENT, GL_FLOAT, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);

	return texture;
}

unsigned int WaterFrameBuffers::CreateDepthBufferAttachment(unsigned int width, unsigned int height) {
	unsigned int depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER, depthBuffer);

	return depthBuffer;
}
