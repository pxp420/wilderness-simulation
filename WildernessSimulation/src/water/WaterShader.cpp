#include "WaterShader.h"

const std::string WaterShader::VERTEX_FILE = "src/water/waterVertex.shader";
const std::string WaterShader::FRAGMENT_FILE = "src/water/waterFragment.shader";

const std::map<unsigned int, std::string> WaterShader::ATTRIBUTES = {
	{ 0, "position" }
};

WaterShader::WaterShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES), rotation(0) {
	GetAllUniformLocations();
}

void WaterShader::LoadProjectionMatrix(glm::mat4 projection) {
	ShaderProgram::LoadMatrix(location_projectionMatrix, projection);
}

void WaterShader::LoadViewMatrix(Camera& camera) {
	glm::mat4 viewMatrix = Maths::CreateViewMatrix(camera);
	ShaderProgram::LoadMatrix(location_viewMatrix, viewMatrix);
	ShaderProgram::LoadVector(location_cameraPosition, camera.GetPosition());
}

void WaterShader::LoadModelMatrix(glm::mat4 modelMatrix) {
	ShaderProgram::LoadMatrix(location_modelMatrix, modelMatrix);
}

void WaterShader::LoadMoveFactor(float factor) {
	ShaderProgram::LoadFloat(location_moveFactor, factor);
}

void WaterShader::LoadLight(Light* sun) {
	ShaderProgram::LoadVector(location_lightColour, sun->GetColour());
	ShaderProgram::LoadVector(location_lightPosition, sun->GetPosition());
}

void WaterShader::LoadPlaneFrustum(float nearPlane, float farPlane) {
	ShaderProgram::LoadFloat(location_nearPlane, nearPlane);
	ShaderProgram::LoadFloat(location_farPlane, farPlane);
}

void WaterShader::ConnectTextureUnits() {
	ShaderProgram::LoadInt(location_reflectionTexture, 0);
	ShaderProgram::LoadInt(location_refractionTexture, 1);
	ShaderProgram::LoadInt(location_dudvMap, 2);
	ShaderProgram::LoadInt(location_normalMap, 3);
	ShaderProgram::LoadInt(location_depthMap, 4);
}

void WaterShader::GetAllUniformLocations() {
	location_projectionMatrix = ShaderProgram::GetUniformLocation("projectionMatrix");
	location_viewMatrix = ShaderProgram::GetUniformLocation("viewMatrix");
	location_modelMatrix = ShaderProgram::GetUniformLocation("modelMatrix");
	location_reflectionTexture = ShaderProgram::GetUniformLocation("reflectionTexture");
	location_refractionTexture = ShaderProgram::GetUniformLocation("refractionTexture");
	location_dudvMap = ShaderProgram::GetUniformLocation("dudvMap");
	location_moveFactor = ShaderProgram::GetUniformLocation("moveFactor");
	location_cameraPosition = ShaderProgram::GetUniformLocation("cameraPosition");
	location_normalMap = ShaderProgram::GetUniformLocation("normalMap");
	location_depthMap = ShaderProgram::GetUniformLocation("depthMap");
	location_lightColour = ShaderProgram::GetUniformLocation("lightColour");
	location_lightPosition = ShaderProgram::GetUniformLocation("lightPosition");
	location_nearPlane = ShaderProgram::GetUniformLocation("nearPlane");
	location_farPlane = ShaderProgram::GetUniformLocation("farPlane");
}
