#pragma once

#include <vector>

#include "WaterShader.h"
#include "WaterTile.h"
#include "WaterFrameBuffers.h"
#include "../renderEngine/MasterRenderer.h"
#include "../models/RawModel.h"

class WaterRenderer {
private:
	const static std::string DUDV_MAP;
	const static std::string NORMAL_MAP;

	const static float WAVE_SPEED;

	std::unique_ptr<RawModel> quad;
	WaterShader shader;
	WaterFrameBuffers fbos;

	unsigned int dudvTexture;
	unsigned int normalMapTexture;

	float moveFactor;
public:
	/**
	 * Constructor for the water rendering on the screen.
	 *
	 * @param loader
	 *		- The loader needed to load the quad on the screen.
	 *
	 * @param shader
	 *		- The shader that passes information to the GPU.
	 *
	 * @param projectionMatrix
	 *		- The matrix for projection on the screen.
	 *
	 * @param fbos
	 *		- The frame buffers for the water reflect/refract images.
	*/
	WaterRenderer(Loader& loader, WaterShader& shader, glm::mat4 projectionMatrix, WaterFrameBuffers& fbos);

	/**
	 * Render the water tile on the screen.
	 *
	 * @param water
	 *		- The water tile to be rendered.
	 *
	 * @param camera
	 *		- The camera needed for the view matrix.
	 *
	 * @param sun
	 *		- The main source of light.
	 */
	void Render(std::vector<WaterTile>& water, Camera* camera, Light* sun);
private:
	/**
	 * Prepare to render the water tile by starting the shader.
	 *
	 * @param camera
	 *		- The camera needed for the view matrix.
	 *
	 * @param sun
	 *		- The main source of light.
	*/
	void PrepareRender(Camera* camera, Light* sun);

	/**
	 * Unbinds the VAO and stops the shader.
	*/
	void Unbind();

	/**
	 * Create a VAO that stores the position and the model
	 * of the water tiled to be rendered.
	 *
	 * @param loader
	 *		- The loader needed to load the quad on the screen.
	*/
	void SetUpVAO(Loader& loader);
};
