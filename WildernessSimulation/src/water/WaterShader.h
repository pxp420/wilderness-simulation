#pragma once

#pragma once

#include "../shaders/ShaderProgram.h"
#include "../entities/Camera.h"
#include "../entities/Light.h"

class WaterShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_modelMatrix;
	unsigned int location_projectionMatrix;
	unsigned int location_viewMatrix;
	unsigned int location_reflectionTexture;
	unsigned int location_refractionTexture;
	unsigned int location_dudvMap;
	unsigned int location_moveFactor;
	unsigned int location_cameraPosition;
	unsigned int location_normalMap;
	unsigned int location_depthMap;
	unsigned int location_lightColour;
	unsigned int location_lightPosition;
	unsigned int location_nearPlane;
	unsigned int location_farPlane;

	float rotation;
public:
	/**
	* Calls the parent constructor to create the program
	*/
	WaterShader();

	/**
	* Loads the transformation matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadProjectionMatrix(glm::mat4 projection);

	/**
	* Loads the view matrix into a uniform.
	*
	* @param camera
	*		- The camera needed to create the view matrix.
	*/
	void LoadViewMatrix(Camera& camera);

	/**
	 * Loads the model matrix into a uniform.
	 *
	 * @param modelMatrix
	 *		- The matrix passed out to the shaders.
	*/
	void LoadModelMatrix(glm::mat4 modelMatrix);

	/**
	 * Loads the move factor value into a uniform.
	 *
	 * @param factor
	 *		- The move factor of the waves.
	*/
	void LoadMoveFactor(float factor);

	/**
	 * Loads the light source into a uniform.
	 *
	 * @param sun
	 *		- The main source of light.
	*/
	void LoadLight(Light* sun);

	/**
	 * Loads the near and far plane of the perspective made by the camera.
	 *
	 * @param nearPlane
	 *		- The near plane.
	 *
	 * @param farPlane
	 *		- The far plane.
	*/
	void LoadPlaneFrustum(float nearPlane, float farPlane);

	/**
	* Loads all the textures for the terrain in their corresponding
	* units.
	*/
	void ConnectTextureUnits();
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};