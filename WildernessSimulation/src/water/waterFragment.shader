#version 330 core

in vec4 clipSpace;
in vec2 textureCoords;
in vec3 toCameraVector;
in vec3 fromLightVector;

out vec4 out_Color;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;

uniform sampler2D dudvMap;
uniform sampler2D normalMap;
uniform sampler2D depthMap;

uniform vec3 lightColour;

uniform float moveFactor;
uniform float nearPlane;
uniform float farPlane;

const float waveStrength = 0.02f;
const float shineDamper = 65.0f;
const float reflectivity = 0.6f;

void main(void) {
	vec2 ndc = (clipSpace.xy / clipSpace.w) / 2.0f + 0.5f;

	vec2 reflectTexCoords = vec2(ndc.x, -ndc.y);
	vec2 refractTexCoords = vec2(ndc.x, ndc.y);

	float depth = texture(depthMap, refractTexCoords).r;
	float floorDistance = 2.0f * nearPlane * farPlane / (farPlane + nearPlane - (2.0f * depth - 1.0f) * (farPlane - nearPlane));
	depth = gl_FragCoord.z;
	float waterDistance = 2.0f * nearPlane * farPlane / (farPlane + nearPlane - (2.0f * depth - 1.0f) * (farPlane - nearPlane));
	float waterDepth = floorDistance - waterDistance;

	vec2 distortedTexCoords = texture(dudvMap, vec2(textureCoords.x + moveFactor, textureCoords.y)).rg * 0.1;
	distortedTexCoords = textureCoords + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
	vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg * 2.0 - 1.0) * waveStrength * clamp(waterDepth / 20.0f, 0.0f, 1.0f);

	reflectTexCoords += totalDistortion;
	reflectTexCoords.x = clamp(reflectTexCoords.x, 0.001f, 0.999f);
	reflectTexCoords.y = clamp(reflectTexCoords.y, -0.999f, -0.001f);

	refractTexCoords += totalDistortion;
	refractTexCoords = clamp(refractTexCoords, 0.001f, 0.999f);

	vec4 reflectColour = texture(reflectionTexture, reflectTexCoords);
	vec4 refractColour = texture(refractionTexture, refractTexCoords);

	vec4 normalMapColour = texture(normalMap, distortedTexCoords);
	vec3 normal = vec3(normalMapColour.r * 2.0f - 1.0f, normalMapColour.b * 3.0f, normalMapColour.g * 2.0f - 1.0f);
	normal = normalize(normal);

	vec3 viewVector = normalize(toCameraVector);
	float refractiveFactor = dot(viewVector, normal);
	refractiveFactor = pow(refractiveFactor, 0.5f);
	refractiveFactor = clamp(refractiveFactor, 0.0f, 1.0f);

	vec3 reflectedLight = reflect(normalize(fromLightVector), normal);
	float specular = max(dot(reflectedLight, viewVector), 0.0f);
	specular = pow(specular, shineDamper);
	vec3 specularHighlights = lightColour * specular * reflectivity * clamp(waterDepth / 5.0f, 0.0f, 1.0f);;

	out_Color = mix(reflectColour, refractColour, refractiveFactor);
	out_Color = mix(out_Color, vec4(0.0f, 0.3f, 0.5f, 1.0f), 0.5f) + vec4(specularHighlights, 0.0f);
	out_Color.a = clamp(waterDepth / 3.0f, 0.0f, 1.0f);
}
