#pragma once

#include <GLAD/glad.h>
#include <GLFW/glfw3.h>

class WaterFrameBuffers {
private:
	const static unsigned int REFLECTION_HEIGHT;
	const static unsigned int REFRACTION_HEIGHT;

	unsigned int reflectionFrameBuffer;
	unsigned int reflectionTexture;
	unsigned int reflectionDepthBuffer;

	unsigned int refractionFrameBuffer;
	unsigned int refractionTexture;
	unsigned int refractionDepthTexture;
protected:
	const static unsigned int REFLECTION_WIDTH;
	const static unsigned int REFRACTION_WIDTH;
public:
	/**
	 * Initialises the buffers for the refraction and reflection images.
	*/
	WaterFrameBuffers();

	/**
	 * Delete all the textures that are not used anymore from memory.
	*/
	void CleanUp();

	/**
	 * Binds the reflection frame buffer.
	*/
	void BindReflectionFrameBuffer();

	/**
	 * Binds the refraction frame buffer.
	*/
	void BindRefractionFrameBuffer();

	/**
	 * Unbinds the currently active frame buffer.
	*/
	void UnbindCurrentFrameBuffer();

	/**
	 * Gets the reflection texture ID.
	 *
	 * @return The reflection texture ID.
	*/
	unsigned int GetReflectionTexture();

	/**
	* Gets the refraction texture ID.
	*
	* @return The refraction texture ID.
	*/
	unsigned int GetRefractionTexture();

	/**
	* Gets the refraction depth texture ID.
	*
	* @return The refraction depth texture ID.
	*/
	unsigned int GetRefractionDepthTexture();
private:
	/**
	 * Initialise the reflection frame buffer.
	*/
	void InitialiseReflectionFrameBuffer();

	/**
	 * Initialise the refraction frame buffer.
	*/
	void InitialiseRefractionFrameBuffer();

	/**
	 * Binds a buffer with a specified width and height.
	 *
	 * @param frameBuffer
	 *		- The buffer to be bound.
	 *
	 * @param width
	 *		- The width of the frame buffer.
	 *
	 * @param height
	 *		- The height of the frame buffer.
	*/
	void BindFrameBuffer(unsigned int frameBuffer, unsigned int width, unsigned int height);

	/**
	 * Creates a frame buffer.
	 *
	 * @return The ID of the frame buffer.
	*/
	unsigned int CreateFrameBuffer();

	/**
	 * Transform the image formed in the buffer into a texture.
	 *
	 * @param width
	 *		- The width of the texture.
	 *
	 * @param height
	 *		- The height of the texture.
	*/
	unsigned int CreateTextureAttachment(unsigned int width, unsigned int height);

	/**
	* Transform the image formed in the buffer into a texture.
	*
	* @param width
	*		- The width of the texture.
	*
	* @param height
	*		- The height of the texture.
	*/
	unsigned int CreateDepthTextureAttachment(unsigned int width, unsigned int height);

	/**
	* Transform the image formed in the buffer into a texture.
	*
	* @param width
	*		- The width of the texture.
	*
	* @param height
	*		- The height of the texture.
	*/
	unsigned int CreateDepthBufferAttachment(unsigned int width, unsigned int height);
};
