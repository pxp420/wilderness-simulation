#include "../water/WaterShader.h"
#include "../water/WaterRenderer.h"
#include "../water/WaterFrameBuffers.h"
#include "../guis/GuiRenderer.h"
#include "../postProcessing/PostProcessing.h"
#include "../inputListener/Input.h"
#include "../pathFinding/AStarGenerator.h"
#include "../fontRenderer/TextMaster.h"
#include "../entities/Human.h"
#include "../map/Map.h"
#include "../generators/VegetationGenerator.h"
#include "../objConverter/Model.h"
#include "../toolbox/ColourInterpolation.h"
#include "../generators/DayCycleGenerator.h"
#include "../reinforcedLearning/Simulation.h"

extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

int main() {
	DisplayManager::CreateDisplay();
	Camera::CreateInstance();
	TextMaster::Init(Loader::GetInstance(), Camera::GetInstance());

	TerrainTexture mudTexture(Loader::GetInstance().LoadTexture("mud.png"));
	TerrainTexture grassTexture(Loader::GetInstance().LoadTexture("grassy.png"));
	TerrainTexture flowerGrassTexture(Loader::GetInstance().LoadTexture("mud2.jpg"));
	TerrainTexture dirtTexture(Loader::GetInstance().LoadTexture("mud2.jpg"));
	TerrainTexture rockTexture(Loader::GetInstance().LoadTexture("rock.jpg"));

	TerrainTexturePack texturePack(mudTexture, grassTexture, flowerGrassTexture, dirtTexture, rockTexture);

	const std::unique_ptr<ModelTexture> terrainTexture = std::make_unique<ModelTexture>(Loader::GetInstance().LoadTexture("white.png"));
	Terrain::CreateInstance(0, 0, texturePack, "hm.png");

	TerrainTexture blendMap = Loader::GetInstance().LoadTexture("hm.png");
	Terrain::GetInstance()->SetBlendMap(blendMap);

	Light* light = new Light(glm::vec3(2000, 10000, 2000), glm::vec3(1.0f, 1.0f, 1.0f));
	DayCycleGenerator::CreateInstance(light);

	Map::GetInstance().InitialiseMap();
	AStarGenerator::Initialise();

	VegetationGenerator::CreateInstance();
	VegetationGenerator::GetInstance()->SpawnModels();

	Human::CreateInstance(glm::vec3(1800.0f, 20.0f, 1800.0f));
	Human::GetInstance()->SetOnMap(180, 180);

	Agent* agent = new Agent();
	Simulation::CreateInstance(agent);

	std::vector<GuiTexture> guiTextures;
	MasterRenderer::CreateInstance(Loader::GetInstance(), Camera::GetInstance(), Entity::ENTITIES);

	MousePicker picker(Camera::GetInstance(), MasterRenderer::GetInstance()->GetProjectionMatrix());
	Input input(*VegetationGenerator::GetInstance(), Camera::GetInstance());
	WaterFrameBuffers fbos;

	WaterShader waterShader;
	WaterRenderer waterRenderer(Loader::GetInstance(), waterShader, MasterRenderer::GetInstance()->GetProjectionMatrix(), fbos);

	std::vector<WaterTile> waters;
	WaterTile water(1800.0f, 1800.0f, -40.0f, 0.0f, 0.0f, 0.0f);
	waters.emplace_back(water);

	Fbo multisampleFbo(DisplayManager::WIDTH, DisplayManager::HEIGHT);
	Fbo outputFbo(DisplayManager::WIDTH, DisplayManager::HEIGHT, Fbo::DEPTH_TEXTURE);

	PostProcessing::Init(Loader::GetInstance());
	GuiRenderer guiRenderer(Loader::GetInstance());
	double lastTime = glfwGetTime();
	int nbFrames = 0;
	float i = 0.2f;
	do {
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1 sec ago
											 // printf and reset timer
			printf("%f ms/frame\n", 1000.0 / double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}

		Camera::GetInstance()->Move();
		Human::GetInstance()->Update();

		DayCycleGenerator::GetInstance()->Cycle(DisplayManager::GetFrameTimeSeconds());
		//light->SetPosition(glm::vec3(light->GetPosition().x + i, light->GetPosition().y, light->GetPosition().z));

		//if (light->GetPosition().x > 5000.0f) {
		//	light->SetPosition(glm::vec3(2000.0f, light->GetPosition().y, light->GetPosition().z));
		//}

		input.Listen();
		//picker.Update();

		MasterRenderer::GetInstance()->RenderShadowMap(Light::LIGHTS.at(0));

		glEnable(GL_CLIP_DISTANCE0);
		fbos.BindReflectionFrameBuffer();
		float distance = 2 * (Camera::GetInstance()->GetPosition().y - water.GetHeight());
		Camera::GetInstance()->SetPosition(glm::vec3(Camera::GetInstance()->GetPosition().x, Camera::GetInstance()->GetPosition().y - distance, Camera::GetInstance()->GetPosition().z));
		Camera::GetInstance()->InvertPitch();
		MasterRenderer::GetInstance()->RenderScene(Entity::ENTITIES, Light::LIGHTS, *Camera::GetInstance(), glm::vec4(0.0f, 1.0f, 0.0f, -water.GetHeight()));
		Camera::GetInstance()->SetPosition(glm::vec3(Camera::GetInstance()->GetPosition().x, Camera::GetInstance()->GetPosition().y + distance, Camera::GetInstance()->GetPosition().z));
		Camera::GetInstance()->InvertPitch();

		fbos.BindRefractionFrameBuffer();
		MasterRenderer::GetInstance()->RenderScene(Entity::ENTITIES, Light::LIGHTS, *Camera::GetInstance(), glm::vec4(0.0f, -1.0f, 0.0f, water.GetHeight()));

		fbos.UnbindCurrentFrameBuffer();
		glDisable(GL_CLIP_DISTANCE0);

		multisampleFbo.BindFrameBuffer();
		MasterRenderer::GetInstance()->RenderScene(Entity::ENTITIES, Light::LIGHTS, *Camera::GetInstance(), glm::vec4(0.0f, -1.0f, 0.0f, 0.0f));
		waterRenderer.Render(waters, Camera::GetInstance(), Light::LIGHTS.at(0));
		multisampleFbo.UnbindFrameBuffer();
		multisampleFbo.ResolveToFbo(outputFbo);
		PostProcessing::DoPostProcessing(outputFbo.GetColourTexture());

		TextMaster::Render();
		guiRenderer.Render();
		DisplayManager::UpdateDisplay();
	} while (DisplayManager::IsCloseRequested());

	Terrain::DestroyInstance();
	guiRenderer.CleanUp();
	TextMaster::CleanUp();
	PostProcessing::CleanUp();
	outputFbo.CleanUp();
	multisampleFbo.CleanUp();
	fbos.CleanUp();
	waterShader.CleanUp();
	MasterRenderer::GetInstance()->CleanUp();
	Loader::GetInstance().CleanUp();
	DisplayManager::CloseDisplay();
}