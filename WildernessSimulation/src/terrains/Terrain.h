#pragma once

#include "../models/RawModel.h"
#include "../textures/ModelTexture.h"
#include "../renderEngine/Loader.h"
#include "../textures/TerrainTexturePack.h"
#include "../libraries/stb_image.h"
#include "../toolbox/Maths.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <vector>
class Maths;
class Terrain {
private:
	std::vector<std::vector<float>> heights;

	float x;
	float z;
	RawModel model;
	TerrainTexturePack texturePack;
	TerrainTexture* blendMap;
	const std::string heightMap;

	/**
	* Generates terrain by creating vertices and textures and storing them in a VAO
	* for further display.
	*
	* @return The model that can be used for rendering the terrain.
	*/
	std::unique_ptr<RawModel> GenerateTerrain(const std::string& heightMap);

	/**
	* Gets the height of a certain pixel on the screen from the image.
	*
	* @param x
	*		- The x coordinate of the pixel.
	*
	* @param y
	*		- The y coordinate of the pixel.
	*
	* @param image
	*		- The data of the height map image.
	*
	* @param imageHeight
	*		- The height of the image.
	*
	* @param nrComponents
	*		- The number of channels the image has.
	*/
	float GetHeight(unsigned int x, unsigned int y,
		unsigned char* image, unsigned int imageHeight, unsigned int nrComponents);

	/**
	* Calculates the normal y position for each vertex.
	*
	* @param x
	*		- The x coordinate of the pixel.
	*
	* @param y
	*		- The y coordinate of the pixel.
	*
	* @param image
	*		- The data of the height map image.
	*
	* @param imageHeight
	*		- The height of the image.
	*
	* @param nrComponents
	*		- The number of channels the image has.
	*/
	glm::vec3 CalculateNormal(unsigned int x, unsigned int y,
		unsigned char* image, unsigned int imageHeight, unsigned int nrComponents);

	/**
	* Constructor that specifies how big the terrain grid should be and what texture
	* each grid cell has.
	*
	* @param gridX
	*		- The horizontal size of the terrain.
	*
	* @param gridZ
	*		- The 3D z size of the terrain.
	*
	* @param texturePack
	*		- The texture placed on each of the grid cells calcuated from all the textures.
	*/
	Terrain(int gridX, int gridZ, TerrainTexturePack texturePack,
		const std::string& heightMap);

	static Terrain* instance;
public:
	const static float MAX_HEIGHT;
	const static float MAX_PIXEL_COLOUR;
	const static float SIZE;

	static void CreateInstance(int gridX, int gridZ, TerrainTexturePack texturePack, const std::string& heightMap);

	static void DestroyInstance();

	static Terrain* GetInstance();

	Terrain(Terrain const&) = delete;
	void operator=(Terrain const&) = delete;

	/**
	* Gets the length of a cell per horizontal.
	*
	* @return The length of a cell on horizontal display;
	*/
	float GetX();

	/**
	* Gets the length of a cell per 3D z display.
	*
	* @return The length of a cell on 3D z display.
	*/
	float GetZ();

	/**
	* Gets the model that will be rendered on the screen.
	*
	* @return The model that will be rendered on the screen.
	*/
	RawModel GetModel();

	/**
	* Gets the textures that will be applied on the cells.
	*
	* @return The textures for each cell.
	*/
	TerrainTexturePack GetTexturePack();

	/**
	* Gets the blend map needed for rendering the textures on the cells.
	*
	* @return The blend map.
	*/
	TerrainTexture GetBlendMap();

	/**
	 * Sets the blend map of the terrain.
	 *
	 * @param blendMap
	 *		- The blend map to be set.
	*/
	void SetBlendMap(TerrainTexture& blendMap);

	/**
	* Get the height of the terrain at a given coordinate position.
	*
	* @param worldX
	*		- The x coordinate of the terrain.
	*
	* @param worldZ
	*		- The z coordinate of the terrain.
	*
	* @return The height of the terrain at the given position.
	*/
	float GetHeightOfTerrain(float worldX, float worldZ);

	/**
	 * Get the heights of each vertex on the map.
	 *
	 * @return The heights of the map.
	*/
	std::vector<std::vector<float>> GetHeights();
	
	/**
	 * Get the dimensions of a square.
	 *
	 * @return The dimensions of a grid square.
	*/
	float GetSquareSize();

	/**
	 * Get the grid size.
	 *
	 * @return The dimensions of the grid.
	*/
	float GetGridSize();

	/**
	 * Transform a number from 0.0 to 1.0f into the world space.
	 *
	 * @param coordinate
	 *		- The coordinate in the OpenGL system.
	 *
	 * @return The new coordinate in the wordl space/
	*/
	float TransformToWorldSpace(float coordinate);
};
