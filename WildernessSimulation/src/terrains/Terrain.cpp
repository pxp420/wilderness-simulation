#include "Terrain.h"

const float Terrain::SIZE = 3600.0f; //3600.0f;
const float Terrain::MAX_HEIGHT = 450.0f; //900.0f
const float Terrain::MAX_PIXEL_COLOUR = 256.0f;

Terrain* Terrain::instance = nullptr;

void Terrain::CreateInstance(int gridX, int gridZ, TerrainTexturePack texturePack, const std::string& heightMap) {
	instance = new Terrain(gridX, gridZ, texturePack, heightMap);
}

void Terrain::DestroyInstance() {
	delete instance;
}

Terrain* Terrain::GetInstance() {
	return instance;
}

Terrain::Terrain(int gridX, int gridZ, TerrainTexturePack texturePack,
	const std::string& heightMap) :
	texturePack(texturePack), x(gridX * Terrain::SIZE),
	z(gridZ * Terrain::SIZE), heightMap(heightMap), model(*GenerateTerrain(heightMap)) {}

std::unique_ptr<RawModel> Terrain::GenerateTerrain(const std::string& heightMap) {
	int width, height, nrComponents;
	unsigned char* data = stbi_load(("res/textures/" + heightMap).c_str(), &width, &height, &nrComponents, 0);

	if (data) {
		unsigned int format;
		if (nrComponents == 1) {
			format = GL_RED;
		}
		else if (nrComponents == 3) {
			format = GL_RGB;
		}
		else if (nrComponents == 4) {
			format = GL_RGBA;
		}
	}
	else {
		std::cout << "Texture failed to load at path: " << heightMap.c_str() << std::endl;
		stbi_image_free(data);
	}

	unsigned int VERTEX_COUNT = height;
	heights = std::vector<std::vector<float>>(VERTEX_COUNT, std::vector<float>(VERTEX_COUNT, 0));

	unsigned int count = VERTEX_COUNT * VERTEX_COUNT;
	std::vector<float> vertices(count * 3);
	std::vector<float> normals(count * 3);
	std::vector<float> textureCoords(count * 2);
	std::vector<unsigned int> indices(6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1));

	unsigned int vertexPointer = 0;
	for (unsigned int i = 0; i < VERTEX_COUNT; i++) {
		for (unsigned int j = 0; j < VERTEX_COUNT; j++) {
			vertices[vertexPointer * 3] = (float)j / ((float)VERTEX_COUNT - 1) * Terrain::SIZE;
			float vertexHeight = GetHeight(j, i, data, height, nrComponents);
			heights[j][i] = vertexHeight;
			vertices[vertexPointer * 3 + 1] = vertexHeight;
			vertices[vertexPointer * 3 + 2] = (float)i / ((float)VERTEX_COUNT - 1) * Terrain::SIZE;

			glm::vec3 normal = CalculateNormal(j, i, data, height, nrComponents);
			normals[vertexPointer * 3] = normal.x;
			normals[vertexPointer * 3 + 1] = normal.y;
			normals[vertexPointer * 3 + 2] = normal.z;

			textureCoords[vertexPointer * 2] = (float)j / ((float)VERTEX_COUNT - 1);
			textureCoords[vertexPointer * 2 + 1] = (float)i / ((float)VERTEX_COUNT - 1);

			vertexPointer++;
		}
	}

	unsigned int pointer = 0;
	for (unsigned int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
		for (unsigned int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
			unsigned int topLeft = (gz * VERTEX_COUNT) + gx;
			unsigned int topRight = topLeft + 1;
			unsigned int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
			unsigned int bottomRight = bottomLeft + 1;

			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}

	return Loader::GetInstance().LoadToVAO(vertices, textureCoords, normals, indices);
}

float Terrain::GetX() {
	return x;
}

float Terrain::GetZ() {
	return z;
}

RawModel Terrain::GetModel() {
	return model;
}

TerrainTexturePack Terrain::GetTexturePack() {
	return texturePack;
}

TerrainTexture Terrain::GetBlendMap() {
	return *blendMap;
}

void Terrain::SetBlendMap(TerrainTexture& blendMap) {
	this->blendMap = &blendMap;
}

float Terrain::GetHeight(unsigned int x, unsigned int y,
	unsigned char* image, unsigned int imageHeight, unsigned int nrComponents) {
	if (x < 0.0f || x >= imageHeight || y < 0.0f || y >= imageHeight) {
		return 0.0f;
	}

	const unsigned char* pixel = image + (nrComponents * (y * imageHeight + x));

	float height = pixel[0];
	height = height - MAX_PIXEL_COLOUR / 2.0f;
	height = height * (MAX_HEIGHT / MAX_PIXEL_COLOUR) / 2.0f;

	return height;
}

glm::vec3 Terrain::CalculateNormal(unsigned int x, unsigned int y,
	unsigned char* image, unsigned int imageHeight, unsigned int nrComponents) {
	float heightL = GetHeight(x - 1, y, image, imageHeight, nrComponents);
	float heightR = GetHeight(x + 1, y, image, imageHeight, nrComponents);
	float heightD = GetHeight(x, y - 1, image, imageHeight, nrComponents);
	float heightU = GetHeight(x, y + 1, image, imageHeight, nrComponents);

	glm::vec3 normal(heightL - heightR, 6.0f, heightD - heightU);
	normal = glm::normalize(normal);

	return normal;
}

float Terrain::GetHeightOfTerrain(float worldX, float worldZ) {
	float terrainX = worldX - this->x;
	float terrainZ = worldZ - this->z;
	float gridSquareSize = SIZE / ((float)heights.size() - 1.0f);

	float gridX = glm::floor(terrainX / gridSquareSize);
	float gridZ = glm::floor(terrainZ / gridSquareSize);

	if (gridX + 0.5f >= heights.size() - 0.5f || gridZ + 0.5f >= heights.size() - 0.5f || gridX < 0.0f || gridZ < 0.0f) {
		return 0.0f;
	}

	float xCoord = (std::fmod(terrainX, gridSquareSize)) / gridSquareSize;
	float zCoord = (std::fmod(terrainZ, gridSquareSize)) / gridSquareSize;

	float answer = 0.0f;
	if (xCoord <= (1.0f - zCoord)) {
		answer = Maths::BarryCentric(glm::vec3(0.0f, heights[gridX][gridZ], 0.0f),
			glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
			glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
			glm::vec2(xCoord, zCoord));
	}
	else {
		answer = Maths::BarryCentric(glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
			glm::vec3(1.0f, heights[gridX + 1][gridZ + 1], 1.0f),
			glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
			glm::vec2(xCoord, zCoord));
	}

	return answer;
}

std::vector<std::vector<float>> Terrain::GetHeights() {
	return heights;
}

float Terrain::GetSquareSize() {
	return SIZE / (heights.size() - 1.0f);
}

float Terrain::GetGridSize() {
	return heights.size();
}

float Terrain::TransformToWorldSpace(float coordinate) {
	float gridSquareSize = this->GetSquareSize();
	float newCoordinate = roundf(coordinate * (Terrain::SIZE - gridSquareSize) / gridSquareSize) *
		gridSquareSize + gridSquareSize / 2;

	return newCoordinate;
}