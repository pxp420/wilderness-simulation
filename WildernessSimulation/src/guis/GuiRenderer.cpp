#include "GuiRenderer.h"

GuiRenderer::GuiRenderer(Loader& loader) { }

void GuiRenderer::Render() {
	shader.Start();
	for (GuiTexture* gui : GuiTexture::GUI_TEXTURES) {
		glBindVertexArray(gui->GetQuad()->GetVaoID());
		glEnableVertexAttribArray(0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gui->GetTexture());
		glm::mat4 matrix = Maths::CreateTransformationMatrix(gui->GetPosition(), gui->GetScale());
		shader.LoadTransformationMatrix(matrix);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, gui->GetQuad()->GetVertexCount());

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
	}

	shader.Stop();
}

void GuiRenderer::CleanUp() {
	shader.CleanUp();
}
