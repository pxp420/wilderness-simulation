#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <vector>
#include <algorithm>

#include "../renderEngine/Loader.h"

class GuiTexture {
private:
	unsigned int texture;

	glm::vec2 position;
	glm::vec2 scale;

	std::vector<float> positions;
	std::unique_ptr<RawModel> quad;
public:
	static std::vector<GuiTexture*> GUI_TEXTURES;
	/**
	 * Constructor of the GUI texture to be rendered on the screen.
	 *
	 * @param texture
	 *		- The texture ID of the texture.
	 *
	 * @param position
	 *		- The position on the screen of the texture.
	 *
	 * @param scale
	 *		- The size of the texture on the screen.
	*/
	GuiTexture(unsigned int texture, glm::vec2 position, glm::vec2 scale);

	static void RemoveGUITexture(GuiTexture* gui);

	/**
	 * Gets the texture to be rendered on the screen.
	 *
	 * @return The texture ID
	*/
	unsigned int GetTexture();

	/**
	 * Gets the position of the displayed texture.
	 *
	 * @return The position of the texture.
	*/
	glm::vec2 GetPosition();

	/**
	 * Gets the scale value of the displayed texture.
	 *
	 * @return The scale value of the texture.
	*/
	glm::vec2 GetScale();

	std::unique_ptr<RawModel>& GetQuad();

	void ChangeQuadPositions(std::vector<float>& quadPositions);
};
