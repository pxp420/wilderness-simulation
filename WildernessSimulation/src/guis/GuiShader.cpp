#include "GuiShader.h"

const std::string GuiShader::VERTEX_FILE = "src/guis/guiVertexShader.shader";
const std::string GuiShader::FRAGMENT_FILE = "src/guis/guiFragmentShader.shader";

const std::map<unsigned int, std::string> GuiShader::ATTRIBUTES = {
	{ 0, "position" }
};

GuiShader::GuiShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void GuiShader::LoadTransformationMatrix(glm::mat4 matrix) {
	ShaderProgram::LoadMatrix(location_transformationMatrix, matrix);
}

void GuiShader::GetAllUniformLocations() {
	location_transformationMatrix = ShaderProgram::GetUniformLocation("transformationMatrix");
}
