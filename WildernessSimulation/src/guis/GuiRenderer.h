#pragma once

#include "../renderEngine/Loader.h"
#include "../toolbox/Maths.h"

#include "GuiTexture.h"
#include "GuiShader.h"

class GuiRenderer {
private:
	GuiShader shader;
public:
	/**
	 * Constructor for the GUI rendering.
	 *
	 * @param loader
	 *		- The loader needed to load the 2D texture.
	*/
	GuiRenderer(Loader& loader);

	/**
	 * Render 2D textures on the screen.
	 *
	 * @param guis
	 *		- The vector of textures to be rendered.
	*/
	void Render();

	/**
	* Detaches the shaders from the program and deletes them. After that, deletes the program
	* altogether so that it doesn't stay in memory.
	*/
	void CleanUp();
};
