#include "GuiTexture.h"


std::vector<GuiTexture*> GuiTexture::GUI_TEXTURES = std::vector<GuiTexture*>();

GuiTexture::GuiTexture(unsigned int texture, glm::vec2 position, glm::vec2 scale) :
	texture(texture),
	positions({
	-1.0f,  1.0f,
	-1.0f, -1.0f,
	 1.0f,  1.0f,
	 1.0f, -1.0f
    }),
	quad(Loader::GetInstance().LoadToVAO(positions, 2)),
	position(position),
	scale(scale) {
	GUI_TEXTURES.emplace_back(this);
}

void GuiTexture::RemoveGUITexture(GuiTexture* gui) {
	GUI_TEXTURES.erase(std::remove(GUI_TEXTURES.begin(), GUI_TEXTURES.end(), gui), GUI_TEXTURES.end());
}

unsigned int GuiTexture::GetTexture() {
	return texture;
}

glm::vec2 GuiTexture::GetPosition() {
	return position;
}

glm::vec2 GuiTexture::GetScale() {
	return scale;
}

std::unique_ptr<RawModel>& GuiTexture::GetQuad() {
	return quad;
}

void GuiTexture::ChangeQuadPositions(std::vector<float>& quadPositions) {
	unsigned int* vboIDs = quad->GetVboIDs();
	glBindVertexArray(quad->GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::POSITIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, 0,
		quadPositions.size() * sizeof(float), &quadPositions.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	quadPositions.clear();
}