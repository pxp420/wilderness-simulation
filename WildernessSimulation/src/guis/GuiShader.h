#pragma once

#pragma once

#include "../shaders/ShaderProgram.h"

class GuiShader : public ShaderProgram {
private:
	const static std::string VERTEX_FILE;
	const static std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_transformationMatrix;
public:
	/**
	* Calls the parent constructor to create the program
	*/
	GuiShader();

	/**
	* Loads the transformation matrix into a uniform.
	*
	* @param matrix
	*		- The matrix passed out to the shaders.
	*/
	void LoadTransformationMatrix(glm::mat4 matrix);
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
