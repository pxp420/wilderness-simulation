#pragma once
#include "../map/Map.h"
#include "../libraries/AStar.hpp"

class AStarGenerator {
private:
	static AStar::Generator* generator;
public:

	/**
	 * Initialises the parameters of the generator and adds collisions to the map.
	*/
	static void Initialise();

	/**
	 * Generate a path from a given initial coordinate to a final coordinate.
	 *
	 * @param startI
	 *		- The starting i point of the map.
	 *
	 * @param startJ
	 *		- The starting j point of the map.
	 *
	 * @param finalI
	 *		- The final i point of the map.
	 *
	 * @param finalJ
	 *		- The final j point of the map.
	 *
	 * @return A sequence of i,j's representing the path.
	*/
	static std::vector<glm::vec2> GeneratePath(int startI, int startJ, int finalI, int finalJ);

	static AStar::Generator* GetGenerator();
};
