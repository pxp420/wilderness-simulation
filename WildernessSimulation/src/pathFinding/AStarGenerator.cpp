#include "AStarGenerator.h"

AStar::Generator* AStarGenerator::generator = new AStar::Generator();

void AStarGenerator::Initialise() {
	std::vector<std::vector<MapBlock>>* map = Map::GetInstance().GetMap();
	int mapSize = map->size();

	generator->setWorldSize({ mapSize, mapSize });
	for (int i = 0; i < map->size(); i++) {
		for (int j = 0; j < map->size(); j++) {
			if (!map->at(i).at(j).canPassThrough) {
				generator->addCollision({ i, j });
			}
		}
	}
	generator->setHeuristic(AStar::Heuristic::euclidean);
	generator->setDiagonalMovement(true);
}

std::vector<glm::vec2> AStarGenerator::GeneratePath(int startI, int startJ, int finalI, int finalJ) {
	auto path = generator->findPath({ startI, startJ }, { finalI, finalJ });

	std::vector<glm::vec2> pathToFollow;
	for (auto& coordinate : path) {
		pathToFollow.emplace_back(glm::vec2(coordinate.x, coordinate.y));
	}

	std::reverse(pathToFollow.begin(), pathToFollow.end());

	return pathToFollow;
}

AStar::Generator* AStarGenerator::GetGenerator() {
	return generator;
}
