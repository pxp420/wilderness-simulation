#pragma once

#include "../renderEngine/DisplayManager.h"

class Fbo {
private:
	const unsigned int width;
	const unsigned int height;
	
	unsigned int frameBuffer;
	unsigned int colourTexture;
	unsigned int depthTexture;

	unsigned int depthBuffer;
	unsigned int colourBuffer;

	bool multisample;
public:
	const static unsigned int NONE;
	const static unsigned int DEPTH_TEXTURE;
	const static unsigned int DEPTH_RENDER_BUFFER;

	/**
	 * Creates an FBO of a specified width and height, with the desired type
	 * of depth buffer attachment.
	 *
	 * @param width
	 *		- The width of the FBO.
	 *
	 * @param height
	 *		- The height of the FBO.
	 *
	 * @param depthBufferType
	 *		- An int indicating the type of depth buffer attachment that
	 *		this FBO should use.
	*/
	Fbo(unsigned int width, unsigned int height, unsigned int depthBufferType);

	/**
	* Creates an FBO of a specified width and height, with the desired type
	* of depth buffer attachment.
	*
	* @param width
	*		- The width of the FBO.
	*
	* @param height
	*		- The height of the FBO.
	*/
	Fbo(unsigned int width, unsigned int height);

	/**
	 * Deletes the frame buffer and its attachments when the game closes.
	*/
	void CleanUp();

	/**
	 * Binds the frame buffer, setting it as the current render target. Anything
	 * rendered after this will be rendered to this FBO, and not to the screen.
	*/
	void BindFrameBuffer();

	/**
	 * Unbinds the frame buffer, setting the default frame buffer as the current
	 * render target. Anything rendered after this will berendered to the screen,
	 * and not this FBO.
	*/
	void UnbindFrameBuffer();

	/**
	 * Binds the current FBO to be read from.
	*/
	void BindToRead();

	/**
	 * Get the ID of the texture containing the colour buffer of the FBO.
	 *
	 * @return The texture ID.
	*/
	unsigned int GetColourTexture();

	/**
	 * Get the ID of the texture containing the FBO's depth buffer.
	 *
	 * @return The texture ID.
	*/
	unsigned int GetDepthTexture();

	/**
	 * Put the frame with the multisampling into another FBO for further
	 * processing.
	 *
	 * @param outputFbo
	 *		- The output FBO where the texture will be stored in.
	*/
	void ResolveToFbo(Fbo& outputFbo);

	/**
	 * Put the frame with the multisampling directly to the screen as a
	 * texture.
	*/
	void ResolveToScreen();

	/**
	 * Creates the FBO along with a colour buffer texture attachment, and
	 * possibly a depth buffer.
	 *
	 * @param type
	 *		- The type of depth buffer attachment to be attached to the FBO.
	*/
	void InitialiseFrameBuffer(unsigned int type);

	/**
	 * Creates a new frame buffer object and sets the buffer to which drawing
	 * will occur - colour attachment 0. This is the attachment where the
	 * colour buffer texture is.
	*/
	void CreateFrameBuffer();

	/**
	 * Creates a texture and sets it as the colour buffer attachment for this
	 * FBO.
	*/
	void CreateTextureAttachment();

	/**
	 * Adds a depth buffer to the FBO in the form of a texture, which can later
	 * be sampled.
	*/
	void CreateDepthTextureAttachment();

	/**
	 * Adds a colour attachment with multisampling on the currently bound FBO.
	*/
	void CreateMultisampleColourAttachment();

	/**
	 * Adds a depth buffer to the FBO in the form of a render buffer. This cannot
	 * be used for sampling in the shaders.
	*/
	void CreateDepthBufferAttachment();
};
