#pragma once

#include "ContrastChanger.h"
#include "../renderEngine/Loader.h"
#include "../gaussianBlur/HorizontalBlur.h"
#include "../gaussianBlur/VerticalBlur.h"
#include "../bloom/BrightFilter.h"
#include "../bloom/CombineFilter.h"

class PostProcessing {
private:
	const static std::vector<float> POSITIONS;

	static std::unique_ptr<RawModel> quad;
	static std::unique_ptr<ContrastChanger> contrastChanger;
	
	static std::unique_ptr<HorizontalBlur> hBlur;
	static std::unique_ptr<VerticalBlur> vBlur;

	static std::unique_ptr<BrightFilter> brightFilter;
	static std::unique_ptr<CombineFilter> combineFilter;

public:
	/**
	 * Load the quad into the VAO
	 *
	 * @param loader
	 *		- The loader needed for loading to VAO.
	*/
	static void Init(Loader& loader);

	/**
	 * Process the FBO image texture before rendering to the screen.
	 *
	 * @param colourTexture
	 *		- The texture to be processed.
	*/
	static void DoPostProcessing(unsigned int colourTexture);

	/**
	 * Remove the shaders and FBO when not needing them anymore.
	*/
	static void CleanUp();

	/**
	 * Binds the VAO for next render.
	*/
	static void Start();

	/**
	 * Unbinds the VAO.
	*/
	static void End();
};