#include "ContrastChanger.h"

ContrastChanger::ContrastChanger() {}

void ContrastChanger::Render(unsigned int texture) {
	shader.Start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	renderer.RenderQuad(false);
	shader.Stop();
}

void ContrastChanger::CleanUp() {
	renderer.CleanUp();
	shader.CleanUp();
}

unsigned int ContrastChanger::GetOutputTexture() {
	return renderer.GetOutputTexture();
}