#include "ContrastShader.h"

const std::string ContrastShader::VERTEX_FILE = "src/postProcessing/contrastVertexShader.shader";
const std::string ContrastShader::FRAGMENT_FILE = "src/postProcessing/contrastFragmentShader.shader";

const std::map<unsigned int, std::string> ContrastShader::ATTRIBUTES = {
	{ 0, "position" }
};

ContrastShader::ContrastShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void ContrastShader::GetAllUniformLocations() {
}
