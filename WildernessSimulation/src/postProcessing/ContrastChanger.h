#pragma once

#include "ImageRenderer.h"
#include "ContrastShader.h"

class ContrastChanger {
private:
	ImageRenderer renderer;
	ContrastShader shader;

public:
	ContrastChanger();

	/**
	 * Render the processed texture on the screen.
	 *
	 * @param texture
	 *		- The texture ID.
	*/
	void Render(unsigned int texture);

	/**
	 * Clean up the shader and the FBO.
	*/
	void CleanUp();

	unsigned int GetOutputTexture();
};