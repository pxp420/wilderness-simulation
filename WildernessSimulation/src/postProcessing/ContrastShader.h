#pragma once

#include "../shaders/ShaderProgram.h"

class ContrastShader : public ShaderProgram {
private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

public:
	ContrastShader();

protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
