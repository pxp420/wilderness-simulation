#include "PostProcessing.h"

const std::vector<float> PostProcessing::POSITIONS = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };

std::unique_ptr<ContrastChanger> PostProcessing::contrastChanger = nullptr;
std::unique_ptr<RawModel> PostProcessing::quad = nullptr;
std::unique_ptr<HorizontalBlur> PostProcessing::hBlur = nullptr;
std::unique_ptr<VerticalBlur> PostProcessing::vBlur = nullptr;
std::unique_ptr<BrightFilter> PostProcessing::brightFilter = nullptr;
std::unique_ptr<CombineFilter> PostProcessing::combineFilter = nullptr;

void PostProcessing::Init(Loader& loader) {
	contrastChanger = std::make_unique<ContrastChanger>();
	quad = loader.LoadToVAO(POSITIONS, 2);

	hBlur = std::make_unique<HorizontalBlur>(DisplayManager::WIDTH / 5, DisplayManager::HEIGHT / 5);
	vBlur = std::make_unique<VerticalBlur>(DisplayManager::WIDTH / 5, DisplayManager::HEIGHT / 5);

	brightFilter = std::make_unique<BrightFilter>(DisplayManager::WIDTH / 2, DisplayManager::HEIGHT / 2);
	combineFilter = std::make_unique<CombineFilter>();
}

void PostProcessing::DoPostProcessing(unsigned int colourTexture) {
	Start();
	brightFilter->Render(colourTexture);
	hBlur->Render(brightFilter->GetOutputTexture());
	vBlur->Render(hBlur->GetOutputTexture());
	contrastChanger->Render(vBlur->GetOutputTexture());
	combineFilter->Render(colourTexture, contrastChanger->GetOutputTexture());
	End();
}

void PostProcessing::CleanUp() {
	combineFilter->CleanUp();
	contrastChanger->CleanUp();
	brightFilter->CleanUp();
	vBlur->CleanUp();
	hBlur->CleanUp();
}

void PostProcessing::Start() {
	glBindVertexArray(quad->GetVaoID());
	glEnableVertexAttribArray(0);
	glDisable(GL_DEPTH_TEST);
}

void PostProcessing::End() {
	glEnable(GL_DEPTH_TEST);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}
