#include "ImageRenderer.h"

ImageRenderer::ImageRenderer(unsigned int width, unsigned int height) :
	fbo(Fbo(width, height, Fbo::NONE)) {}

ImageRenderer::ImageRenderer() : 
	fbo(Fbo(DisplayManager::WIDTH, DisplayManager::HEIGHT, Fbo::NONE)) {}

void ImageRenderer::RenderQuad(bool last) {
	if (!last) {
		fbo.BindFrameBuffer();
	}

	glClear(GL_COLOR_BUFFER_BIT);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (!last) {
		fbo.UnbindFrameBuffer();
	}
}

unsigned int ImageRenderer::GetOutputTexture() {
	return fbo.GetColourTexture();
}

void ImageRenderer::CleanUp() {
	if (&fbo) {
		fbo.CleanUp();
	}
}
