#pragma once

#include "Fbo.h"

class ImageRenderer {
private:
	Fbo fbo;

public:
	/**
	 * Constructor for rendering the image stored in the FBO.
	 *
	 * @param width
	 *		- The width of the image of the FBO.
	 *
	 * @param height
	 *		- The height of the image of the FBO.
	*/
	ImageRenderer(unsigned int width, unsigned int height);

	/**
	 * Constructor for rendering the image stored in the FBO.
	*/
	ImageRenderer();

	/**
	 * Render the texture from the FBO onto a quad on the screen.
	 *
	 * @param last
	 *		- Is this the last post process effect? If yes then
	 *		the next draw will be on the screen.
	*/
	void RenderQuad(bool last);

	/**
	 * Get the texture ID of the image stored in the FBO.
	 *
	 * @return The texture ID.
	*/
	unsigned int GetOutputTexture();

	/**
	 * Clean up the FBO when not needing it anymore.
	*/
	void CleanUp();
};
