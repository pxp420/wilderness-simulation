#include "Fbo.h"

const unsigned int Fbo::NONE = 0;
const unsigned int Fbo::DEPTH_TEXTURE = 1;
const unsigned int Fbo::DEPTH_RENDER_BUFFER = 2;

Fbo::Fbo(unsigned int width, unsigned int height, unsigned int depthBufferType) :
	width(width), height(height), multisample(false) {
	InitialiseFrameBuffer(depthBufferType);
}

Fbo::Fbo(unsigned int width, unsigned int height) :
	width(width), height(height), multisample(true) {
	InitialiseFrameBuffer(DEPTH_RENDER_BUFFER);
}

void Fbo::CleanUp() {
	glDeleteFramebuffers(1, &frameBuffer);
	glDeleteTextures(1, &colourTexture);
	glDeleteTextures(1, &depthTexture);
	glDeleteRenderbuffers(1, &depthBuffer);
	glDeleteRenderbuffers(1, &colourBuffer);
}

void Fbo::BindFrameBuffer() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
	glViewport(0, 0, width, height);
}

void Fbo::UnbindFrameBuffer() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, DisplayManager::WIDTH, DisplayManager::HEIGHT);
}

void Fbo::BindToRead() {
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
}

unsigned int Fbo::GetColourTexture() {
	return colourTexture;
}

unsigned int Fbo::GetDepthTexture() {
	return depthTexture;
}

void Fbo::ResolveToFbo(Fbo& outputFbo) {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, outputFbo.frameBuffer);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, this->frameBuffer);
	glBlitFramebuffer(0, 0, width, height, 0, 0, outputFbo.width, outputFbo.height,
		GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	UnbindFrameBuffer();
}

void Fbo::ResolveToScreen() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, this->frameBuffer);
	glDrawBuffer(GL_BACK);
	glBlitFramebuffer(0, 0, width, height, 0, 0, DisplayManager::WIDTH, DisplayManager::HEIGHT,
		GL_COLOR_BUFFER_BIT, GL_NEAREST);
	UnbindFrameBuffer();
}

void Fbo::InitialiseFrameBuffer(unsigned int type) {
	CreateFrameBuffer();
	if (multisample) {
		CreateMultisampleColourAttachment();
	}
	else {
		CreateTextureAttachment();
	}
	if (type == DEPTH_RENDER_BUFFER) {
		CreateDepthBufferAttachment();
	}
	else if (type == DEPTH_TEXTURE) {
		CreateDepthTextureAttachment();
	}
	UnbindFrameBuffer();
}

void Fbo::CreateFrameBuffer() {
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
}

void Fbo::CreateTextureAttachment() {
	glGenTextures(1, &colourTexture);
	glBindTexture(GL_TEXTURE_2D, colourTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colourTexture, 0);
}

void Fbo::CreateDepthTextureAttachment() {
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, 
		GL_DEPTH_COMPONENT, GL_FLOAT, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);
}

void Fbo::CreateMultisampleColourAttachment() {
	glGenRenderbuffers(1, &colourBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, colourBuffer);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGBA8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colourBuffer);
}

void Fbo::CreateDepthBufferAttachment() {
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	if (!multisample) {
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
	}
	else {
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT24, width, height);
	}
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
}
