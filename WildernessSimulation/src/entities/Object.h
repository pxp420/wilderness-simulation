#pragma once

#include "../guis/GuiTexture.h"

class Object {
private:
	static unsigned int OBJECT_COUNTER;
	unsigned int id;

	GuiTexture* gui;
public:
	enum InventorySlot {
		LOG, PLANT, WATER
	} item;


	Object(InventorySlot item, GuiTexture* gui);

	InventorySlot GetItem();

	GuiTexture* GetGuiTexture();
};