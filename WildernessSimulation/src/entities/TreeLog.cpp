#include "TreeLog.h"
#include "../map/Map.h"

unsigned int TreeLog::ATLAS_INDEX = 0;
unsigned int TreeLog::TEXTURE_ROWS = 1;

float TreeLog::DEFAULT_ROTX = 0.0f;
float TreeLog::DEFAULT_ROTY = 0.0f;
float TreeLog::DEFAULT_ROTZ = 0.0f;
float TreeLog::DEFAULT_SCALE = 0.1f;

bool TreeLog::CAN_PASS_THROUGH = false;

unsigned int TreeLog::COUNT = 0;
const std::string TreeLog::MODEL_NAME = "log";

std::vector<TexturedModel*> TreeLog::texturedModels = std::vector<TexturedModel*>(0, nullptr);

std::vector<TreeLog*> TreeLog::LOGS = std::vector<TreeLog*>();

TreeLog::TreeLog(const glm::vec3& position) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, TreeLog::DEFAULT_ROTX,
		TreeLog::DEFAULT_ROTY, TreeLog::DEFAULT_ROTZ, TreeLog::DEFAULT_SCALE, COUNT) {
	TreeLog::COUNT++;
	this->SetPosition(position);
	this->UpdateMap();
	LOGS.emplace_back(this);
}

void TreeLog::UpdateMap() {
	MapBlock mapBlock(MapBlock::LOG, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock);
}

TexturedModel TreeLog::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, 0);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}


std::string TreeLog::ToName() {
	return "Log";
}

void TreeLog::DeleteInstance() {
	TreeLog::COUNT--;
	Entity::DeleteInstance();
	TreeLog::LOGS.erase(std::remove(TreeLog::LOGS.begin(), TreeLog::LOGS.end(), this), TreeLog::LOGS.end());
	for (unsigned int i = 0; i < LOGS.size(); i++) {
		LOGS[i]->SetID(i);
		LOGS[i]->SetPosition(LOGS[i]->GetPosition());
	}
}
