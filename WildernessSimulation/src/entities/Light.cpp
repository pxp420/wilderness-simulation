#include "Light.h"

std::vector<Light*> Light::LIGHTS = std::vector<Light*>();

Light::Light(glm::vec3 position, glm::vec3 colour) :
	position(position), colour(colour), attenuation(glm::vec3(1.0f, 0.0f, 0.0f)) {
	Light::LIGHTS.emplace_back(this);
}

Light::Light(glm::vec3 position, glm::vec3 colour, glm::vec3 attenuation) :
	position(position), colour(colour), attenuation(attenuation) {
	Light::LIGHTS.emplace_back(this);
}

glm::vec3 Light::GetPosition() {
	return position;
}

void Light::SetPosition(const glm::vec3& position) {
	this->position = position;
}

glm::vec3 Light::GetColour() {
	return colour;
}

void Light::SetColour(const glm::vec3& colour) {
	this->colour = colour;
}

glm::vec3 Light::GetAttenuation() {
	return attenuation;
}

void Light::SetAttenuation(const glm::vec3& attenuation) {
	this->attenuation = attenuation;
}
