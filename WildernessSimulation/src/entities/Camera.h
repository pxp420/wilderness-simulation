#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <GLAD/glad.h>
#include <GLFW/glfw3.h>

#include "../renderEngine/DisplayManager.h"
#include "../terrains/Terrain.h"

class Terrain;
class Camera {
private:
	static float BOUNDARY;
	static float MOVEMENT_SPEED;
	static float SCROLL_DISTANCE;
	static float INITIAL_OFFSET;
	static float INITIAL_Y_POS;
	static float MIN_SCROLL_POS;
	static float MAX_SCROLL_POS;

	float pitch;
	float yaw;
	float roll;
	float offset;

	glm::vec3 position;

	static Camera* instance;
public:
	/**
	 * The camera constructor that will be used on the screen.
	*/
	Camera();

	static void CreateInstance();

	static void DeleteInstance();

	static Camera* GetInstance();

	/**
	* Move the camera every frame.
	*/
	void Move();

	/**
	 * Gets the current position of the camera.
	 *
	 * @return The position of the camera on the coordiante system.
	*/
	glm::vec3 GetPosition();

	/**
	* Sets the current position of the camera.
	*
	* @param position
	*		The position of the camera on the coordiante system.
	*/
	void SetPosition(const glm::vec3& position);

	/**
	 * Gets the pitch value of the camera.
	 *
	 * @return The pitch value of the camera.
	*/
	float GetPitch();

	/**
	 * Turns the camera looking at upside down.
	*/
	void InvertPitch();

	/**
	* Gets the yaw value of the camera.
	*
	* @return The yaw value of the camera.
	*/
	float GetYaw();

	/**
	* Gets the roll value of the camera.
	*
	* @return The roll value of the camera.
	*/
	float GetRoll();

	/**
	 * Update the camera position if the cursor gets out of bounds.
	*/
	void UpdateBoundary();

	/**
	* Checks how much the user rotates the wheel.
	*
	* @param window
	*		- The window where everything is rendered on.
	*
	* @param xoffset
	*		- The zoom level horizontally.
	*
	* @param yoffset
	*		- The zoom level vertically.
	*/
	static void CalculateZoom(GLFWwindow* window, double xoffset, double yoffset);
};
