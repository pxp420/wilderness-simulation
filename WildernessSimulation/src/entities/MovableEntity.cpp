#include "MovableEntity.h"
#include "../pathFinding/AStarGenerator.h"
#include "../fontRenderer/TextMaster.h"

float MovableEntity::TOP_LEFT = 225.0f;
float MovableEntity::TOP = 180.0f;
float MovableEntity::TOP_RIGHT = 135.0f;
float MovableEntity::RIGHT = 90.0f;
float MovableEntity::BOTTOM_RIGHT = 45.0f;
float MovableEntity::BOTTOM = 0.0f;
float MovableEntity::BOTTOM_LEFT = 315.0f;
float MovableEntity::LEFT = 270.0f;

float MovableEntity::SPEED = 2.0f;

float MovableEntity::ROTATION_SPEED = 500.0f;
unsigned int MovableEntity::MAX_ACTIONS = 6;

MovableEntity::MovableEntity(const TexturedModel& model, unsigned int index, const glm::vec3& position,
	float rotX, float rotY, float rotZ, float scale, unsigned int id) :
	Entity(model, index, position, rotX, rotY, rotZ, scale, id),
	startingPosition(this->GetPosition()),
	squareSize(Terrain::GetInstance()->GetSquareSize())
	{
}

bool MovableEntity::PushAction(Action::Direction direction) {
	float finalPosX = 0;
	float finalPosZ = 0;
	
	Action* action = new Action;
	if (direction == Action::Direction::TL) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z - squareSize;
		action->move = std::bind(&MovableEntity::MoveTopLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::T) {
		finalPosX = startingPosition.x;
		finalPosZ = startingPosition.z - squareSize;
		action->move = std::bind(&MovableEntity::MoveTop, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::TR) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z - squareSize;
		action->move = std::bind(&MovableEntity::MoveTopRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::R) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z;
		action->move = std::bind(&MovableEntity::MoveRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::BR) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z + squareSize;
		action->move = std::bind(&MovableEntity::MoveBottomRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::B) {
		finalPosX = startingPosition.x;
		finalPosZ = startingPosition.z + squareSize;
		action->move = std::bind(&MovableEntity::MoveBottom, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::BL) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z + squareSize;
		action->move = std::bind(&MovableEntity::MoveBottomLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else if (direction == Action::Direction::L) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z;
		action->move = std::bind(&MovableEntity::MoveLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, squareSize, SPEED);
	}
	else {
		std::cout << "Could not move " << std::endl;
	}

	action->finalPosX = finalPosX;
	action->finalPosZ = finalPosZ;
	action->name = "Moving...";

	auto gridMap = Map::GetInstance().GetMap();

	glm::vec2 mapIndexes = this->GetMapIndexes(finalPosX, finalPosZ);
	int i = mapIndexes.x;
	int j = mapIndexes.y;
	if (i > 0 && i < gridMap->size() && j > 0 && j < gridMap->size()) {
		if (gridMap->at(i).at(j).canPassThrough) {
			AddBlock(i, j);
			moveActions.emplace_back(action);
			startingPosition.x = finalPosX;
			startingPosition.z = finalPosZ;
			startingPosition.y = Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ);

			return true;
		}
	}

	return false;
}

void MovableEntity::GenerateMoveActions(unsigned int traversalCount) {
	glm::vec3 startingPosition = GetPosition();
	if (moveActions.size() < MAX_ACTIONS) {
		Action::Direction randomMove = (Action::Direction)RandomGenerator::GetNextInt(0, 7);
		for (unsigned int i = 0; i < traversalCount; i++) {
			if (!PushAction(randomMove)) {
				i--;
				randomMove = (Action::Direction)RandomGenerator::GetNextInt(0, 7);
			}
		}
	}
}

void MovableEntity::GeneratePathToFollow(std::vector<glm::vec2> path) {
	if (moveActions.size() > 0) {
		Action* action = moveActions.front();
		startingPosition = glm::vec3(action->finalPosX,
			Terrain::GetInstance()->GetHeightOfTerrain(action->finalPosX, action->finalPosZ), action->finalPosZ);
	}

	for (unsigned int i = 0; i < path.size() - 1; i++) {
		if (path[i].x > path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::TL);
		}
		else if (path[i].x == path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::T);
		}
		else if (path[i].x < path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::TR);
		}
		else if (path[i].x < path[i + 1].x && path[i].y == path[i + 1].y) {
			PushAction(Action::Direction::R);
		}
		else if (path[i].x < path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::BR);
		}
		else if (path[i].x == path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::B);
		}
		else if (path[i].x > path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::BL);
		}
		else if (path[i].x > path[i + 1].x && path[i].y == path[i + 1].y) {
			PushAction(Action::Direction::L);
		}
		else {
			std::cout << "Error, could not move!" << std::endl;
		}
	}
}

bool MovableEntity::MoveTopLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x > finalPosX && GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), TOP_LEFT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), TOP_LEFT);
	return true;
}

bool MovableEntity::MoveTop(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), TOP);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), TOP);
	return true;
}

bool MovableEntity::MoveTopRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x < finalPosX && GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), TOP_RIGHT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), TOP_RIGHT);
	return true;
}

bool MovableEntity::MoveRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x < finalPosX) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), RIGHT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), RIGHT);
	return true;
}

bool MovableEntity::MoveBottomRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x < finalPosX && GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), BOTTOM_RIGHT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), BOTTOM_RIGHT);
	return true;
}

bool MovableEntity::MoveBottom(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), BOTTOM);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), BOTTOM);
	return true;
}

bool MovableEntity::MoveBottomLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x > finalPosX && GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), BOTTOM_LEFT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), BOTTOM_LEFT);
	return true;
}

bool MovableEntity::MoveLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed) {
	if (GetPosition().x > finalPosX) {
		glm::vec3 currentPosition = GetPosition();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		SetPosition(glm::vec3(posX, posY, posZ), LEFT);
		return false;
	}

	glm::ivec2 mapIndexes = this->GetMapIndexes(squareSize);
	RemoveBlock(mapIndexes.x, mapIndexes.y);

	SetPosition(glm::vec3(finalPosX, Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ), finalPosZ), LEFT);
	return true;
}

std::deque<struct Action*>* MovableEntity::GetQueueOfActions() {
	return &moveActions;
}

void MovableEntity::ClearQueue() {
	if (moveActions.size() > 0) {
		Action* action = moveActions.front();
		while (moveActions.size() > 0) {
			moveActions.pop_front();
		}
		moveActions.emplace_back(action);
	}
}

void MovableEntity::RemoveBlock(int i, int j) {
	AStar::Generator* generator = AStarGenerator::GetGenerator();
	auto gridMap = Map::GetInstance().GetMap();

	gridMap->at(i).at(j).canPassThrough = true;
	generator->removeCollision({ i, j });
}

void MovableEntity::AddBlock(int i, int j) {
	AStar::Generator* generator = AStarGenerator::GetGenerator();
	auto gridMap = Map::GetInstance().GetMap();

	gridMap->at(i).at(j).canPassThrough = false;
	generator->addCollision({ i, j });
}

void MovableEntity::RemoveEntity(Entity* entity) {
	AStar::Generator* generator = AStarGenerator::GetGenerator();
	auto gridMap = Map::GetInstance().GetMap();
	glm::vec2 mapIndexes = entity->GetMapIndexes(Terrain::GetInstance()->GetSquareSize());

	int i = mapIndexes.x;
	int j = mapIndexes.y;

	RemoveBlock(i, j);
	gridMap->at(i).at(j) = MapBlock(MapBlock::Type::EMPTY, true, nullptr);
	generator->removeCollision({ i, j });

	MasterRenderer::GetInstance()->RemoveEntity(entity);
}

void MovableEntity::SetOnMap(unsigned int i, unsigned int j) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();
	float gridSize = Terrain::GetInstance()->GetGridSize();
	if (i >= gridSize || j >= gridSize) {
		return;
	}

	float xCoord = i * gridSquareSize + gridSquareSize / 2;
	float zCoord = j * gridSquareSize + gridSquareSize / 2;
	float yCoord = Terrain::GetInstance()->GetHeightOfTerrain(xCoord, zCoord);

	this->startingPosition = glm::vec3(xCoord, yCoord, zCoord);
	this->SetPosition(glm::vec3(xCoord, yCoord, zCoord));
}

float MovableEntity::GetSquareSize() {
	return squareSize;
}

void MovableEntity::DeleteInstance() {
	Entity::DeleteInstance();
}
