#include "Tree.h"
#include "../map/Map.h"

unsigned int Tree::ATLAS_INDEX = 0;
unsigned int Tree::TEXTURE_ROWS = 1;

float Tree::DEFAULT_ROTX = 0.0f;
float Tree::DEFAULT_ROTY = 0.0f;
float Tree::DEFAULT_ROTZ = 0.0f;
float Tree::DEFAULT_SCALE = 15.0f;

bool Tree::CAN_PASS_THROUGH = false;

unsigned int Tree::COUNT = 0;
const std::string Tree::MODEL_NAME = "tree";

std::vector<TexturedModel*> Tree::texturedModels = std::vector<TexturedModel*>(0, nullptr);

std::vector<Tree*> Tree::TREES = std::vector<Tree*>();

Tree::Tree(const glm::vec3& position) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, Tree::DEFAULT_ROTX,
		Tree::DEFAULT_ROTY, Tree::DEFAULT_ROTZ, Tree::DEFAULT_SCALE, COUNT) {
	Tree::COUNT++;
	this->SetPosition(position);
	this->UpdateMap();
	TREES.emplace_back(this);
}

void Tree::UpdateMap() {
	MapBlock mapBlock(MapBlock::WOOD, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock);
}

TexturedModel Tree::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, VegetationGenerator::TREE_COUNT);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}


std::string Tree::ToName() {
	return "Tree";
}

void Tree::DeleteInstance() {
	Tree::COUNT--;
	Entity::DeleteInstance();
	Tree::TREES.erase(std::remove(Tree::TREES.begin(), Tree::TREES.end(), this), Tree::TREES.end());
	for (unsigned int i = 0; i < TREES.size(); i++) {
		TREES[i]->SetID(i);
		TREES[i]->SetPosition(TREES[i]->GetPosition());
	}
}