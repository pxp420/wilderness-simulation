#pragma once

#include "Entity.h"
#include "../generators/VegetationGenerator.h"
#include "../objConverter/OBJFileLoader.h"

class Tree : public Entity {
private:
	static unsigned int ATLAS_INDEX;
	static unsigned int TEXTURE_ROWS;

	static float DEFAULT_ROTX;
	static float DEFAULT_ROTY;
	static float DEFAULT_ROTZ;
	static float DEFAULT_SCALE;

	static bool CAN_PASS_THROUGH;

	const static std::string MODEL_NAME;

	static std::vector<TexturedModel*> texturedModels;
public:
	static std::vector<Tree*> TREES;

	static unsigned int COUNT;

	Tree(const glm::vec3& position);

	void UpdateMap();

	TexturedModel GenerateTexturedModel();

	virtual std::string ToName() override;

	virtual void DeleteInstance() override;
};
