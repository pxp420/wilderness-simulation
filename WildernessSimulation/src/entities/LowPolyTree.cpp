#include "LowPolyTree.h"
#include "../map/Map.h"

unsigned int LowPolyTree::ATLAS_INDEX = 0;
unsigned int LowPolyTree::TEXTURE_ROWS = 1;

float LowPolyTree::DEFAULT_ROTX = 0.0f;
float LowPolyTree::DEFAULT_ROTY = 0.0f;
float LowPolyTree::DEFAULT_ROTZ = 0.0f;
float LowPolyTree::DEFAULT_SCALE = 1.9f;

bool LowPolyTree::CAN_PASS_THROUGH = false;

unsigned int LowPolyTree::COUNT = 0;
const std::string LowPolyTree::MODEL_NAME = "lowPolyTree";

std::vector<TexturedModel*> LowPolyTree::texturedModels = std::vector<TexturedModel*>(0, nullptr);

std::vector<LowPolyTree*> LowPolyTree::LOW_POLY_TREES = std::vector<LowPolyTree*>();

LowPolyTree::LowPolyTree(const glm::vec3& position) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, LowPolyTree::DEFAULT_ROTX,
		LowPolyTree::DEFAULT_ROTY, LowPolyTree::DEFAULT_ROTZ, LowPolyTree::DEFAULT_SCALE, COUNT) {
	LowPolyTree::COUNT++;
	this->SetPosition(position);
	this->UpdateMap();
	LOW_POLY_TREES.emplace_back(this);
}

LowPolyTree::LowPolyTree(const glm::vec3& position, float rotY) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, LowPolyTree::DEFAULT_ROTX,
		rotY, LowPolyTree::DEFAULT_ROTZ, LowPolyTree::DEFAULT_SCALE, COUNT) {
	LowPolyTree::COUNT++;
	this->SetPosition(position, rotY);
	this->UpdateMap();
	LOW_POLY_TREES.emplace_back(this);
}

void LowPolyTree::UpdateMap() {
	MapBlock mapBlock(MapBlock::WOOD, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock);
}

TexturedModel LowPolyTree::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, VegetationGenerator::LOW_POLY_TREE_COUNT);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}


std::string LowPolyTree::ToName() {
	return "LowPolyTree";
}

void LowPolyTree::DeleteInstance() {
	LowPolyTree::COUNT--;
	Entity::DeleteInstance();
	LowPolyTree::LOW_POLY_TREES.erase(std::remove(LowPolyTree::LOW_POLY_TREES.begin(), LowPolyTree::LOW_POLY_TREES.end(), this), LowPolyTree::LOW_POLY_TREES.end());
	for (unsigned int i = 0; i < LOW_POLY_TREES.size(); i++) {
		LOW_POLY_TREES[i]->SetID(i);
		LOW_POLY_TREES[i]->SetPosition(LOW_POLY_TREES[i]->GetPosition());
	}
}
