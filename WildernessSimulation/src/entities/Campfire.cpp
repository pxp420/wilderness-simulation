#include "Campfire.h"
#include "Human.h"

unsigned int Campfire::AREA = 25;

Campfire::Campfire(std::vector<TreeLog*>& logs, Light* light) : logs(logs), light(light), attenuationThreshold(75.0f),
	diminuationFactor(2.0f), position(light->GetPosition()) { }

std::vector<TreeLog*> Campfire::GetLogs() {
	return logs;
}

Light* Campfire::GetLight() {
	return light;
}

glm::vec3 Campfire::GetPosition() {
	return position;
}

void Campfire::Update(float deltaTime) {
	float diminuationFactor = this->diminuationFactor * deltaTime;
	glm::vec3 attenuation = light->GetAttenuation();

	light->SetAttenuation(glm::vec3(attenuation.r + diminuationFactor, attenuation.g, attenuation.b));
	for (Entity* entity : logs) {
		entity->Scale(entity->GetScale() - (this->diminuationFactor / 750.0f) * deltaTime);
	}

	glm::vec2 mapIndexes = Entity::GetMapIndexes(position.x, position.z);
	int minI = mapIndexes.x - AREA;
	int minJ = mapIndexes.y - AREA;
	int maxI = mapIndexes.x + AREA;
	int maxJ = mapIndexes.y + AREA;

	if (minI < 0) {
		minI = 0;
	} 
	if (minJ < 0) {
		minJ = 0;
	}

	if (maxI + AREA > Terrain::GetInstance()->GetGridSize() - 1) {
		maxI = Terrain::GetInstance()->GetGridSize() - 1;
	}
	if (maxJ + AREA > Terrain::GetInstance()->GetGridSize() - 1) {
		maxJ = Terrain::GetInstance()->GetGridSize() - 1;
	}

	auto gridMap = Map::GetInstance().GetMap();
	for (int i = minI; i < maxI; i++) {
		for (int j = minJ; j < maxJ; j++) {
			if (gridMap->at(i).at(j).type == MapBlock::Type::HUMAN) {
				Human* human = static_cast<Human*>(gridMap->at(i).at(j).entity);
				human->SetBodyTemperature(Human::MAX_BODY_TEMPERATURE);
			}
		}
	}

	if (attenuation.r >= attenuationThreshold) {
		this->Remove();
	}
}

void Campfire::Remove() {
	for (auto treelog : logs) {
		MovableEntity::RemoveEntity(treelog);
		treelog->DeleteInstance();

		Entity::ENTITIES.erase(std::remove(Entity::ENTITIES.begin(), Entity::ENTITIES.end(), treelog), Entity::ENTITIES.end());
	}
	Light::LIGHTS.erase(std::remove(Light::LIGHTS.begin(), Light::LIGHTS.end(), light), Light::LIGHTS.end());
	Human::CAMPFIRES.erase(std::remove(Human::CAMPFIRES.begin(), Human::CAMPFIRES.end(), this), Human::CAMPFIRES.end());
}
