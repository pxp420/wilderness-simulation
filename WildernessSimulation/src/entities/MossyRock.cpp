#include "MossyRock.h"
#include "../map/Map.h"

unsigned int MossyRock::ATLAS_INDEX = 0;
unsigned int MossyRock::TEXTURE_ROWS = 1;

float MossyRock::DEFAULT_ROTX = 0.0f;
float MossyRock::DEFAULT_ROTY = 0.0f;
float MossyRock::DEFAULT_ROTZ = 0.0f;
float MossyRock::DEFAULT_SCALE = 8.0f;

bool MossyRock::CAN_PASS_THROUGH = false;

unsigned int MossyRock::COUNT = 0;
const std::string MossyRock::MODEL_NAME = "mossyRock";

std::vector<TexturedModel*> MossyRock::texturedModels = std::vector<TexturedModel*>(0, nullptr);

MossyRock::MossyRock(const glm::vec3& position) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, MossyRock::DEFAULT_ROTX,
		MossyRock::DEFAULT_ROTY, MossyRock::DEFAULT_ROTZ, MossyRock::DEFAULT_SCALE, COUNT) {
	MossyRock::COUNT++;
	this->SetPosition(position);
	this->UpdateMap();
}

void MossyRock::UpdateMap() {
	MapBlock mapBlock(MapBlock::ROCK, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock);
}

TexturedModel MossyRock::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, VegetationGenerator::MOSSY_ROCK_COUNT);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}


std::string MossyRock::ToName() {
	return "MossyRock";
}

void MossyRock::DeleteInstance() {
	MossyRock::COUNT--;
	Entity::DeleteInstance();
}