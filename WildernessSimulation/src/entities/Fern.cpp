#include "Fern.h"
#include "../map/Map.h"
#include "../pathFinding/AStarGenerator.h"

unsigned int Fern::ATLAS_INDEX = 0;
unsigned int Fern::TEXTURE_ROWS = 2;

float Fern::DEFAULT_ROTX = 0.0f;
float Fern::DEFAULT_ROTY = 0.0f;
float Fern::DEFAULT_ROTZ = 0.0f;
float Fern::DEFAULT_SCALE = 2.0f;

bool Fern::CAN_PASS_THROUGH = true;

unsigned int Fern::COUNT = 0;
const std::string Fern::MODEL_NAME = "fern";

std::vector<TexturedModel*> Fern::texturedModels = std::vector<TexturedModel*>(0, nullptr);

std::vector<Fern*> Fern::FERNS = std::vector<Fern*>();

Fern::Fern(const glm::vec3& position) :
	Entity::Entity(GenerateTexturedModel(), ATLAS_INDEX, position, Fern::DEFAULT_ROTX, 
		Fern::DEFAULT_ROTY, Fern::DEFAULT_ROTZ, Fern::DEFAULT_SCALE, COUNT) {
	Fern::COUNT++;
	this->SetPosition(position);
	this->UpdateTextureOffsets();
	this->UpdateMap();
	FERNS.emplace_back(this);
}

void Fern::UpdateMap() {
	MapBlock mapBlock(MapBlock::PLANT, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock); 
}

void Fern::UpdateTextureOffsets() {
	unsigned int textureIndex = RandomGenerator::GetNextInt(0, glm::pow(TEXTURE_ROWS, 2) - 1);
	glm::vec2 textureOffset(GetTextureXOffset(TEXTURE_ROWS, textureIndex),
		GetTextureYOffset(TEXTURE_ROWS, textureIndex));

	std::vector<float> textureOffsetsData;
	textureOffsetsData.emplace_back(textureOffset.x);
	textureOffsetsData.emplace_back(textureOffset.y);

	unsigned int* vboIDs = GetModel().GetRawModel().GetVboIDs();
	glBindVertexArray(GetModel().GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TEXTURE_OFFSETS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * textureOffsetsData.size() * sizeof(float),
		textureOffsetsData.size() * sizeof(float), &textureOffsetsData.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	textureOffsetsData.clear();
}

TexturedModel Fern::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, VegetationGenerator::FERN_COUNT);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}

void Fern::DeleteInstance() {
	Fern::COUNT--;
	Entity::DeleteInstance();
	Fern::FERNS.erase(std::remove(Fern::FERNS.begin(), Fern::FERNS.end(), this), Fern::FERNS.end());
	for (unsigned int i = 0; i < FERNS.size(); i++) {
		FERNS[i]->SetID(i);
		FERNS[i]->SetPosition(FERNS[i]->GetPosition());
	}
}

std::string Fern::ToName() {
	return "Fern";
}
