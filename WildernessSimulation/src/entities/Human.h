#pragma once

#include "MovableEntity.h"
#include "Object.h"
#include "../guis/GuiTexture.h"
#include "Campfire.h"
#include "../generators/DayCycleGenerator.h"

#include <cstdio>
#include <functional>

class Human : public MovableEntity {
private:
	static unsigned int ATLAS_INDEX;
	static unsigned int TEXTURE_ROWS;

	static int MAP_RANGE;

	static float DEFAULT_ROTX;
	static float DEFAULT_ROTY;
	static float DEFAULT_ROTZ;
	static float DEFAULT_SCALE;

	static bool CAN_PASS_THROUGH;

	const static std::string MODEL_NAME;

	static float ABOVE_HEAD;
	static float MIDDLE;

	static float BODY_TEMPERATURE_FACTOR;

	static float WIDTH_BETWEEN_FRAMES;
	static float HEIGHT_BETWEEN_ICONS;

	static glm::vec3 INVENTORY_BOTTOM_RIGHT_POSITION;
	static glm::vec3 FRAME_BOTTOM_RIGHT_POSITION;
	static glm::vec3 ICON_TOP_RIGHT_POSITION;

	float health;
	float energy;
	float hunger;
	float thirst;
	float bodyTemperature;

	float speed;

	unsigned int maxInventorySpace;
	std::map<unsigned int, Object*> items;

	bool rotationFinished;
	Action::Direction previousDirection;

	enum Trait { HEART_ICON, HUNGER_ICON, ENERGY_ICON, THIRST_ICON, TEMP_ICON };
	std::map<Trait, GuiTexture*> guiTextures;

	std::vector<GUIText*> guiTexts;
	std::vector<GUIText*> inventoryGuiTexts;
	static std::vector<TexturedModel*> texturedModels;

	TerrainTexture logFrame;
	TerrainTexture plantFrame;
	TerrainTexture waterFrame;

	float terrainSquareSize;
	float terrainGridSize;

	float timePerAction;

	static Human* instance;
public:
	static unsigned int MAX_INVENTORY_SPACE;

	static float HEALTH;
	static float ENERGY;
	static float HUNGER;
	static float THIRST;
	static float BODY_TEMPERATURE;

	static std::vector<Campfire*> CAMPFIRES;
	static float MAX_BODY_TEMPERATURE;
	static float HUNGER_BEFORE_EATING;
	static float THIRST_BEFORE_DRINKING;
	static float TEMPERATURE_BEFORE_CAMPFIRE;

	static unsigned int COUNT;

	static void CreateInstance(const glm::vec3& position);

	static void DestroyInstance();

	static Human* GetInstance();

	Human(const glm::vec3& position);

	virtual std::string ToName() override;

	virtual void DeleteInstance() override;

	void SetPreviousDirection(Action::Direction previousDirection);

	void UpdateMap();

	float GetHealth();

	void SetHealth(float health);

	float GetEnergy();

	void SetEnergy(float energy);
	
	float GetHunger();

	void SetHunger(float hunger);

	float GetThirst();

	void SetThirst(float thirst);

	float GetBodyTemperature();

	void SetBodyTemperature(float bodyTemperature);

	unsigned int GetMaxInventorySpace();

	void SetMaxInventorySpace(unsigned int inventorySpace);

	void DisplayInventoryCounter(unsigned int counter);

	void DisplayFrames();

	void DisplayIcons();

	bool HasBerries();

	bool HasCampfireLogs();

	bool HasOneLog();

	bool CampfireInRange();

	unsigned int GetNumberOfLogs();

	unsigned int GetNumberOfBerries();

	unsigned int GetNumberOfWater();

	bool HasWater();

	unsigned int GetCurrentItemsNumber();

	GuiTexture* DisplayItem(Object::InventorySlot object, unsigned int counter);

	std::map<unsigned int, Object*> GetItems();

	TexturedModel GenerateTexturedModel();

	void Update();

	bool Stay(float deltaTime, float& timeToFinish);

	void GenerateMovesToLand();

	bool FindWood();

	bool FindPlant();

	bool ChopWood(Entity* entity, float deltaTime, float percentage);

	bool GatherPlant(Entity* entity, float deltaTime, float percentage);

	bool EatBerry(float deltaTime, float& timeToFinish);

	bool FindWater();

	bool CollectWater(float deltaTime, float& timeToFinish);

	bool DrinkWater(float deltaTime, float& timeToFinish);

	bool PlaceItem(unsigned int inventorySlotNumber, float deltaTime, float& timeToFinish);

	bool PlaceCampFire(float deltaTime, float& timeToFinish);

	void Explore();

	void GenerateActions();

	void UpdateTexts();

	void SetGUITextPosition();

	virtual bool PushAction(Action::Direction direction) override;

	virtual void GeneratePathToFollow(std::vector<glm::vec2> path) override;

	virtual void GenerateMoveActions(unsigned int traversalCount) override;

	bool Rotate(float deltaTime, float posX, float posZ, 
		Action::Direction fromDirection, Action::Direction toDirection, float* currentRotY, bool moveFinished);

	/**
	* Moves one square towards top left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveTopLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards top.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveTop(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards top right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveTopRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards bottom right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveBottomRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards bottom.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveBottom(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards bottom left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveBottomLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);

	/**
	* Moves one square towards left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	bool MoveLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed);
};
