#pragma once

#include "Entity.h"
#include "../toolbox/RandomGenerator.h"
#include "../map/Map.h"

class MovableEntity : public Entity {
protected:
	glm::vec3 startingPosition;

	static float TOP_LEFT;
	static float TOP;
	static float TOP_RIGHT;
	static float RIGHT;
	static float BOTTOM_RIGHT;
	static float BOTTOM;
	static float BOTTOM_LEFT;
	static float LEFT;
	static float ROTATION_SPEED;

	static unsigned int MAX_ACTIONS;

	static float SPEED;

	std::deque<struct Action*> moveActions;
	float squareSize;
public:
	MovableEntity(const TexturedModel& model, unsigned int index, const glm::vec3& position,
		float rotX, float rotY, float rotZ, float scale, unsigned int id);

	/**
	* Generates a sequence of moves for the entity to make.
	*
	* @param entity
	*		- The entity that will follow the path.
	*
	* @param path
	*		- The path created by the path finding algorithm.
	*
	* @param terrain
	*		- The terrain where the entity will move on.
	*/
	void GeneratePathToFollow(std::vector<glm::vec2> path);

	static void RemoveEntity(Entity* entity);

	virtual void GenerateMoveActions(unsigned int traversalCount) override;

	/**
	* Get the queue of actions the entity will make.
	*
	* @return The queue of actions.
	*/
	virtual std::deque<struct Action*>* GetQueueOfActions() override;

	/**
	* Remove all the actions the current entity is taking apart from the last one.
	*/
	void ClearQueue();

	static void RemoveBlock(int i, int j);

	void AddBlock(int i, int j);

	virtual bool PushAction(Action::Direction direction);

	/**
	* Moves one square towards top left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveTopLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards top.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveTop(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards top right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveTopRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards bottom right.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveBottomRight(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards bottom.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveBottom(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards bottom left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveBottomLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Moves one square towards left.
	*
	* @param entity
	*		- The entity that moves.
	*
	* @param deltaTime
	*		- The time between frames.
	*
	* @param finalPosX
	*		- The X position where the entity has to arrive to.
	*
	* @param finalPosZ
	*		- The Z position where the entity has to arrive to.
	*
	* @param squareSize
	*		- The size of a grid square of the map.
	*
	* @param terrain
	*		- The terrain where the entity moves on.
	*
	* @return True if the action has completed.
	*/
	virtual bool MoveLeft(float deltaTime, float finalPosX, float finalPosZ, float squareSize, float speed);

	/**
	* Set this entity on a given i,j index on the map.
	*
	* @param i
	*		- The i-th square of the map.
	*
	* @param j
	*		- The j-th square of the map.
	*
	* @param terrain
	*		- The terrain where this entity will be placed on.
	*/
	virtual void SetOnMap(unsigned int i, unsigned int j) override;

	float GetSquareSize();

	virtual void DeleteInstance() override;
};
