#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <vector>

class Light {
private:
	glm::vec3 position;
	glm::vec3 colour;
	glm::vec3 attenuation;
public:
	static std::vector<Light*> LIGHTS;

	/**
	 * Sets the position and colour of a light source.
	 *
	 * @param position
	 *		- The position in the space.
	 *
	 * @param colour
	 *		- The colour the light emmanates.
	*/
	Light(glm::vec3 position, glm::vec3 colour);

	/**
	* Sets the position and colour of a light source.
	*
	* @param position
	*		- The position in the space.
	*
	* @param colour
	*		- The colour the light emmanates.
	*
	* @param attenuation
	*		- The birghtness of the light based on the distance.
	*/
	Light(glm::vec3 position, glm::vec3 colour, glm::vec3 attenuation);

	/**
	 * Gets the position of the light source.
	 *
	 * @return The position of the light source.
	*/
	glm::vec3 GetPosition();

	/**
	 * Sets the position of the light source.
	 *
	 * @param position
	 *		- The position of the light source.
	*/
	void SetPosition(const glm::vec3& position);

	/**
	 * Gets the colour of the light source.
	 *
	 * @return The colour of the light source.
	*/
	glm::vec3 GetColour();

	/**
	 * Sets the colour of the light source.
	 *
	 * @param colour
	 *		- The colour of the light source.
	*/
	void SetColour(const glm::vec3& colour);

	/**
	* Gets the brightness of the light source.
	*
	* @return The brightness of the light source.
	*/
	glm::vec3 GetAttenuation();

	void SetAttenuation(const glm::vec3& attenuation);
};
