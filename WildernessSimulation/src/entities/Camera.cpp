#include "Camera.h"

Camera* Camera::instance = nullptr;

float Camera::SCROLL_DISTANCE = 0.0f;
float Camera::BOUNDARY = 10.0f;
float Camera::MOVEMENT_SPEED = 20.0f;
float Camera::INITIAL_OFFSET = 500.0f;
float Camera::INITIAL_Y_POS = 615.0f;
float Camera::MIN_SCROLL_POS = 275.0f;
float Camera::MAX_SCROLL_POS = 550.0f;

Camera::Camera() :
	pitch(40.0f), yaw(0.0f), roll(0.0f), offset(240.0f),
	position(glm::vec3(1800.0f, 300.0f, 1800.0f)) {
	glfwSetScrollCallback(DisplayManager::window, CalculateZoom);
}

void Camera::CreateInstance() {
	instance = new Camera();
}

void Camera::DeleteInstance() {
	delete instance;
}

Camera* Camera::GetInstance() {
	return instance;
}

void Camera::Move() {
	UpdateBoundary();
}

glm::vec3 Camera::GetPosition() {
	return position;
}

void Camera::SetPosition(const glm::vec3& position) {
	this->position = position;
}

float Camera::GetPitch() {
	return pitch;
}

void Camera::InvertPitch() {
	pitch = -pitch;
}

float Camera::GetYaw() {
	return yaw;
}

float Camera::GetRoll() {
	return roll;
}

void Camera::UpdateBoundary() {
	double xpos, ypos;
	glfwGetCursorPos(DisplayManager::window, &xpos, &ypos);

	glm::vec3 cameraPosition = GetPosition();
	if (xpos > DisplayManager::WIDTH - BOUNDARY) {
		cameraPosition.x += MOVEMENT_SPEED;
	}

	if (xpos < 0.0f + BOUNDARY) {
		cameraPosition.x -= MOVEMENT_SPEED;
	}

	if (ypos > DisplayManager::HEIGHT - BOUNDARY) {
		cameraPosition.z += MOVEMENT_SPEED;
	}

	if (ypos < 0.0f + BOUNDARY) {
		cameraPosition.z -= MOVEMENT_SPEED;
	}

	float yPosBefore = cameraPosition.y;
	SCROLL_DISTANCE *= 2;
	cameraPosition.y += SCROLL_DISTANCE;
	SCROLL_DISTANCE = 0.0f;

	if (cameraPosition.y < MIN_SCROLL_POS) {
		cameraPosition.y = MIN_SCROLL_POS;
	}
	else if (cameraPosition.y > MAX_SCROLL_POS) {
		//cameraPosition.y = MAX_SCROLL_POS;
	}

	if ((cameraPosition.x >= offset && cameraPosition.z > offset + 150.0f) &&
		(cameraPosition.x < Terrain::SIZE - offset && cameraPosition.z < Terrain::SIZE)) {
		SetPosition(cameraPosition);
	}
	offset = INITIAL_OFFSET * cameraPosition.y / INITIAL_Y_POS;
}

void Camera::CalculateZoom(GLFWwindow* window, double xoffset, double yoffset) {
	SCROLL_DISTANCE -= (float)yoffset;
}
