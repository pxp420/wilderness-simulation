#pragma once

#include <vector>
#include <algorithm>

#include "TreeLog.h"

class Campfire {
private:
	std::vector<TreeLog*> logs;
	Light* light;

	float attenuationThreshold;
	float diminuationFactor;

	glm::vec3 position;
public:
	static unsigned int AREA;

	Campfire(std::vector<TreeLog*>& logs, Light* light);

	Light* GetLight();

	std::vector<TreeLog*> GetLogs();

	glm::vec3 GetPosition();

	void Update(float deltaTime);

	void Remove();
};