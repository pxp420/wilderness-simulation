#include "Object.h"

unsigned int Object::OBJECT_COUNTER = 0;

Object::Object(InventorySlot item, GuiTexture* gui) : item(item), id(OBJECT_COUNTER),
	gui(gui) {
	OBJECT_COUNTER++;
}

Object::InventorySlot Object::GetItem() {
	return item;
}

GuiTexture* Object::GetGuiTexture() {
	return gui;
}
