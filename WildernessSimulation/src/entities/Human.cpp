#include "Human.h"
#include "../fontRenderer/TextMaster.h"
#include "../pathFinding/AStarGenerator.h"
#include "../generators/VegetationGenerator.h"
#include "../renderEngine/MasterRenderer.h"
#include "../reinforcedLearning/Simulation.h"
#include "../entities/TreeLog.h"


Human* Human::instance = nullptr;

unsigned int Human::ATLAS_INDEX = 0;
unsigned int Human::TEXTURE_ROWS = 1;
unsigned int Human::COUNT = 0;

int Human::MAP_RANGE = 399;
	
float Human::DEFAULT_ROTX = 0.0f;
float Human::DEFAULT_ROTY = 0.0f;
float Human::DEFAULT_ROTZ = 0.0f;
float Human::DEFAULT_SCALE = 3.0f;

bool Human::CAN_PASS_THROUGH = false;

float Human::ABOVE_HEAD = 25.0f;
float Human::MIDDLE = 9.0f;

float Human::HEALTH = 200.0f;
float Human::ENERGY = 100.0f;
float Human::HUNGER = 100.0f;
float Human::THIRST = 100.0f;

float Human::HUNGER_BEFORE_EATING = 100.0f;
float Human::THIRST_BEFORE_DRINKING = 100.0f;
float Human::TEMPERATURE_BEFORE_CAMPFIRE = 5.0f;

float Human::BODY_TEMPERATURE = 0.0f;
float Human::BODY_TEMPERATURE_FACTOR = 2.5f;
float Human::MAX_BODY_TEMPERATURE = 5.0f;

unsigned int Human::MAX_INVENTORY_SPACE = 21;

float Human::WIDTH_BETWEEN_FRAMES = 60.0f;
float Human::HEIGHT_BETWEEN_ICONS = 80.0f;

glm::vec3 Human::INVENTORY_BOTTOM_RIGHT_POSITION = glm::vec3(1240.0f, 40.0f, 0.1f);
glm::vec3 Human::FRAME_BOTTOM_RIGHT_POSITION = glm::vec3 (1240.0f, 30.0f, 0.1f);
glm::vec3 Human::ICON_TOP_RIGHT_POSITION = glm::vec3(1230.0f, 490.0f, 0.1f);

const std::string Human::MODEL_NAME = "person";
std::vector<TexturedModel*> Human::texturedModels = std::vector<TexturedModel*>(0, nullptr);

std::vector<Campfire*> Human::CAMPFIRES = std::vector<Campfire*>(0);

Human::Human(const glm::vec3& position) :
	MovableEntity::MovableEntity(GenerateTexturedModel(), ATLAS_INDEX, position, Human::DEFAULT_ROTX,
		Human::DEFAULT_ROTY, Human::DEFAULT_ROTZ, Human::DEFAULT_SCALE, COUNT),
	health(HEALTH), 
	energy(ENERGY), 
	hunger(HUNGER),
	thirst(THIRST), 
	bodyTemperature(BODY_TEMPERATURE), 
	speed(2.0f),
	maxInventorySpace(MAX_INVENTORY_SPACE),
	rotationFinished(false),
	inventoryGuiTexts(std::vector<GUIText*>(21)),
	previousDirection(Action::Direction::B),
	items(std::map<unsigned int, Object*>({ 
		{0, nullptr},
		{1, nullptr},
		{2, nullptr},
		{3, nullptr},
		{4, nullptr},
		{5, nullptr},
		{6, nullptr},
		{7, nullptr},
		{8, nullptr},
		{ 9, nullptr },
		{ 10, nullptr },
		{ 11, nullptr },
		{ 12, nullptr },
		{ 13, nullptr },
		{ 14, nullptr },
		{ 15, nullptr },
		{ 16, nullptr },
		{ 17, nullptr },
		{ 18, nullptr },
		{ 19, nullptr },
		{ 20, nullptr }
	})),
	terrainSquareSize(Terrain::GetInstance()->GetSquareSize()), 
	terrainGridSize(Terrain::GetInstance()->GetGridSize()),
	logFrame(TerrainTexture(Loader::GetInstance().LoadTexture("logFrame.png"))),
	plantFrame(TerrainTexture(Loader::GetInstance().LoadTexture("plantFrame.png"))),
    waterFrame(TerrainTexture(Loader::GetInstance().LoadTexture("waterFrame.png"))),
	timePerAction(10.0f) {
	Human::COUNT++;
	guiTexts.emplace_back(new GUIText(std::string("Human"), Entity::FONT, position, ABOVE_HEAD, GUIText::Dimensions::THREE_D));
	guiTexts.emplace_back(new GUIText(std::string("Action"), Entity::FONT, position, MIDDLE, GUIText::Dimensions::THREE_D));
	this->SetPosition(position);
	this->SetRotZ(DEFAULT_ROTZ);
	this->DisplayFrames();
	this->DisplayIcons();
}

void Human::CreateInstance(const glm::vec3& position) {
	instance = new Human(position);
}

void Human::DestroyInstance() {
	for (GUIText* guiText : Human::GetInstance()->guiTexts) {
		TextMaster::RemoveText(guiText);
	}
	Human::GetInstance()->guiTexts.clear();
	GuiTexture::GUI_TEXTURES.clear();

	for (auto item : Human::GetInstance()->items) {
		delete item.second;
	}
	Human::GetInstance()->items.clear();

	for (auto texturedModel : texturedModels) {
		delete texturedModel;
	}
	texturedModels.clear();

	Entity::ENTITIES.erase(std::remove(Entity::ENTITIES.begin(), Entity::ENTITIES.end(), instance), Entity::ENTITIES.end());
	delete instance;
}

Human* Human::GetInstance() {
	return instance;
}

void Human::UpdateMap() {
	MapBlock mapBlock(MapBlock::HUMAN, CAN_PASS_THROUGH, this);
	Map::GetInstance().AddBlockGivenPosition(this->GetPosition(), mapBlock);
}

TexturedModel Human::GenerateTexturedModel() {
	if (texturedModels.empty()) {
		TexturedModel* model = Entity::GenerateTexturedModel(MODEL_NAME, TEXTURE_ROWS, VegetationGenerator::HUMAN_COUNT);

		texturedModels.emplace_back(model);
		return *model;
	}

	return *texturedModels.front();
}

float Human::GetHealth() {
	return health;
}

void Human::SetHealth(float health) {
	this->health = health;
}

float Human::GetEnergy() {
	return energy;
}

void Human::SetEnergy(float energy) {
	this->energy = energy;
}

float Human::GetHunger() {
	return hunger;
}

void Human::SetHunger(float hunger) {
	this->hunger = hunger;
}

float Human::GetThirst() {
	return thirst;
}

void Human::SetThirst(float thirst) {
	this->thirst = thirst;
}

float Human::GetBodyTemperature() {
	return bodyTemperature;
}

void Human::SetBodyTemperature(float bodyTemperature) {
	this->bodyTemperature = bodyTemperature;
}

unsigned int Human::GetMaxInventorySpace() {
	return maxInventorySpace;
}

void Human::SetMaxInventorySpace(unsigned int maxInventorySpace) {
	this->maxInventorySpace = maxInventorySpace;
}

void Human::DisplayInventoryCounter(unsigned int counter) {
	glm::vec3 framePosition = INVENTORY_BOTTOM_RIGHT_POSITION;
	framePosition.x = INVENTORY_BOTTOM_RIGHT_POSITION.x - counter * WIDTH_BETWEEN_FRAMES;
	glm::vec4 positionAndScale = Maths::TransformToScreenSpace(framePosition);

	inventoryGuiTexts.at(6 - counter - 1) = new GUIText(std::string("x1"), Entity::FONT,
		glm::vec2(positionAndScale.x / 2.0f, -positionAndScale.y), GUIText::Dimensions::TWO_D);
}

void Human::DisplayFrames() {
	TerrainTexture frameTexture(Loader::GetInstance().LoadTexture("frame.png"));
	glm::vec3 framePosition = FRAME_BOTTOM_RIGHT_POSITION;
	for (int i = 0; i < maxInventorySpace; i++) {
		glm::vec4 positionAndScale = Maths::TransformToScreenSpace(framePosition);

		framePosition.x -= WIDTH_BETWEEN_FRAMES;
		GuiTexture* frameGUI = new GuiTexture(frameTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	}
}

void Human::DisplayIcons() {
	TerrainTexture emptyHeartTexture(Loader::GetInstance().LoadTexture("heart.png"));
	TerrainTexture fullHeartTexture(Loader::GetInstance().LoadTexture("fullHeart.png"));
	glm::vec3 heartPosition = ICON_TOP_RIGHT_POSITION;

	glm::vec4 positionAndScale = Maths::TransformToScreenSpace(heartPosition);
	GuiTexture* emptyHeartGUI = new GuiTexture(emptyHeartTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	GuiTexture* fullHeartGUI = new GuiTexture(fullHeartTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	guiTextures.emplace(std::make_pair(Trait::HEART_ICON, fullHeartGUI));

	TerrainTexture emptyHungerTexture(Loader::GetInstance().LoadTexture("ham.png"));
	TerrainTexture fullHungerTexture(Loader::GetInstance().LoadTexture("fullHam.png"));
	glm::vec3 hungerPosition = glm::vec3(heartPosition.x, heartPosition.y - HEIGHT_BETWEEN_ICONS, heartPosition.z);

	positionAndScale = Maths::TransformToScreenSpace(hungerPosition);
	GuiTexture* emptyHungerGUI = new GuiTexture(emptyHungerTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	GuiTexture* fullHungerGUI = new GuiTexture(fullHungerTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	guiTextures.emplace(std::make_pair(Trait::HUNGER_ICON, fullHungerGUI));

	TerrainTexture emptyThirstTexture(Loader::GetInstance().LoadTexture("water.png"));
	TerrainTexture fullThirstTexture(Loader::GetInstance().LoadTexture("waterFull.png"));
	glm::vec3 thirstPosition = glm::vec3(heartPosition.x, heartPosition.y - 2 * HEIGHT_BETWEEN_ICONS, heartPosition.z);

	positionAndScale = Maths::TransformToScreenSpace(thirstPosition);
	GuiTexture* emptyThirstGUI = new GuiTexture(emptyThirstTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	GuiTexture* fullThirstGUI = new GuiTexture(fullThirstTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	guiTextures.emplace(std::make_pair(Trait::THIRST_ICON, fullThirstGUI));

	TerrainTexture emptyTempTexture(Loader::GetInstance().LoadTexture("temp.png"));
	TerrainTexture fullTempTexture(Loader::GetInstance().LoadTexture("tempFull.png"));
	glm::vec3 tempPosition = glm::vec3(heartPosition.x, heartPosition.y - 3 * HEIGHT_BETWEEN_ICONS, heartPosition.z);

	positionAndScale = Maths::TransformToScreenSpace(tempPosition);
	GuiTexture* emptyTempGUI = new GuiTexture(emptyTempTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	GuiTexture* fullTempGUI = new GuiTexture(fullTempTexture.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
	guiTextures.emplace(std::make_pair(Trait::TEMP_ICON, fullTempGUI));
}

GuiTexture* Human::DisplayItem(Object::InventorySlot object, unsigned int counter) {
	glm::vec3 framePosition = FRAME_BOTTOM_RIGHT_POSITION;
	if (object == Object::LOG) {
		framePosition.x = FRAME_BOTTOM_RIGHT_POSITION.x - counter * WIDTH_BETWEEN_FRAMES;
		glm::vec4 positionAndScale = Maths::TransformToScreenSpace(framePosition);

		GuiTexture* logFrameGUI = new GuiTexture(logFrame.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
		return logFrameGUI;
	}
	else if (object == Object::PLANT) {
		framePosition.x = FRAME_BOTTOM_RIGHT_POSITION.x - counter * WIDTH_BETWEEN_FRAMES;
		glm::vec4 positionAndScale = Maths::TransformToScreenSpace(framePosition);

		GuiTexture* plantFrameGUI = new GuiTexture(plantFrame.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
		return plantFrameGUI;
	}
	else if (object == Object::WATER) {
		framePosition.x = FRAME_BOTTOM_RIGHT_POSITION.x - counter * WIDTH_BETWEEN_FRAMES;
		glm::vec4 positionAndScale = Maths::TransformToScreenSpace(framePosition);

		GuiTexture* waterFrameGUI = new GuiTexture(waterFrame.GetTextureID(), glm::vec2(positionAndScale.x, positionAndScale.y), glm::vec2(positionAndScale.z, positionAndScale.w));
		return waterFrameGUI;
	}
}

std::map<unsigned int, Object*> Human::GetItems() {
	return items;
}

void Human::Update() {
	float deltaTime = DisplayManager::GetFrameTimeSeconds();
	if (health > 0.0f) {
		hunger -= deltaTime / 2;
		thirst -= deltaTime / 2;
	}

	auto heart = guiTextures.find(Trait::HEART_ICON);
	std::vector<float> heartPositions = {
		-1.0f,  ((health / 200.0f) * 2.0f) - 1.0f,
		-1.0f, -1.0f,
		1.0f,  ((health / 200.0f) * 2.0f) - 1.0f,
		1.0f, -1.0f
	};
	heart->second->ChangeQuadPositions(heartPositions);

	auto ham = guiTextures.find(Trait::HUNGER_ICON);
	std::vector<float> hamPositions = {
		-1.0f,  ((hunger / 100.0f) * 2.0f) - 1.0f,
		-1.0f, -1.0f,
		1.0f,  ((hunger / 100.0f) * 2.0f) - 1.0f,
		1.0f, -1.0f
	};
	ham->second->ChangeQuadPositions(hamPositions);

	auto bottle = guiTextures.find(Trait::THIRST_ICON);
	std::vector<float> bottlePositions = {
		-1.0f,  ((thirst / 100.0f) * 2.0f) - 1.0f,
		-1.0f, -1.0f,
		1.0f,  ((thirst / 100.0f) * 2.0f) - 1.0f,
		1.0f, -1.0f
	};
	bottle->second->ChangeQuadPositions(bottlePositions);


	auto fire = guiTextures.find(Trait::TEMP_ICON);
	std::vector<float> firePositions = {
		-1.0f,  bodyTemperature / 5.0f,
		-1.0f, -1.0f,
		1.0f,   bodyTemperature / 5.0f,
		1.0f, -1.0f
	};
	fire->second->ChangeQuadPositions(firePositions);

	hunger = std::fmax(0.0f, hunger);
	energy = std::fmax(0.0f, energy);
	thirst = std::fmax(0.0f, thirst);

	for (Campfire* campfire : CAMPFIRES) {
		campfire->Update(deltaTime);
	}

	unsigned int counter = 0;
	if (DayCycleGenerator::TIME_OF_DAY == DayCycleGenerator::Time::NIGHT && !CampfireInRange()) {
		bodyTemperature -=  BODY_TEMPERATURE_FACTOR * deltaTime / 2;
		if (bodyTemperature < 0.0f) {
			counter++;
		}

		if (bodyTemperature < -MAX_BODY_TEMPERATURE) {
			bodyTemperature = -MAX_BODY_TEMPERATURE;
		}
	}
	else {
		bodyTemperature = 5.0f;
	}

	TEMPERATURE_BEFORE_CAMPFIRE = bodyTemperature;

	if (hunger <= 0.0f) {
		counter++;
	}
	if (thirst <= 0.0f) {
		counter++;
	}

	health -= counter * deltaTime * 2;
	GenerateActions();

	//std::cout << "COUNTERS " << counter << std::endl;
	//std::cout << "TEMPERATURE: " << this->GetBodyTemperature() << std::endl;
}

void Human::SetGUITextPosition() {
	for (GUIText* guiText : guiTexts) {
		if (guiText != nullptr) {
			glm::vec3 entityPosition = GetPosition();
			entityPosition.y += guiText->GetOffset();
			guiText->SetPosition(entityPosition);
		}
	}
}

void Human::UpdateTexts() {
	auto actions = GetQueueOfActions();

	std::string actionName = "Idle...";
	if (!actions->empty()) {
		actionName = actions->front()->name;
	}

	GUIText* guiText = guiTexts.back();
	if (guiText->GetTextString() != actionName) {
		TextMaster::RemoveText(guiTexts.back());
		guiText->SetTextString(actionName);
		guiText->SetFontSize(5.0f);
		guiTexts.emplace_back(guiText);
		TextMaster::LoadText(guiText);
	}

	SetGUITextPosition();
}

void Human::GenerateActions() {
	UpdateTexts();
	auto simulator = Simulation::GetInstance();
	//simulator->Simulate("res/weights/weight133-survived19d0h.xml", "res/weights/memory1937.txt");
	simulator->Simulate();
}

bool Human::HasBerries() {
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::PLANT) {
				return true;
			}
		}
	}

	return false;
}

bool Human::HasWater() {
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::WATER) {
				return true;
			}
		}
	}

	return false;
}

bool Human::HasCampfireLogs() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::LOG) {
				counter++;
				if (counter == 2) {
					return true;
				}
			}
		}
	}

	return false;
}

bool Human::HasOneLog() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::LOG) {
				counter++;
			}
		}
	}
	return (counter == 1);
}

bool Human::CampfireInRange() {
	glm::vec2 humanIndexes = GetMapIndexes(terrainSquareSize);
	for (Campfire* campfire : CAMPFIRES) {
		glm::vec3 campfirePos = campfire->GetPosition();
		glm::vec2 campFireIndexes = GetMapIndexes(campfirePos.x, campfirePos.z);

		if (glm::abs(campFireIndexes.x - humanIndexes.x) < Campfire::AREA ||
			glm::abs(campFireIndexes.y - humanIndexes.y) < Campfire::AREA) {
			return true;
		}
	}

	return false;
}

unsigned int Human::GetNumberOfLogs() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::LOG) {
				counter++;
			}
		}
	}
	return counter;
}

unsigned int Human::GetNumberOfBerries() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::PLANT) {
				counter++;
			}
		}
	}
	return counter;
}

unsigned int Human::GetNumberOfWater() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::WATER) {
				counter++;
			}
		}
	}
	return counter;
}

unsigned int Human::GetCurrentItemsNumber() {
	unsigned int counter = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			counter++;
		}
	}

	return counter;
}

void Human::SetPreviousDirection(Action::Direction previousDirection) {
	this->previousDirection = previousDirection;
}

bool Human::FindWood() {
	glm::vec2 mapIndexes = this->GetMapIndexes(terrainSquareSize);
	int mapI = mapIndexes.x;
	int mapJ = mapIndexes.y;

	auto gridMap = Map::GetInstance().GetMap();

	bool found = false;
	glm::vec2 woodPos;
	glm::ivec2 humanPos;

	int center = MAP_RANGE / 2;
	int counter = 3;
	int index = 1;
	while (center - index >= 0 && !found) {
		for (unsigned int i = mapI - index; i < mapI - index + counter && !found && i > 0 && i < Terrain::GetInstance()->GetGridSize(); i++) {
			for (unsigned int j = mapJ - index; j < mapJ - index + counter && !found && j > 0 && j < Terrain::GetInstance()->GetGridSize(); j++) {
				if (gridMap->at(i).at(j).type == MapBlock::WOOD) {
					woodPos = glm::vec2(i, j);
					if (gridMap->at(i + 1).at(j).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j);
					}
					else if (gridMap->at(i - 1).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j - 1);
					}
					else if (gridMap->at(i - 1).at(j).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j);
					}
					else if (gridMap->at(i - 1).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j + 1);
					}
					else if (gridMap->at(i).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i, j - 1);
					}
					else if (gridMap->at(i).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i, j + 1);
					}
					else if (gridMap->at(i + 1).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j - 1);
					}
					else if (gridMap->at(i + 1).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j + 1);
					}
				}
			}
		}

		counter = counter + 2;
		index++;
	}

	if (found) {
		std::vector<glm::vec2> path = AStarGenerator::GeneratePath(mapI, mapJ, (int)humanPos.x, (int)humanPos.y);
		if (!path.empty()) {
			this->GeneratePathToFollow(path);

			AStar::Generator* gen = AStarGenerator::GetGenerator();

			Entity* tree = gridMap->at(woodPos.x).at(woodPos.y).entity;
			Action* action = new Action;
			action->name = "Chopping wood!";
			action->move = std::bind(&Human::ChopWood, this, tree, std::placeholders::_1, tree->GetScale() / 4);

			auto moveActions = this->GetQueueOfActions();
			moveActions->emplace_back(action);
		}
	}

	return found;
}

bool Human::FindPlant() {
	glm::vec2 mapIndexes = this->GetMapIndexes(terrainSquareSize);
	int mapI = mapIndexes.x;
	int mapJ = mapIndexes.y;

	auto gridMap = Map::GetInstance().GetMap();

	bool found = false;
	glm::vec2 plantPos;
	glm::ivec2 humanPos;

	int center = MAP_RANGE / 2;
	int counter = 3;
	int index = 1;
	while (center - index >= 0 && !found) {
		for (unsigned int i = mapI - index; i < mapI - index + counter && !found && i > 0 && i < Terrain::GetInstance()->GetGridSize(); i++) {
			for (unsigned int j = mapJ - index; j < mapJ - index + counter && !found && j > 0 && j < Terrain::GetInstance()->GetGridSize(); j++) {
				if (gridMap->at(i).at(j).type == MapBlock::PLANT) {
					plantPos = glm::vec2(i, j);
					if (gridMap->at(i + 1).at(j).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j);
					}
					else if (gridMap->at(i - 1).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j - 1);
					}
					else if (gridMap->at(i - 1).at(j).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j);
					}
					else if (gridMap->at(i - 1).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i - 1, j + 1);
					}
					else if (gridMap->at(i).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i, j - 1);
					}
					else if (gridMap->at(i).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i, j + 1);
					}
					else if (gridMap->at(i + 1).at(j - 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j - 1);
					}
					else if (gridMap->at(i + 1).at(j + 1).canPassThrough) {
						found = true;
						humanPos = glm::vec2(i + 1, j + 1);
					}
				}
			}
		}

		counter = counter + 2;
		index++;
	}

	if (found) {
		std::vector<glm::vec2> path = AStarGenerator::GeneratePath(mapI, mapJ, (int)humanPos.x, (int)humanPos.y);
		if (!path.empty()) {
			this->GeneratePathToFollow(path);

			AStar::Generator* gen = AStarGenerator::GetGenerator();

			Entity* plant = gridMap->at(plantPos.x).at(plantPos.y).entity;
			Action* action = new Action;
			action->name = "Gathering Plant!";
			action->move = std::bind(&Human::GatherPlant, this, plant, std::placeholders::_1, plant->GetScale() / 2);

			auto moveActions = this->GetQueueOfActions();
			moveActions->emplace_back(action);
		}
	}

	return found;
}

bool Human::ChopWood(Entity* entity, float deltaTime, float percentage) {
	float entityScale = entity->GetScale();
	if (entityScale >= 0.0f) {
		entity->Scale(entityScale - percentage * deltaTime);
		return false;
	}

	RemoveEntity(entity);
	
	unsigned int counter = items.size();
	for (auto& inventorySlot : items) {
		counter--;
		if (inventorySlot.second == nullptr) {
			//DisplayInventoryCounter(counter);
			GuiTexture* gui = DisplayItem(Object::LOG, counter);
			inventorySlot.second = new Object(Object::LOG, gui);
			break;
		}
	}

	return true;
}

bool Human::GatherPlant(Entity* entity, float deltaTime, float percentage) {
	float entityScale = entity->GetScale();
	if (entityScale >= 0.0f) {
		entity->Scale(entityScale - percentage * deltaTime);
		return false;
	}

	RemoveEntity(entity);

	unsigned int counter = items.size();
	for (auto& inventorySlot : items) {
		counter--;
		if (inventorySlot.second == nullptr) {
			//DisplayInventoryCounter(counter);
			GuiTexture* gui = DisplayItem(Object::PLANT, counter);
			inventorySlot.second = new Object(Object::PLANT, gui);
			break;
		}
	}

	return true;
}

bool Human::Stay(float deltaTime, float& timeToFinish) {
	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	return true;
}

bool Human::EatBerry(float deltaTime, float& timeToFinish) {
	bool foundBerry = false;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->item == Object::InventorySlot::PLANT) {
				foundBerry = true;
				break;
			}
		}
	}

	if (!foundBerry) {
		std::cout << "No berries in inventory: " << std::endl;
		auto actions = this->GetQueueOfActions();
		return false;
	}


	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	float berryHungerIncrease = 50.0f;
	unsigned int inventorySlotNumber = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::PLANT) {
				GuiTexture::RemoveGUITexture(item.second->GetGuiTexture());
				delete item.second;
				item.second = nullptr;
				//TextMaster::RemoveText(inventoryGuiTexts.at(inventorySlotNumber));
				//inventoryGuiTexts.at(inventorySlotNumber) = nullptr;

				HUNGER_BEFORE_EATING = this->GetHunger();
				if (this->GetHunger() + berryHungerIncrease > HUNGER) {
					this->SetHunger(HUNGER);
				}
				else {
					this->SetHunger(this->GetHunger() + berryHungerIncrease);
				}
				break;
			}
		}
		
		inventorySlotNumber++;
	}

	return true;
}

bool Human::FindWater() {
	glm::vec2 mapIndexes = this->GetMapIndexes(terrainSquareSize);
	int mapI = mapIndexes.x;
	int mapJ = mapIndexes.y;

	bool found = false;
	glm::ivec2 waterPos;

	int center = MAP_RANGE / 2;
	int counter = 3;
	int index = 1;
	while (center - index >= 0 && !found) {
		for (unsigned int i = mapI - index; i < mapI - index + counter && !found && i > 0 && i < Terrain::GetInstance()->GetGridSize(); i++) {
			for (unsigned int j = mapJ - index; j < mapJ - index + counter && !found && j > 0 && j < Terrain::GetInstance()->GetGridSize(); j++) {
				if (this->ToWorldPos(i, j).y < -42.0f) {
					waterPos = glm::ivec2(i, j);
					found = true;
					break;
				}
			}
		}

		counter = counter + 2;
		index++;
	}

	if (found) {
		std::vector<glm::vec2> path = AStarGenerator::GeneratePath(mapI, mapJ, (int)waterPos.x, (int)waterPos.y);
		if (!path.empty()) {
			this->GeneratePathToFollow(path);

			AStar::Generator* gen = AStarGenerator::GetGenerator();
			Action* action = new Action;
			action->name = "Collecting water!";
			action->move = std::bind(&Human::CollectWater, this, std::placeholders::_1, timePerAction);

			auto moveActions = this->GetQueueOfActions();
			moveActions->emplace_back(action);
		}
	}

	return found;

}

bool Human::CollectWater(float deltaTime, float& timeToFinish) {
	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	unsigned int counter = items.size();
	for (auto& inventorySlot : items) {
		counter--;
		if (inventorySlot.second == nullptr) {
			//DisplayInventoryCounter(counter);
			GuiTexture* gui = DisplayItem(Object::WATER, counter);
			inventorySlot.second = new Object(Object::WATER, gui);
			break;
		}
	}

	return true;
}

bool Human::DrinkWater(float deltaTime, float& timeToFinish) {
	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	float waterThirstIncrease = 50.0f;
	unsigned int inventorySlotNumber = 0;
	for (auto& item : items) {
		if (item.second != nullptr) {
			if (item.second->GetItem() == Object::InventorySlot::WATER) {
				GuiTexture::RemoveGUITexture(item.second->GetGuiTexture());
				delete item.second;
				item.second = nullptr;
				//TextMaster::RemoveText(inventoryGuiTexts.at(inventorySlotNumber));
				//inventoryGuiTexts.at(inventorySlotNumber) = nullptr;

				THIRST_BEFORE_DRINKING = this->GetThirst();
				if (this->GetThirst() + waterThirstIncrease > THIRST) {
					this->SetThirst(THIRST);
				}
				else {
					this->SetThirst(this->GetThirst() + waterThirstIncrease);
				}
				break;
			}
		}

		inventorySlotNumber++;
	}

	return true;
}

bool Human::PlaceItem(unsigned int inventorySlotNumber, float deltaTime, float& timeToFinish) {
	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	auto it = items.find(inventorySlotNumber);
	if (it != items.end()) {
		if (it->second != nullptr) {
			glm::vec2 mapIndexes = GetMapIndexes(terrainSquareSize);
			unsigned int objectIndexI = mapIndexes.x + 1;
			unsigned int objectIndexJ = mapIndexes.y;

			glm::vec3 objectPos = ToWorldPos(objectIndexI, objectIndexJ);
			TreeLog* log = new TreeLog(glm::vec3(objectPos.x, objectPos.y, objectPos.z));
			MasterRenderer::GetInstance()->ProcessEntity(*log);

			GuiTexture::RemoveGUITexture(it->second->GetGuiTexture());
			it->second = nullptr;
			//TextMaster::RemoveText(inventoryGuiTexts.at(inventorySlotNumber));
			//inventoryGuiTexts.at(inventorySlotNumber) = nullptr;
		}
	}

	return true;
}

void Human::GenerateMovesToLand() {
	glm::ivec2 mapIndexes = this->GetMapIndexes(terrainSquareSize);
	unsigned int mapI = mapIndexes.x;
	unsigned int mapJ = mapIndexes.y;

	int center = MAP_RANGE / 2;
	int counter = 3;
	int index = 1;
	glm::vec2 landPos;

	bool found = false;
	while (center - index >= 0 && !found) {
		for (unsigned int i = mapI - index; i < mapI - index + counter && !found && i > 0 && i < Terrain::GetInstance()->GetGridSize(); i++) {
			for (unsigned int j = mapJ - index; j < mapJ - index + counter && !found && j > 0 && j < Terrain::GetInstance()->GetGridSize(); j++) {
				if (this->ToWorldPos(i, j).y > -38.0f) {
					landPos = glm::vec2(i, j);
					found = true;
					break;
				}
			}
		}

		counter = counter + 2;
		index++;
	}
	
	GeneratePathToFollow(AStarGenerator::GeneratePath(mapI, mapJ, landPos.x, landPos.y));
}

bool Human::PlaceCampFire(float deltaTime, float& timeToFinish) {
	timeToFinish -= deltaTime;
	if (timeToFinish >= 0.0f) {
		return false;
	}

	std::vector<unsigned int> inventorySlots;
	unsigned int counter = 0;
	unsigned int numberOfLogs = 2;
	for (auto const& item : items) {
		if (item.second != nullptr) {
			if (counter == numberOfLogs) {
				break;
			}

			if (item.second->GetItem() == Object::InventorySlot::LOG) {
				inventorySlots.emplace_back(item.first);
				counter++;
			}
		}
	}

	if (counter < numberOfLogs) {
		this->GetQueueOfActions()->pop_front();
		return false;
	}

	float rotationY = 0.0f;
	glm::vec2 mapIndexes = GetMapIndexes(terrainSquareSize);
	unsigned int objectIndexI = mapIndexes.x + 1;
	unsigned int objectIndexJ = mapIndexes.y;
	glm::vec3 objectPos = ToWorldPos(objectIndexI, objectIndexJ);

	if (objectPos.y < -40.0f) {
		Action* action = this->GetQueueOfActions()->front();
		this->GetQueueOfActions()->pop_front();
		GenerateMovesToLand();
		this->GetQueueOfActions()->emplace_back(action);
		return false;
	}

	std::vector<TreeLog*> logs;
	for (unsigned int inventorySlot : inventorySlots) {
		TreeLog* log = new TreeLog(glm::vec3(objectPos.x, objectPos.y, objectPos.z));
		MasterRenderer::GetInstance()->ProcessEntity(*log);
		log->SetPosition(log->GetPosition(), rotationY);
		logs.emplace_back(log);

		auto it = items.find(inventorySlot);
		GuiTexture::RemoveGUITexture(it->second->GetGuiTexture());
		delete it->second;
		it->second = nullptr;
		//TextMaster::RemoveText(inventoryGuiTexts.at(inventorySlot));
		//inventoryGuiTexts.at(inventorySlot) = nullptr;

		rotationY += 90.0f;
	}
	Light* campfireLight = new Light(glm::vec3(objectPos.x, objectPos.y + 45.0f, objectPos.z), glm::vec3(31.0f, 12.0f, 0.0f), glm::vec3(2.0f, 0.2f, 0.005f));
	Campfire* campfire = new Campfire(logs, campfireLight);
	CAMPFIRES.emplace_back(campfire);

	return true;
}

void Human::Explore() {
	GenerateMoveActions(5);
}

std::string Human::ToName() {
	return "Human";
}

void Human::DeleteInstance() {
	Human::COUNT--;
	Entity::DeleteInstance();
}

bool Human::PushAction(Action::Direction toDirection) {
	float finalPosX = 0;
	float finalPosZ = 0;

	Action* action = new Action;
	if (toDirection == Action::Direction::TL) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z - squareSize;
		action->direction = Action::Direction::TL;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveTopLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::T) {
		finalPosX = startingPosition.x;
		finalPosZ = startingPosition.z - squareSize;
		action->direction = Action::Direction::T;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveTop, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::TR) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z - squareSize;
		action->direction = Action::Direction::TR;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveTopRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::R) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z;
		action->direction = Action::Direction::R;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::BR) {
		finalPosX = startingPosition.x + squareSize;
		finalPosZ = startingPosition.z + squareSize;
		action->direction = Action::Direction::BR;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveBottomRight, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::B) {
		finalPosX = startingPosition.x;
		finalPosZ = startingPosition.z + squareSize;
		action->direction = Action::Direction::B;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveBottom, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::BL) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z + squareSize;
		action->direction = Action::Direction::BL;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveBottomLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else if (toDirection == Action::Direction::L) {
		finalPosX = startingPosition.x - squareSize;
		finalPosZ = startingPosition.z;
		action->direction = Action::Direction::L;
		float* rotY = new float(0.0f);
		action->move = std::bind(&Human::MoveLeft, this, std::placeholders::_1, finalPosX,
			finalPosZ, rotY, speed);
	}
	else {
		std::cout << "Could not move " << std::endl;
	}

	action->finalPosX = finalPosX;
	action->finalPosZ = finalPosZ;
	action->type = Action::Type::MOVE;
	if (Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ) < -45.0f) {
		action->name = "Swimming...";
	}
	else {
		action->name = "Moving...";
	}

	moveActions.emplace_back(action);
	startingPosition.x = finalPosX;
	startingPosition.z = finalPosZ;
	startingPosition.y = Terrain::GetInstance()->GetHeightOfTerrain(finalPosX, finalPosZ);

	return true;
}

void Human::GenerateMoveActions(unsigned int traversalCount) {
	glm::vec3 startingPosition = GetPosition();
	if (moveActions.size() < MAX_ACTIONS) {
		Action::Direction randomMove = (Action::Direction)RandomGenerator::GetNextInt(0, 7);
		for (unsigned int i = 0; i < traversalCount; i++) {
			if (!PushAction(randomMove)) {
				i--;
				randomMove = (Action::Direction)RandomGenerator::GetNextInt(0, 7);
			}
		}
	}
}

void Human::GeneratePathToFollow(std::vector<glm::vec2> path) {
	if (moveActions.size() > 0) {
		Action* action = moveActions.front();
		startingPosition = glm::vec3(action->finalPosX,
			Terrain::GetInstance()->GetHeightOfTerrain(action->finalPosX, action->finalPosZ), action->finalPosZ);
	}

	for (unsigned int i = 0; i < path.size() - 1; i++) {
		if (path[i].x > path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::TL);
		}
		else if (path[i].x == path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::T);
		}
		else if (path[i].x < path[i + 1].x && path[i].y > path[i + 1].y) {
			PushAction(Action::Direction::TR);
		}
		else if (path[i].x < path[i + 1].x && path[i].y == path[i + 1].y) {
			PushAction(Action::Direction::R);
		}
		else if (path[i].x < path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::BR);
		}
		else if (path[i].x == path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::B);
		}
		else if (path[i].x > path[i + 1].x && path[i].y < path[i + 1].y) {
			PushAction(Action::Direction::BL);
		}
		else if (path[i].x > path[i + 1].x && path[i].y == path[i + 1].y) {
			PushAction(Action::Direction::L);
		}
		else {
			std::cout << "Error, could not move!" << std::endl;
		}
	}
}

bool Human::Rotate(float deltaTime, float posX, float posZ, 
	Action::Direction fromDirection, Action::Direction toDirection, float* rotY, bool moveFinished) {
	int direction = toDirection - fromDirection;
	bool negative = false;
	if (direction < 0) {
		negative = false;
		if (glm::abs(direction) > 4) {
			direction = 8 - glm::abs(direction);
			negative = true;
		}
	}
	else {
		negative = true;
		if (glm::abs(direction) > 4) {
			direction = 8 - glm::abs(direction);
			negative = false;
		}
	}

	if (direction == 0) {
		rotationFinished = true;
	}

	if (!rotationFinished) {
		if (negative) {
			*rotY = glm::abs(*rotY);
			unsigned int steps = direction;
			if (moveFinished) {
				SetRotY(this->GetRotY() - steps * 45);
				rotationFinished = true;
			}
			else {
				*rotY = *rotY + 45 * deltaTime * 6.0f;
				if (*rotY < steps * 45) {
					SetPosition(this->GetPosition(), this->GetRotX(), this->GetRotY() - *rotY, this->GetRotZ(), false);
				}
				else {
					*rotY = steps * 45;
					SetRotY(this->GetRotY() - *rotY);
					rotationFinished = true;
				}
			}
		}
		else {
			*rotY = glm::abs(*rotY);
			unsigned int steps = glm::abs(direction);

			if (moveFinished) {
				SetRotY(this->GetRotY() + steps * 45);
				rotationFinished = true;
			}
			else {
				*rotY = *rotY + 45 * deltaTime * 6.0f;
				if (*rotY < steps * 45) {
					SetPosition(this->GetPosition(), this->GetRotX(), this->GetRotY() + *rotY, this->GetRotZ(), false);
				}
				else {
					*rotY = steps * 45;
					SetRotY(this->GetRotY() + *rotY);
					rotationFinished = true;
				}
			}
		}
	}
	

	float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);
	if (posY < -45.0f) {
		speed = 1.0f;
		if (rotationFinished) {
			this->SetPosition(glm::vec3(posX, -42.0f, posZ), -90.0f, 0.0f, 180.0f + this->GetRotY(), false);
		}
		else {
			if (negative) {
				this->SetPosition(glm::vec3(posX, -42.0f, posZ), -90.0f, 0.0f, 180.0f + this->GetRotY() - *rotY, false);
			}
			else {
				this->SetPosition(glm::vec3(posX, -42.0f, posZ), -90.0f, 0.0f, 180.0f + this->GetRotY() + *rotY, false);
			}
		}
	}
	else {
		speed = 2.0f;
		if (rotationFinished) {
			this->SetPosition(glm::vec3(posX, posY, posZ), 0.0f, this->GetRotY(), this->GetRotZ(), false);
		}
		else {
			if (negative) {
				this->SetPosition(glm::vec3(posX, posY, posZ), 0.0f, this->GetRotY() - *rotY, this->GetRotZ(), false);
			}
			else {
				this->SetPosition(glm::vec3(posX, posY, posZ), 0.0f, this->GetRotY() + *rotY, this->GetRotZ(), false);
			}
		}
	}

	return false;
}

bool Human::MoveTopLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x > finalPosX && GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;
		float posY = Terrain::GetInstance()->GetHeightOfTerrain(posX, posZ);

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::TL, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::TL, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveTop(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::T, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::T, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveTopRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x < finalPosX && GetPosition().z > finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z - squareSize * deltaTime * speed;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::TR, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::TR, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x < finalPosX) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::R, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::R, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveBottomRight(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x < finalPosX && GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x + squareSize * deltaTime * speed;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::BR, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::BR, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveBottom(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::B, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::B, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveBottomLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x > finalPosX && GetPosition().z < finalPosZ) {
		glm::vec3 currentPosition = GetPosition();
		float currentRotY = GetRotY();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z + squareSize * deltaTime * speed;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::BL, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::BL, rotY, true);
	rotationFinished = false;
	return true;
}

bool Human::MoveLeft(float deltaTime, float finalPosX, float finalPosZ, float* rotY, float speed) {
	if (GetPosition().x > finalPosX) {
		glm::vec3 currentPosition = GetPosition();
		float posX = currentPosition.x - squareSize * deltaTime * speed;
		float posZ = currentPosition.z;

		Rotate(deltaTime, posX, posZ, previousDirection, Action::Direction::L, rotY, false);
		return false;
	}

	Rotate(deltaTime, finalPosX, finalPosZ, previousDirection, Action::Direction::L, rotY, true);
	rotationFinished = false;
	return true;
}
