#include "Entity.h"

int Entity::COUNT = 0;
FontType* Entity::FONT = nullptr;

unsigned int Entity::RESERVE = 50;

std::vector<Entity*> Entity::ENTITIES = std::vector<Entity*>(0, nullptr);

Entity::Entity(const TexturedModel& model, const glm::vec3& position,
	float rotX, float rotY, float rotZ, float scale, unsigned int id) :
	model(model), position(position), rotX(rotX), rotY(rotY), rotZ(rotZ),
	scale(scale), textureIndex(0), id(id) {
	ENTITIES.emplace_back(this);
	COUNT++;
	if (FONT == nullptr) {
		FONT = new FontType(Loader::GetInstance().LoadTexture("arial.png"), "res/fonts/arial.fnt");
	}
}

Entity::Entity(const TexturedModel& model, unsigned int index, const glm::vec3& position,
	float rotX, float rotY, float rotZ, float scale, unsigned int id) :
	model(model), position(position), rotX(rotX), rotY(rotY), rotZ(rotZ),
	scale(scale), textureIndex(index), id(id) {
	ENTITIES.emplace_back(this);
	COUNT++;
	if (FONT == nullptr) {
		FONT = new FontType(Loader::GetInstance().LoadTexture("arial.png"), "res/fonts/arial.fnt");
	}
}

TexturedModel* Entity::GenerateTexturedModel(const std::string& modelName, unsigned int nrOfRows, unsigned int modelCount) {
	ModelData modelData = OBJFileLoader::LoadOBJ(modelName);
	const std::unique_ptr<ModelTexture> texture = std::make_unique<ModelTexture>(Loader::GetInstance().LoadTexture(modelName + ".png"));
	texture->SetNumberOfRows(nrOfRows);

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		this->GetPosition(), this->GetRotX(), this->GetRotY(), this->GetRotZ(), this->GetScale());

	glm::vec2 textureOffsets(0.0f, 0.0f);
	TexturedModel* model = new TexturedModel(*Loader::GetInstance().LoadToVAO(modelData.GetVertices(),
		modelData.GetTextureCoords(),
		modelData.GetNormals(),
		modelData.GetIndices(),
		modelCount + RESERVE,
		transformationMatrix,
		textureOffsets),
		*texture);

	return model;
}

void Entity::IncreasePosition(float dx, float dy, float dz) {
	this->position.x += dx;
	this->position.y += dy;
	this->position.z += dz;
}

void Entity::IncreaseRotation(float dx, float dy, float dz) {
	this->rotX += dx;
	this->rotY += dy;
	this->rotZ += dz;
}

TexturedModel Entity::GetModel() {
	return model;
}

void Entity::SetModel(const TexturedModel& model) {
	this->model = model;
}

glm::vec3 Entity::GetPosition() {
	return position;
}

void Entity::SetPosition(const glm::vec3& position) {
	this->position = position;
	std::vector<float> transformationMatrixData;

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		position, rotX, rotY, rotZ, scale);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transformationMatrixData.emplace_back(transformationMatrix[i][j]);
		}
	}

	unsigned int* vboIDs = model.GetRawModel().GetVboIDs();
	glBindVertexArray(model.GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TRANSFORMATIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * transformationMatrixData.size() * sizeof(float), 
		transformationMatrixData.size() * sizeof(float), &transformationMatrixData.at(0));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	transformationMatrixData.clear();
}

void Entity::SetID(unsigned int id) {
	this->id = id;
}

void Entity::Scale(float scale) {
	this->scale = scale;
	std::vector<float> transformationMatrixData;

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		position, rotX, rotY, rotZ, scale);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transformationMatrixData.emplace_back(transformationMatrix[i][j]);
		}
	}

	unsigned int* vboIDs = model.GetRawModel().GetVboIDs();
	glBindVertexArray(model.GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TRANSFORMATIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * transformationMatrixData.size() * sizeof(float),
		transformationMatrixData.size() * sizeof(float), &transformationMatrixData.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	transformationMatrixData.clear();
}

void Entity::SetPosition(const glm::vec3& position, float rotY) {
	this->position = position;
	this->rotY = rotY;
	std::vector<float> transformationMatrixData;

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		position, rotX, rotY, rotZ, scale);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transformationMatrixData.emplace_back(transformationMatrix[i][j]);
		}
	}

	unsigned int* vboIDs = model.GetRawModel().GetVboIDs();
	glBindVertexArray(model.GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TRANSFORMATIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * transformationMatrixData.size() * sizeof(float),
		transformationMatrixData.size() * sizeof(float), &transformationMatrixData.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	transformationMatrixData.clear();
}

void Entity::SetPosition(const glm::vec3& position, float rotX, float rotY, float rotZ, bool set) {
	this->position = position;
	if (set) {
		this->rotY = rotY;
	}
	std::vector<float> transformationMatrixData;

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		position, rotX, rotY, rotZ, scale);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transformationMatrixData.emplace_back(transformationMatrix[i][j]);
		}
	}

	unsigned int* vboIDs = model.GetRawModel().GetVboIDs();
	glBindVertexArray(model.GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TRANSFORMATIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * transformationMatrixData.size() * sizeof(float),
		transformationMatrixData.size() * sizeof(float), &transformationMatrixData.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	transformationMatrixData.clear();
}

void Entity::SetPosition(const glm::vec3& position, float rotX, float rotY, float rotZ) {
	this->position = position;
	this->rotX = rotX;
	this->rotY = rotY;
	this->rotZ = rotZ;
	std::vector<float> transformationMatrixData;

	glm::mat4 transformationMatrix = Maths::CreateTransformationMatrix(
		position, rotX, rotY, rotZ, scale);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transformationMatrixData.emplace_back(transformationMatrix[i][j]);
		}
	}

	unsigned int* vboIDs = model.GetRawModel().GetVboIDs();
	glBindVertexArray(model.GetRawModel().GetVaoID());
	glBindBuffer(GL_ARRAY_BUFFER, vboIDs[Loader::ATTRIBUTE::TRANSFORMATIONS]);
	glBufferSubData(GL_ARRAY_BUFFER, this->GetID() * transformationMatrixData.size() * sizeof(float),
		transformationMatrixData.size() * sizeof(float), &transformationMatrixData.at(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	transformationMatrixData.clear();
}

void Entity::SetOnMap(unsigned int i, unsigned int j) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();
	float gridSize = Terrain::GetInstance()->GetGridSize();
	if (i >= gridSize || j >= gridSize) {
		return;
	}

	float xCoord = i * gridSquareSize + gridSquareSize / 2;
	float zCoord = j * gridSquareSize + gridSquareSize / 2;
	float yCoord = Terrain::GetInstance()->GetHeightOfTerrain(xCoord, zCoord);

	this->SetPosition(glm::vec3(xCoord, yCoord, zCoord));
}

glm::vec3 Entity::ToWorldPos(unsigned int i, unsigned int j) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();
	float gridSize = Terrain::GetInstance()->GetGridSize();
	if (i >= gridSize || j >= gridSize) {
		return glm::vec3(0.0f, 0.0f, 0.0f);
	}

	float xCoord = i * gridSquareSize + gridSquareSize / 2;
	float zCoord = j * gridSquareSize + gridSquareSize / 2;
	float yCoord = Terrain::GetInstance()->GetHeightOfTerrain(xCoord, zCoord);

	return glm::vec3(xCoord, yCoord, zCoord);
}

float Entity::GetRotX() {
	return rotX;
}

void Entity::SetRotX(float rotX) {
	this->rotX = rotX;
}

float Entity::GetRotY() {
	return rotY;
}

void Entity::SetRotY(float rotY) {
	this->rotY = rotY;
}

float Entity::GetRotZ() {
	return rotZ;
}

void Entity::SetRotZ(float rotZ) {
	this->rotZ = rotZ;
}

float Entity::GetScale() {
	return scale;
}

void Entity::SetScale(float scale) {
	this->scale = scale;
}

float Entity::GetTextureXOffset() {
	ModelTexture texture = model.GetTexture();
	unsigned int numberOfRows = texture.GetNumberOfRows();
	unsigned int column = textureIndex % numberOfRows;

	return (float)column / (float)numberOfRows;
}

float Entity::GetTextureYOffset() {
	ModelTexture texture = model.GetTexture();
	unsigned int numberOfRows = texture.GetNumberOfRows();
	unsigned int row = textureIndex / numberOfRows;

	return (float)row / (float)numberOfRows;
}

float Entity::GetTextureXOffset(unsigned int numberOfRows, unsigned int textureIndex) {
	unsigned int column = textureIndex % numberOfRows;

	return (float)column / (float)numberOfRows;
}

float Entity::GetTextureYOffset(unsigned int numberOfRows, unsigned int textureIndex) {
	unsigned int row = textureIndex / numberOfRows;

	return (float)row / (float)numberOfRows;
}


unsigned int Entity::GetID() {
	return id;
}

glm::vec2 Entity::GetMapIndexes(float gridSquareSize) {
	glm::vec3 position = GetPosition();

	unsigned int i = std::round((position.x - gridSquareSize / 2) / gridSquareSize);
	unsigned int j = std::round((position.z - gridSquareSize / 2) / gridSquareSize);

	return glm::vec2(i, j);
}

glm::vec2 Entity::GetMapIndexes(float positionX, float positionZ) {
	float gridSquareSize = Terrain::GetInstance()->GetSquareSize();
	unsigned int i = std::round((positionX - gridSquareSize / 2) / gridSquareSize);
	unsigned int j = std::round((positionZ - gridSquareSize / 2) / gridSquareSize);

	return glm::vec2(i, j);
}

Entity* Entity::GetEntityByName(std::vector<Entity*>& entities, const std::string& name) {
	for (Entity* entity : entities) {
		if (entity->GetName() == name) {
			return entity;
		} 
	}

	return nullptr;
}

std::string Entity::GetName() {
	return name;
}

void Entity::SetName(const std::string& name) {
	this->name = name;
}

std::string Entity::ToName() {
	return "Entity";
}

void Entity::DeleteInstance() {
	Entity::COUNT--;
}

void Entity::GenerateMoveActions(unsigned int traversalCount) { }

void Entity::SetGUITextPosition() { }

std::deque<struct Action*>* Entity::GetQueueOfActions() { return nullptr; }

void Entity::ClearQueue() { }

void Entity::GeneratePathToFollow(std::vector<glm::vec2> path) { }

void Entity::UpdateTexts(Action* action) { }