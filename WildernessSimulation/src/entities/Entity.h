#pragma once

#include "../models/TexturedModel.h"
#include "../toolbox/Maths.h"
#include "../renderEngine/Loader.h"
#include "../fontMeshCreator/GUIText.h"
#include "../toolbox/RandomGenerator.h"
#include "../fontMeshCreator/FontType.h"
#include "../objConverter/OBJFileLoader.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <vector>
#include <queue>
#include <functional>

struct Action {
	unsigned int id;
	float finalPosX;
	float finalPosZ;
	std::string name = "";
	std::function<bool(float)> move;

	enum Direction { TL, T, TR, R, BR, B, BL, L };
	Direction direction;

	enum Type {MOVE, GATHER, CONSUME};
	Type type;
};

class Entity {
private:
	TexturedModel model;
	glm::vec3 position;
	float rotX, rotY, rotZ;
	float scale;
	unsigned int textureIndex;
	unsigned int id;

	std::string name;
	static unsigned int RESERVE;

public:
	static std::vector<Entity*> ENTITIES;
	static FontType* FONT;

	static int COUNT;

	/**
	 * Creates an entity with a textured model on it, a position and
	 * translation of the object in the world such as rotation and scale.
	 *
	 * @param model
	 *		- The texture of the model on this entity.
	 *
	 * @param position
	 *		- The position of the model on this entity.
	 *
	 * @param rotX
	 *		- The rotation of the entity on the x axis.
	 *
	 * @param rotY
	 *		- The rotation of the entity on the y axis.
	 *
	 * @param rotZ
	 *		- The rotation of the entity on the z axis.
	 *
	 * @param scale
	 *		- The scale factor of the entity.
	 *
	 * @param id
	 *		- The id of the entity.
	*/
	Entity(const TexturedModel& model, unsigned int index, const glm::vec3& position,
		float rotX, float rotY, float rotZ, float scale, unsigned int id);

	/**
	* Creates an entity with a textured model on it, a position and
	* translation of the object in the world such as rotation and scale.
	*
	* @param model
	*		- The texture of the model on this entity.
	*
	* @param position
	*		- The position of the model on this entity.
	*
	* @param rotX
	*		- The rotation of the entity on the x axis.
	*
	* @param rotY
	*		- The rotation of the entity on the y axis.
	*
	* @param rotZ
	*		- The rotation of the entity on the z axis.
	*
	* @param scale
	*		- The scale factor of the entity.
	*
	* @param id
	*		- The id of the entity.
	*/
	Entity(const TexturedModel& model, const glm::vec3& position,
		float rotX, float rotY, float rotZ, float scale, unsigned int id);

	TexturedModel* GenerateTexturedModel(const std::string& modelName, unsigned int nrOfRows, unsigned int modelCount);

	/**
	 * Increase the position of this entity.
	 *
	 * @param dx 
	 *		- Position increment on the x axis.
	 *
	 * @param dy
	 *		- Position increment on the y axis.
	 *
	 * @param dz
	 *		- position increment on the z axis.
	*/
	void IncreasePosition(float dx, float dy, float dz);

	/**
	* Increase the rotation degrees of this entity.
	*
	* @param dx
	*		- Rotation increment on the x axis.
	*
	* @param dy
	*		- Rotation increment on the y axis.
	*
	* @param dz
	*		- Rotation increment on the z axis.
	*/
	void IncreaseRotation(float dx, float dy, float dz);

	/**
	 * Gets the textured model of this entity.
	 *
	 * @return The textured model of this entity.
	*/
	TexturedModel GetModel();

	/**
	 * Sets the model of this entity.
	 *
	 * @param model
	 *		- The textured model to be set for the entity.
	*/
	void SetModel(const TexturedModel& model);

	/**
	 * Gets the position of this entity.
	 *
	 * @return The position of this entity.
	*/
	glm::vec3 GetPosition();

	/**
	 * Sets the position of this entity.
	 *
	 * @param position
	 *		- The position to be set for the entity.
	*/
	void SetPosition(const glm::vec3& position);


	void SetID(unsigned int id);

	/**
	* Sets the position and the rotation on the y axis of this entity.
	*
	* @param position
	*		- The position to be set for the entity.
	*
	* @param rotZ
	*		-  The rotation degree for this entity on the z axis.
	*/
	void SetPosition(const glm::vec3& position, float rotX, float rotY, float rotZ);

	/**
	 * Sets the position and the rotation on the y axis of this entity.
	 *
	 * @param position
	 *		- The position to be set for the entity.
	 *
	 * @param rotY
	 *		-  The rotation degree for this entity on the y axis.
	*/
	void SetPosition(const glm::vec3& position, float rotY);

	/**
	* Sets the position and the rotation on the y axis of this entity.
	*
	* @param position
	*		- The position to be set for the entity.
	*
	* @param rotY
	*		-  The rotation degree for this entity on the y axis.
	*/
	void SetPosition(const glm::vec3& position, float rotX, float rotY, float rotZ, bool set);

	/**
	* Sets the position and the rotation on the y axis of this entity.
	*
	* @param position
	*		- The position to be set for the entity.
	*
	* @param rotY
	*		-  The rotation degree for this entity on the y axis.
	*/
	void Scale(float scale);

	/**
	 * Set this entity on a given i,j index on the map.
	 *
	 * @param i
	 *		- The i-th square of the map.
	 *
	 * @param j
	 *		- The j-th square of the map.
	 *
	 * @param terrain
	 *		- The terrain where this entity will be placed on.
	*/
	virtual void SetOnMap(unsigned int i, unsigned int j);

	glm::vec3 ToWorldPos(unsigned int i, unsigned int j);

	/**
	 * Gets the rotation degrees on the x axis.
	 *
	 * @return The rotation degrees on the x axis.
	*/
	float GetRotX();

	/**
	 * Sets the rotation in degrees on the x axis.
	 *
	 * @param rotX
	 *		- The rotation in degrees on the x axis.
	*/
	void SetRotX(float rotX);

	/**
	* Gets the rotation degrees on the y axis.
	*
	* @return The rotation degrees on the y axis.
	*/
	float GetRotY();

	/**
	* Sets the rotation in degrees on the y axis.
	*
	* @param rotY
	*		- The rotation in degrees on the y axis.
	*/
	void SetRotY(float rotY);

	/**
	* Gets the rotation degrees on the z axis.
	*
	* @return The rotation degrees on the z axis.
	*/
	float GetRotZ();

	/**
	* Sets the rotation in degrees on the z axis.
	*
	* @param rotZ
	*		- The rotation in degrees on the z axis.
	*/
	void SetRotZ(float rotZ);

	/**
	 * Gets the scale factor of the entity representing how big
	 * or small the entity currently is.
	 *
	 * @return The scale factor of the entity.
	*/
	float GetScale();

	/** 
	 * Sets the scale factor of the entity.
	 *
	 * @param scale
	 *		- The scale factor the entity.
	*/
	void SetScale(float scale);

	/**
	 * Gets the texture x offset.
	 *
	 * @return The texture x offset.
	*/
	float GetTextureXOffset();

	/**
	 * Gets the texture y offset.
	 *
	 * @return The texture y offset.
	*/
	float GetTextureYOffset();

	/**
	* Gets the texture x offset.
	*
	* @return The texture x offset.
	*/
	float GetTextureXOffset(unsigned int numberOfRows, unsigned int textureIndex);

	/**
	* Gets the texture y offset.
	*
	* @return The texture y offset.
	*/
	float GetTextureYOffset(unsigned int numberOfRows, unsigned int textureIndex);

	/**
	 * Gets the ID of the entity.
	 *
	 * @return The ID of the entity.
	*/
	unsigned int GetID();

	virtual void DeleteInstance();

	glm::vec2 GetMapIndexes(float gridSquareSize);

	static glm::vec2 GetMapIndexes(float positionX, float positionZ);

	static Entity* GetEntityByName(std::vector<Entity*>& entities, const std::string& name);

	void SetName(const std::string& name);

	std::string GetName();

	virtual std::string ToName();

	virtual void GenerateMoveActions(unsigned int traversalCount);

	virtual void SetGUITextPosition();

	virtual std::deque<struct Action*>* GetQueueOfActions();

	virtual void ClearQueue();

	virtual void GeneratePathToFollow(std::vector<glm::vec2> path);

	virtual void UpdateTexts(Action* action);
};
