#pragma once

#include <map>
#include <vector>

#include "../models/TexturedModel.h"
#include "../renderEngine/EntityRenderer.h"
#include "../toolbox/Maths.h"
#include "ShadowShader.h"

class ShadowMapEntityRenderer {

private:
	glm::mat4* projectionViewMatrix;
	ShadowShader shader;

public:
	/**
	 * Shadow map entity renderer for rendering shadows for individual entities.
	 *
	 * @param shader
	 *		- The simple shader program being use for shadow render pass.
	 *
	 * @param projectionViewMatrix
	 *		- The ortographic projection matrix multiplied by the light's
	 *		view matrix.
	*/
	ShadowMapEntityRenderer(ShadowShader& shader, glm::mat4* projectionViewMatrix);

	/**
	 * Renderes entities to the shadow map. Each model is first bound and then all
	 * of the entities using that model are rendered to the shadow map.
	 *
	 * @param entities
	 *		- The entities to be rendered to the shadow map.
	*/
	void Render(std::map<TexturedModel, std::vector<Entity*>>& entities);

	/**
	 * Binds a raw model before rendering.
	 *
	 * @param rawModel
	 *		- The model to be bound.
	*/
	void BindModel(RawModel& rawModel);

	/**
	 * Prepares an entity to be rendered. The model matrix is created in the
	 * usual way andd the multiplied with the projection and view matrix.
	 * This is then loaded to the vertex shader as a uniform.
	 *
	 * @param entity
	 *		- The entity to be prepared for rendering.
	*/
	void PrepareInstance(Entity& entity);
};
