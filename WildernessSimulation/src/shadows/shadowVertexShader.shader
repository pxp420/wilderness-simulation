#version 330

in vec3 in_position;
in vec2 in_textureCoords;
in mat4 in_transformMatrix;

out vec2 textureCoords;

uniform mat4 vpMatrix;

void main(void) {
	gl_Position = vpMatrix * in_transformMatrix * vec4(in_position, 1.0);
	textureCoords = in_textureCoords;
}
