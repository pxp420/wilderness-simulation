#include "ShadowShader.h"

const std::string ShadowShader::VERTEX_FILE = "src/shadows/shadowVertexShader.shader";
const std::string ShadowShader::FRAGMENT_FILE = "src/shadows/shadowFragmentShader.shader";

const std::map<unsigned int, std::string> ShadowShader::ATTRIBUTES = {
	{ 0, "in_position" },
    { 1, "in_textureCoords"},
    { 3, "in_transformMatrix"}
};

ShadowShader::ShadowShader() :
	ShaderProgram::ShaderProgram(VERTEX_FILE, FRAGMENT_FILE, ATTRIBUTES) {
	GetAllUniformLocations();
}

void ShadowShader::LoadVpMatrix(glm::mat4 vpMatrix) {
	ShaderProgram::LoadMatrix(location_vpMatrix, vpMatrix);
}

void ShadowShader::GetAllUniformLocations() {
	location_vpMatrix = ShaderProgram::GetUniformLocation("vpMatrix");
}
