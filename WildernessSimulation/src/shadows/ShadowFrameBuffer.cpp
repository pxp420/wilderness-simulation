#include "ShadowFrameBuffer.h"

ShadowFrameBuffer::ShadowFrameBuffer(unsigned int width, unsigned int height) :
	WIDTH(width), HEIGHT(height) {
	InitialiseFrameBuffer();
}

void ShadowFrameBuffer::CleanUp() {
	glDeleteFramebuffers(1, &fbo);
	glDeleteTextures(1, &shadowMap);
}

void ShadowFrameBuffer::BindFrameBuffer() {
	BindFrameBuffer(fbo, WIDTH, HEIGHT);
}

void ShadowFrameBuffer::UnbindFrameBuffer() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, DisplayManager::WIDTH, DisplayManager::HEIGHT);
}

unsigned int ShadowFrameBuffer::GetShadowMap() {
	return shadowMap;
}

void ShadowFrameBuffer::InitialiseFrameBuffer() {
	fbo = CreateFrameBuffer();
	shadowMap = CreateDepthBufferAttachment(WIDTH, HEIGHT);
	UnbindFrameBuffer();
}

void ShadowFrameBuffer::BindFrameBuffer(unsigned int frameBuffer, unsigned int width,
	unsigned int height) {
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
	glViewport(0, 0, width, height);
}

unsigned int ShadowFrameBuffer::CreateFrameBuffer() {
	unsigned int frameBuffer;
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	return frameBuffer;
}

unsigned int ShadowFrameBuffer::CreateDepthBufferAttachment(unsigned int width, unsigned int height) {
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, width, height, 0,
		GL_DEPTH_COMPONENT, GL_FLOAT, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);

	return texture;
}
