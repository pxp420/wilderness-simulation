#pragma once

#include "../shaders/ShaderProgram.h"

class ShadowShader : public ShaderProgram {
private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	const static std::map<unsigned int, std::string> ATTRIBUTES;

	unsigned int location_vpMatrix;
public:
	ShadowShader();

	/**
	* Loads the view projection matrix to a uniform.
	*
	* @param vpMatrix
	*		- The matrix loaded in the uniform.
	*/
	void LoadVpMatrix(glm::mat4 vpMatrix);
protected:
	/**
	* To make sure that all the shader classes have the method to get all uniform
	* locations
	*/
	void GetAllUniformLocations();
};
