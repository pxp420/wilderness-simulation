#pragma once

#include "ShadowFrameBuffer.h"
#include "ShadowShader.h"
#include "ShadowMapEntityRenderer.h"
#include "../toolbox/Frustum.h"

class ShadowMapMasterRenderer {
private:
	ShadowFrameBuffer shadowFbo;
	ShadowShader shader;
	Frustum shadowBox;

	glm::mat4 projectionMatrix;
	glm::mat4 lightViewMatrix;
	glm::mat4 projectionViewMatrix;
	glm::mat4 offset;

	ShadowMapEntityRenderer entityRenderer;

public:
	const static unsigned int SHADOW_MAP_SIZE;
	const static float SHADOW_DISTANCE;
	const static float NEAR_PLANE;

	/**
	 * Creates instances of the important objects needed for rendering the scene
	 * to the shadow map. This includes the ShadowBox which calculates the position
	 * and size of the "view cuboid", the simple renderer and shader program that
	 * are used to render objects to the shadow map, and the ShadowFrameBuffer to
	 * which the scene is rendered. The size of the shadow map is determined here.
	 *
	 * @param camera
	 *		- The camera being used in the scene.
	*/
	ShadowMapMasterRenderer(Camera* camera);

	/**
	 * Carries out the shadow render pass. This renders the entities to the shadow map.
	 * First the shadow box is updated to calculate the size and position of the "view"
	 * cuboid". The light direction is assumed to be "-lightPosition" which will be
	 * fairly accurate assuming that the light is very far from the scene. It then 
	 * prepares to render, rendres the entities to the shaow map, and finishes
	 * rendering.
	 *
	 * @param entities
	 *		- The list of entities to be rendered. Each list is associated with a
	 *		textured model that all of the entities in that list use.
	 *
	 * @param sun
	 *		- The light acting as the sun in the scene.
	*/
	void Render(std::map<TexturedModel, std::vector<Entity*>>& entities, Light& sun);

	/**
	 * This biased projection-view matrix is used to convert fragments into "shadow"
	 * map space" when rendering the main render pass. It converts a world space
	 * position into a 2D coordinate on the shadow map. This is needed for the
	 * second part of shadow mapping.
	 *
	 * @return The to-shadow-map-space matrix.
	*/
	glm::mat4 GetToShadowMapSpaceMatrix();

	/**
	 * Clean up the shader and FBO on exiting.
	*/
	void CleanUp();

	/**
	 * Get the ID of the shadow map texture.
	 *
	 * @return The ID of the shadow map texture. The ID will always stay the same,
	 *         even when the contents of the shadow map texture change each frame.
	*/
	unsigned int GetShadowMap();

	/**
	 * Get the light's view matrix.
	 *
	 * @return The light's view matrix.
	*/
	glm::mat4 GetLightSpaceTransform();

	/**
	 * Prepare for the shadow render pass. This first updates the dimensions of the
	 * ortographic "view cuboid" based on the information that was calculated in the
	 * ShadowBox class. The light's view matix is also calculated based on the light's
	 * direction and the center position of the "view cuboid" which was also calculated
	 * in the ShadowBox class.
	 *
	 * These two matrices are multiplied together to create the projection-view matrix.
	 * This matrix determines the size, position, and orientation of the "view cuboid"
	 * in the world. This method also binds the shadows FBO so that everything is
	 * rendered after this gets rendered to the FBO.
	 *
	 * It also enables depth testing, and clears any data that is in the FBOs depth
	 * attachment from last frame. The simple shader program is also started.
	 *
	 * @param lightDirection
	 *		- The direction of the light rays coming from the sun.
	 *
	 * @param box
	 *		- The shadow box, which contains all the info about the "view cuboid".
	*/
	void Prepare(glm::vec3 lightDirection, Frustum& box);

	/**
	 * Finish the shadow render pass. Stops the shader and unbinds the shadow FBO,
	 * so everything rendered after this point is rendered to the screen, rather
	 * than to the shadow FBO.
	*/
	void Finish();

	/**
	 * Updates the view matrix of the light. This creates a view matrix which will 
	 * line up the direction of the "view cuboid" with the direction of the
	 * light. The light itself has no position, so the view matrix is centered at
	 * the center of the "view cuboid". 
	 *
	 * The created view matrix determines where and how the "view cuboid" is
	 * positioned in the world. The size of the view cuboid, however, is
	 * determined by the projection matrix.
	 *
	 * @param direction
	 *		- The light direction, and therefore the direction that the "view cuboid"
	 *		should be pointing.
	 *
	 * @param center
	 *		- The center of the "view cuboid" in world space.
	*/
	void UpdateLightViewMatrix(glm::vec3 direction, glm::vec3 center);

	/**
	 * Creates the ortographic projection matrix. This projection matrix basically
	 * sets the width, the length and height of the "view cuboid", based on
	 * the valeus that were calculated in the ShadowBox class.
	 *
	 * @param width
	 *		- The shadow box width.
	 *
	 * @param height
	 *		- The shadow box height.
	 *
	 * @param length
	 *		- The shadow box length.
	*/
	void UpdateOrthoProjectionMatrix(float width, float height, float length);

	/**
	 * Create the offset for part of the conversion to shadow map space. This 
	 * conversion is necessary to convert from one coordinate system to the
	 * coordinate system that we can use to sample to shadow map.
	 *
	 * @return The offset as a matrix (so that it's easy to apply to other 
	 *         matrices.
	*/
	static glm::mat4 CreateOffset();
};
