#include "ShadowMapEntityRenderer.h"

ShadowMapEntityRenderer::ShadowMapEntityRenderer(ShadowShader& shader, glm::mat4* projectionViewMatrix) :
	shader(shader), projectionViewMatrix(projectionViewMatrix) {}

void ShadowMapEntityRenderer::Render(std::map<TexturedModel, std::vector<Entity*>>& entities) {
	for (std::map<TexturedModel, std::vector<Entity*>>::iterator it = entities.begin(); it != entities.end(); ++it) {
		TexturedModel model = it->first;
		RawModel rawModel = model.GetRawModel();
		BindModel(rawModel);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, model.GetTexture().GetID());

		auto iterator = entities.find(model);
		if (iterator != entities.end()) {
			shader.LoadVpMatrix(*projectionViewMatrix);
			glDrawElementsInstanced(GL_TRIANGLES, rawModel.GetVertexCount(),
				GL_UNSIGNED_INT, 0, iterator->second.size());
		}
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glDisableVertexAttribArray(5);
	glDisableVertexAttribArray(6);
	glBindVertexArray(0);
}

void ShadowMapEntityRenderer::BindModel(RawModel& rawModel) {
	glBindVertexArray(rawModel.GetVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
}
