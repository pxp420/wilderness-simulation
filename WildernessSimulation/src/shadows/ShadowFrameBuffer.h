#pragma once

#include "../renderEngine/DisplayManager.h"

class ShadowFrameBuffer {
private:
	const unsigned int WIDTH;
	const unsigned int HEIGHT;
	
	unsigned int fbo;
	unsigned int shadowMap;

public:
	/**
	 * Initialises the frame buffer with a specific width and height.
	 *
	 * @param width
	 *		- The width of the fbo.
	 *
	 * @param height
	 *		- The height of the fbo.
	*/
	ShadowFrameBuffer(unsigned int width, unsigned int height);

	/**
	 * Deletes the frame buffer and the texture IDs.
	*/
	void CleanUp();

	/**
	 * Binds the frame buffer, setting it as the current render target.
	*/
	void BindFrameBuffer();

	/**
	 * Unbinds the frame buffer in order to use the original fbo.
	*/
	void UnbindFrameBuffer();

	/**
	 * Gets the ID of the shadow map texture.
	 *
	 * @return The ID of the shadow map texture.
	*/
	unsigned int GetShadowMap();

private:
	/**
	 * Creates the frame buffer and adds its depth attachment texture.
	*/
	void InitialiseFrameBuffer();

	/**
	 * Binds the frame buffer as the current render target.
	 *
	 * @param frameBuffer
	 *		- The frame buffer.
	 * 
	 * @param width
	 *		- The width of the frame buffer.
	 *
	 * @param height
	 *		- The height of the frame buffer.
	*/
	static void BindFrameBuffer(unsigned int frameBuffer, unsigned int width,
		unsigned int height);

	/**
	 * Creates a frame buffer and binds it so that attachments can be added to
	 * it. The draw buffer is set to none, indicating that there's no coliur
	 * buffer to be rendered to.
	 *
	 * @return The newly created frame buffer's ID.
	*/
	static unsigned int CreateFrameBuffer();

	/**
	 * Creates a depth buffer texture attachment.
	 *
	 * @param width
	 *		- The width of the texture.
	 *
	 * @param height
	 *		- The height of the texture.
	 *
	 * @return The ID of the depth texture.
	*/
	static unsigned int CreateDepthBufferAttachment(unsigned int width,
		unsigned int height);
};
