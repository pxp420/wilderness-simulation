#include "ShadowMapMasterRenderer.h"

const unsigned int ShadowMapMasterRenderer::SHADOW_MAP_SIZE = 4096 * 4;
const float ShadowMapMasterRenderer::SHADOW_DISTANCE = 1000.0f;
const float ShadowMapMasterRenderer::NEAR_PLANE = 0.1f;

ShadowMapMasterRenderer::ShadowMapMasterRenderer(Camera* camera) :
	projectionMatrix(1.0f), lightViewMatrix(1.0f), projectionViewMatrix(1.0f), 
	offset(CreateOffset()),
	shader(ShadowShader()), shadowBox(Frustum(&lightViewMatrix, Camera::GetInstance(), NEAR_PLANE, SHADOW_DISTANCE)),
	shadowFbo(ShadowFrameBuffer(SHADOW_MAP_SIZE, SHADOW_MAP_SIZE)),
	entityRenderer(ShadowMapEntityRenderer(shader, &projectionViewMatrix)) {}

void ShadowMapMasterRenderer::Render(std::map<TexturedModel, std::vector<Entity*>>& entities, Light& sun) {
	shadowBox.Update();
	glm::vec3 sunPosition = sun.GetPosition();
	glm::vec3 lightDirection(-sunPosition.x, -sunPosition.y, -sunPosition.z);
	Prepare(lightDirection, shadowBox);
	entityRenderer.Render(entities);
	Finish();
}

glm::mat4 ShadowMapMasterRenderer::GetToShadowMapSpaceMatrix() {
	return offset * projectionViewMatrix;
}

void ShadowMapMasterRenderer::CleanUp() {
	shader.CleanUp();
	shadowFbo.CleanUp();
}

unsigned int ShadowMapMasterRenderer::GetShadowMap() {
	return shadowFbo.GetShadowMap();
}

glm::mat4 ShadowMapMasterRenderer::GetLightSpaceTransform() {
	return lightViewMatrix;
}

void ShadowMapMasterRenderer::Prepare(glm::vec3 lightDirection, Frustum& box) {
	UpdateOrthoProjectionMatrix(box.GetWidth(), box.GetHeight(), box.GetLength());
	UpdateLightViewMatrix(lightDirection, box.GetCenter());
	
	projectionViewMatrix = projectionMatrix * lightViewMatrix;
	shadowFbo.BindFrameBuffer();
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);
	shader.Start();
}

void ShadowMapMasterRenderer::Finish() {
	shader.Stop();
	shadowFbo.UnbindFrameBuffer();
}

void ShadowMapMasterRenderer::UpdateLightViewMatrix(glm::vec3 direction, glm::vec3 center) {
	direction = glm::normalize(direction);
	center = glm::vec3(-center.x, -center.y, -center.z);
	lightViewMatrix = glm::mat4(1.0f);

	float pitch = (float)glm::acos(glm::length(glm::vec2(direction.x, direction.z)));
	lightViewMatrix = glm::rotate(lightViewMatrix, pitch, glm::vec3(1.0f, 0.0f, 0.0f));

	float yaw = (float)glm::degrees(((float)glm::atan(direction.x / direction.z)));
	yaw = direction.z > 0.0f ? yaw - 180.0f : yaw;

	lightViewMatrix = glm::rotate(lightViewMatrix, -glm::radians(yaw), glm::vec3(0.0f, 1.0f, 0.0f));
	lightViewMatrix = glm::translate(lightViewMatrix, center);
}

void ShadowMapMasterRenderer::UpdateOrthoProjectionMatrix(float width, float height, float length) {
	projectionMatrix = glm::mat4(1.0f);
	projectionMatrix[0][0] = 2.0f / width;
	projectionMatrix[1][1] = 2.0f / height;
	projectionMatrix[2][2] = -2.0f / length;
	projectionMatrix[3][3] = 1.0f;
}

glm::mat4 ShadowMapMasterRenderer::CreateOffset() {
	glm::mat4 offset(1.0f);
	offset = glm::translate(offset, glm::vec3(0.5f, 0.5f, 0.5f));
	offset = glm::scale(offset, glm::vec3(0.5f, 0.5f, 0.5f));

	return offset;
}
