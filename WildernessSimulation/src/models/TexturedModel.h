#pragma once

#include "RawModel.h"
#include "../textures/ModelTexture.h"

class TexturedModel {
private:
	RawModel rawModel;
	ModelTexture texture;
public:
	/**
	 * Constructor for the textured model that contains the model and the texture.
	 *
	 * @param model 
	 *		- The model on which we want to apply the texture.
	 * 
	 * @param texture
	 *		- The texture for the model.
	*/
	TexturedModel(const RawModel& model, const ModelTexture& texture);

	/**
	 * @return The raw model.
	*/
	RawModel GetRawModel();

	/**
	 * @return The texture model.
	*/
	ModelTexture GetTexture() const;
};
