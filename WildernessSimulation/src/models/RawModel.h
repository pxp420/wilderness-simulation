#pragma once

#include <vector>

class RawModel {
private:
	unsigned int vaoID;
	unsigned int* vboIDs;
	unsigned int vertexCount;

public:
	RawModel(int vaoID, unsigned int* vboIDs, int vertexCount);

	/**
	 * @return The ID of the VAO which contains the data about all
	 * the geometry of this model.
	*/
	int GetVaoID();

	/**
	 * @return The IDs of the VBOs used for the specified model.
	*/
	unsigned int* GetVboIDs();

	/**
	 * @return The number of vertices in the model.
	*/
	int GetVertexCount();
};
