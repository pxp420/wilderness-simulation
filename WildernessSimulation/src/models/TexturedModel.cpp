#include "TexturedModel.h"

TexturedModel::TexturedModel(const RawModel& model, const ModelTexture& texture) :
	rawModel(model), texture(texture) {}

RawModel TexturedModel::GetRawModel() {
	return rawModel;
}

ModelTexture TexturedModel::GetTexture() const{
	return texture;
}
