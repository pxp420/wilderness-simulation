#include "RawModel.h"

RawModel::RawModel(int vaoID, unsigned int* vboIDs, int vertexCount) :
	vaoID(vaoID), vertexCount(vertexCount), vboIDs(vboIDs) {}

int RawModel::GetVaoID() {
	return vaoID;
}

unsigned int* RawModel::GetVboIDs() {
	return vboIDs;
}

int RawModel::GetVertexCount() {
	return vertexCount;
}
