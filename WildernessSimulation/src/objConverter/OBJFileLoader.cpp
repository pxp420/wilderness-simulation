#include "OBJFileLoader.h"

const std::string OBJFileLoader::RES_LOC = "res/models/";
std::vector<Vertex*> OBJFileLoader::allocatedVertices = std::vector<Vertex*>();

void OBJFileLoader::ProcessVertex(std::vector<std::string>& vertex, std::vector<Vertex>& vertices, std::vector<unsigned int>& indices) {
	int index = std::abs(stoi(vertex.at(0))) - 1;

	Vertex& currentVertex = vertices.at(index);
	int textureIndex = std::abs(stoi(vertex.at(1))) - 1;
	int normalIndex = std::abs(stoi(vertex.at(2))) - 1;

	currentVertex.SetTextureIndex(textureIndex);
	currentVertex.SetNormalIndex(normalIndex);
	indices.emplace_back(index);

}

float OBJFileLoader::ConvertDataToVectors(std::vector<Vertex>& vertices, std::vector<glm::vec2>& textures,
	std::vector<glm::vec3>& normals, std::vector<float>& verticesVector,
	std::vector<float>& texturesVector, std::vector<float>& normalsVector) {
	float furthestPoint = 0.0f;

	for (unsigned int i = 0; i < vertices.size(); i++) {
		Vertex& currentVertex = vertices.at(i);
		float length = currentVertex.GetLength();
		if (length > furthestPoint) {
			furthestPoint = length;
		}

		glm::vec3 position = currentVertex.GetPosition();
		glm::vec2 textureCoord = textures.at(currentVertex.GetTextureIndex());
		glm::vec3 normal = normals.at(currentVertex.GetNormalIndex());

		verticesVector[i * 3] = position.x;
		verticesVector[i * 3 + 1] = position.y;
		verticesVector[i * 3 + 2] = position.z;

		texturesVector[i * 2] = textureCoord.x;
		texturesVector[i * 2 + 1] = 1 - textureCoord.y;

		normalsVector[i * 3] = normal.x;
		normalsVector[i * 3 + 1] = normal.y;
		normalsVector[i * 3 + 2] = normal.z;
	}

	return furthestPoint;
}

void OBJFileLoader::DealWithAlreadyProcessedVertex(Vertex& previousVertex, int newTextureIndex,
	int newNormalIndex, std::vector<unsigned int>& indices, std::vector<Vertex>& vertices) {
	if (previousVertex.HasSameTextureAndNormal(newTextureIndex, newNormalIndex)) {
		indices.emplace_back(previousVertex.GetIndex());
	}
	else {
		Vertex* anotherVertex = previousVertex.GetDuplicateVertex();
		if (anotherVertex != nullptr) {
			DealWithAlreadyProcessedVertex(*anotherVertex, newTextureIndex, newNormalIndex, indices, vertices);
		}
		else {
			Vertex* duplicateVertex = new Vertex(vertices.size(), previousVertex.GetPosition());
			if (std::find(allocatedVertices.begin(), allocatedVertices.end(), duplicateVertex) != allocatedVertices.end()) {
				allocatedVertices.emplace_back(duplicateVertex);
			}

			duplicateVertex->SetTextureIndex(newTextureIndex);
			duplicateVertex->SetNormalIndex(newNormalIndex);

			previousVertex.SetDupicateVertex(duplicateVertex);
			vertices.emplace_back(*duplicateVertex);
			indices.emplace_back(duplicateVertex->GetIndex());
		}
	}
}

void OBJFileLoader::RemoveUnusedVertices(std::vector<Vertex>& vertices) {
	for (Vertex& vertex : vertices) {
		if (!vertex.IsSet()) {
			vertex.SetTextureIndex(0);
			vertex.SetNormalIndex(0);
		}
	}
}

void OBJFileLoader::CleanUp(std::vector<Vertex>& vertices) {
	RemoveUnusedVertices(vertices);

	for (Vertex* vertex : allocatedVertices) {
		delete(vertex);
	}
}

ModelData OBJFileLoader::LoadOBJ(const std::string& objFileName) {
	//std::ifstream stream(OBJFileLoader::RES_LOC + objFileName + ".obj");

	//std::string line;
	//std::vector<Vertex> vertices;
	//std::vector<glm::vec2> textures;
	//std::vector<glm::vec3> normals;
	//std::vector<unsigned int> indices;

	//while (getline(stream, line)) {
	//	if (line.find("v ") != std::string::npos) {
	//		std::vector<std::string> lineValues = StringManipulation::Split(line, ' ');
	//		glm::vec3 vertex = glm::vec3(atof(lineValues.at(1).c_str()), atof(lineValues.at(2).c_str()),
	//			atof(lineValues.at(3).c_str()));
	//		Vertex newVertex(vertices.size(), vertex);
	//		vertices.emplace_back(newVertex);
	//	}
	//	else if (line.find("vt ") != std::string::npos) {
	//		std::vector<std::string> lineValues = StringManipulation::Split(line, ' ');
	//		glm::vec2 texture = glm::vec2(atof(lineValues.at(1).c_str()), atof(lineValues.at(2).c_str()));
	//		textures.emplace_back(texture);
	//	}
	//	else if (line.find("vn ") != std::string::npos) {
	//		std::vector<std::string> lineValues = StringManipulation::Split(line, ' ');
	//		glm::vec3 normal = glm::vec3(atof(lineValues.at(1).c_str()), atof(lineValues.at(2).c_str()),
	//			atof(lineValues.at(3).c_str()));
	//		normals.emplace_back(normal);
	//	}
	//	else if (line.find("f ") != std::string::npos) {
	//		std::vector<std::string> lineValues = StringManipulation::Split(line, ' ');
	//		std::vector<std::string> vertex1 = StringManipulation::Split(lineValues[1], '/');
	//		std::vector<std::string> vertex2 = StringManipulation::Split(lineValues[2], '/');
	//		std::vector<std::string> vertex3 = StringManipulation::Split(lineValues[3], '/');

	//		ProcessVertex(vertex1, vertices, indices);
	//		ProcessVertex(vertex2, vertices, indices);
	//		ProcessVertex(vertex3, vertices, indices);
	//	}
	//}

	//while (getline(stream, line)) {
	//	if (line.find("f ") != std::string::npos) {
	//		std::vector<std::string> lineValues = StringManipulation::Split(line, ' ');
	//		std::vector<std::string> vertex1 = StringManipulation::Split(lineValues[1], '/');
	//		std::vector<std::string> vertex2 = StringManipulation::Split(lineValues[2], '/');
	//		std::vector<std::string> vertex3 = StringManipulation::Split(lineValues[3], '/');

	//		ProcessVertex(vertex1, vertices, indices);
	//		ProcessVertex(vertex2, vertices, indices);
	//		ProcessVertex(vertex3, vertices, indices);
	//	}
	//}

	//stream.close();
	//CleanUp(vertices);

	//std::vector<float> verticesVector(vertices.size() * 3);
	//std::vector<float> texturesVector(vertices.size() * 2);
	//std::vector<float> normalsVector(vertices.size() * 3);

	//float furthest = ConvertDataToVectors(vertices, textures, normals, verticesVector,
	//	texturesVector, normalsVector);

	std::vector<float> verticesVector;
	std::vector<float> texturesVector;
	std::vector<float> normalsVector;

	std::vector<unsigned int> indices;
	float furthest = 0.0f;

	Model model(OBJFileLoader::RES_LOC + objFileName + ".obj");
	for (Mesh& mesh : model.GetMeshes()) {
		for (Verx& vertex : mesh.vertices) {
			verticesVector.emplace_back(vertex.Position.x);
			verticesVector.emplace_back(vertex.Position.y);
			verticesVector.emplace_back(vertex.Position.z);

			texturesVector.emplace_back(vertex.TexCoords.x);
			texturesVector.emplace_back(1 - vertex.TexCoords.y);

			normalsVector.emplace_back(vertex.Normal.x);
			normalsVector.emplace_back(vertex.Normal.y);
			normalsVector.emplace_back(vertex.Normal.z);
		}
		for (int index : mesh.indices) {
			indices.emplace_back(index);
		}
	}


	ModelData data(verticesVector, texturesVector, normalsVector, indices, furthest);
	return data;
}
