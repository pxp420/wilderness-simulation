#pragma once

#include <stdlib.h>
#include <memory>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "../models/RawModel.h"
#include "../renderEngine/Loader.h"
#include "../toolbox/StringManipulation.h"

#include "Model.h"
#include "Vertex.h"
#include "ModelData.h"

class OBJFileLoader {
private:
	const static std::string RES_LOC;
	static std::vector<Vertex*> allocatedVertices;

	/**
	 * Processes all the vertices and adds the indices in the array. If a vertex has already been
	 * processed then we use a different function to further optimise.
	 *
	 * @param vertex
	 *		- The vertex vector with values.
	 *
	 * @param vertices
	 *		- The vector of vertices that stores the Vertex data correctly.
	 *
	 * @param indices
	 *		- The indices vector that is formed.
	*/
	static void ProcessVertex(std::vector<std::string>& vertex, std::vector<Vertex>& vertices, 
		std::vector<unsigned int>& indices);

	/**
	 * Adjusts the data in their individual array for all vertices, textures and normals.
	 * Gets the furthest away point (the one with the biggest length).
	 *
	 * @param vertices
     *		- The vector with the vertex data.
	 *
	 * @param textures
	 *		- The vector with the texture data.
	 *
	 * @param normals
	 *		- The vector with the normals data.
	 *
	 * @param verticesVector
	 *		- The vertices vector for each individual vertex (sorted).
	 *
	 * @param texturesVector
	 *		- The textures vector for each individual vertex (sorted).
	 *
	 * @param normalsVector
	 *		- The normals vector for each individual vertex (sorted).
	 *
	 * @return The furthest away point of the model.
	 */
	static float ConvertDataToVectors(std::vector<Vertex>& vertices, std::vector<glm::vec2>& textures,
		std::vector<glm::vec3>& normals, std::vector<float>& verticesVector, 
		std::vector<float>& texturesVector, std::vector<float>& normalsVector);

	/**
	 * Deals with the vertices that have already been processed once. This is for optimisation.
	 *
	 * @param previousVertex
	 *		- The previous vertex processed.
	 *
	 * @param newTextureIndex
	 *		- Sets the the new texture index for this vertex.
	 *
	 * @param newNormalIndex
	 *		- Sets the new normal index for this vertex.
	 *
	 * @param indices
	 *		- The indices vector of the model.
	 *
	 * @param vertices
	 *		- The vertices vector of the model.
	*/
	static void DealWithAlreadyProcessedVertex(Vertex& previousVertex, int newTextureIndex,
		int newNormalIndex, std::vector<unsigned int>& indices, std::vector<Vertex>& vertices);

	/**
	 * Loops through all the vertices and removes any unused ones.
	 *
	 * @param vertices
	 *		- The vector containing all the vertices.
	*/
	static void RemoveUnusedVertices(std::vector<Vertex>& vertices);

	/**
	 * Deletes all the unused pointers and removes the vertices that were not used.
	 *
	 * @param vertices
	 *		- The vector containing all the vertices.
	*/
	static void CleanUp(std::vector<Vertex>& vertices);
public:

	/**
	 * Loads the model from the filesystem and processes it.
	 *
	 * @param objFileName
	 *		- The file path of the model.
	 *
	 * @return The vertices, indices of the model.
	*/
	static ModelData LoadOBJ(const std::string& objFileName);
};
