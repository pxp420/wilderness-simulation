#pragma once

#include <vector>

class ModelData {
private:
	std::vector<float> vertices;
	std::vector<float> textureCoords;
	std::vector<float> normals;
	std::vector<unsigned int> indices;
	
	float furthestPoint;
public:
	/**
	 * Constructor for the data of an object.
	 *
	 * @param vertices
	 *		- The vertices the model contains.
	 *
	 * @param textureCoords
	 *		- The texture coordinates the model contains.
	 *
	 * @param normals
	 *		- The normal the model contans.
	 *
	 * @param indices
	 *		- The indices of the model needed for optimised rendering.
	 *
	 * @param furthestPoint
	 *		- The furthest point in the model.
	*/
	ModelData(std::vector<float>& vertices, std::vector<float>& textureCoords, 
		std::vector<float>& normals, std::vector<unsigned int>& indices, float furthestPoint);

	/**
	 * Gets the vertices of the model.
	 *
	 * @return The vertices of the model.
	*/
	std::vector<float> GetVertices();

	/**
	* Gets the texture coordinates of the model.
	*
	* @return The texture coordinates of the model.
	*/
	std::vector<float> GetTextureCoords();

	/**
	* Gets the normals of the model.
	*
	* @return The normals of the model.
	*/
	std::vector<float> GetNormals();

	/**
	* Gets the indices of the model.
	*
	* @return The indices of the model.
	*/
	std::vector<unsigned int> GetIndices();

	/**
	 * Gets the furthest point in the model.
	 *
	 * @return The furthest point of the model.
	*/
	float GetFurthestPoint();
};
