#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

class Vertex {
private:
	const static int NO_INDEX;

	int textureIndex;
	int normalIndex;
	int index;

	float length;
	glm::vec3 position;

	Vertex* duplicateVertex;
public:
	/**
	 * Constructor for the vertex. Creates a new verte with the index and
	 * position.
	 *
	 * @param index
	 *		- The index of the vertex.
	 *
	 * @param position
	 *		- The position of the vertex;
	*/
	Vertex(int index, glm::vec3 position);

	/**
	 * Gets the index of the vertex.
	 *
	 * @return The index of the vertex.
	*/
	int GetIndex();

	/**
	 * Gets the length of the vertex.
	 *
	 * @return The length of the vertex.
	*/
	float GetLength();

	/**
	 * Function to check if the vertex has been set before.
	 *
	 * @return True or false depending if the vertex has been set before.
	*/
	bool IsSet();

	/**
	 * Function to check if two vertices have the same texture and normal index.
	 *
	 * @return True or false depeding if the vertices have the same texture and 
	 * normal index.
	*/
	bool HasSameTextureAndNormal(int textureIndexOther, int normalIndexOther);

	/**
	 * Sets the texture index of the vertex.
	 *
	 * @param textureIndex
	 *		- The texture index for the vertex.
	*/
	void SetTextureIndex(int textureIndex);

	/**
	* Sets the normal index of the vertex.
	*
	* @param normalIndex
	*		- The normal index for the vertex.
	*/
	void SetNormalIndex(int normalIndex);

	/**
	 * Gets the position of the vertex in the world.
	 *
	 * return The position of the vertex in the world.
	*/
	glm::vec3 GetPosition();

	/**
	 * Gets the texture index of the vertex.
	 *
	 * @return The texture index of the vertex.
	*/
	int GetTextureIndex();

	/**
	 * Gets the normal index of the vertex.
	 *
	 * @return The normal index of the vertex.
	*/
	int GetNormalIndex();

	/**
	 * Gets a dupliate of this current vertex.
	 *
	 * @return The duplicate vertex.
	*/
	Vertex* GetDuplicateVertex();

	/**
	 * Sets the duplicate vertex variable with a new value.
	 *
	 * @param duplicateVertex
	 *		- The new vertex for the duplicate vertex.
	*/
	void SetDupicateVertex(Vertex* duplicateVertex);
};
