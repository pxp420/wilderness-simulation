#include "ModelData.h"

ModelData::ModelData(std::vector<float>& vertices, std::vector<float>& textureCoords,
	std::vector<float>& normals, std::vector<unsigned int>& indices, float furthestPoint) : 
	vertices(vertices), textureCoords(textureCoords), normals(normals), indices(indices),
	furthestPoint(furthestPoint) {}

std::vector<float> ModelData::GetVertices() {
	return vertices;
}

std::vector<float> ModelData::GetTextureCoords() {
	return textureCoords;
}

std::vector<float> ModelData::GetNormals() {
	return normals;
}

std::vector<unsigned int> ModelData::GetIndices() {
	return indices;
}

float ModelData::GetFurthestPoint() {
	return furthestPoint;
}
