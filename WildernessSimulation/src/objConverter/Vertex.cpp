#include "Vertex.h"

const int Vertex::NO_INDEX = -1;

Vertex::Vertex(int index, glm::vec3 position) :
	index(index), position(position) {
	textureIndex = Vertex::NO_INDEX;
	normalIndex = Vertex::NO_INDEX;

	duplicateVertex = nullptr;

	this->length = glm::length(position);
}

int Vertex::GetIndex() {
	return index;
}

float Vertex::GetLength() {
	return length;
}

bool Vertex::IsSet() {
	return textureIndex != Vertex::NO_INDEX && normalIndex != Vertex::NO_INDEX;
}

bool Vertex::HasSameTextureAndNormal(int textureIndexOther, int normalIndexOther) {
	return textureIndexOther == textureIndex && normalIndexOther == normalIndex;
}

void Vertex::SetTextureIndex(int textureIndex) {
	this->textureIndex = textureIndex;
}

void Vertex::SetNormalIndex(int normalIndex) {
	this->normalIndex = normalIndex;
}

glm::vec3 Vertex::GetPosition() {
	return position;
}

int Vertex::GetTextureIndex() {
	return textureIndex;
}

int Vertex::GetNormalIndex() {
	return normalIndex;
}

Vertex* Vertex::GetDuplicateVertex() {
	return duplicateVertex;
}

void Vertex::SetDupicateVertex(Vertex* duplicateVertex) {
	this->duplicateVertex = duplicateVertex;
}
